<?php

require 'vendor/autoload.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

include"includes/conexao.php";

$id = $_GET['id_pedido'];

$sql_pedidos = mysqli_query($con, "select * from pedidos where id_pedido = '$id'");
$vetor_pedido = mysqli_fetch_array($sql_pedidos);

$sql_os = mysqli_query($con, "select * from os where id_pedido = '$id'");
$vetor_os = mysqli_fetch_array($sql_os);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$sql_veiculo = mysqli_query($con, "select * from cliente_veiculo where id_cliente = '$vetor_pedido[id_cliente]' order by id_veiculo DESC limit 0,1");
$vetor_veiculo = mysqli_fetch_array($sql_veiculo);

$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$vetor_veiculo[marca]'") or die (mysqli_error($con));
$vetor_marca = mysqli_fetch_array($sql_marca);

$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$vetor_veiculo[anomod]'") or die (mysqli_error($con));
$vetor_anomod = mysqli_fetch_array($sql_anomod);

if($vetor_veiculo['valor'] != NULL) { $valorveiculo = number_format($vetor_veiculo['valor'],2,',','.'); } else { $valorveiculo = number_format($vetor_anomod['valor'],2,',','.'); }

$nomecliente = $vetor_cliente['nome'];

$qtddoc = strlen($vetor_cliente['cpfcnpj']);

if($qtddoc == 14) {
                
$cpfcnpj = Mask("##.###.###/####-##",$vetor_cliente['cpfcnpj']); 
                
}
                
if($qtddoc == 11) {
                
$cpfcnpj = Mask("###.###.###-##",$vetor_cliente['cpfcnpj']); 
                
}

$vigenciainicio = date('d/m/Y', strtotime($vetor_pedido['vigenciainicio']));
$vigenciafim = date('d/m/Y', strtotime($vetor_pedido['vigenciafim']));
$datainstalacao = date('d/m/Y', strtotime($vetor_os['datainstalacao']));


$dompdf->loadHtml('<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
.style2 {
	font-size: 12
}
.style3 {font-size: 12px}

@page {
                margin: 100px 25px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                color: white;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                color: #000000;
                text-align: center;
                line-height: 35px;
            }

            footer .pagenum:before {
		      	content: counter(page);
			}
-->
    </style>
</head>
<body>
		<header>
            <img src="imgs/logo.png" width="150px">
            <hr style="height:1px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
        </header>

        <footer>
            <hr style="height:1px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
            <table width="100%">

            	<tr><td width="80%">
				<div class="style3">
            	Rua Olívia Guedes Penteado,156 - Socorro
            	</br>
				São Paulo/SP – CEP 04764-000
				</br>
				www.jlsegurosystem.com.br
				</div>

            	</td> <td width="20%"><div class="style3">JLSS - V.012018</div></td></tr>


            </table> 
            <div class="pagenum-container style3"><span class="pagenum"></span></div>
        </footer>

<div class="style3">
<table width="100%">
	<tr>
		<td align="center"><span class="style3">CONTRATO DE PRESTAÇÃO DE SERVIÇOS DE EMISSÃO DE SINAIS  PARA RASTREAMENTO DE VEÍCULO A DISTÂNCIA COM PACTO ADJETO DE PROMESSA DE COMPRA  SOBRE DOCUMENTOS. ATÉ O PERCENTUAL CONTRATADO CONFORME ESPECIFICADO NA CAPA DETALHAMENTO DO PRODUTO DOCUMENTO INTEGRANTE DO CONTRATO.</span></td>
	</tr>
	<tr>
		<td>
		CONTRATO Nº: '.$id.'
		</td>
	</tr>
	<tr>
		<td>
		EQUIPAMENTO Nº: '.$vetor_os[nrastreador].'
		</td>
	</tr>
	<tr>
		<td>
		<div align="justify">
		Prezado Cliente,
		<p>
		Você  acaba de adquirir o mais completo e avançado sistema de rastreamento e  localização automática de veículos por satélite-GPSPRS para o seu veículo,  aproveitamos a oportunidade para lhe agradecer por ter escolhido a JL SEGURO  SYSTEM DO BRASIL como sua prestadora de serviço de rastreamento e localização.  Esperamos plenamente a sua satisfação com a JL SEGURO SYSTEM DO BRASIL, pois, a  partir de agora, você tem a tranquilidade e segurança para o seu veículo.
		</p>
		O  sistema JL SEGURO SYSTEM DO BRASIL – GPSPRS fabricado no Brasil (componentes  importados) com um dos mais sofisticados softwares de monitoramento e  rastreamento, que permite localizar e bloquear o seu veículo em caso de furto  qualificado ou roubo com precisão pelo sistema GPS (Global Positioning System)  através de nossa Central de Monitoramento, que funciona 24 horas/365 dias por ano.
		<p>
		<strong>IMPORTANTE:</strong>
		</p>
		Em  caso de roubo e furto qualificado, o cliente deverá informar a placa do veiculo  e o nome do titular do contrato para que sejam acionados os procedimentos de  recuperação do veículo, sendo que o CONTRATANTE se responsabiliza pela  veracidade das informações prestadas, e em caso de falsa comunicação de  roubo/furto qualificado, serão cobrados a parte os custos de deslocamento de  equipe e gastos reais com o procedimento, sendo que em caso de fraude, haverá o  cancelamento do contrato bem como, sansões judiciais.

		</td>
	</tr>
	<tr>

		<td>
		
		<p>
<strong>COMO AGIR EM CASO DE ROUBO E FURTO  QUALIFICADO</strong>
</p>
<span class="style3">
Basta  ligar para a Central de Monitoramento JL SEGURO SYSTEM DO BRASIL O MAIS RÁPIDO  POSSÍVEL, informar a ocorrência do roubo/furto qualificado, informando a placa  do seu veículo e o nome do titular do contrato, que será prontamente iniciado a  recuperação do veículo. (Seu veículo sendo recuperado será solicitado à  presença do proprietário do veículo para a devida entrega mediante a presença  das autoridades policiais).
</span>
<p>
<div align="center" class="style3"><strong>TELEFONE DA CENTRAL 24hrs JL SEGURO  SYSTEM: 0800 030 6672 ou 0800 777 9740</strong></div>
</p>
</td>
</tr>

<tr>

<td>
<div align="justify" class="style3">
JL  SEGURO SYSTEM DO BRASIL empresa de proteção veicular, pessoa jurídica de  direito privado, inscrita no CNPJ/MF sob o número 29.940.383/0001-60, com sede  na Rua Olivia Guedes Penteado nº 156, Bairro Socorro, na cidade de São  Paulo/SP, CEP 04766-000, que opera principalmente serviços de sistema de  rastreamento para veiculo ativada a distancia, doravante denominada CONTRATADA  e de outro lado o (a) cliente conforme dados encontra-se na capa deste contrato  é ao final da última página deste,  denominado(a) CONTRATANTE.
</td>
</tr>
<tr>

<td>
1.1 O Objeto deste contrato é a aquisição  em comodato pelo cliente do Rastreador/ bloqueador e a prestação de serviços de  monitoramento, bem como rastreamento em caso de furto qualificado e roubo,  desta forma a Empresa JL SEGURO SYSTEM DO BRASIL cede em comodato o equipamento  rastreador/bloqueador que deverá ser restituído ao termino do contrato (caso  não haja a renovação),

	</td>

	</tr>

	<tr>

	<td>

		1.2 O SISTEMA RASTREADOR é aquele que  permite ao seu usuário requerer da Central de Operações da CONTRATADA a emissão  de comandos destinados ao rastreamento do veículo, parado ou em movimento,  permitindo que o cliente tenha a visualização do veículo através da internet,  utilizando-se de nosso portal ou aplicativo da nossa central, com uma senha  (login) fornecida pela CONTRATADA. O Sistema de rastreamento é composto por um  kit que contém: 1 (um) módulo eletrônico; 1 (um) conjunto de chicote completo,  antena GPS e 1 (uma) antena GSM (as antenas são embutidas dentro do  equipamento.

	</td>

	</tr>

	<tr>

	<td>

	1.3 O Sistema apoia-se na tecnologia GPS,  no funcionamento eletrônico do equipamento adquirido pelo CONTRATANTE, que  deverá sempre manter a sua conservação e verificação através de sua  visualização constante pela internet ou pelo aplicativo da nossa central.

	</td>

	</tr>

	<tr>

	<td>

	<strong>2. SERVIÇOS</strong>

	</td>

	</tr>

	<tr>

	<td>

	O  objetivo da prestação de serviços é a emissão e recepção de comandos (sinais)  destinados a desencadear o processo de monitoramento do veículo, respeitando os  limites decorrentes da área de cobertura.
  
	</td>

	</tr>

	<tr>

	<td>

	O  serviço adquirido, não tem natureza de seguro, portanto, não é e não engloba  qualquer tipo de gerenciamento de risco, não resultando em responsabilidade da  CONTRATADA as consequências de qualquer natureza advindas do sequestro de  pessoas ou do roubo de cargas, quando da ocorrência de roubo ou furto  qualificado, a CONTRATADA não se responsabiliza por cargas, documentos  valiosos, Objetos ou qualquer outro bem do CONTRATANTE ou de terceiros que esteja  em poder do CONTRATANTE.

	</td>

	</tr>

	<tr>

	<td>

	<strong>3. DO PRODUTO E DO SERVIÇO</strong>

	</td>

	</tr>

	<tr>

	<td>

	A  CONTRATADA, quando acionada por meio de sua Central de Operações, emitirá  sinais para conexão com o Sistema de rastreamento do veículo dentro da área de  cobertura. Para efeito deste Contrato, área de cobertura indicada pela  operadora de telefonia móvel VODAFONE (Todo Território Nacional), respeitadas  as áreas onde não houver sinal suficiente para realizar a conexão necessária  para a efetivação do serviço.

	</td>

	</tr>

	<tr>

	<td>

	Em  caso de necessidade de assistência por defeito apresentado no (s) equipamento  (s) objeto (s) deste Contrato, o Serviço de Atendimento ao Cliente da  CONTRATADA designará um técnico que prestará à assistência no prazo máximo de  48 (quarenta e oito) horas (sendo dentro da grande São Paulo), após a comunicação  à central 24 horas através dos números 0800 030 6672 ou 0800 777 9740 e de  melhor conveniência para a CONTRATANTE.

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  único - O equipamento rastreador é projetado para as especificações de bateria  automotiva, possuindo um sistema “sleep” (acionado 5 minutos após o veículo ser  desligado) que reduz o consumo de bateria do veículo (1,5 mA/h), para um  perfeito funcionamento é necessário que a manutenção do veículo esteja em dia,  assim como não seja submetido a longos períodos sem uso, o que comprometeria a  carga da bateria automotiva e consequentemente o seu acionamento.

	</td>

	</tr>

	<tr>

	<td>

	Os  serviços da assistência técnica e manutenção por ventura decorrente de defeitos  de fabricação não serão cobrados, se ocorridos dentro do prazo de garantia do  equipamento 1 (um) ano a contar da data de instalação).

	</td>

	</tr>

	<tr>

	<td>

	A  garantia dos equipamentos objeto deste Contrato está restrita a defeitos de  fabricação não abrangendo danos provocados por mau uso, incêndios, infiltração  de água, agentes químicos ou decorrentes de forças da natureza entre outros.

	</td>

	</tr>

	<tr>

	<td>

	É  de inteira responsabilidade do (a) CONTRATANTE, o uso indevido dos serviços de  monitoramento, assim como, a divulgação à terceiros do número telefônico da  Central de Monitoramento JL SEGURO SYSTEM DO BRASIL, do Serviço de Atendimento  à Clientes e de seus dados pessoais.

	</td>

	</tr>

	<tr>

	<td>

	A  JL SEGURO SYSTEM DO BRASIL se exime de qualquer responsabilidade perante  terceiros, decorrente do acionamento do bloqueio do veículo solicitado pelo  contratante. Todo e qualquer problema, incidente ou acidente ocasionado por seu  acionamento, será realizado de forma instantânea e automática pela central de  monitoramento, quando assim solicitado pelo usuário, cabendo a ele avaliar os  riscos de seu acionamento.

	</td>

	</tr>

	<tr>

	<td>

	O  equipamento JL SEGURO SYSTEM DO BRASIL é um dispositivo complementar de apoio  ao veículo, não possui natureza de seguro, tendo como finalidade a localização,  monitoramento e recuperação do veículo. A sua aquisição não dispensa a  contratação de seguro.

	</td>

	</tr>

	<tr>

	<td>

	<strong>4. SERVIÇOS EXCEDENTES</strong>

	</td>

	</tr>

	<tr>

	<td>

	Os  seguintes serviços excedentes serão cobrados separadamente:

	</td>

	</tr>

	<tr>

	<td>

	a)  a transferência do Sistema de um veículo para outro veículo, sendo que o custo  de cada um dos serviços e a forma de pagamento, sempre serão informados pela  CONTRATADA ao CONTRATANTE, sendo informado o valor no momento da solicitação da  prestação dos serviços de acordo com a tabela vigente à época.

	</td>

	</tr>

	<tr>

	<td>

	b)  a substituição ou troca e manutenção de qualquer equipamento, cujo o defeito se  deu pela má utilização do produto ou em desconformidade com o indicado pela  empresa CONTRATADA e de responsabilidade do CONTRATANTE pelo pagamento da  visita técnica.

	</td>

	</tr>


	<tr>

	<td>

	 Parágrafo  único – Caso o CONTRATANTE solicite a visita de um técnico e seja constatado  que o defeito não é do equipamento, a visita do técnico será cobrada a parte,  sendo enviado um boleto para pagamento em até 20 (vinte) dias.

	</td>

	</tr>

	<tr>

	<td>

	<strong>5. OBRIGAÇÕES DA CONTRATADA</strong>

	</td>

	</tr>

	<tr>

	<td>

	Tão  logo receba o comunicado de roubo ou furto qualificado feito pelo CONTRATANTE,  a CONTRATADA deverá iniciar o processo de envio de sinais destinados ao  rastreamento do veículo e, concomitantemente deverá dar início ao serviço de  buscas do veículo, por si ou por empresas contratadas, que terá a duração de 30  (trinta) dias.

	</td>

	</tr>

	<tr>

	<td>

	A  CONTRATADA ao instalar o Sistema no veículo do CONTRATANTE, fará a avaliação do  estado geral do veículo na presença do CONTRATANTE, ou de pessoa por este  indicada, e executará a vistoria, cuja data da realização servirá de termo  inicial para a vigência do pacto adjeto de promessa de compra sobre documentos,  de acordo com a cláusula 10.1.

	</td>

	</tr>

	<tr>

	<td>

	<strong>6. CONFIDENCIALIDADE</strong>, o contratante também fornecerá autorização ao técnico  para tirar fotos do seu veículo em partes especificas que serão anexadas a sua  pasta.

	</td>

	</tr>

	<tr>

	<td>

	Fica  claro ao CONTRATANTE que o (s) equipamento (s) objeto (s) deste Contrato é  composto dos mais modernos componentes destinados ao bloqueio, rastreamento e  monitoramento de veículos, constituindo-se em produto (s) essencial (ais) e de  elevada tecnologia à respeito dos quais o CONTRATANTE manterá total  confidencialidade, comprometendo-se a não permitir consultas, exames ou  divulgação dos mesmos, sem prévia e expressa autorização da CONTRATADA.

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  único – O (A) CONTRATANTE deve permitir que a empresa contratada instale o  equipamento em total sigilo, sem a presença do (a) CONTRATANTE ou qualquer  pessoa que não seja autorizada pela CONTRATADA, evitando assim, que o (a)  CONTRATANTE ou outra pessoa, saiba o local exato em que se encontra o  equipamento, como medida de segurança para o (a) CONTRATANTE e para a  CONTRATADA, sob pena de nulidade do pacto adjeto de compra sobre documentos.


	</td>

	</tr>

	<tr>

	<td>

	<strong>7. OBRIGAÇÕES DO CONTRATANTE</strong>

	</td>

	</tr>

	<tr>

	<td>

	O  CONTRATANTE deverá comparecer ao local indicado pela CONTRATADA para a  instalação do Sistema ou optar que a mesma seja efetuada no seu domicílio.

	</td>

	</tr>

	<tr>

	<td>

	É  obrigação de o CONTRATANTE comunicar a CONTRATADA sobre eventuais reparos  mecânicos ou elétricos que sujeitar o veículo protegido, decorrentes de  desgaste natural, viagens longas, intempéries ou acidentes, pois todas estas  circunstâncias podem resultar em vício no funcionamento do Sistema.

	</td>

	</tr>

	<tr>

	<td>

	É  obrigação de o CONTRATANTE zelar pela segurança do seu bem, evitando deixar seu  veículo fora de estabelecimentos que não tenham segurança, ou quando o fizer  ATIVAR o sistema de âncora pelo aplicativo, <strong>sob pena de nulidade da cláusula do pacto adjeto. </strong>O sistema âncora  quando ativado envia mensagens ou sinais sonoros no celular do proprietário em  caso de violação do sistema elétrico do veículo, possibilitando ao CONTRATANTE  a rápida comunicação de qualquer atividade anormal para central de  monitoramento, que tomará as medidas necessárias (rastreamento, bloqueio do  veículo, acionamento das equipes táticas de recuperação) propiciando resposta  em tempo hábil, otimizando o rastreamento e a recuperação do veículo em caso de  furto qualificado.

	</td>

	</tr>

	<tr>

	<td>

	<strong>8. DA INADIMPLÊNCIA</strong>

	</td>

	</tr>

	<tr>

	<td>

	A  CONTRATADA não prestará qualquer serviço previsto ou decorrente deste contrato  para o CONTRATANTE que estiver financeiramente inadimplente, incluindo valor da  habilitação e da prestação de serviços. A retomada da prestação do serviço está  condicionada à efetiva comprovação da regularização do débito, incluindo o  pagamento da taxa de reativação do sistema sob pena de imediata interrupção.

	</td>

	</tr>

	<tr>

	<td>

	O  CONTRATANTE NÃO PODERÁ EXIGIR DA CONTRATADA O CUMPRIMENTO DO PACTO ADJETO DE  PROMESSA DE COMPRA SOBRE DOCUMENTOS, SE NA DATA DO FURTO QUALIFICADO/ROUBO  ESTIVER INADIMPLENTE. EM RELAÇÃO: (I) A QUALQUER PAGAMENTO DEVIDO PELO  CONTRATANTE (VALOR DA HABILITAÇÃO E DA PRESTAÇÃO DE SERVIÇOS); OU (2) AO NÃO  COMUNICADO ÀO DEPTO. TÉCNICO SOB QUAISQUER FALHAS OU POSSÍVEIS DANOS QUE O  EQUIPAMENTO POSSA APRESENTAR, CARACTERIZANDO-SE ASSIM NEGLIGÊNCIA, O QUE  DESOBRIGA À CONTRATADA O CUMPRIMENTO DA INDENIZAÇÃO; (3) NÃO TER REALIZADO A  “VACINA” CONFORME DESCRITO NO PRESENTE CONTRATO.

	</td>

	</tr>

	<tr>

	<td>

	 OBS.: A vacina aplica-se apenas para  motocicleta no valor (tabela FIPE) igual ou superior à R$20.000,00 (vinte mil  reais)

	</td>

	</tr>

	<tr>

	<td>

	<strong>9. COBRANÇAS</strong>

	</td>

	</tr>

	<tr>

	<td>

	O  não recebimento do boleto referente à cobrança da prestação de serviço  (mensalidade) até a data do vencimento não exime o CONTRATANTE do pagamento,  devendo este entrar em contato até dez dias antes do vencimento, com a  CONTRATADA comunicando o seu não recebimento, ou TRANSFERIR através de DOC ou  TED o valor devido na data do vencimento em conta da JL SEGURO SYSTEM DO  BRASIL, Banco Inter 077, Agência.: 001, Conta Corrente: 1232553-8, CNPJ  29.940.383/0001-60 enviando posteriormente cópia do comprovante no  e-mail: jlsegurosystem@gmail.com caso o pagamento seja feito com atraso, e os  serviços interrompidos comunicar a contratada o quanto antes, para  restabelecimento da prestação de serviços.

	</td>

	</tr>

	<tr>

	<td>

	a) Em caso dos não pagamentos da  habilitação/adesão se aplicam as mesmas regras previstas nas cláusulas “8.1” e  “8.2”.

	</td>

	</tr>

	<tr>

	<td>

	b)	Após o vencimento, será cobrado multa  de 10% além de 1% de juro mensal.

	</td>

	</tr>

	<tr>

	<td>

	O  pagamento da mensalidade ativa o Pacto indenizatório até o próximo pagamento da  mensalidade e os serviços de rastreamento.

	</td>

	</tr>

	<tr>

	<td>

	 A  partir do atraso de quaisquer mensalidades pelo CONTRATANTE, OS SERVIÇOS DE  RASTREAMENTO, BLOQUEIO E RECUPERAÇÃO DO VEÍCULO ESTARÃO SUSPENSOS e a empresa  CONTRATADA, se eximirá da responsabilidade de cumprir o pacto adjeto de  promessa de compra sobre o documento.

	</td>

	</tr>

	<tr>

	<td>

	O  CONTRATANTE poderá ser reabilitado na base da CONTRATADA mediante cumprimento  das obrigações a seguir cumulativamente: (I) pagamento dos valores vencidos e  não quitados, acrescido da multa bancaria descrita no boleto, na ordem de 10%  do valor em aberto; (II) pagamento de uma taxa de reabilitação (taxa de nova  vistoria), cujo valor será informado ao CONTRATANTE por meio do departamento de  cobrança da CONTRATADA (11) 4171-5781 e, após regularização dos pagamentos,  deverá o CONTRATANTE comparecer ao local indicado pela CONTRATADA para nova  vistoria do sistema ou pagar taxa de ativação em domicílio.

	</td>

	</tr>

	<tr>

	<td>

	A  ocorrência de desabilitarão afasta o dever de cumprimento do pacto adjeto de  promessa de compra sobre documentos. O pacto somente volta a obrigar a CONTRATADA  quando o CONTRATANTE quitar os débitos e proceder a nova vistoria/avaliação.

	</td>

	</tr>

	<tr>

	<td>

	Em  caso de controvérsia sobre os pagamentos, a CONTRATADA poderá solicitar a  qualquer tempo que o CONTRATANTE envie a CONTRATADA os comprovantes de  pagamento (mensalidade, compra equipamento, etc.), sendo que a recusa do envio  deste comprovante acarretará a desobrigação da CONTRATADA em cumprir o pacto  adjeto de promessa de compra sobre documentos e demais avenças e suspensão dos  serviços.

	</td>

	</tr>

	<tr>

	<td>

	<strong>10. PRAZO DE VIGÊNCIA DO CONTRATO DE  PRESTAÇÃO DE SERVIÇOS COM PACTO ADJETO DE PROMESSA DE COMPRA SOBRE DOCUMENTOS  DE VEÍCULOS DE USO PARTICULAR.</strong>

	</td>

	</tr>

	<tr>

	<td>

	CLÁUSULA  10.1 – O presente Contrato de prestação de serviços vigerá por 24 (vinte e  quatro) meses, a contar da data de instalação do equipamento descrito neste  contrato no veículo descrito na capa deste instrumento, sendo que a primeira  parcela de prestação de serviço vencer-se-á trinta dias após a instalação do  equipamento no veículo. Salvo negociação especial.

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  primeiro – Serão emitidos 12 (doze) boletos (referente à 12 meses) para  pagamento, sendo que após o término destes boletos será feita uma nova vistoria  e o envio de mais 12 (doze) boletos. (não haverá custo de vistoria)

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo segundo – Não haverá custo de  vistoria pelo CONTRATANTE, sendo necessária para a continuação da vigência do  pacto adjeto de compra sobre documentos; A não realização da vistoria isenta a  CONTRATADA da responsabilidade de cumprimento do pacto adjeto de compra sobre  documentos descrita neste instrumento. (Salvo quando esta vistoria seja  dispensada pela CONTRATADA.

	</td>

	</tr>

	<tr>

	<td>

	CLÁUSULA  10.2 – O presente contrato poderá ser rescindido por qualquer uma das partes  mediante aviso, por escrito, com antecedência de 30 (trinta) dias. O  CONTRATANTE é obrigado ao pagamento dos serviços relativos ao período completo  do mês em que solicitar o cancelamento, sendo que a CONTRATADA não prestará  mais os serviços descrito neste contrato a partir da data informada da  rescisão. A solicitação deve ser formal por e-mail ou por correio nunca por  telefone. jlsegurosystem@gmail.com

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  único – Da solicitação de rescisão antecipada pelo CONTRATANTE. Entretanto,  fica estabelecida multa de 20% (vinte por cento) sobre o período restante em  caso de rescisão antes dos 12 (doze) meses da vigência deste contrato.

	</td>

	</tr>

	<tr>

	<td>

	<strong>11. DO PACTO ADJETO DE PROMESSA DE  COMPRA SOBRE DEMAIS AVENÇAS</strong>

	</td>

	</tr>

	<tr>

	<td>

	CLÁUSULA  MANDATO – Com a assinatura do presente Contrato, o CONTRATANTE outorga a  CONTRATADA poderes especiais para JL SEGURO SYSTEM DO BRASIL em caso de roubo e  furto qualificado do veículo monitorado, proceder aos atos necessários junto às  autoridades judiciais, policiais e administrativas competentes, bem como  promover a contratação de outras empresas especializadas em busca com o único  fim de tentar recuperar o seu veículo ora roubado ou furtado.

	</td>

	</tr>

	<tr>

	<td>

	A Cláusula 11.1 – não ofende a qualquer  princípio legal e, em especial não contraria o artigo 51, inciso VIII do Código  de Defesa do Consumidor, uma vez que o presente mandato é instituído para a  prática de atos favoráveis aos interesses do CONTRATANTE, pois visa auxiliá-lo  no intento de otimizar as possibilidades de recuperação do bem.

	</td>

	</tr>

	<tr>

	<td>

	O  pacto adjeto de promessa de compra sobre documentos somente será válido se o  evento ocorrer dentro da área de cobertura (TERRITÓRIO BRASILEIRO).

	</td>

	</tr>

	<tr>

	<td>

	DA  PROMESSA DE COMPRA SOBRE DOCUMENTOS DE VEÍCULOS – A CONTRATADA compromete-se a  comprar do CONTRATANTE os documentos do veículo monitorado caso o mesmo seja  objeto roubo ou furto qualificado não seja recuperado em até 30 (trinta) dias  contados da data do evento, ou mesmo que recuperado, se houver a perda total do  veículo (perda superior a 75% do valor do veículo). A presente promessa se  converterá em obrigação de compra somente se respeitados pelo CONTRATANTE os  deveres contratuais atinentes aos pagamentos, á conservação e utilização do  equipamento, sob pena de desobrigação da promessa de compra ora estabelecida,  sendo que descumprimento de qualquer destas obrigações por parte do CONTRATANTE  configura sua culpa e exonera a CONTRATADA do dever de dar cumprimento à  promessa de compra sobre os documentos do veículo

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  segundo: O pacto adjeto de compra sobre documentos descrito neste instrumento  trata-se de garantia referente a prestação de serviços, equiparando-se a  responsabilidade civil pela correta prestação do serviço de recuperação do bem,  não sendo equivalente e não dispensando a contratação de seguros.

	</td>

	</tr>

	<tr>

	<td>

	DA  CONVERSÃO DA PROMESSA EM COMPROMISSO – No 30º (trigésimo) dia após o roubo, não  se verificando a recuperação do veículo e certificadas por cumpridas as  obrigações do CONTRATANTE, o pacto adjeto de promessa de compra sobre  documentos converte-se em compromisso de compra e venda ou de substituição do  bem, vinculando CONTRATANTE e CONTRATADA que passam a ser vendedor e comprador,  respectivamente.

	</td>

	</tr>

	<tr>

	<td>

	DA  OBRIGAÇÃO DE ENTREGA DOS DOCUMENTOS – Para possibilitar a venda sobre  documentos do veículo, o CONTRATANTE deverá entregar à CONTRATADA, ora  compradora, os seguintes documentos:

	</td>

	</tr>

	<tr>

	<td>

	A  – Comprovante de quitação do veículo, bem como de todas as despesas á ele  inerentes, incluindo débitos junto ao Detran e demais departamentos de Transito  (que em caso de existência de débitos, ou ônus de qualquer natureza deverão ser  descontados da indenização dos valores decorrentes do pacto adjeto), bem como  das demais obrigações com a CONTRATADA previstas no presente contrato;

	</td>

	</tr>

	<tr>

	<td>

	B  – Boletim de Ocorrência e formulário de descrição do furto qualificado ou  roubo, preferencialmente escrito de próprio punho pelo CONTRATANTE ou usuário  na data.

	</td>

	</tr>

	<tr>

	<td>

	C  – Original do Certificado de Registro e Licenciamento do Veículo (CRLV)  (“documento de porte obrigatório”);

	</td>

	</tr>

	<tr>

	<td>

	D  – Documento Único de Transferência (“DUT”) assinado pelo CONTRATANTE ou  proprietário do veículo, com firma reconhecida por verdadeira, transferindo a  propriedade do veículo para a CONTRATADA ou para quem esta indicar;

	</td>

	</tr>

	<tr>

	<td>

	E-  Na falta do DUT: original da Declaração de Extravio do DUT, endereçada ao órgão  de trânsito competente [no caso do Estado de São Paulo, o DEPARTAMENTO ESTADUAL  DE TRÂNSITO DE SÃO PAULO (“DETRANSP”)], com firma reconhecida por verdadeira,  bem como procuração outorgada pelo proprietário do veículo à CONTRATADA, com  poderes específicos para, em nome do outorgante, comparecer aos órgãos de  trânsito ou a qualquer outra repartição Pública ou Particular para regularizar  a condição do veículo, com firma reconhecida, por verdadeira e cópia  autenticada do RG, CPF/MF e comprovante de residência do CONTRATANTE ou  proprietário do veículo;

	</td>

	</tr>

	<tr>

	<td>

	F  – Demonstrativos de débitos perante os órgãos de trânsito competentes até a  data do furto qualificado/roubo (por meio da emissão de certidão oficial, sendo  inaceitável a mera impressão de consulta pela internet), bem como Termo de  Responsabilidade por Multas, com firma reconhecida;

	</td>

	</tr>

	<tr>

	<td>

	G – No caso de veículos adquiridos pela  CONTRATANTE e/ou pelo proprietário por meio de financiamento é obrigatório,  apresentar carnê, e contrato de financiamento, quando não estiver quitado,  sendo deduzido dos valores pagos à título de indenização do pacto adjeto as  parcelas vincendas.

	</td>

	</tr>

	<tr>

	<td>

	H  – Cópia do RG e CPF/MF e da CNH do CONTRATANTE e do condutor na data do furto  qualificado ou roubo;

	</td>

	</tr>

	<tr>

	<td>

	I  – IPVA referente ao exercício anterior, até a data do furto qualificado/roubo,  sendo aceitável o “Nada consta de débitos” desde que acompanhado de comprovante  do exercício anual;

	</td>

	</tr>

	<tr>

	<td>

	J  – Termo de realização da “vacina” emitida pela JL SEGURO SYSTEM DO BRASIL no  ato da realização do serviço ou se realizado por outra empresa, a nota fiscal  de realização do serviço, bem como laudo de vistoria realizado pela JL SEGURO  SYSTEM. 

	</td>

	</tr>

	<tr>

	<td>

	A  JL SEGURO SYSTEM DO BRASIL entende que é de responsabilidade do CONTRATANTE a  prevenção contra furto e só oferece pacto indenizatório para roubo/furto  qualificado, salvo quando for acionado o sistema âncora no aplicativo central  24 horas.

	</td>

	</tr>

	<tr>

	<td>

	<strong>12. DO PREÇO E DO PAGAMENTO – A  CONTRATADA</strong>, ora compradora, pagará ao  CONTRATANTE, ora vendedor, pelos documentos de seu veículo o preço conforme o  plano ajustado e contratado conforme descrito na capa do contrato e de acordo  com a vistoria prévia, respeitando-se sempre conforme o plano pactuado. ( o veículo  deve está livre de ônus de qualquer natureza)

	</td>

	</tr>

	<tr>

	<td>

	Em  caso de Roubo ou Recuperação com Perda Total, do valor mencionado na Cláusula  “12”, deverá ser descontado o valor das mensalidades referentes ao período  faltante para conclusão de todo o contrato.

	</td>

	</tr>

	<tr>

	<td>

	Será  descontado do valor a ser pago os valores referentes a taxa de transferência do  veículo e eventuais multas e débitos existentes referentes ao veículo, sendo  pago referido pacto de acordo com a forma estabelecida pela empresa.

	</td>

	</tr>

	<tr>

	<td>

	No  caso do valor do bem exceder o limite previsto na cláusula 12, a CONTRATADA  fica desobrigada ao pagamento da diferença, reembolso ou compensação de  qualquer espécie. O mesmo se aplica aos acessórios e outros equipamentos  instalados no veículo.

	</td>

	</tr>

	<tr>

	<td>

	NO  CASO DE VEÍCULO FINANCIADO OU ALIENADO, A CONTRATADA FARÁ A QUITAÇÃO DO BEM ATÉ  O LIMITE PREVISTO NA CLÁUSULA 12. SE O VALOR PARA QUITAÇÃO DO FINANCIAMENTO DO  VEÍCULO EXCEDER ESTE LIMITE, A CONTRATADA SOMENTE SE OBRIGA PELO CUMPRIMENTO DO  COMPROMISSO DE COMPRA E VENDA SOBRE OS DOCUMENTOS OU SUBSTITUIÇÃO DO BEM, CASO  O CONTRATANTE SE RESPONSABILIZE PELO PAGAMENTO DA DIFERENÇA DO VALOR JUNTO À  INSTITUIÇÃO FINANCEIRA, tendo em vista que a compra é efetuado sobre o  documento do veículo, sendo necessário transferência de propriedade e a despesa  desta transferência será de responsabilidade do CONTRATANTE.

	</td>

	</tr>

	<tr>

	<td>

	SE  O VEÍCULO POSSUIR SEGURO CONTRA ROUBO E/OU FURTO EM OUTRA CIA DE SEGUROS, O  CONTRATANTE DEVERÁ OPTAR EM RECEBER A INDENIZAÇÃO DA COMPANHIA DE SEGURO OU DA  CONTRATADA, NUNCA DAS DUAS, SOB PENA DE CARACTERIZAR FRAUDE, tendo em vista que  o presente contrato de compromisso de compra e venda prevista na cláusula 11.4,  se dá sobre o documento do veículo, ou seja, com transferência de propriedade  do bem à CONTRATADA. 

	</td>

	</tr>

	<tr>

	<td>

	PARÁGRAFO  ÚNICO – Caso o CONTRATANTE opte por receber a indenização da empresa  CONTRATADA, o CONTRATANTE deverá transferir seus direitos a indenização a ser  paga pela companhia de seguros ao CONTRATADO, desta forma, portanto, caberá ao  CONTRATANTE providenciar a sub-rogação dos direitos a indenização inerente ao  seguro perante a companhia de seguros em favor da CONTRATADA.

	</td>

	</tr>

	<tr>

	<td>

	DO  PRAZO PARA PAGAMENTO – O prazo para pagamento é de 30 (trinta) dias após a entrega  de todos os documentos obrigatórios, os quais somente serão recebidos a partir  do 30º (trigésimo) dia do roubo ou furto qualificado sem que haja a entrega de  todos os documentos não se computará o presente prazo, o qual somente passará a  correr quando da entrega efetiva de todos os documentos exigidos.

	</td>

	</tr>

	<tr>

	<td>

	É  dever de o CONTRATANTE providenciar a imediata comunicação do furto  qualificado/roubo do veículo para a empresa CONTRATADA, bem como a autoridade  policial, elaborando o competente boletim de ocorrência (B.O.), bem como  informar o COPOM (190) no momento do roubo/furto.

	</td>

	</tr>

	<tr>

	<td>

	PARÁGRAFO  PRIMEIRO – Será considerada para todos os efeitos deste contrato (inclusive  para pagamento de eventual indenização), a data da elaboração do boletim de  ocorrência e comunicação a empresa CONTRATADA da ocorrência do furto  qualificado/roubo como sendo a data e hora do efetivo furto qualificado/roubo.

	</td>

	</tr>

	<tr>

	<td>

	PARÁGRAFO  SEGUNDO – Caso o CONTRATANTE não comunique imediatamente (no primeiro momento  possível) a ocorrência do furto qualificado/roubo de seu veículo a empresa  CONTRATADA, acarretará na perda do direito a indenização. Para este ato a  CONTRATADA disponibiliza um canal 0800, números 0800 030 6672 e/ou 0800 777  9740  24 (vinte e quatro) horas por dia  todos dias 7(sete) dias por semana, 365 (trezentos e sessenta e cinco) dias por  ano.

	</td>

	</tr>

	<tr>

	<td>

	<strong>13. DO INDÍCIO DE FRAUDE</strong> – Havendo indícios de fraude, má-fé e dolo do  CONTRATANTE na perda do veículo monitorado, ou verificado o descumprimento das  demais condições deste Contrato, o pacto adjeto de promessa de compra sobre  documentos não se converterá em compromisso de compra e venda, desobrigando a  CONTRATADA da promessa e do consequente compromisso.

	</td>

	</tr>

	<tr>

	<td>

	Será  considerada fraude para efeitos desse contrato a comunicação de roubo ou de  furto do veículo ou do veículo fora do local em que ocorreu o fato, devendo  nesses casos, o CONTRATANTE comunicar ou efetuar o Boletim de Ocorrência no  Distrito Policial mais próximo do local do sinistro, sob pena de invalidar o  pacto adjeto de promessa de compra sobre documentos. Se houver indícios de  inobservância dessa cláusula, a CONTRATADA adotará todas as providências no  sentido de se apurar os fatos e onde efetivamente ocorreu o sinistro, podendo  inclusive o CONTRATANTE responder civil e criminalmente se comprovada a fraude.

	</td>

	</tr>

	<tr>

	<td>

	As  informações pessoais e do veículo preenchidas pelo CONTRATANTE no momento da  contratação, deverão corresponder à verdade, sob pena de responder por falsa  declaração e nulidade do pacto adjeto de promessa de compra sobre documentos.  Na mesma pena ocorrerá o CONTRATANTE que omitir ou ocultar informações  necessárias e indispensáveis para análise de seu perfil pela CONTRATADA.

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  único – A efetivação desse contrato ficará condicionada à análise do perfil do  contratante, ficando à cargo da contratada a sua aprovação.

	</td>

	</tr>

	<tr>

	<td>

	Caso  seja apurado que o CONTRATANTE declarou informação falsa, omitiu fatos ou  ocultou dados de sua pessoa, esta atitude será considerada fraude e a  CONTRATADA poderá declarar rescindido o pacto adjeto de promessa de compra  sobre documento em face ocorrido.

	</td>

	</tr>

	<tr>

	<td>

	<strong>14. DISPOSIÇÕES GERAIS – RESCISÃO</strong>

	</td>

	</tr>

	<tr>

	<td>

	Na  hipótese de rescisão pelo CONTRATANTE, as parcelas pagas até o momento da  rescisão, não serão restituídas pela CONTRATADA. No caso de existirem parcelas  pendentes com relação à habilitação ou instalação do equipamento, o CONTRATANTE  deverá pagar à CONTRATADA.

	</td>

	</tr>

	<tr>

	<td>

	Na  hipótese de rescisão pela CONTRATADA, os valores pagos pelo CONTRATANTE não  serão reembolsados, tendo em vista que os serviços foram prestados até a  presente data.

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  primeiro: Após a rescisão o CONTRATANTE terá o prazo máximo de 20 dias a partir  do cancelamento/encerramento do contrato, ou (45 dias em caso de rescisão por  inadimplência), não havendo a devolução do equipamento Rastreador nesse prazo,  instalado em regime de comodato pela JL SEGURO SYSTEM DO BRASIL, será cobrado  valor da venda do equipamento ao cliente, equivalente a R$1.000,00(Hum mil reais)  à título de compra do equipamento.

	</td>

	</tr>

	<tr>

	<td>

	Parágrafo  segundo: A CONTRATADA não aceitará a devolução do equipamento com avarias (em  decorrência da retirada do equipamento pelo próprio CONTRATANTE), bem como não  aceitará a devolução do equipamento caso a CONTRATADA tenha impetrado ação  judicial (após decurso dos prazos descrito nesta cláusula) para reaver o valor  referente a compra do equipamento descrito no parágrafo primeiro desta  cláusula, nestes casos somente serão aceitos o valor requerente a compra do  equipamento conforme já descrito.

	</td>

	</tr>

	<tr>

	<td>

	O  descumprimento de qualquer disposição deste Contrato pelo CONTRATANTE  autorizará a CONTRATADA a suspender imediatamente à prestação de serviço,  independentemente de prévia comunicação, notificação ou interpelação judicial  ou extrajudicial e sem que qualquer direito à indenização assista o  CONTRATANTE.

	</td>

	</tr>

	<tr>

	<td>

	O  CONTRATANTE declara estar devidamente instruído pela CONTRATADA quanto a forma  e condições deste Contrato, bem como declara que tomou conhecimento prévio do  inteiro teor deste instrumento.

	</td>

	</tr>

	<tr>

	<td>

	<strong>15. DA PREFERÊNCIA DE COMPRA PELO  CONTRATANTE EM CASO DE RECUPERAÇÃO DO BEM APÓS A VENDA SOBRE DOCUMENTO</strong> – As partes ajustam que na hipótese de recuperação do  veículo monitorado, dentro do período de 12 (doze) meses contados à partir do  pagamento decorrente da venda sobre documentos, a CONTRATADA, na condição de  nova proprietária do veículo, notificará, por escrito, o CONTRATANTE acerca da  recuperação e da faculdade deste optar pela compra do bem em iguais condições  que serão ofertadas aos terceiros.

	</td>

	</tr>

	<tr>

	<td>

	<strong>15.1 O CONTRATANTE</strong> terá prazo de 30 (trinta) dias, a partir do  recebimento da notificação, para manifestar interesse pela recompra do bem,  caso contrário perderá o direito de preferência e a CONTRATADA poderá dispor  livremente do veículo, sem necessidade de prestação de conta ao CONTRATANTE.

	</td>

	</tr>

	<tr>

	<td>

	<strong>16. O SISTEMA DE SEGURANÇA, RASTREAMENTO  E MONITORAMENTO OPERADO PELA CONTRATADA NÃO TEM NATUREZA DE SEGURO, PORTANTO  NÃO COBRE INCÊNDIO, COLISÃO, ENCHENTE ENTRE OUTROS, NÃO ESTABELECE PRÉMIO E NÃO  INDENIZA TERCEIROS.</strong>

	</td>

	</tr>

	<tr>

	<td>

	<strong>17. DO PREÇO DA HABILITAÇÃO E DO  MONITORAMENTO. DA FORMA DE PAGAMENTO</strong>.  O valor da habilitação do sistema e o valor da mensalidade do monitoramento  estão descrita no recibo (capa do contrato), que faz parte integrante deste  instrumento; a mensalidade aqui descrita deverá ser paga mediante boleto  bancário, sendo o primeiro 30 dias após a instalação do equipamento (pagamento  referente ao período já utilizado) Salvo negociação especial.

	</td>

	</tr>

	<tr>

	<td>

	<strong>18. DA RENOVAÇÃO CONTRATUAL</strong>

	</td>

	</tr>

	<tr>

	<td>

	As  partes em comum acordo poderão fazer a renovação deste contrato por igual  período (24 meses), devendo o CONTRATANTE entrar em contato com a empresa  CONTRATADA no mês do vencimento de seu contrato para efetuar a renovação  contratual a CONTRATADA poderá solicitar vistoria sem custo.

	</td>

	</tr>

	<tr>

	<td>
	
	O  cliente optou por um plano de menor valor abaixo da tabela FIPE conforme  especificado na capa do contrato, valor este que não tem como base a tabela  FIPE.

	</td>

	</tr>

	<tr>

	<td>

	19. As partes elegem o Foro de São Paulo para dirimir quaisquer controvérsias oriundas do presente Contrato e por estarem assim, justas e contratadas, as partes obrigam-se ao integral cumprimento do presente instrumento, assinando-o em 2 (duas) vias de igual teor, na presença de duas testemunhas. Vigência início '.$vigenciainicio.' a '.$vigenciafim.'

	</td>

	</tr>

	<tr>

	<td>

	São Paulo, '.$datainstalacao.'

	<img src="imgs/transp.png">
	<p><img src="imgs/transp.png"></p>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	<table width="100%">
	<tr>
		<td width="16%"><strong>MARCA:</strong></td>
		<td>'.$vetor_marca[marca].'</td>
		<td><strong>MODELO:</strong></td>
		<td>'.$vetor_veiculo[modelo].'</td>
		<td><strong>VALOR R$</strong></td>
		<td>'.$valorveiculo.'</td>
	</tr>
	<tr>
		<td width="16%"><strong>COR:</strong></td>
		<td>'.$vetor_veiculo[cor].'</td>
		<td><strong>COMB.:</strong></td>
		<td>'.$vetor_veiculo[combustivel].'</td>
		<td><strong>PLACA:</strong></td>
		<td>'.$vetor_veiculo[placa].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>RENAVAN:</strong></td>
		<td>'.$vetor_veiculo[renavan].'</td>
		<td><strong>CHASSI:</strong></td>
		<td>'.$vetor_veiculo[chassi].'</td>
		<td><strong>ANO MOD:</strong></td>
		<td>'.$vetor_anomod[ano].'</td>
	</tr>
	</table>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	<table width="100%">
	<tr>
		<td width="50%" valign="top" align="center">
			
			_______________________________
			<p>
			JL SEGURO SYSTEM
			</p>
			CNPJ:29.940.383/0001-60		</td>
		<td width="50%" valign="top" align="center">
			
			_______________________________
			<p>
			'.$nomecliente.'
			</p>
			CPF.: '.$cpfcnpj.'</td>
	</tr>
</table>

	</td>

	</tr>

	<tr>

	<td>


	</td>

	</tr>
	<tr>

	<td>


	</td>

	</tr>
	<tr>

	<td>
	
	<img src="imgs/transp.png">
	
	</td>

	</tr>

	<tr>

	<td align="center">

	TESTEMUNHAS:

	<img src="imgs/transp.png">
	<p><img src="imgs/transp.png"></p>

	</td>

	</tr>

	<tr>

	<td align="center">

	_______________________________________

	<img src="imgs/transp.png">
	<p><img src="imgs/transp.png"></p>

	</td>

	</tr>

	<tr>

	<td align="center">

	_______________________________________

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

</table>
</div>
</body>
</html>');

$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$pdf = $dompdf->output();
$dompdf->stream();

?>