<?php

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
}

	include"includes/conexao.php";
	 
    session_start();

    $id_pagina = '8';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    
    
  <?php include "calc_css.php"; ?>
  
  
</head>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include"includes/menu_sistema.php"; ?>
  
  
  <?php include "calculadora.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title text-ionc-2">Histórico de Saldos</h3>


                <h4><a href="cadastrosaldo"><i class="far fa-circle"></i>&nbsp;Cadastro de Saldos &nbsp;</a><h4>
                <h4><a href="cadastro_conta">&nbsp;<i class="far fa-circle"></i>&nbsp;Cadastro de Contas</a></h4>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-lg-2" style="background-color: #2c3e50; margin-left: 13px;">
                  <h5 style="color: white">Data do Saldo</h5>
                </div>

                <div class="col-lg-3">
                  <fieldset class="form-group">
                    <input type="date" onkeyup="formatar_ano(this.id)" name="data_busca" value="" required class="form-control" id="data-busca" placeholder="Digite a Descrição">
                  </fieldset>
                </div>

                <div class="col-lg-3" style="position: relative; right: 20px;">
                   <button onclick="mandarData('data-busca')" data-toggle="modal" data-target="#modalData" type="button" style="background-color: #2c3e50;" class="btn mesmo-tamanho" title="Pesquisar / Somar Valores"><i style="color: white" class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
              
              <table id="example3" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;">Banco</th>
                        <th>Agencia</th>
                        <th>Conta</th>
                        <th>Saldo</th>
                        <th width="13%">Data</th>
                        <th width="10%;"></th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				  
				$sql_atual = mysqli_query($con, "SELECT * FROM hiscontacorrente ORDER BY data_saldo ASC");

				while ($vetor=mysqli_fetch_array($sql_atual)) {
				
				 ?>
                <tr>
                  <td style="text-align: center;"><?php echo $vetor['nome_banco']; ?></td>
                  <td><?php echo $vetor['nagencia']; ?></td>
                  <td><?php echo $vetor['nconta']; ?></td>
                  <td><?php echo number_format($vetor['nsaldo'],2,",","."); ?></td>
                  <td><?php echo date('d-m-Y', strtotime($vetor['data_saldo'])); ?> / <?php $diaSem = date('D', strtotime($vetor['data_saldo']));
                      if($diaSem == 'Fri')
                      {echo 'Sexta';}
                      elseif($diaSem == 'Mon')
                      {echo 'Segunda';}
                      elseif($diaSem == 'Tue')
                      {echo 'Terça';}
                      elseif($diaSem == 'Wed')
                      {echo 'Quarta';}
                      elseif($diaSem == 'Thu')
                      {echo 'Quinta';}
                      elseif($diaSem == 'Sat')
                      {echo 'Sabado';}
                      elseif($diaSem == 'Sun')
                      {echo 'Domingo';}
                      ?>
                  </td>
                  <td style="text-align: center;"><!-- Button trigger modal -->
                    <a href="excluirSaldo.php?id=<?php echo $vetor['id']; ?>" ><button type="button" class="btn btn-danger mesmo-tamanho" title="Excluir Cadastro"><i class="fa fa-close"></i></button></a>
                    
                    <button type="button" class="btn btn-info mesmo-tamanho" title="Visualizar / Alterar Conta" onclick="mandarId(<?php echo $vetor['id']; ?>)" data-toggle="modal" data-target="#modalExemplo">
                      <i class="fa fa-edit"></i>
                    </button>
                  </td>

                </tr>
                <?php } ?>
                </tbody> 
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <input type="hidden" id="pegadorDeId" name="pegadorDeId">

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versão</b> 1.0    </div>
    <strong>Todos direitos reservados JL Seguro.
  </footer>
</div>
<!-- ./wrapper -->
<form action="alterarsaldo.php" method="post">
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Alterar saldo</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id_do_saldo" id="id_do_saldo" value="">
        <div class="row">
          <div class="col-lg-4">
              <label class="form-label semibold" for="exampleInput">AGENCIA:</label>
              <input name="agencia" class="form-control" type="text" Use readonly="true" id="alteraAgencia" placeholder="Digite a Nova Agencia">
          </div>
          <div class="col-lg-4">
              <label class="form-label semibold" for="exampleInput">CONTA:</label>
              <input name="conta" class="form-control"type="text" Use readonly="true" id="alteraConta" placeholder="Digite a Nova Conta">
          </div>
          <div class="col-lg-4">
              <label class="form-label semibold" for="exampleInput">SALDO:</label>
              <input name="saldo" class="form-control" onKeyPress="return(moeda(this,'.',',',event))" type="text" id="alteraSaldo" placeholder="Digite o Nova Saldo">
          </div>
        </div><!--.row-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Salvar mudanças</button>
      </div>
    </div>
  </div>
</div>
</form>

<!---  MODAL DE BUSCA PELA DATA DO DIA --->
<div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col-lg-6" style="background-color: #2c3e50; margin-left: 13px;">
            <h2 style="color: white; height: 40px;"class="modal-title" id="exampleModalLabel">Busca pelo dia : </h2>
          </div>
          <div class="col-lg-3">
            <input style="width: 130px; height: 40px;" type="text" name="data_puxada" id="data_puxada" value="" Use readonly="true" class="form-control">
          </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-3" style="background-color: #2c3e50; margin-left: 13px;">
            <h4 style="color: white; height: 20px;">Valor Total :</h4>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <input style="width: 130px; height: 40px;" type="text" id="valor-somado" name="valor-somado" value="" Use readonly="true" class="form-control">
            </fieldset>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <a href="imprimirSaldoDoDia.php?data_saldo=" id="impressao" target="_blank"><button type="button" class="btn btn-warning mesmo-tamanho" title="Imprimir Saldo"><i class="fa fa-print"></i></button></a>
      </div>
    </div>
  </div>
</div>

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->

<script type="text/javascript">
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.fromCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
</script>


<!-- FUNÇÃO PARA O MODAL DE BUSCAR SALDO PELA DATA -->
<script type="text/javascript">

  function mandarData(id_input){

    var data = document.getElementById(id_input).value;
    var local_data = document.getElementById("data_puxada");
    var link = document.getElementById('impressao');
    link.href = "imprimirSaldoDoDia.php?data_saldo=" + data;

    /// FORMATA A DATA E INSERE NO INPUT DO MODAL
    data_formatada = data.split('-');
    data_formatada1 = data_formatada[2] + '-' + data_formatada[1] + '-' + data_formatada[0];
    local_data.value = data_formatada1;
    
    /// MANDA A DATA PARA O BANCO DE DADOS E FAZ A CONTA
      var req1;
      // Verificando Browser
      if(window.XMLHttpRequest) {
         req1 = new XMLHttpRequest();
      }
      else if(window.ActiveXObject) {
         req1 = new ActiveXObject("Microsoft.XMLHTTP");
      }
       
      // Arquivo PHP juntamente com o valor digitado no campo (método GET)
      var url = "buscaSaldoData.php?data="+data;
       
      // Chamada do método open para processar a requisição
      req1.open("get", url, true);
       
      // Quando o objeto recebe o retorno, chamamos a seguinte função;
      req1.onreadystatechange = function() {
       
        // Exibe a mensagem "Buscando Noticias..." enquanto carrega
        if(req1.readyState == 1) {
          document.getElementById('valor-somado').value = '....';
        }
       
        // Verifica se o Ajax realizou todas as operações corretamente
        if(req1.readyState == 4 && req1.status == 200) {
       
        // Resposta retornada pelo busca.php
        var resposta = req1.responseText;

        // Abaixo colocamos a(s) resposta(s) na div resultado
        document.getElementById("valor-somado").value = resposta;
        }
    }
    req1.send(null);

  }

</script>

<!--  FUNÇÃO PARA EXECUTAR O MAODAL, PEGAR O ID, E PUXAR DADOS PARA ALTERAR  -->
<script type="text/javascript">
   
  // FUNÇÃO PARA BUSCA NOTICIA
  function mandarId(idSaldo) {
    var iddosaldo = idSaldo;
    var num1 = 1;
    var num2 = 2;
    var num3 = 3;
    var num4 = 4;
    
    repetirEpassar(iddosaldo, num1, "alteraConta");
    repetirEpassar(iddosaldo, num2, "alteraAgencia");
    repetirEpassar(iddosaldo, num3, "alteraSaldo");
    repetirEpassar(iddosaldo, num4, "id_do_saldo");

      function repetirEpassar(iddosaldo,numvezes,idlocal){
      var req;
     
      // Verificando Browser
      if(window.XMLHttpRequest) {
         req = new XMLHttpRequest();
      }
      else if(window.ActiveXObject) {
         req = new ActiveXObject("Microsoft.XMLHTTP");
      }
      // Arquivo PHP juntamente com o valor digitado no campo (método GET)
      var url = "buscaSaldoInfo.php?idSaldo="+iddosaldo+"&num="+numvezes;
      // Chamada do método open para processar a requisição
      req.open("Get", url, true); 
      // Quando o objeto recebe o retorno, chamamos a seguinte função;
      req.onreadystatechange = function() {
       
        // Exibe a mensagem "Buscando Noticias..." enquanto carrega
        if(req.readyState == 1) {
          document.getElementById('alteraAgencia').value = 'Buscando..';
          document.getElementById('alteraConta').value = 'Buscando..';
          document.getElementById('alteraSaldo').value = 'Buscando..';
        }
       
        // Verifica se o Ajax realizou todas as operações corretamente
        if(req.readyState == 4 && req.status == 200) {
        // Resposta retornada pelo busca.php
        var resposta = req.responseText;
        // Abaixo colocamos a(s) resposta(s) na div resultado
        document.getElementById(idlocal).value = resposta;
        }
      }
      req.send(null);
      }
}
</script>
<script>
  jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-br-pre": function ( a ) {
        var x;
        if ( $.trim(a) !== '' ) {
            var frDatea = $.trim(a).split(' ');
            var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [00,00,00];
            var frDatea2 = frDatea[0].split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        }
        else {
            x = Infinity;
        }
        return x;
    },
    "date-br-asc": function ( a, b ) {
        return a - b;
    },
    "date-br-desc": function ( a, b ) {
        return b - a;
    }
} );
  $('#meuModal').on('shown.bs.modal', function () {
  $('#meuInput').trigger('focus')
  })
  $(function () {
    $('#example1').DataTable({
      columnDefs: [
       { type: 'date-br', targets: 3 }
       ],
       "order": [[3, "asc"]] 
    })
    $('#example2').DataTable({
      columnDefs: [
       { type: 'date-br', targets: 3 }
       ],
       "order": [[3, "asc"]] 
    })
  })
 $(function () {
    $('#example3').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "pageLength"  : 50
    });
  });

 $(function(){
      $('html').css('background-color', '#E5FFE4');
      $('body').css('background-color', '#E5FFE4');
      $('.container').css('background-color', '#E5FFE4');
      $('.content').css('background-color', '#E5FFE4');
      $('.box').css('background-color', '#E5FFE4');
    });
</script>

<!-- Script que não deixa o usuario digitar mais do que 4 digitos no campo de ano no input type date,
requer somente um id e um onkeyup passando o this.id lembre-se de verificar se não outro id no mesmo input-->
<script type="text/javascript">

function formatar_ano(id){
    var array_data = [];

      var data = $("#"+id).val();

      if (data.length > 10) {

        array_data = data.split("-");

        var ano = array_data[0];

        ano = ano.substring(0,(ano.length - 1));

        var dia = array_data[2];
        var mes = array_data[1];

        var data_final = ano + "-" + mes + "-" + dia;

        $("#"+id).val(data_final);

      }
}

</script>


</body>
</html>
<?php } } ?>