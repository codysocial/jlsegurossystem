<?php

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
 
}

	 include "includes/conexao.php";
	 
	 session_start();

   $id_pagina = '9';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$sql_cliente = "select * from clientes where tipocad = '2' order by nome ASC";
  $res1 = mysqli_query($con, $sql_cliente);

  $sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <script type="text/javascript" src="aplicacoes/aplicjava.js"></script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
  <?php include "calc_css.php"; ?>
  


  <script type="text/javascript">

            $(function(){

            $('#formID').on('submit', function(e) {

            e.preventDefault();

            $.ajax({
                url: "http://v2.systemjl.com.br/recebe_cp.php",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                success: function(data){

                    var html = '';


                    if(parseInt(data.success) == 1){
                        html = `<div class="alert alert-success">Cliente cadastrado com Sucesso! <br/>
                                Em 5 segundos você será Redirecionado para a Lista de Clientes</div>`;
                    }else{
                        html = `<div class="alert alert-danger">Ops :(, algo deu errado tente novamente mais tarde!</div>`;
                    }

                    $("#formResult").html(html);

                    $("#formID")[0].reset();


                    if (parseInt(data.tipocad) == 1) {
                        setTimeout(() => {
                            window.location.href = 'listarclientes.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 2) {
                        setTimeout(() => {
                            window.location.href = 'listarfornecedores.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 3) {
                        setTimeout(() => {
                            window.location.href = 'listarvendedores.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 4) {
                        setTimeout(() => {
                            window.location.href = 'listarprestadores.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 5) {
                        setTimeout(() => {
                            window.location.href = 'listarparceiros.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 6) {
                        setTimeout(() => {
                            window.location.href = 'listarrepresentantes.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 7) {
                        setTimeout(() => {
                            window.location.href = 'listarparcerias.php';
                        }, 5000);
                    }


                }
            });

            });


            });

            </script>


  
</head>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>
  
  
  <?php include "calculadora.php"; ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title text-ionc-3">Contas a Pagar</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div id="formResult"></div>
            <form method="post" name="cliente" enctype="multipart/form-data" id="formID">

            <table width="100%">
                <tr>
                  <td width="95%"></td>
                  <td><button type="submit" class="btn btn-primary"  style="    float: left;">Cadastrar</button></td>
                </tr>
              </table>
              </br>
              </br>
        
                <div class="row">
          <div class="col-lg-12">
                    <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Fornecedor</label>
                            <select name="cliente" id="exampleSelect" class="form-control select2" required>
                              <option value="" selected="selected">Selecione um Fornecedor</option>
                              <?php while ($vetor=mysqli_fetch_array($res1)) { ?>
                              <option value="<?php echo  $vetor['id_cli'] ."_". $vetor['nome']; ?>"><?php echo $vetor['nome'] ?></option>
                              <?php } ?>
                            </select>
          </fieldset>
          </div>
        </div>
                
                <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Descrição</label>
              <input type="text" name="descricao" required class="form-control" id="exampleInput" placeholder="Digite a Descrição">
            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Valor</label>
              <input type="text" name="valor" class="form-control" id="exampleInput" placeholder="Valor Pagamento" onKeyPress="mascara(this,mvalor)" required>
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Data de Entrada</label>
              <input type="date" name="datalancamento" value="<?php echo date('Y-m-d'); ?>"  class="form-control" id="data_form2" onkeyup="formatar_ano(this.id)" placeholder="Digite o Data" required>
            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Data de Vencimento</label>
              <input type="date" name="datavencimento" id="data_form" onkeyup="formatar_ano(this.id)" class="form-control" placeholder="Digite o Data" required>
            </fieldset>
          </div>
        </div><!--.row-->
                
          <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Quantidade de Parcelas</label>
              <input type="number" name="quantidade" class="form-control" value="1" placeholder="Quantidade de Parcelas">
            </fieldset>
          </div>
                    
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInputEmail1">Intervalo entre as Parcelas</label>
              <input type="number" name="intervalo" class="form-control" value="1" placeholder="Intervalo entre as Parcelas">
            </fieldset>
          </div>
        </div><!--.row-->

          <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Forma de Pagamento</label>
              <select name="formapag" class="form-control">
                <option value="" selected="">Selecione...</option>
                <?php

                    $sql_forma = mysqli_query($con, "select * from formaspag order by nome ASC");

                    while($vetor_forma = mysqli_fetch_array($sql_forma)) { 

                    ?>

                    <option value="<?php echo $vetor_forma['nome']; ?>" <?php if (strcasecmp($vetor['formapag24'], $vetor_forma['nome']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_forma['nome']; ?></option>

                    <?php } ?>
              </select>
            </fieldset>
          </div>
                    
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInputEmail1">Pago?</label>
              <select name="recebido" id="exampleSelect" class="form-control">
                            <option value="1">Sim</option>
                            <option value="2" selected="selected">Não</option>
                          </select>
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInputEmail1">Imprimir?</label>
              <select name="imprimir" id="exampleSelect" class="form-control">
                            <option value="1">Sim</option>
                            <option value="2" selected="selected">Não</option>
                          </select>
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">

        <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Centro de Custo / Departamento</label>
              <select name="centrodecusto" class="form-control" required="">
                <option value="" selected="">Selecione...</option>
                <?php

                    $sql_centro = mysqli_query($con, "select * from centrodecustos order by nome ASC");

                    while($vetor_centro = mysqli_fetch_array($sql_centro)) { 

                    ?>

                    <option value="<?php echo $vetor_centro['id_centro']; ?>" <?php if (strcasecmp($vetor['centrodecusto'], $vetor_centro['id_centro']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_centro['nome']; ?></option>

                    <?php } ?>
              </select>
            </fieldset>
          </div>

          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Despesa</label>
              <select name="despesa" class="form-control" required="">
                <option value="" selected="">Selecione...</option>
                <?php

                    $sql_despesa = mysqli_query($con, "select * from despesa order by nome ASC");

                    while($vetor_despesa = mysqli_fetch_array($sql_despesa)) { 

                    ?>

                    <option value="<?php echo $vetor_despesa['id_despesa']; ?>" <?php if (strcasecmp($vetor['despesa'], $vetor_despesa['id_despesa']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_despesa['nome']; ?></option>

                    <?php } ?>
              </select>
            </fieldset>
          </div>

          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Projeto / Finalidade</label>
              <select name="evento" class="form-control">
                <option value="" selected="">Selecione...</option>
                <?php

                    $sql_evento = mysqli_query($con, "select * from eventos order by nome ASC");

                    while($vetor_evento = mysqli_fetch_array($sql_evento)) { 

                    ?>

                    <option value="<?php echo $vetor_evento['id_evento']; ?>" <?php if (strcasecmp($vetor['evento'], $vetor_evento['id_evento']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_evento['nome']; ?></option>

                    <?php } ?>
              </select>
            </fieldset>
          </div>

        </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
                  <tr>
                    <td width="9%"><button type="submit" class="btn btn-primary"  style="    float: left;">Cadastrar</button> </form></td>
                    <td><a href="javascript:history.back()"><button type="button" class="btn btn-danger"  style="    float: left;">Cancelar</button></a></td>
                  </tr>
                </table>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });

  $(function(){
      $('html').css('background-color', '#F9EBFF');
      $('body').css('background-color', '#F9EBFF');
      $('.container').css('background-color', '#F9EBFF');
      $('.content').css('background-color', '#F9EBFF');
      $('.box').css('background-color', '#F9EBFF');
      $('.tab-content').css('background-color', '#F9EBFF');
    });
</script>


<!-- Script que não deixa o usuario digitar mais do que 4 digitos no campo de ano no input type date,
requer somente um id e um onkeyup passando o this.id lembre-se de verificar se não outro id no mesmo input-->
<script type="text/javascript">

function formatar_ano(id){
    var array_data = [];

      var data = $("#"+id).val();

      if (data.length > 10) {

        array_data = data.split("-");

        var ano = array_data[0];

        ano = ano.substring(0,(ano.length - 1));

        var dia = array_data[2];
        var mes = array_data[1];

        var data_final = ano + "-" + mes + "-" + dia;

        $("#"+id).val(data_final);

      }
}

</script>

</body>
</html>
<?php } } ?>