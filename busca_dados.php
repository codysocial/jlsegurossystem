<?php

	include"includes/conexao.php";

	$id = $_GET['id'];

	
	$sql_atual = mysqli_query($con, "SELECT * from pcld where id_cliente = '$id' order by datavencimento ASC");

	$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$id' ");
	$vetor_cliente = mysqli_fetch_array($sql_cliente);

	$sql_sub_total = mysqli_query($con, "SELECT SUM(valor) FROM pcld WHERE id_cliente = '$id' ");
	$vetor_sub_total = mysqli_fetch_array($sql_sub_total);
	$subtotal = $vetor_sub_total['SUM(valor)'];

	$subtotal = number_format($subtotal,2,",",".");

	
    $cont = mysqli_affected_rows($con);
    // Verifica se a consulta retornou linhas 
    if ($cont > 0) {
        // Atribui o código HTML para montar uma tabela
        $tabela = "<thead>
                        <tr>
                            <th>Contr.</th>
                            <th>Cliente</th>
                            <th>Descrição</th>
                            <th>Vencimento</th>
                            <th>Valor Conta</th>
                            <th>Status</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>";
        $return = "$tabela";
        // Captura os dados da consulta e inseri na tabela HTML
        while ($linha = mysqli_fetch_array($sql_atual)) {

        	$valor = $linha["valor"];

        	$id_cr = $linha["id_cr"];

        	$valor = number_format($valor,2,",",".");

        	$status = $linha["status"];

        	if ($status == 1) {
        		$status = "Pendente";
        	}elseif ($status == 2) {
        		$status = "Pago";
        	}elseif ($status == 3) {
        		$status = "Cancelado";
        	}

        	$codigo_botoes = "

            <input style='width: 30px; height: 30px; cursor: pointer;' type='checkbox' style='width: 30px; height: 30px;' value='{$id_cr}' name='sel[]'>
            ";


        	$return.= "<input id='id_cr_pcld' type='hidden' value='" . $id_cr . "' </input>";
            $return.= "<td>" . utf8_encode($linha["id_pedido"]) . "</td>";
            $return.= "<td>" . utf8_encode($vetor_cliente["nome"]) . "</td>";
            $return.= "<td>" . $linha["descricao"] . "</td>";
            $return.= "<td>" . utf8_encode(date('d/m/Y', strtotime($linha["datavencimento"]))) . "</td>";
            $return.= "<td>" . utf8_encode($valor) . "</td>";
            $return.= "<td>" . utf8_encode($status) . "</td>";
            $return.= "<td>" . $codigo_botoes . "</td>";

            $return.= "</tr>";
        }
        echo $return.="</tbody>";



        echo "<tr><td colspan='4' style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;''> VALOR TOTAL</td>
                        <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='3'>
                          R$ $subtotal
                        </td>";
    } else {
        // Se a consulta não retornar nenhum valor, exibi mensagem para o usuário
        echo "Não foram encontrados registros!";
    }

?>