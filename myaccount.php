<?php

require 'components/upload.php';

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
 
}

	 include"includes/conexao.php";
	 
	 session_start();

   $id_pagina = '11';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$sql_cadastro = "select * from usuarios where id_user = '".$_SESSION['id']."'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
 
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Meu Perfil</h3>
            </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                    <div class="col-md-2 col-12 photo-profile">
                        <div class="area-photo-profile">
                            <?php if (isset($vetor_cadastro['photo']) and !empty($vetor_cadastro['photo'])): ?>
                                <img src="./imgs/profile/<?php echo $vetor_cadastro['photo']; ?>" />
                            <?php else: ?>
                                <img src="./imgs/user.png"/>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-9 col-12">

                        <?php if (isset($_POST['svprofile']) and !empty($_POST['svprofile'])):
                            
                            $upload = new upload;
                            $response = $upload->save_photo();

                            if (isset($_FILES['myfile']['tmp_name']) && !empty($_FILES['myfile']['tmp_name'])) {
                                $r = updateMyAccount(
                                    $response, 
                                    addslashes($_POST['nome']), 
                                    addslashes($_POST['usuario']), 
                                    $_SESSION['id'],
                                    $conn
                                );
                            }else{
                                $r = updateMyAccountData(
                                    addslashes($_POST['nome']), 
                                    addslashes($_POST['usuario']), 
                                    $_SESSION['id'],
                                    $conn
                                );
                            }
                            

                         endif; ?>
                        
                        <?php if ($r): ?>
                            <div class="alert alert-success">Alteração realizada com sucesso!</div>
                        <?php endif; ?>
                         
                        <form method="POST" enctype="multipart/form-data">

                            <table class="table">
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="file" class="form-control" name="myfile"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for=""><strong>Seu nome</strong></label>
                                        <input style="text-transform: uppercase;" type="text" value="<?php echo $vetor_cadastro['nome']; ?>" class="form-control" name="nome" placeholder="Seu nome" required="true" />
                                    </td>
                                    <td>
                                        <label for=""><strong>Nome de Usuário Usuário</strong></label>
                                        <input style="text-transform: uppercase;" type="text" value="<?php echo $vetor_cadastro['usuario'] ?>" class="form-control" name="usuario" placeholder="@usuario_exemplo" required="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td><button type="submit" name="svprofile" value="svtrue" class="btn btn-sm btn-primary">Salvar Alterações</button></td>
                                </tr>
                            </table>

                        </form>
                        
                    </div>
                    </div>
                </div>
                <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
<?php } } ?>