<ul class="mobile-menu" id="mobile-menu">

            <div id="titleDesktopWhite" style="background-color: none;font-size: 1.4em;margin-bottom: 30px;color: #fff;font-weight: bold;">Menu Geral</div>

            <li class="navigation-modern-li">

                <a href="#">

                    <i class="fa ionc-1 fa-pie-chart"></i>

                    <span class="navigation-title-link">Cadastros Gerais</span>
                    
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                    <a href="listarclientes"><i class="far fa-circle"></i> Cliente</a>
                    <a href="listarfornecedores"><i class="far fa-circle"></i> Fornecedor</a>
                    <a href="listarparcerias"><i class="far fa-circle"></i> Parceiro</a>
                    <a href="listarvendedores"><i class="far fa-circle"></i> Vendedor</a>
                    <a href="listarparceiros"><i class="far fa-circle"></i> Indicador</a>
                    <a href="listarrepresentantes"><i class="far fa-circle"></i> Representante</a>
                    <a href="listarprestadores"><i class="far fa-circle"></i> Técnico - Instalador</a>
                    <a href="aniversariantes"><i class="far fa-circle"></i> Aniversariantes</a>
                    <a href="listarEstatisticas"><i class="far fa-circle"></i> Estatisticas</a>
                </ul>

            </li>

            <li class="navigation-modern-li">
                <a href="#">
                <i class="fa fa-money ionc-2"></i> <span class="navigation-title-link">Financeiro</span>
                <span class="pull-right-container  ">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">

                    <a href="extratoconta"><i class="far fa-circle"></i> Fluxo de Caixa</a>
                    <a href="cadastro_conta"><i class="far fa-circle"></i> Cadastro de Contas</a>
                    <a href="cadastrosaldo"><i class="far fa-circle"></i> Cadastro de Saldos</a>
                    <a href="contascorrente"><i class="far fa-circle"></i> Histórico de Saldos</a>

                </ul>
            </li>

            <li class="navigation-modern-li">

            <a href="#">
                <i class="fas fa-cart-arrow-down ionc-3"></i> <span class="navigation-title-link">Contas a Pagar</span>
                <span class="pull-right-container  ">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
            </a>

            <ul class="dropdown-navigator-modern  dropdown-navigator-unable">

                <a href="listarcp"><i class="far fa-circle"></i> Todos</a>

                <a href="gerarcomissoes"><i class="far fa-circle"></i> Gerar Comissões</a>
                
                <li class="navigation-modern-li">

                    <a href="#">
                        <i class="far fa-circle"></i> Relatórios C. a Pagar
                    </a>

                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                    <a href="relcpanalitico"><i class="far fa-circle"></i> Analítico</a>
                    <a href="relcpsintetico"><i class="far fa-circle"></i> Sintético</a>
                    <a href="relcomissoesapagar"><i class="far fa-circle"></i> Comissões a Pagar</a>
                    <a href="relcpcomissoes"><i class="far fa-circle"></i> Comissões</a>
                    <a href="relcpfornecedor"><i class="far fa-circle"></i> Por Fornecedor</a>
                    <a href="relcpcentrodecustos"><i class="far fa-circle"></i> Por Centro de Custos</a>
                    <a href="relcpdespesa"><i class="far fa-circle"></i> Por Despesa</a>
                    <a href="relcpprojetofinalidade"><i class="far fa-circle"></i> Por Projeto / Finalidade</a>
                </ul>

                </li> 

            </ul>

            </li>


            <li class="navigation-modern-li">
                <a href="#">
                <i class="fas fa-cart-plus ionc-4"></i> <span class="navigation-title-link">Contas a Receber</span>
                <span class="pull-right-container  ">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                    <a href="listarcr"><i class="far fa-circle"></i> Todos</a>
                    <a href="listar_excluidos"><i class="far fa-circle"></i> Histórico De Exclusão</a>
                    <a href="relcranalitico"><i class="far fa-circle"></i> Analítico</a>
                    <a href="relcrsintetico"><i class="far fa-circle"></i> Sintético</a>
                    <a href="listarpcld"><i class="far fa-circle"></i> PCLD</a>            
                </ul>
            </li>


            <li class="navigation-modern-li">
            <a href="#">
            <i class="fas fa-money-check-alt ionc-5"></i> <span class="navigation-title-link">Faturamento</span>
            <span class="pull-right-container  ">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
            </a>
            <ul class="dropdown-navigator-modern  dropdown-navigator-unable">

                <a href="relcrglobal"><i class="far fa-circle"></i> Por Periodo</a>
                <a href="relcrporitem"><i class="far fa-circle"></i> Por Produto</a>
                <a href="relcrcliente"><i class="far fa-circle"></i> Por Cliente</a>

            </ul>

            </li>

            <li class="navigation-modern-li">
                <a href="#">
                <i class="fa fa-cart-plus ionc-6"></i>
                <span class="navigation-title-link">Pedidos</span>
                <span class="pull-right-container  ">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                    <a href="cadastropedido"><i class="far fa-circle"></i> Novo Pedido</a>
                    <a href="listarpedidos"><i class="far fa-circle"></i> Pedidos</a>
                </ul>
            </li>


            <li class="navigation-modern-li ">
          <a href="#">
            <i class="fa fa-files-o ionc-7"></i>
            <span class="navigation-title-link">OS</span>
            <span class="pull-right-container  ">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
            <a href="listaros"><i class="far fa-circle"></i> OS</a>
          </ul>
        </li>

        <li class="navigation-modern-li ">
          <a href="#">
            <i class="fa fa-check-circle ionc-8"></i>
            <span class="navigation-title-link">Orçamentos</span>
            <span class="pull-right-container  ">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
            <a href="cadastroorcamento"><i class="far fa-circle"></i> Novo Orçamento</a>
            <a href="listarorcamentos"><i class="far fa-circle"></i> Orçamentos</a>
          </ul>
        </li>

        <li class="navigation-modern-li ">
          <a href="#">
            <i class="fa fa-archive ionc-9"></i>
            <span class="navigation-title-link">Equipamentos</span>
            <span class="pull-right-container  ">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                <a href="listarprodutos"><i class="far fa-circle"></i> Produtos</a>
                <a href="entradaestoque"><i class="far fa-circle"></i> Entrada de Estoque</a>
                <a href="listar_rastreadores"><i class="far fa-circle"></i> Rastreadores</a>
                <a href="listar_chips"><i class="far fa-circle"></i> Chips</a>

          </ul>
        </li>
            
            <li class="navigation-modern-li">

                <a href="#">
                    <i class="fa fa-address-card ionc-10"></i>
                    <span class="navigation-title-link">CRM</span>
                    <span class="pull-right-container  ">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                    <a href="listarcrm"><i class="far fa-circle"></i> CRM</a>
                    <a href="listarrenovacoes"><i class="far fa-circle"></i> Renovações</a>
                    <a href="listarcontratos"><i class="far fa-circle"></i> Contratos</a>
                </ul>

            </li> 

            <li class="navigation-modern-li">
                <a href="#">
                    <i class="fa fa-cogs ionc-11"></i>
                    <span class="navigation-title-link">Configurações</span>
                    <span class="pull-right-container  ">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                    <a href="listarusuarios"><i class="far fa-circle"></i> Usuários</a>
                    <a href="listarchecklist"><i class="far fa-circle"></i> CheckList</a>
                    <a href="listarservicos"><i class="far fa-circle"></i> Serviços</a>
                    <a href="listarcentrodecustos"><i class="far fa-circle"></i> Centro de Custo</a>
                    <a href="listardespesas"><i class="far fa-circle"></i> Despesas</a>
                    <a href="listareventos"><i class="far fa-circle"></i> Projetos / Finalidades</a>
                    <a href="fipe/fipe_form"><i class="far fa-circle"></i> Atualizar Fipe</a>
                </ul>
            </li>

             <!-- <div class="boxThemeSystem ">
            <h4>Tema do Sistema</h4>
            <form method="POST" id="altertheme" action="http://v2.systemjl.com.br/themeUpdate.php">
                <ul>
                    <li><label for="claro"><input type="radio" name="theme" value="0" id="claro">Claro</label> </li>

                    <li><label for="dark"><input type="radio" name="theme" value="1" id="dark">Escuro</label></li>
                </ul>

                <div style="width: 100%;height: auto;display: flex;justify-content: center;">
                    <button type="submit" class="btn btn-sm btn-primary"> Muda Tema </button>
                </div>
            </form>
            </div> -->

        </ul>

        <script type="text/javascript">

            $(function(){

                $('.navigation-modern-li').on('click', function() {
                    var dropdownList = $(this)[0].lastElementChild;

                    if($(dropdownList).hasClass('dropdown-navigator-enable')){
                        $(dropdownList).fadeOut().removeClass('dropdown-navigator-enable').addClass('dropdown-navigator-unable');
                    }else{
                        $(dropdownList).slideDown(300).removeClass('dropdown-navigator-unable').addClass('dropdown-navigator-enable');
                    }
                    
                    
                });

                $(document).keyup(function(event) {


                    if(event.keyCode == 115){
                        window.location.href = 'http://v2.systemjl.com.br/listarclientes';
                    }

                    if(event.keyCode == 27){
                        $('.dropdown-navigator-modern').hide();
                    }

                });

                // $(document).keydown(function (e) {

                //     if(e.which == 16) pressedShift = true; //Informando que Crtl está acionado

                //     if((e.which == 67|| e.keyCode == 67) && pressedShift == true) {
                //         window.location.href = 'http://v2.systemjl.com.br/listarclientes';
                //     } 

                //     if((e.which == 70|| e.keyCode == 70) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/listarfornecedores';
                //     }

                //     if((e.which == 86|| e.keyCode == 86) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/listarvendedores';
                //     }

                //     if((e.which == 73|| e.keyCode == 73) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/listarparceiros';
                //     }

                //     if((e.which == 82|| e.keyCode == 82) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/listarrepresentantes';
                //     }

                //     if((e.which == 84|| e.keyCode == 84) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/listarprestadores';
                //     }

                //     if((e.which == 65|| e.keyCode == 65) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/aniversariantes';
                //     }

                //     if((e.which == 69|| e.keyCode == 69) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/listarEstatisticas';
                //     }

                //     if((e.which == 88|| e.keyCode == 88) && pressedShift == true){
                //         window.location.href = 'http://v2.systemjl.com.br/sair';
                //     }

                // });


            });
        </script>
        
        <!-- <div style="width: 100%;height: auto;background-color: #ddd;box-shadow: 0px 0px 10px rgba(0,0,0,0.3);z-index: 2000;margin-bottom: 30px;" id="hideUpdatesMonths">
        
            <button type="button" class="btn" style="background-color: none;box-shadow: none;" data-toggle="modal" data-target="#modalExemplo">
              <strong><i class="fa fa-info-circle" aria-hidden="true"></i> Confira as novas atualizações do sistema clicando aqui!</strong>
            </button>
        </div> -->

<!-- Modal -->
<!-- <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="TituloModalLongoExemplo" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="TituloModalLongoExemplo">Atualizações de Março - Novidades</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <h4>Novidades</h4>
            
            <div style="background-color: #f6f6f6;border:1px solid #ccc;padding: 20px;margin-bottom: 30px;">
               <h5 style="color: green;font-size: 1em;"><strong>Novos Atalhos</strong></h5>
                <li><strong> SHIT + C </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>CLIENTES</strong></li>
                <li><strong> SHIT + F </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>FORNECEDORES</strong></li>
                <li><strong> SHIT + P </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>PARCEIROS</strong></li>
                <li><strong> SHIT + V </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>VENDEDORES</strong></li>
                <li><strong> SHIT + I </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>INDICADORES</strong></li>
                <li><strong> SHIT + R </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>REPRESENTANTES</strong></li>
                <li><strong> SHIT + T </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>TÉCNICO - INSTALADOR</strong></li>
                <li><strong> SHIT + A </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>ANIVERSARIANTES</strong></li>
                <li><strong> SHIT + E </strong> = ATALHO PARA ABRIR A SESSÃO DE <strong>ESTATISTICAS</strong></li>
            </div>

            <div style="background-color: #f6f6f6;border:1px solid #ccc;padding: 20px;margin-bottom: 30px;">
               <h5 style="color: green;font-size: 1em;margin-bottom: 10px;"><strong>Novo Menu Lateral</strong></h5>
               <p>- Alterado o menu de navegação para um mais prático e com acessibilidade de acesso com atalhos rápidos via teclado.</p>

               <p>============================================================</p>
               <p>- Agora você pode coloca sua foto de perfil!</p>
               <p>- Alguns bugs foram corrigidos para a melhoria do sistema.</p>
               <p>- Agora você pode alterar seu nome ou seu nome de usuário em <strong>Minha .</strong></p>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div> -->