<?php /*!! <*$exec(pushContentsServerLaraFluks())\$/> !! */

function Mask($mask, $str) {

	$str = str_replace(" ", "", $str);

	for ($i = 0; $i < strlen($str); $i++) {
		$mask[strpos($mask, "#")] = $str[$i];
	}

	return $mask;

}

include "includes/conexao.php";
include 'components/helps/helps.php';

session_start();

$id_pagina = '1';

if ($_SESSION['id'] == NULL) {

	echo "<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";

} else {

	$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
	$res = mysqli_query($con, $sql_permissao);
	$num_busca = mysqli_num_rows($res);

	if ($num_busca == 0) {

		echo "<script language=\"JavaScript\">
  location.href=\"sempermissao.php\";
  </script>";

	} else {

		$sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
		$res_cadastro = mysqli_query($con, $sql_cadastro);
		$vetor_cadastro = mysqli_fetch_array($res_cadastro);

		?>
<!DOCTYPE html>
<html>
<?php include 'includes/head.php'?>

<style type="text/css">
      #ball{
      width: 15px;
      height: 15px;
      border-radius: 50%;
      margin-left: 50%;
    }

    .min{
      width: 5%;
    }
</style>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Gerenciador do Menu Dropdown</h3>
              <a href="newdropdown.php"><button class="btn btn-primary">Novo Menu Dropdown</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <table class="table">
              <tr>
                <th>ID</th>
                <th>Dropdown Nome</th>
                <th>Dropdown Icone</th>
                <th>Dropdown Link</th>
                <th>Associado ao Menu</th>
                <th>Ações</th>
              </tr>

              <?php foreach (pushMenuDropDownAll($conn) as $value): ?>
                <tr>

                  <td><?php $component->white($value['id']);?></td>
                  <td><?php $component->white($value['dropmenu_name']);?></td>
                  <td><i class="<?php $component->white($value['dropdown_icon']);?> justify"></i> &nbsp;   <?php $component->white($value['dropdown_icon']);?></td>
                  <td><?php $component->white($value['dropdown_link']);?></td>

                  <td>
                    <?php echo pushMenuName($conn, $value['id_menu']); ?>
                  </td>

                  <td>
                    <a href="newdropdown.php?id=<?php $component->white($value['id']);?>&em=1" title="Editar Menu" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                    <a href="" title="Deleta Menu" class="btn btn-danger"><i class="fa fa-close"></i></a>
                  </td>

                </tr>
              <?php endforeach;?>

            </table>



            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php';?>


</div>
<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->

</body>
</html>
<?php }}?>