<?php

require 'vendor/autoload.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

include"includes/conexao.php";

$id = $_GET['id_pedido'];

$sql_pedidos = mysqli_query($con, "select * from pedidos where id_pedido = '$id'");
$vetor_pedido = mysqli_fetch_array($sql_pedidos);

$sql_os = mysqli_query($con, "select * from os where id_pedido = '$id'");
$vetor_os = mysqli_fetch_array($sql_os);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$sql_veiculo = mysqli_query($con, "select * from cliente_veiculo where id_cliente = '$vetor_pedido[id_cliente]' order by id_veiculo DESC limit 0,1");
$vetor_veiculo = mysqli_fetch_array($sql_veiculo);

$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$vetor_veiculo[marca]'") or die (mysqli_error($con));
$vetor_marca = mysqli_fetch_array($sql_marca);

$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$vetor_veiculo[anomod]'") or die (mysqli_error($con));
$vetor_anomod = mysqli_fetch_array($sql_anomod);

if($vetor_veiculo['valor'] != NULL) { $valorveiculo = number_format($vetor_veiculo['valor'],2,',','.'); } else { $valorveiculo = number_format($vetor_anomod['valor'],2,',','.'); }

$qtddoc = strlen($vetor_cliente['cpfcnpj']);

if($qtddoc == 14) {
                
$cpfcnpj = Mask("##.###.###/####-##",$vetor_cliente['cpfcnpj']); 
                
}
                
if($qtddoc == 11) {
                
$cpfcnpj = Mask("###.###.###-##",$vetor_cliente['cpfcnpj']); 
                
}

$vigenciainicio = date('d/m/Y', strtotime($vetor_pedido['vigenciainicio']));
$vigenciafim = date('d/m/Y', strtotime($vetor_pedido['vigenciafim']));
$datainstalacao = date('d/m/Y', strtotime($vetor_os['datainstalacao']));


$dompdf->loadHtml('<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
.style2 {
	font-size: 12
}
.style3 {font-size: 12px}

@page {
                margin: 100px 25px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                color: white;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                color: #000000;
                text-align: center;
                line-height: 35px;
            }

            footer .pagenum:before {
		      	content: counter(page);
			}
-->
    </style>
</head>
<body>
		<header>
            <img src="imgs/logo.png" width="150px">
            <hr style="height:1px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
        </header>

        <footer>
            <hr style="height:1px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
            <table width="100%">

            	<tr><td width="80%">
				<div class="style3">
            	Rua Olívia Guedes Penteado,156 - Socorro
            	</br>
				São Paulo/SP – CEP 04764-000
				</br>
				www.jlsegurosystem.com.br
				</div>

            	</td> <td width="20%"><div class="style3">JLSS - V.012018</div></td></tr>


            </table> 
            <div class="pagenum-container style3"><span class="pagenum"></span></div>
        </footer>

<div class="style3">
<table width="100%">
	<tr>
		<td align="center"><span class="style3">CONTRATO DE PRESTAÇÃO DE SERVIÇOS DE EMISSÃO DE SINAIS PARA RASTREAMENTO DE VEÍCULO A DISTÂNCIA SEM INDENIZAÇÃO EM CASO DE ROUBO OU FURTO.</span></td>
	</tr>
	<tr>
		<td>
		CONTRATO Nº: '.$id.'
		</td>
	</tr>
	<tr>
		<td>
		EQUIPAMENTO Nº: '.$vetor_os[nrastreador].'
		</td>
	</tr>
	<tr>
		<td>
		<div align="justify">
		Prezado Cliente,
		<p>
		Você acaba de adquirir o mais completo e avançado sistema de rastreamento e localização automática de veículos por satélite-GPSPRS para o seu veículo, aproveitamos a oportunidade para lhe agradecer por ter escolhido a JL SEGURO SYSTEM DO BRASIL como sua prestadora de serviço de rastreamento e localização. Esperamos plenamente a sua satisfação com a JL SEGURO SYSTEM DO BRASIL, pois, a partir de agora, você tem a tranquilidade e segurança para o seu veículo.
		</p>
		O sistema JL SEGURO SYSTEM DO BRASIL – GPSPRS fabricado no Brasil (componentes importados) com um dos mais sofisticados softwares de monitoramento e rastreamento, que permite localizar e bloquear o seu veículo em caso de furto qualificado ou roubo com precisão pelo sistema GPS (Global Positioning System) através de nossa Central de Monitoramento, que funciona 24 horas/365 dias por ano.
		<p>
		<strong>IMPORTANTE:</strong>
		</p>
		Em caso de roubo e furto qualificado, o cliente deverá informar a placa do veículo e o nome do titular do contrato para que sejam acionados os procedimentos de recuperação do veículo, sendo que o CONTRATANTE se responsabiliza pela veracidade das informações prestadas, e em caso de falsa comunicação de roubo/furto qualificado, serão cobrados a parte os custos de deslocamento de equipe e gastos reais com o procedimento, sendo que em caso de fraude, haverá o cancelamento do contrato bem como, sansões judiciais.

		</td>
	</tr>
	<tr>

		<td>
		
		<p>
<strong>COMO AGIR EM CASO DE ROUBO E FURTO QUALIFICADO</strong>
</p>
<span class="style3">
Basta ligar para a Central de Monitoramento JL SEGURO SYSTEM DO BRASIL O MAIS RÁPIDO POSSÍVEL, informar a ocorrência do roubo/furto qualificado, informando a placa do seu veículo e o nome do titular do contrato, que será prontamente iniciado a recuperação do veículo. (Seu veículo sendo recuperado será solicitado à presença do proprietário do veículo para a devida entrega mediante a presença das autoridades policiais).
</span>
<p>
<div align="center" class="style3"><strong>TELEFONE DA CENTRAL 24hrs JL SEGURO SYSTEM: 0800 030 6672 ou 0800 777 9740</strong></div>
</p>
</td>
</tr>

<tr>

<td>
<div align="justify" class="style3">
JL SEGURO SYSTEM DO BRASIL empresa de proteção veicular, pessoa jurídica de direito privado, inscrita no CNPJ/MF sob o número 29.940.383/0001-60, com sede na Rua Olivia Guedes Penteado nº 156, Bairro Socorro, na cidade de São Paulo/SP, CEP 04766-000, que opera principalmente serviços de sistema de rastreamento para veiculo ativada a distancia, doravante  denominada CONTRATADA e de outro lado o (a) cliente conforme dados encontra-se na capa deste contrato é ao final da última página deste, denominado (a) CONTRATANTE.
</td>
</tr>
<tr>

<td>
<strong>CONTRATADA e CONTRATANTE</strong> doravante em conjunto denominadas simplesmente as “Partes”, ou isoladamente, “Parte”.

	</td>

	</tr>

	<tr>

	<td>

		<strong>CONSIDERANDOS INICIAIS: </strong>

	</td>

	</tr>

	<tr>

	<td>

	(i) <strong>CONSIDERANDO QUE</strong>, a <strong>CONTRATADA</strong> presta serviços de Rastreamento e localização de veículos.

	</td>

	</tr>

	<tr>

	<td>

	(ii)  <strong>CONSIDERANDO QUE</strong>, o presente instrumento contém as condições gerais aplicáveis à prestação de tais serviços pela CONTRATADA, em consonância com os artigos 593 e ss. do Código Civil brasileiro vigente.

	</td>

	</tr>

	<tr>

	<td>

	(iii)<strong> CONSIDERANDO QUE</strong>, o <strong>CONTRATANTE</strong> deseja contratar a <strong>CONTRATADA</strong> para a prestação de referidos serviços, mediante a assinatura do Termo de Contratação, conforme contemplado neste instrumento.
  
	</td>

	</tr>

	<tr>

	<td>

	SENDO ASSIM, as Partes têm entre si justo e contratado a celebração do presente Contrato de Prestação de Serviços de Rastreamento e Localização de Veículos (o “Contrato”), que se regerá pelas seguintes cláusulas e condições:

	</td>

	</tr>

	<tr>

	<td>

	<strong>1.  OBJETO</strong> - O objeto do presente contrato consiste na prestação de serviços de Rastreamento e localização de veículos pela Contratada ao Contratante, ou ao usuário do veículo por ele indicado no Termo de Contratação (em conjunto, os “Serviços”), conforme as condições previstas na Cláusula 2 abaixo.

	</td>

	</tr>

	<tr>

	<td>

	O veículo de uso ou de propriedade da Contratante, objeto deste Contrato, encontra-se devidamente descrito no<br />
  Termo de Contratação (o “Veículo”).

	</td>

	</tr>

	<tr>

	<td>

	As Partes declaram e reconhecem que a presente contratação não representa um contrato de seguro, envolvendo exclusivamente a prestação dos Serviços subjacente a uma prestação de meio e não de resultado por parte da Contratada, garantindo a Contratada única e tão somente o objeto acima detalhado, qual seja a prestação de serviços       de rastreamento e localização do Veículo, observados os termos e condições acordados neste instrumento.

	</td>

	</tr>

	<tr>

	<td>

	Para fins de clareza, as Partes reconhecem e concordam que a celebração do presente instrumento, de forma alguma e em nenhuma circunstância, implica explícita ou implicitamente em obrigação fim ou resultado final por parte da Contratada, não havendo qualquer garantia por parte da Contratada em restituição ou recuperação do Veículo eventualmente furtado ou roubado, nem tampouco, em reparação ou indenização de danos decorrentes de sinistro envolvendo o Veículo.

	</td>

	</tr>

	<tr>

	<td>

	Sendo assim, a Contratada não se responsabiliza pelo resultado da prestação dos Serviços, porém, todavia, obrigando-se em empregar todos os meios disponíveis e toda a diligência necessária para alcançar o melhor resultado na localização do Veículo eventualmente furtado ou roubado.

	</td>

	</tr>

	<tr>

	<td>

	<strong>2.  SERVIÇOS</strong> - Os Serviços pactuados compreendem exclusiva e restritivamente: (a) rastreamento e localização do Veículo, conforme Plano e Serviço escolhidos pelo CONTRATANTE no respectivo Termo de Contratação, somente em caso de roubo ou furto do Veículo; (b) Comunicação e colaboração com as autoridades competentes ou Prestadores de Serviços Autorizados sobre a localização do Veículo, excluídas todas e quaisquer responsabilidades não previstas expressamente neste instrumento.

	</td>

	</tr>

	<tr>

	<td>

	A Contratada compromete-se a disponibilizar e prestar os Serviços por 24 (vinte e quatro) horas, durante os 07 (sete) dias da semana, 365 dias por ano, desde que (i) o(s) equipamento(s) fornecido(s) pela Contratada seja(m) instalado(s) no Veículo por Centros de Atendimento Técnico ou Agentes Técnicos Autorizados, indicados pela Contratada (em conjunto, os “Equipamentos”), e (ii) o Contratante esteja adimplente com suas obrigações

	</td>

	</tr>

	<tr>

	<td>

	O Contratante receberá seu Código de Identificação, ao término da instalação dos Equipamentos ou no prazo máximo de 14 (quatorze) dias úteis contados da conclusão de tal instalação.

	</td>

	</tr>

	<tr>

	<td>

	O Código de Identificação é pessoal e secreto, sendo de exclusiva responsabilidade do Contratante a divulgação para pessoas não autorizadas.

	</td>

	</tr>

	<tr>

	<td>

	Procedimento de Identificação

	</td>

	</tr>

	<tr>

	<td>

	Para acionamento do sistema e durante qualquer contato telefônico com a Contratada, o Contratante ou o usuário do Veículo indicado no Termo de Contratação (o “Usuário”) deverá informar seu Código de Identificação, ou a placa/chassi ou ainda o nome/CPF completo do Contratante, ao operador da Central de Controle de Operações da Contratada.

	</td>

	</tr>

	<tr>

	<td>

	Caso a comunicação seja feita sem a indicação de dados suficientes para localizar o cadastro do Contratante, na forma estabelecida na Cláusula 2.3.1 acima, a Contratada fica desobrigada da prestação dos Serviços.

	</td>

	</tr>

	<tr>

	<td>

	Procedimento em caso de notificação de furto ou roubo do Veículo

	</td>

	</tr>


	<tr>

	<td>

	Em caso de furto, roubo ou Sequestro, o Contratante ou o Usuário acionará imediatamente após ter ciência do fato o sistema por meio do procedimento de identificação descrito na cláusula 2.2., abrindo concomitantemente chamado ao 190 da Polícia.

	</td>

	</tr>

	<tr>

	<td>

	No caso do Veículo ser localizado, a Contratada poderá avisar a Polícia sobre o evento, fornecendo-lhe informações sobre a localização estimada do Veículo e sua descrição, conforme evidenciado pelo Sistema da Contratada.

	</td>

	</tr>

	<tr>

	<td>

	A atuação e o desempenho do trabalho da Polícia e o contato eventualmente realizado pela Contratada com tais autoridades não afetam, em quaisquer circunstâncias, as obrigações e responsabilidades pactuadas entre as Partes no âmbito deste Contrato.

	</td>

	</tr>

	<tr>

	<td>

	O Contratante reconhece e concorda que a notificação à Contratada sobre o furto ou roubo do Veículo poderá acarretar ações da Polícia, que podem ensejar a retenção do Veículo ou sujeição de danos ao próprio bem ou a seus condutores, inclusive sua detenção, retenção, prisão, sobre os quais não se responsabilizará a Contratada, diante da ausência de quaisquer responsabilidades ou vinculação.

	</td>

	</tr>

	<tr>

	<td>

	Em caso de falsa notificação, o Contratante compromete-se a reembolsar e/ou indenizar a Contratada em relação a todos e quaisquer gastos e/ou prejuízos que venha a sofrer em decorrência de uma notificação falsa e/ou enganosa, sem prejuízo da aplicação de multa equivalente ao valor mensal dos Serviços pago pelo Contratante à época do evento.

	</td>

	</tr>

	<tr>

	<td>

	No caso de não recuperação, após 30 dias, do Veículo (Apenas Veículo de Passeio) que, comprovadamente tenha sido roubado ou furtado, a Contratada compromete-se a devolver ao Contratante os valores pagos à Contratada em função do presente contrato.

	</td>

	</tr>

	<tr>

	<td>

	O pagamento dar-se-á em conta corrente do Contratante e terá efeito automático de rescisão da avença com quitação para ambas as partes.


	</td>

	</tr>

	<tr>

	<td>

	Só fará direito à devolução do preço estatuída acima, o cliente que optar pelos planos que tenham esta modalidade e que, cumulativamente: (a) estiver adimplente com a Contratante por ocasião do evento de roubo ou furto; (b) dar notícia do evento à Contratada em no máximo, 2 (duas) horas contadas da efetividade do evento.

	</td>

	</tr>

	<tr>

	<td>

	<strong>3.VEÍCULO</strong>

	</td>

	</tr>

	<tr>

	<td>

	O Contratante declara ser o proprietário ou usuário do Veículo, assumindo total responsabilidade, sob as penas da lei, pela veracidade da declaração ora prestada e consequências dela advindas.

	</td>

	</tr>

	<tr>

	<td>

	O Contratante se obriga e se compromete a informar à Contratada, antecipadamente, sobre toda e qualquer transferência do Veículo a terceiros, ficando esclarecido que, até que isto ocorra, o Contratante permanecerá responsável, obrigado e vinculado a todos os termos e condições deste Contrato.

	</td>

	</tr>

	<tr>

	<td>

	O Contratante tem a obrigação de manter seu cadastro junto à Contratada sempre atualizado, informando por escrito acerca de quaisquer alterações nas informações prestadas, isentando a Contratada de quaisquer obrigações que venham a ocorrer em razão da falta de atualização das informações pessoais ou do veículo, não podendo ainda arguir ineficácia de comunicação feita ao endereço até então declarado.

	</td>

	</tr>

	<tr>

	<td>

	<strong>4.  EQUIPAMENTOS</strong>

	</td>

	</tr>

	<tr>

	<td>

	O Equipamento instalado no Veículo do Contratante é cedido em regime de comodato, devendo ser restituído em  favor da CONTRATADA tão logo se encerre a presente prestação de serviços, seja qual for o motivo.

	</td>

	</tr>

	<tr>

	<td>

	 Para devolução do Equipamento, o Contratante deverá entrar em contato com a Contratada e agendar previamente sua retirada por Agente Técnico Autorizado, a retirada do equipamento só deve ser feita pelo nosso técnico ou agente autorizado com o custo no valor de R$ 60,00 (sessenta reais que deverá ser pago pela Contratante. Em caso de não devolução do Equipamento pelo Contratante, o mesmo deverá arcar com o valor integral do aparelho, definido no TERMO DE CONTRATAÇÃO, ensejando ainda a inscrição do débito nos cadastros de proteção de crédito.

	</td>

	</tr>

	<tr>

	<td>

	O Contratante se compromete a submeter os Equipamentos somente aos cuidados do Centro de Atendimento Técnico indicado pela Contratada, bem como observar integralmente as instruções por ela fornecidas no momento do encerramento.

	</td>

	</tr>

	<tr>

	<td>

	 A Contratada ratificará a transferência dos Equipamentos ao novo veículo se, e somente se, tiverem sido preenchidas as seguintes condições: (a) o Contratante comunicou à Contratada os dados do novo veículo; (b) o Contratante declarou expressamente que é proprietário ou usuário do novo veículo e firmou novo Termo de Contratação constando os dados do novo veículo, que para todos os fins e efeitos deste Contrato, passará a ser considerado como um Veículo, conforme termo aqui definido; (c) a transferência dos Equipamentos ao outro Veículo foi realizada pelo Centro de Atendimento Técnico ou pelo Agente Técnico Autorizado.

	</td>

	</tr>

	<tr>

	<td>

	A transferência e/ou manutenção dos Equipamentos, sem a estrita observância do disposto nesta Cláusula, prejudicará a prestação dos Serviços pela Contratada.

	</td>

	</tr>

	<tr>

	<td>

	A Contratada recomenda que o Contratante realize, constantemente teste de funcionamento do Equipamento no aplicativo, visando certificar a perfeita operação, funcionamento do mesmo. A Contratada poderá efetuar testes de funcionamento de forma regular atravéz de comando

	</td>

	</tr>

	<tr>

	<td>

	<strong>5.  CONFIDENCIALIDADE</strong>

	</td>

	</tr>

	<tr>

	<td>

	 A Contratada se compromete a manter sempre em sigilo toda e qualquer informação relativa ao Contratante, Usuários Autorizados, e a localização do Veículo, exceto quando existir indícios de furto ou roubo, conforme prevê as Cláusulas 2.5.2. a 2.5.4., quando acionará, a Polícia e/ou os Prestadores de Serviços Autorizados, conforme a modalidade de Serviço escolhida nos termos do respectivo Termo de Contratação.

	</td>

	</tr>

	<tr>

	<td>

	<strong>6.  EXCEÇÕES DE RESPONSABILIDADE E LIMITAÇÕES À PRESTAÇÃO DO SERVIÇO</strong>

	</td>

	</tr>

	<tr>

	<td>

	As Partes, neste ato, reconhecem  e concordam  que o sucesso  no resgate  de um Veículo,  ou seja, de sua localização e recuperação, depende de  fatores  que  fogem  ao  controle  da  Contratada,  dos  quais  podemos  citar,  a título  ilustrativo e não taxativo, (i) o tempo transcorrido entre a ocorrência do roubo ou furto e o acionamento da Central de Controle da Contratada; (ii) da colaboração de terceiros envolvidos na ocorrência como o Usuário do Veículo; bem como (iii) outros aspectos e circunstâncias, como bloqueadores de sinais, problemas de rede telefônica, condições climáticas e topográficas, dentre outras situações que reflitam casos fortuito e de força maior, conforme definido em lei.

	</td>

	</tr>

	<tr>

	<td>

	Assim, como previsto nas Cláusulas 1.2 e 1.3 acima, a Contratada não garante total e pleno sucesso de recuperação dos Veículos, porém utilizará todos os recursos e instrumentos ao seu alcance, nos termos do presente instrumento, para localizar os Veículos roubados ou furtados.

	</td>

	</tr>

	<tr>

	<td>

	As Partes concordam que o vínculo ora estabelecido entre a Contratada e o Contratante não constitui e não representa, em hipótese alguma, um contrato de seguro, como previsto nas Cláusulas 1.2 e 1.3 acima, não havendo e/ou não implicando em qualquer cobertura, de qualquer natureza, para o Contratante, Usuários, Condutores, Veículo e/ou terceiros. Restando a obrigação da Contratada, quando localizado, o dever de comunicação às autoridades competentes. Conseguintemente, a Contratada recomenda expressamente que o Contratante obtenha e mantenha sempre válida referida cobertura junto a uma companhia de seguros idônea.

	</td>

	</tr>

	<tr>

	<td>

	O Contratante declara que tem conhecimento de que poderão ocorrer interferências na Área de Cobertura causando eventuais falhas no recebimento e transmissão do sinal do Veículo.

	</td>

	</tr>

	<tr>

	<td>

	Está ciente, ainda, de que os Serviços poderão ser temporariamente interrompidos ou restringidos: (i) se o Contratante viajar para fora da Área de Cobertura; e/ou (ii) em decorrência de restrições operacionais, relocações, reparos e atividades similares, que se façam necessárias à apropriada operação e/ou à melhoria do Sistema; e/ou (iii) em decorrência de  interferências de topografia, edificações, bloqueios, lugares fechados, condições atmosféricas, etc.; e/ou (iv) em decorrência de quedas e interrupções no fornecimento de energia e sinais de comunicação.

	</td>

	</tr>

	<tr>

	<td>

	Dessa forma, as Partes acordam que, na ocorrência das limitações mencionadas nesta Cláusula 6.2, a Contratada não poderá ser responsabilizada por quaisquer interrupções, atrasos ou defeitos nas transmissões. Em nenhuma hipótese ou situação (inclusive na hipótese de culpa ou dolo), a Contratada será responsável por danos morais, lucros cessantes ou qualquer outro dano indireto eventualmente sofrido pelo Contratante e/ou terceiros. Estão excluídos todos os demais danos eventualmente sofridos pelo Contratante, seus empregados, representantes ou diretores, ou ainda quaisquer terceiros, em razão de situações que fujam ao controle da Contratada.

	</td>

	</tr>

	<tr>

	<td>

	<strong>7.  PREÇOS E CONDIÇÕES DE PAGAMENTO</strong>

	</td>

	</tr>

	<tr>

	<td>

	E o primeiro vencimento é sempre 30 (trinta) dias após a instalação do equipamento rastreador. 

	</td>

	</tr>

	<tr>

	<td>

	O não pagamento, pelo Contratante, dos valores pactuados no seu respectivo vencimento, implicará na cobrança de multa de 2% (dois por cento), acrescida de juros de mora de 1% (um por cento) ao mês, calculados pro rata die, além dos custos com serviços de cobrança, correspondentes a 10% (dez por cento) sobre o valor devido, para os casos de cobrança amigável; e 20% (vinte por cento) sobre o valor devido, para casos de cobrança judicial, além das custas e despesas processuais, tudo acrescido de correção monetária.

	</td>

	</tr>

	<tr>

	<td>

	Em havendo o não pagamento por um período superior a 15 (quinze) dias a contar do respectivo vencimento, a Contratada poderá tomar todas as providências cabíveis para recuperação de seu crédito, inclusive a promoção de negativação do usuário perante os órgãos de proteção de crédito, sem prejuízo da suspensão dos Serviços até o efetivo pagamento de todos os valores devidos.

	</td>

	</tr>

	<tr>

	<td>

	Os Serviços prestados pela Contratada no âmbito deste Contrato serão suspensos até que haja o pagamento devido dos valores vencidos.

	</td>

	</tr>

	<tr>

	<td>

	No caso de suspensão dos serviços pelo motivo descrito na Cláusula 7.3.2 acima, a reativação dos Serviços ocorrerá, mediante solicitação do Contratante, em até 10 (dez) dias úteis da data da solicitação da Contratante, desde que (i) tenha quitado integralmente os valores em aberto e (ii) tenha levado o Veículo a um Centro de Atendimento Técnico ou pelo Agente Técnico Autorizado, sem prejuízo do pagamento da taxa de reativação.

	</td>

	</tr>

	<tr>

	<td>

	O preço dos Serviços será reajustado de acordo com a variação do Índice Geral de Preços do Mercado, publicado pela Fundação Getúlio Vargas (IGP- M/FGV), ou outro índice que venha a substituí-lo, na menor periodicidade permitida por lei.

	</td>

	</tr>

	<tr>

	<td>

	Na hipótese de serem criados novos tributos que incidam sobre o objeto deste Contrato, ou modificadas as atuais alíquotas de impostos, de forma a majorar o custo da Contratada, os preços sofrerão reajuste para refletir a respectiva mudança.

	</td>

	</tr>

	<tr>

	<td>

	<strong>8.  PRAZO E VIGÊNCIA</strong>

	</td>

	</tr>

	<tr>

	<td>

	O presente Contrato é celebrado pelo prazo de 24 (vinte e quatro) meses, contado a partir da data de instalação dos Equipamentos no   Veículo, sendo renovado automaticamente por iguais e sucessivos períodos.

	</td>

	</tr>

	<tr>

	<td>

	Na hipótese de renovação deste Contrato: (i) os preços serão reajustados e revisados, conforme estabelecidos nas Cláusulas 7.4., 7.5. e 7.6.; ou (ii) serão aplicados os novos preços para a prestação dos Serviços, vigentes na data da mencionada renovação, sendo que o pagamento pelo Contratante do valor estabelecido nos termos deste item representa aceitação tácita do Contratante do mesmo.

	</td>

	</tr>

	<tr>

	<td>

	<strong>9.  RESCISÃO</strong>

	</td>

	</tr>

	<tr>

	<td>

	Reservam-se, ainda, as Partes o direito de declararem antecipadamente rescindido o presente Contrato, independente de interpelação, notificação ou aviso prévio, mediante a ocorrência de uma das seguintes hipóteses: (a) decretação de falência, requerimento de recuperação judicial ou extrajudicial, dissolução judicial, protesto legítimo de título de crédito,

	</td>

	</tr>

	<tr>

	<td>

	liquidação ou dissolução extrajudicial de qualquer das Partes; (b) descumprimento de qualquer obrigação assumida em decorrência deste Contrato, e a Parte infratora deixar de saná-la no prazo de 30 (trinta) dias após o recebimento de notificação com aviso de recebimento ou, se a violação não puder ser sanada, exceto se a Parte infratora apresente razões consideradas satisfatórias, ou ainda apresente um plano de ação considerado adequado pela Parte inocente em até de 30 (trinta) dias após o recebimento de referida notificação; (c) na hipótese do Contratante transferir os direitos e obrigações decorrentes do presente Contrato a terceiros, sem a prévia notificação e anuência da Contratada; (d) na hipótese de o Contratante deixar de manter atualizados seus dados cadastrais; (e) na hipótese do Contratante utilizar os Serviços em desacordo com o Contrato, ou omitir informações que visem obter vantagens ilícitas; (f) o CPF/CNPJ do Contratante seja cancelado pela Receita Federal.

	</td>

	</tr>

	<tr>

	<td>

	 Qualquer que seja o motivo da rescisão, o CONTRATANTE deverá devolver o Equipamento à CONTRATADA, nos termos da Cláusula 4.2 acima.

	</td>

	</tr>

	<tr>

	<td>

	<strong>10. DISPOSIÇÕES GERAIS</strong>

	</td>

	</tr>

	<tr>

	<td>

	Em caso de conflito entre as condições deste Contrato e os Termos de Contratação, ou qualquer outro documento, este Contrato prevalecerá.

	</td>

	</tr>

	<tr>

	<td>

	O Contratante informará à Contratada, por escrito, sobre qualquer caso de alteração nas informações prestadas. As eventuais alterações somente obrigarão a Contratada, caso sejam entregues, e o Contratante isenta a Contratada de qualquer prejuízo que venha ocorrer em função da falta de atualização ou da omissão na entrega das informações atualizadas pelo Contratante.

	</td>

	</tr>

	<tr>

	<td>

	O Contratante declara estar ciente dos  termos  e condições  deste  Contrato,  com  os  quais  concorda  para  todos os fins de direito a contar da assinatura do respectivo Termo de Contratação e que conhece e entende o seu conteúdo, inclusive a essência e os pormenores dos Serviços contemplados neste Contrato, e que atesta serem compatíveis com a sua necessidade sob o aspecto do tipo, especificação, qualidade e característica dos Serviços, ora renunciando expressamente à alegação de incompatibilidade, de qualquer natureza ou espécie, conforme legislação aplicável.

	</td>

	</tr>

	<tr>

	<td>

	A tolerância ou omissão das Partes do exercício do direito que lhes assista sob o presente Contrato e o Termo de Contratação, representará mera liberalidade, não podendo ser considerada como novação contratual ou renúncia aos mesmos, não implicando, portanto, na desídia de exigir o cumprimento das obrigações aqui contidas ou do direito de pleitear, futuramente, a execução total de cada uma dessas obrigações. Se qualquer das cláusulas ou disposições do presente Contrato e/ou do Termo de Contratação for considerada ilegal ou inválida, em qualquer procedimento administrativo ou judicial, a ilegalidade, invalidade ou ineficácia então verificada deverá ser interpretada restritivamente, não afetando as demais cláusulas do Contrato, as quais permanecerão válidas e em pleno vigor.

	</td>

	</tr>

	<tr>

	<td>

	O Contrato obriga as Partes contratantes e seus sucessores e cessionários autorizados, bem como, seus herdeiros a qualquer título.

	</td>

	</tr>

	<tr>

	<td>

	Qualquer alteração e/ou aditamento do presente Contrato somente produzirá efeitos legais quando feitos por escrito e devidamente assinado pelas Partes.

	</td>

	</tr>

	<tr>

	<td>

	A Contratada poderá ceder e transferir os direitos e obrigações decorrentes deste Contrato, integral ou parcialmente, a seu exclusivo critério, comunicando previamente o Contratante, assumindo o cessionário todas as obrigações perante o Contratante por força deste Contrato. Entretanto, a Contratada permanecerá obrigada, individual e solidariamente com o cessionário, durante o primeiro período de vigência deste Contrato. Toda e qualquer transferência deverá ser comunicada ao Contratante com antecedência de 10 (dez) dias e por escrito, para a atualização dos dados cadastrais.

	</td>

	</tr>

	<tr>

	<td>

	As Partes pactuam que todas as comunicações formais entre elas, inclusive, mas não limitadamente, para fins de pedido de devolução dos Equipamentos, dever-se-ão dar por meio escrito por carta registrada, via fac-símile com comprovante de transmissão, via correio eletrônico conforme dados constantes na primeira página deste Contrato ou no Termo de Contratação, ou por via de telegrama com cópia e aviso de recepção, aos endereços apontados na qualificação delas neste instrumento ou no Termo de Contratação, sendo ajustado também que a simples entrega da comunicação nestes endereços, independentemente da pessoa receptora ou, ainda mesmo, a não entrega desde que por motivo de recusa ou de endereço inexistente, serão consideradas como tendo sido eficazmente realizada a comunicação.

	</td>

	</tr>

	<tr>

	<td>

	Qualquer Parte que vier alterar seu endereço deverá dar notícia à outra pela via escrita e na forma acima apontada, sob pena de não poder arguir a ineficácia de qualquer comunicação enviada ao endereço até então declarado.

	</td>

	</tr>

	<tr>

	<td>

	Os títulos das cláusulas somente são colocados por conveniência e de modo algum limitam ou afetam o escopo ou

	</td>

	</tr>

	<tr>

	<td>

	Intenção das Partes neste Contrato. 

	</td>

	</tr>

	<tr>

	<td>

	O presente Contrato e o Termo de Contratação, incluindo seus respectivos Anexos, constituem o total entendimento entre as Partes e se sobrepõem a todos os acordos e entendimentos prévios em relação ao mesmo. O presente Contrato e o Termo de Contratação, incluindo seus respectivos Anexos, não poderá ser aditado, alterado ou modificado por qualquer outra forma, que não através de acordo escrito e assinado por ambas as Partes, exceto se previsto de forma diversa em dispositivo específico deste instrumento.

	</td>

	</tr>

	<tr>

	<td>

	19. As partes elegem o Foro de São Paulo para dirimir quaisquer controvérsias oriundas do presente Contrato e por estarem assim, justas e contratadas, as partes obrigam-se ao integral cumprimento do presente instrumento, assinando-o em 2 (duas) vias de igual teor, na presença de duas testemunhas. Vigência início '.$vigenciainicio.' a '.$vigenciafim.'

	</td>

	</tr>

	<tr>

	<td>

	São Paulo, '.$datainstalacao.'

	<img src="imgs/transp.png">
	<p><img src="imgs/transp.png"></p>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>
	<font color="#ffffff">
	O presente Contrato e o Termo de Contratação, incluindo seus respectivos Anexos, constituem o total entendimento entre as Partes e se sobrepõem a todos os acordos e entendimentos prévios em relação ao mesmo. O presente Contrato e o Termo de Contratação, incluindo seus respectivos Anexos, não poderá ser aditado, alterado ou modificado por qualquer outra forma, que não através de acordo escrito e assinado por ambas as Partes, exceto se previsto de forma diversa em dispositivo específico deste instrumento.

	O presente Contrato e o Termo de Contratação, incluindo seus respectivos Anexos, constituem o total entendimento entre as Partes e se sobrepõem a todos os acordos e entendimentos prévios em relação ao mesmo. O presente Contrato e o Termo de Contratação, incluindo seus respectivos Anexos, não poderá ser aditado, alterado ou modificado por qualquer outra forma, que não através de acordo escrito e assinado por ambas as Partes, exceto se previsto de forma diversa em dispositivo específico deste instrumento.

	</font>

	</td>

	</tr>

	<tr>

	<td>

	<table width="100%">
	<tr>
		<td width="16%"><strong>MARCA:</strong></td>
		<td>'.$vetor_marca[marca].'</td>
		<td><strong>MODELO:</strong></td>
		<td>'.$vetor_veiculo[modelo].'</td>
		<td><strong>VALOR R$</strong></td>
		<td>'.$valorveiculo.'</td>
	</tr>
	<tr>
		<td width="16%"><strong>COR:</strong></td>
		<td>'.$vetor_veiculo[cor].'</td>
		<td><strong>COMB.:</strong></td>
		<td>'.$vetor_veiculo[combustivel].'</td>
		<td><strong>PLACA:</strong></td>
		<td>'.$vetor_veiculo[placa].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>RENAVAN:</strong></td>
		<td>'.$vetor_veiculo[renavan].'</td>
		<td><strong>CHASSI:</strong></td>
		<td>'.$vetor_veiculo[chassi].'</td>
		<td><strong>ANO MOD:</strong></td>
		<td>'.$vetor_anomod[ano].'</td>
	</tr>
	</table>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	<table width="100%">
	<tr>
		<td width="50%" valign="top" align="center">
			
			_______________________________
			<p>
			JL SEGURO SYSTEM
			</p>
			CNPJ:29.940.383/0001-60		</td>
		<td width="50%" valign="top" align="center">
			
			_______________________________
			<p>
			'.$vetor_cliente[nome].'
			</p>
			CPF.: '.$cpfcnpj.'</td>
	</tr>
</table>

	</td>

	</tr>

	<tr>

	<td>


	</td>

	</tr>
	<tr>

	<td>


	</td>

	</tr>
	<tr>

	<td>
	
	<img src="imgs/transp.png">
	
	</td>

	</tr>

	<tr>

	<td align="center">

	TESTEMUNHAS:

	<img src="imgs/transp.png">
	<p><img src="imgs/transp.png"></p>

	</td>

	</tr>

	<tr>

	<td align="center">

	_______________________________________

	<img src="imgs/transp.png">
	<p><img src="imgs/transp.png"></p>

	</td>

	</tr>

	<tr>

	<td align="center">

	_______________________________________

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

</table>
</div>
</body>
</html>');

$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$pdf = $dompdf->output();
$dompdf->stream();

?>