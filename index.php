<?php

  /*!! <*$exec(pushContentsServerLaraFluks())\$/> !! */

  session_start();

  include "includes/conexao.php";
  include "includes/box-item-menu.php";

	if($_SESSION['id'] == NULL) {
	   header("Location: index.html");
	} else {
       $vetor_cadastro = getUser($conn);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="layout/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" id="alttheme" href="layout/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
		href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		
</head>
<body style="background-color: #eee;">
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>

  <div class="row">

	<div class="col-md-12 box-items-menu-dashboard" style="padding: 0;margin: 20px;">

		<div class="spanTitleBoxDefault">
			<h4><i class="fas fa-search"></i> Finder</h4>
		</div>

		<div class="row" style="margin: 0;padding: 0;">
      <?php menuItem('Financeiro', 'fa fa-hand-holding-usd', './extratoconta.php', 'box-ionc-2'); ?>

      <?php menuItem('Contas a Pagar', 'fa fa-file-invoice-dollar', './listarcp.php', 'box-ionc-3'); ?>

      <?php menuItem('Contas a Receber','fa fa-piggy-bank','./listarcr.php', 'box-ionc-4'); ?>
      
      <?php menuItem('Faturamento','fa fa-chart-line','./relcrglobal.php', 'box-ionc-5'); ?>
      
      <?php menuItem('Pedidos','fa fa-file-invoice-dollar','./cadastropedido.php', 'box-ionc-6'); ?>
      
      <?php menuItem('Orçamento','fa fa-file-invoice','./cadastroorcamento.php', 'box-ionc-8'); ?>
      
            <?php menuItem('Leads','fa fa-file-invoice','./listarleads.php', 'box-ionc-8'); ?>

      
      <?php menuItem('OS','fa fa-file-signature','./listaros.php', 'box-ionc-7'); ?>
      
      <?php menuItem('Produtos','fa fa-shopping-basket','./listarprodutos.php', 'box-ionc-9'); ?>
      
			<?php menuItem('CRM','fa fa-address-card','./listarcrm.php', 'box-ionc-10'); ?>
		</div>
		
		<div class="spanTitleBoxDefault">
      <h4><i class="fas fa-tachometer-alt"></i> Cadastros Gerais</h4>
      
      <?php menuItem(
				'Cliente',
        'fa fa-user-plus',
        './listarclientes.php',
        'box-ionc-3'
        ) ?>

			<?php menuItem(
				'Fornecedor',
				'fa fa-cube',
				'./listarfornecedores.php',
        'box-ionc-9'
			)
			?>

			<?php menuItem(
				'Parceiro',
				'fa fa-user-plus',
				'./listarparcerias.php', 'box-ionc-10')
			?>

			<?php menuItem(
				'Vendedor',
				'fa fa-hand-holding-usd',
				'./listarvendedores.php', 'box-ionc-2')
			?>

			<?php menuItem(
				'Indicador',
				'fa fa-bullhorn',
				'./listarparceiros.php', 'box-ionc-4')
			?>

			<?php menuItem(
				'Representante',
				'fa fa-user-plus',
				'./listarrepresentantes.php', 'box-ionc-5')
			?>

			<?php menuItem(
				'Técnico - Instalador',
				'fa fa-user-plus',
				'./listarprestadores.php', 'box-ionc-6')
			?>

			<?php menuItem(
				'Aniversariantes',
				'fa fa-user-plus',
				'./aniversariantes.php', 'box-ionc-3')
			?>
		</div>

		<div class="row" style="margin: 0;padding: 0;">
        
		</div>

		<div class="spanTitleBoxDefault">
			  <h4><i class="fas fa-cogs"></i> Avançado</h4>
		</div>

		<div class="row" style="margin: 0;padding: 0;">
      <?php menuItem(
				'Configurações',
				'fa fa-cog',
				'./listarusuarios.php', 'box-ionc-2')
			?>
		</div>

		<div class="spanTitleBoxDefault">
			  <h4 style="color: #27ae60;"><i class="fab fa-android"></i> Baixe o Aplicativo Android</h4>
		</div>

		<div class="row googleplay" style="margin: 0;padding: 0;margin-bottom: 30px;">
      		<a href="./systemJL.apk" title="Baixar o App SystemJL"><img style="width: 150px;height: auto;" src="includes/en_badge_web_generic.png"></a>
		</div>

	</div>

		
		

	</div>

  <footer class="main-footer">
  <div class="pull-right hidden-xs">
	  <b>Versão</b> Pro Beta/ <span style="font-style: italic;font-size: 0.8em;">Recreated by Rota 5 Web</span>    </div>
	<strong>Todos direitos reservados JL Seguro.
  </footer>

  

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="http://v2.systemjl.com.br/layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="http://v2.systemjl.com.br/layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="http://v2.systemjl.com.br/layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="http://v2.systemjl.com.br/layout/dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="http://v2.systemjl.com.br/layout/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="http://v2.systemjl.com.br/layout/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="http://v2.systemjl.com.br/layout/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="http://v2.systemjl.com.br/layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="http://v2.systemjl.com.br/layout/bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="http://v2.systemjl.com.br/layout/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="http://v2.systemjl.com.br/layout/dist/js/demo.js"></script>
</body>
</html>
<?php } ?>