<?php

function formatarCPF_CNPJ($campo, $formatado = true){
  //retira formato
  $codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
  // pega o tamanho da string menos os digitos verificadores
  $tamanho = (strlen($codigoLimpo) -2);
  //verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
  if ($tamanho != 9 && $tamanho != 12){
    return false; 
  }
 

  if ($formatado){ 
    // seleciona a mÃ¡scara para cpf ou cnpj
    $mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
    $indice = -1;
    for ($i=0; $i < strlen($mascara); $i++) {
      if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
    }
    //retorna o campo formatado
    $retorno = $mascara;
 
  }else{
    //se nÃ£o quer formatado, retorna o campo limpo
    $retorno = $codigoLimpo;
  }
 
  return $retorno;
 
}

  include"includes/conexao.php";
   
  session_start();

  $id_pagina = '9';

  if($_SESSION['id'] == NULL) {
  
  echo"<script language=\"JavaScript\">
  location.href=\"index.html\";
  </script>";
  
  } else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
    
  $id = $_GET['id'];
  $tipo = $GET['tipo'];
  
  $sql = mysqli_query($con, "select * from clientes where id_cli = '$id'");
  $vetor = mysqli_fetch_array($sql);

  $sql_pedido_vigencia = mysqli_query($con, "select * from pedidos where id_cliente = '$id' order by id_pedido DESC limit 0,1");
  $vetor_pedido_vegencia = mysqli_fetch_array($sql_pedido_vigencia);

  $sql_referencias1 = mysqli_query($con, "select * from cliente_referencias where id_cliente = '$id' order by id_referencia ASC limit 0,1");
  $vetor_referencia1 = mysqli_fetch_array($sql_referencias1);

  $sql_referencias2 = mysqli_query($con, "select * from cliente_referencias where id_cliente = '$id' order by id_referencia DESC limit 0,1");
  $vetor_referencia2 = mysqli_fetch_array($sql_referencias2);

  $sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
  $res_cadastro = mysqli_query($con, $sql_cadastro);
  $vetor_cadastro = mysqli_fetch_array($res_cadastro);

  $sql_renovacoes = mysqli_query($con, "select * from renovacoes where id_cliente = '$id'");
  $vetor_renovacao = mysqli_fetch_array($sql_renovacoes);

  $sql_ultimo_pedido = mysqli_query($con, "select * from pedidos where id_cliente = '$id' order by id_pedido DESC limit 0,1");
  $vetor_ultimo_pedido = mysqli_fetch_array($sql_ultimo_pedido);

  $sql_os = mysqli_query($con, "select * from os where id_pedido = '$vetor_ultimo_pedido[id_pedido]'");
  $vetor_os = mysqli_fetch_array($sql_os);
  
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>

<?php include "calc_css.php"; ?>

<script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
                $("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...")
                        $("#bairro").val("...")
                        $("#cidade").val("...")
                        $("#uf").val("...")
                        $("#ibge").val("...")

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>



    <?php

    




    ?>




    <script type="text/javascript">
/* MÃ¡scaras ER */
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que nÃ£o Ã© dÃ­gito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parÃªnteses em volta dos dois primeiros dÃ­gitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hÃ­fen entre o quarto e o quinto dÃ­gitos
    return v;
}
function id( el ){
  return document.getElementById( el );
}
window.onload = function(){  
    id('telefone').onkeypress = function(){  
        mascara( this, mtel);  
    }
    id('telefone2').onkeypress = function(){  
        mascara( this, mtel);  
    }
}
</script>

<!--  PESQUISA COM AJAX -->
<script type="text/javascript">
  var req;
   
  // FUNÇÃO PARA BUSCA NOTICIA
  function buscarNoticias(valor) {
   
  // Verificando Browser
  if(window.XMLHttpRequest) {
     req = new XMLHttpRequest();
  }
  else if(window.ActiveXObject) {
     req = new ActiveXObject("Microsoft.XMLHTTP");
  }
   
  // Arquivo PHP juntamente com o valor digitado no campo (método GET)
  var url = "busca.php?valor="+valor;
   
  // Chamada do método open para processar a requisição
  req.open("Get", url, true); 
   
  // Quando o objeto recebe o retorno, chamamos a seguinte função;
  req.onreadystatechange = function() {
   
    // Exibe a mensagem "Buscando Noticias..." enquanto carrega
    if(req.readyState == 1) {
      document.getElementById('resultado').value = 'Buscando Noticias...';
    }
   
    // Verifica se o Ajax realizou todas as operações corretamente
    if(req.readyState == 4 && req.status == 200) {
   
    // Resposta retornada pelo busca.php
    var resposta = req.responseText;
   
    // Abaixo colocamos a(s) resposta(s) na div resultado
    document.getElementById('resultado').value = resposta;
    }
  }
  req.send(null);
  }

</script>

<style type="text/css">
  #panel-button{
    -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.10);
    -moz-box-shadow:    9px 7px 5px rgba(50, 50, 50, 0.10);
    box-shadow:         9px 7px 5px rgba(50, 50, 50, 0.10);
    width: 100%;
    margin-bottom: 10px;
    background-color: #eee;
    padding: 5px;
    border-radius: 10px;
  }
</style>

</head>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include"includes/menu_sistema.php"; ?>
    
    
  <?php include "calculadora.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title text-ionc-3">Cadastro de Cliente <span style="color: red;"><?php echo $vetor['nome']; ?></span></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div style="width: 100%; display: flex; ">

                <div style="width: 60%">
                Pedido: <?php echo $vetor_ultimo_pedido['id_pedido']; ?>
                  </br>
                  Rastreador: <?php echo $vetor_os['nrastreador']; ?>
                  </br>
                  Cliente desde: <?php

                    echo date('d/m/Y', strtotime($vetor['datacad']));

                    $data_cad = $vetor['datacad'];

                    $data_pri_renova = date('d/m/Y', strtotime("+365 days",strtotime($data_cad)));

                    $data_pri_renova2 = date('d/m/Y', strtotime("+730 days",strtotime($data_cad)));

                    $data_vigencia_fim = date('d/m/Y', strtotime("+1095 days",strtotime($data_cad)));


                    $data_pri_renov = date('Y/m/d', strtotime("+365 days",strtotime($data_cad)));

                    $data_pri_renov2 = date('Y/m/d', strtotime("+730 days",strtotime($data_cad)));

                    

                    



                    $sql_update_vigencias = mysqli_query($con, "UPDATE renovacoes SET data1 = '$data_pri_renov', data2 = '$data_pri_renov2' WHERE id_cliente = '$id' ");


                  ?>

            </div>
              <div style="width: 50%">
                <div style="display: flex;">
                  <div style="width: 50%; display: flex;">
                    <div style="width: 40%">
                       Vigência:
                    </div>
                    <div style="width: 30%">
                       <?php echo date('d/m/Y', strtotime($vetor['datacad'])); ?>
                    </div>
                    <div style="width: 30%">
                       <a href="alterarrenovacao.php?id=<?php echo $vetor_renovacao['id_renovacao']; ?>">
                        <button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Alterar Renovação">
                          <i class="fa fa-edit"></i>
                        </button>
                      </a>
                    </div>
                  </div>
                  <div style="display: flex; width: 50%">
                    <div style="width: 40%">
                        <p style="text-align: center;">A</p>
                    </div>

                    <div style="width: 30%">
                      <?php echo $data_pri_renova2; ?>
                    </div>

                    <div style="width: 30%">
                        <a href="alterarrenovacao.php?id=<?php echo $vetor_renovacao['id_renovacao']; ?>">
                          <button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Alterar Renovação">
                            <i class="fa fa-edit"></i>
                          </button>
                        </a>
                    </div>
                  </div>
                </div>
                <div style="display: flex;">
                  <div style="width: 50%; display: flex;">
                    <div style="width: 40%">
                      <font color="#FF0000">1° Renovação:</font>
                    </div>
                    <div style="width: 30%">
                          <?php
                            echo $data_pri_renova;
                          ?>
                    </div>
                    <div style="width: 30%">
                          <a href="alterarrenovacao.php?id=<?php echo $vetor_renovacao['id_renovacao']; ?>">
                            <button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Alterar Renovação">
                              <i class="fa fa-edit"></i>
                            </button>
                          </a>
                    </div>
                  </div>
                  <div style="width: 50%; display: flex;">
                    <div style="width: 40%">
                          <font color="#FF0000">2° Renovação:</font>
                    </div>
                    <div style="width: 30%">
                          <?php
                            echo $data_pri_renova2;
                          ?>
                    </div>
                    <div style="width: 30%">
                          <a href="alterarrenovacao.php?id=<?php echo $vetor_renovacao['id_renovacao']; ?>">
                            <button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Alterar Renovação"><i class="fa fa-edit"></i>
                            </button>
                          </a>
                    </div>
                  </div>
                </div>
              </div>
                
                
              </div>

            
                    
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">

            <li class="active"><a href="#tab_1" data-toggle="tab">Dados Cadastrais</a></li>
            <li><a href="#tab_2" data-toggle="tab">Faturamento</a></li>
            <li><a href="#tab_3" data-toggle="tab">Pedidos</a></li>
            <li><a href="#tab_4" data-toggle="tab">OS</a></li>
            <li><a href="#tab_5" data-toggle="tab">CRM</a></li>
            <li><a href="#tab_6" data-toggle="tab">Produtos</a></li>
            <li><a href="#tab_7" data-toggle="tab">Veículos</a></li>
            <li><a href="autorizacaotransf.php?id=<?php echo $id; ?>" target="_blank">Autoriz. de Transf.</a></li>
            <li><a href="autorizacaoinstal.php?id=<?php echo $id; ?>" target="_blank">Autoriz. de Instal.</a></li>
            <li><a href="comunicadorenovacao.php?id=<?php echo $id; ?>" target="_blank">C. Renovação</a></li>
            <li><a href="termocancelamento.php?id=<?php echo $id; ?>" target="_blank">Termo Canc.</a></li>
            <li><a href="imprimirtermoquitacao.php?id=<?php echo $id; ?>" target="_blank">Termo de Quitação</a></li>
            <li><a href="imprimirnotificacao.php?id=<?php echo $id; ?>" target="_blank">Notificação E.J.</a></li>
            <li><a href="#tab_8" data-toggle="tab">Documentos</a></li>
  
            </ul>
            <div class="tab-content">


           
            <div class="tab-pane active" id="tab_1">

            <form action="recebe_alterarcadastro.php?id=<?php echo $id; ?>" method="post" name="cliente" enctype="multipart/form-data" id="formID">

            <table width="100%">
                <tr>
                  <td width="95%"></td>
                  <td><button type="submit" class="btn btn-ionc-4"  style="    float: left;">Alterar</button></td>
                </tr>
              </table>
              </br>
              </br>
              
        <input type="hidden" name="tipocad" value="1">  
        <div class="row">
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Nome ou Razão Social</label>
              <input type="text" name="nome" class="form-control" id="exampleInput" value="<?php echo $vetor['nome']; ?>" placeholder="Digite o nome">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">CPF (Só numeros)</label>
              <input type="text" name="cpfcnpj" class="form-control" id="exampleInput" value="<?php echo $vetor['cpfcnpj']; ?>" placeholder="Digite apenas os numeros" pattern="[0-9]+$">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Representante:</label>
              <select name="vendedor" id="exampleSelect" class="form-control">
                <option value="" selected="selected">Selecione o Representante</option>
                <?php 
                $sql_representante = mysqli_query($con, "select * from clientes where tipocad IN ('6') order by nome ASC");
                while ($vetor_representante=mysqli_fetch_array($sql_representante)) { ?>
                <option value="<?php echo $vetor_representante['id_cli']; ?>" <?php if (strcasecmp($vetor['vendedor'], $vetor_representante['id_cli']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_representante['nome'] ?></option>
                <?php } ?>
              </select>
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Indicador:</label>
              <select name="indicador" id="exampleSelect" class="form-control">
                <option value="" selected="selected">Selecione o Representante</option>
                <?php 
                $sql_indicador = mysqli_query($con, "select * from clientes where tipocad IN ('5') order by nome ASC");
                while ($vetor_indicador=mysqli_fetch_array($sql_indicador)) { ?>
                <option value="<?php echo $vetor_indicador['id_cli']; ?>" <?php if (strcasecmp($vetor['vendedor'], $vetor_indicador['id_cli']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_indicador['nome'] ?></option>
                <?php } ?>
              </select>
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">RG</label>
              <input type="text" name="rg" class="form-control" value="<?php echo $vetor['rg']; ?>" id="exampleInput" placeholder="Digite o RG">
            </fieldset>
          </div>
          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Data de Nascimento</label>
              <input type="date" id="datanas" onkeyup="formatar_ano(this.id)" name="datanasc" value="<?php echo $vetor['datanasc']; ?>" class="form-control">
            </fieldset>
          </div>
          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="sexo">Sexo</label>
              <select id="sexo" name="sexo" required="required" class="form-control">

                <?php
                  $sql_genero = mysqli_query($con, "SELECT genero FROM clientes WHERE id_cli = '$id' ");
                  $genero = mysqli_fetch_array($sql_genero);
                  $genero = $genero['genero'];
                  if ($genero == NULL OR $genero == 0) {
                  
                ?>

                <option value="0" selected="selected">Escolha o Gênero</option>
                <option value="1">Masculino</option>
                <option value="2">Feminino</option>
                <option value="3">Outros</option>

                <?php }else{

                  if ($genero == 1) {$nome_genero = "Masculino";  $genero_valor = 1;}
                  elseif($genero == 2) {$nome_genero = "Feminino";$genero_valor = 2;}
                  elseif($genero == 3) {$nome_genero = "Outros";  $genero_valor = 3;}
                ?>

                  <option value="<?php echo $genero_valor; ?>" selected="selected"><?php echo $nome_genero;?></option>
                  <?php if($genero != 1){echo "<option value='1'>Masculino</option>";}?>
                  <?php if($genero != 2){echo "<option value='2'>Feminino</option>";}?>
                  <?php if($genero != 3){echo "<option value='3'>Outros</option>";}?>
                  
                <?php } ?>
              </select>
            </fieldset>
          </div>
          <div class="col-lg-2">
            <?php
                $sql_situacao = mysqli_query($con, "SELECT situacao FROM clientes WHERE id_cli = '$id' ");
                $situacao = mysqli_fetch_array($sql_situacao);
                $situacao = $situacao['situacao'];


                if ($situacao == '1') {
                  $color = "green";
                }elseif ($situacao == '2') {
                  $color = "black";
                }elseif ($situacao == '3') {
                  $color = "red";
                } elseif ($situacao == '4') {
                  $color = "firebrick";
                }else{
                  $color = "silver";
                }


            ?>
            <fieldset class="form-group">
              <label class="form-label" for="situacao">Situação Cad. <div style='display: inline-block; width: 12px; height: 12px; border-radius: 50%; background-color: <?php echo $color;?>; border: 1px solid silver'> </div></label>
              <select id="situacao" name="situacao" required="required" class="form-control">

                <?php
                  if ($situacao == NULL OR $situacao == 0) {
                ?>

                <option value="0" selected="selected">Escolha a Situação </option>
                <option value="1">Ativo</option>
                <option value="2">Inativo</option>
                <option value="3">Cancelado</option>
                <option value="4">Furtado</option>
                <option value="5">Roubado</option>
                <option value="6">Não Instalado</option>

                <?php }else{

                  if ($situacao == 1) {   $nome_situacao = "Ativo";          $situacao_valor = 1;}
                  elseif($situacao == 2) {$nome_situacao = "Inativo";        $situacao_valor = 2;}
                  elseif($situacao == 3) {$nome_situacao = "Cancelado";      $situacao_valor = 3;}
                  elseif($situacao == 4) {$nome_situacao = "Furtado";        $situacao_valor = 4;}
                  elseif($situacao == 5) {$nome_situacao = "Roubado";        $situacao_valor = 5;}
                  elseif($situacao == 6) {$nome_situacao = "Não Instalado";  $situacao_valor = 6;}

                ?>

                  <option value="<?php echo $situacao_valor; ?>" selected="selected"><?php echo $nome_situacao;?></option>
                  <?php if($situacao != 1){echo "<option value='1'>Ativo</option>";}?>
                  <?php if($situacao != 2){echo "<option value='2'>Inativo</option>";}?>
                  <?php if($situacao != 3){echo "<option value='3'>Cancelado</option>";}?>
                  <?php if($situacao != 4){echo "<option value='4'>Furtado</option>";}?>
                  <?php if($situacao != 5){echo "<option value='5'>Roubado</option>";}?>
                  <?php if($situacao != 5){echo "<option value='6'>Não Instalado</option>";}?>

                <?php } ?>
              </select>
            </fieldset>
          </div>

          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="sexo">Situação Financeira</label>
              <select id="sexo" name="situacao_finan" required="required" class="form-control">

                <?php
                  $sql_situa_finaceiro = mysqli_query($con, "SELECT situa_financeiro FROM clientes WHERE id_cli = '$id' ");
                  $situa = mysqli_fetch_array($sql_situa_finaceiro);
                  $situa_financeiro = $situa['situa_financeiro'];
                  if ($situa_financeiro == NULL OR $situa_financeiro == 0) {
                  
                ?>

                <option value="0" selected="selected">Escolha a Situação</option>
                <option value="1">Adimplente</option>
                <option value="2">Inadimplente</option>
                <option value="3">Sem Restrição</option>
                <option value="4">SCPC</option>
                <option value="5">SERASA</option>
                <option value="6">Processo Judicial</option>

                <?php }else{

                  if ($situa_financeiro == 1) {   $nome_situa = "Adimplente";     $situ_valor = 1;}
                  elseif($situa_financeiro == 2) {$nome_situa = "Inadimplente";   $situ_valor = 2;}
                  elseif($situa_financeiro == 3) {$nome_situa = "Sem Restrição";  $situ_valor = 3;}
                  elseif($situa_financeiro == 4) {$nome_situa = "SCPC";           $situ_valor = 4;}
                  elseif($situa_financeiro == 5) {$nome_situa = "SERASA";         $situ_valor = 5;}
                  elseif($situa_financeiro == 6) {$nome_situa = "Processo Judicial";  $situ_valor = 6;}
                ?>

                  <option value="<?php echo $situ_valor; ?>" selected="selected"><?php echo $nome_situa;?></option>
                  <?php if($situa_financeiro != 1){echo "<option value='1'>Adimplente</option>";}?>
                  <?php if($situa_financeiro != 2){echo "<option value='2'>Inadimplente</option>";}?>
                  <?php if($situa_financeiro != 3){echo "<option value='3'>Sem Restrição</option>";}?>
                  <?php if($situa_financeiro != 4){echo "<option value='4'>SCPC</option>";}?>
                  <?php if($situa_financeiro != 5){echo "<option value='5'>SERASA</option>";}?>
                  <?php if($situa_financeiro != 6){echo "<option value='6'>Processo Judicial</option>";}?>
                  
                <?php } ?>
              </select>
            </fieldset>
          </div>



        </div><!--.row-->

        <div class="row">

          

          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="sexo">Controle de Sinistro</label>
              <select id="sexo" name="sinistro1" required="required" class="form-control">

                <?php
                  $sql_sinistro = mysqli_query($con, "SELECT sinis1 FROM clientes WHERE id_cli = '$id' ");
                  $vetor_sinistro = mysqli_fetch_array($sql_sinistro);
                  $sinistro = $vetor_sinistro['sinis1'];
                  if ($sinistro == NULL OR $sinistro == 0) {
                  
                ?>

                <option value="0" selected="selected">Escolha uma opção</option>
                <option value="1">Sem Sinistro</option>
                <option value="2">Roubado</option>
                <option value="3">Furtado</option>
                <option value="4">Recuperado</option>
                <option value="5">Não Recuperado</option>
                <option value="6">Idenizado</option>
                <option value="7">Não Idenizado</option>

                <?php }else{

                  if ($sinistro == 1)    {$nome_si = "Sem Sinistro";  $situ_valor = 1;}
                  elseif($sinistro == 2) {$nome_si = "Roubado";       $situ_valor = 2;}
                  elseif($sinistro == 3) {$nome_si = "Furtado";       $situ_valor = 3;}
                  elseif($sinistro == 4) {$nome_si = "Recuperado";    $situ_valor = 4;}
                  elseif($sinistro == 5) {$nome_si = "Não Recuperado";$situ_valor = 5;}
                  elseif($sinistro == 6) {$nome_si = "Idenizado";     $situ_valor = 6;}
                  elseif($sinistro == 7) {$nome_si = "Não Idenizado"; $situ_valor = 7;}
                ?>

                  <option value="<?php echo $situ_valor; ?>" selected="selected"><?php echo $nome_si;?></option>
                  <?php if($sinistro != 1){echo "<option value='1'>Sem Sinistro</option>";}?>
                  <?php if($sinistro != 2){echo "<option value='2'>Roubado</option>";}?>
                  <?php if($sinistro != 3){echo "<option value='3'>Furtado</option>";}?>
                  <?php if($sinistro != 4){echo "<option value='4'>Recuperado</option>";}?>
                  <?php if($sinistro != 5){echo "<option value='5'>Não Recuperado</option>";}?>
                  <?php if($sinistro != 6){echo "<option value='6'>Idenizado</option>";}?>
                  <?php if($sinistro != 7){echo "<option value='7'>Não Idenizado</option>";}?>

                <?php } ?>
              </select>
            </fieldset>
          </div>


          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Data</label>
              <input type="date" id="datanas" onkeyup="formatar_ano(this.id)" name="data_s1" value="<?php echo $vetor['data_sinis1']; ?>" class="form-control">
            </fieldset>
          </div>

          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="sexo">Controle de Sinistro</label>
              <select id="sexo" name="sinistro2" required="required" class="form-control">

                <?php
                  $sql_sinistro = mysqli_query($con, "SELECT sinis2 FROM clientes WHERE id_cli = '$id' ");
                  $vetor_sinistro = mysqli_fetch_array($sql_sinistro);
                  $sinistro = $vetor_sinistro['sinis2'];
                  if ($sinistro == NULL OR $sinistro == 0) {
                  
                ?>

                <option value="0" selected="selected">Escolha uma opção</option>
                <option value="1">Sem Sinistro</option>
                <option value="2">Roubado</option>
                <option value="3">Furtado</option>
                <option value="4">Recuperado</option>
                <option value="5">Não Recuperado</option>
                <option value="6">Idenizado</option>
                <option value="7">Não Idenizado</option>

                <?php }else{

                  if ($sinistro == 1)    {$nome_si = "Sem Sinistro";  $situ_valor = 1;}
                  elseif($sinistro == 2) {$nome_si = "Roubado";       $situ_valor = 2;}
                  elseif($sinistro == 3) {$nome_si = "Furtado";       $situ_valor = 3;}
                  elseif($sinistro == 4) {$nome_si = "Recuperado";    $situ_valor = 4;}
                  elseif($sinistro == 5) {$nome_si = "Não Recuperado";$situ_valor = 5;}
                  elseif($sinistro == 6) {$nome_si = "Idenizado";     $situ_valor = 6;}
                  elseif($sinistro == 7) {$nome_si = "Não Idenizado"; $situ_valor = 7;}
                ?>

                  <option value="<?php echo $situ_valor; ?>" selected="selected"><?php echo $nome_si;?></option>
                  <?php if($sinistro != 1){echo "<option value='1'>Sem Sinistro</option>";}?>
                  <?php if($sinistro != 2){echo "<option value='2'>Roubado</option>";}?>
                  <?php if($sinistro != 3){echo "<option value='3'>Furtado</option>";}?>
                  <?php if($sinistro != 4){echo "<option value='4'>Recuperado</option>";}?>
                  <?php if($sinistro != 5){echo "<option value='5'>Não Recuperado</option>";}?>
                  <?php if($sinistro != 6){echo "<option value='6'>Idenizado</option>";}?>
                  <?php if($sinistro != 7){echo "<option value='7'>Não Idenizado</option>";}?>

                <?php } ?>
              </select>
            </fieldset>
          </div>


          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Data</label>
              <input type="date" id="datanas" onkeyup="formatar_ano(this.id)" name="data_s2" value="<?php echo $vetor['data_sinis2']; ?>" class="form-control">
            </fieldset>
          </div>


          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="sexo">Controle de Sinistro</label>
              <select id="sexo" name="sinistro3" required="required" class="form-control">

                <?php
                  $sql_sinistro = mysqli_query($con, "SELECT sinis3 FROM clientes WHERE id_cli = '$id' ");
                  $vetor_sinistro = mysqli_fetch_array($sql_sinistro);
                  $sinistro = $vetor_sinistro['sinis3'];
                  
                  if ($sinistro == NULL OR $sinistro == 0) {
                  
                ?>

                <option value="0" selected="selected">Escolha uma opção</option>
                <option value="1">Sem Sinistro</option>
                <option value="2">Roubado</option>
                <option value="3">Furtado</option>
                <option value="4">Recuperado</option>
                <option value="5">Não Recuperado</option>
                <option value="6">Idenizado</option>
                <option value="7">Não Idenizado</option>

                <?php }else{

                  if ($sinistro == 1)    {$nome_si = "Sem Sinistro";  $situ_valor = 1;}
                  elseif($sinistro == 2) {$nome_si = "Roubado";       $situ_valor = 2;}
                  elseif($sinistro == 3) {$nome_si = "Furtado";       $situ_valor = 3;}
                  elseif($sinistro == 4) {$nome_si = "Recuperado";    $situ_valor = 4;}
                  elseif($sinistro == 5) {$nome_si = "Não Recuperado";$situ_valor = 5;}
                  elseif($sinistro == 6) {$nome_si = "Idenizado";     $situ_valor = 6;}
                  elseif($sinistro == 7) {$nome_si = "Não Idenizado"; $situ_valor = 7;}
                ?>

                  <option value="<?php echo $situ_valor; ?>" selected="selected"><?php echo $nome_si;?></option>
                  <?php if($sinistro != 1){echo "<option value='1'>Sem Sinistro</option>";}?>
                  <?php if($sinistro != 2){echo "<option value='2'>Roubado</option>";}?>
                  <?php if($sinistro != 3){echo "<option value='3'>Furtado</option>";}?>
                  <?php if($sinistro != 4){echo "<option value='4'>Recuperado</option>";}?>
                  <?php if($sinistro != 5){echo "<option value='5'>Não Recuperado</option>";}?>
                  <?php if($sinistro != 6){echo "<option value='6'>Idenizado</option>";}?>
                  <?php if($sinistro != 7){echo "<option value='7'>Não Idenizado</option>";}?>

                <?php } ?>
              </select>
            </fieldset>
          </div>

          <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Data</label>
              <input type="date" id="datanas" onkeyup="formatar_ano(this.id)" name="data_s3" value="<?php echo $vetor['data_sinis3']; ?>" class="form-control">
            </fieldset>
          </div>


        </div>
                
               
                <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">CEP</label>
              <input type="text" name="cep" value="<?php echo $vetor['cep']; ?>" id="cep" class="form-control" id="exampleInput" placeholder="CEP">
            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Rua</label>
              <input type="text" name="endereco" id="rua" value="<?php echo $vetor['endereco']; ?>" class="form-control" id="exampleInput" placeholder="EndereÃ§o">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Numero</label>
              <input type="text" name="numero" value="<?php echo $vetor['numero']; ?>" class="form-control" id="exampleInput" placeholder="Numero">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Complemento</label>
              <input type="text" name="complemento" class="form-control" value="<?php echo $vetor['complemento']; ?>" id="exampleInput" placeholder="Complemento">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Bairro</label>
              <input type="text" name="bairro" id="bairro" class="form-control" value="<?php echo $vetor['bairro']; ?>" id="exampleInput" placeholder="Bairro">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Cidade</label>
              <input type="text" name="cidade" id="cidade" class="form-control" value="<?php echo $vetor['cidade']; ?>" id="exampleInput" placeholder="Cidade">
            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Estado</label>
              <input type="text" name="estado" id="uf" class="form-control" value="<?php echo $vetor['estado']; ?>" id="exampleInput" placeholder="Estado">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Telefone</label>
              <input type="text" name="telefone" id="telefone" class="form-control" value="<?php echo $vetor['telefone']; ?>" id="exampleInput" placeholder="Telefone">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Telefone</label>
              <input type="text" name="telefone3" id="telefone3" class="form-control" value="<?php echo $vetor['telefone2']; ?>" id="exampleInput" placeholder="Telefone">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Celular</label>
              <input type="text" name="celular" id="telefone2" class="form-control" value="<?php echo $vetor['celular']; ?>" id="exampleInput" placeholder="Celular">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">E-mail</label>
              <input type="text" name="email" class="form-control" value="<?php echo $vetor['email']; ?>" id="exampleInput" placeholder="Digite seu E-mail">
            </fieldset>
          </div>
        </div><!--.row-->

        <h3>REFERÊNCIAS PESSOAIS</h3>

        <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Nome</label>
              <input type="text" name="nomep" value="<?php echo $vetor_referencia1['nome']; ?>" class="form-control" placeholder="Nome" style="text-transform:uppercase;">
            </fieldset>
          </div>
          <input type="hidden" name="id_referencia1" value="<?php echo $vetor_referencia1['id_referencia']; ?>">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Parentesco</label>
              <input type="text" name="parentesco" value="<?php echo $vetor_referencia1['parentesco']; ?>" class="form-control" placeholder="Parentesco" style="text-transform:uppercase;">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Telefone</label>
              <input type="text" name="telefonep" value="<?php echo $vetor_referencia1['telefone']; ?>" id="telefone4" class="form-control" placeholder="Telefone">
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Nome</label>
              <input type="text" name="nomep1" value="<?php echo $vetor_referencia2['nome']; ?>" class="form-control" placeholder="Nome" style="text-transform:uppercase;">
            </fieldset>
          </div>
          <input type="hidden" name="id_referencia2" value="<?php echo $vetor_referencia2['id_referencia']; ?>">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Parentesco</label>
              <input type="text" name="parentesco1" value="<?php echo $vetor_referencia2['parentesco']; ?>" class="form-control" placeholder="Parentesco" style="text-transform:uppercase;">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Telefone</label>
              <input type="text" name="telefonep1" value="<?php echo $vetor_referencia2['telefone']; ?>" id="telefone5" class="form-control" placeholder="Telefone">
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Empresa que Trabalha</label>
              <input type="text" name="empresa" value="<?php echo $vetor['empresa']; ?>" class="form-control" placeholder="Empresa que Trabalha">
            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Telefone Empresa</label>
              <input type="text" name="telempresa" value="<?php echo $vetor['telempresa']; ?>" id="telefone3" class="form-control" placeholder="Telefone Empresa">
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">
           <div class="col-lg-1">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Cod. B</label>
              <input type="text" id="busca" onkeyup="buscarNoticias(this.value)" name="banco" value="" class="form-control" placeholder="Cod">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Banco</label>
              <input type="text" id="resultado" name="banco" value="<?php echo $vetor['banco']; ?>" class="form-control" placeholder="Banco">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Agencia</label>
              <input type="text" name="agencia" value="<?php echo $vetor['agencia']; ?>" class="form-control" placeholder="Agencia">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Conta</label>
              <input type="text" name="conta" value="<?php echo $vetor['conta']; ?>" class="form-control" id="exampleInput" placeholder="Conta">
            </fieldset>
          </div>
           <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Tipo da Conta</label>
              <select name="tipoconta" class="form-control">
                <option value=" <?php echo $vetor['tipoconta']; ?> " selected="">
                  <?php if ($vetor['tipoconta'] == 1) {
                    echo "Corrente";
                  }elseif ($vetor['tipoconta'] == 2) {
                    echo "Poupança";
                  }
                ?>
                </option>
                <option value="1" <?php if (strcasecmp($vetor['tipoconta'], '1') == 0) : ?>selected="selected"<?php endif; ?>>Corrente</option>
                <option value="2" <?php if (strcasecmp($vetor['tipoconta'], '2') == 0) : ?>selected="selected"<?php endif; ?>>Poupança</option>
              </select>
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="form-group row">
                  <div class="col-sm-12">
            <label for="exampleSelect" class="col-sm-1 form-control-label">Observações</label>
            
              <textarea rows="10" id="exampleSelect" name="anotacoes" class="form-control" placeholder="Digite suas Observações"><?php echo $vetor['anotacoes']; ?></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="exampleSelect" class="col-sm-2 form-control-label">Data Cadastro</label>
            <div class="col-sm-12">
              <input type="date" onkeyup="formatar_ano(this.id)" id="dacad" name="datacad" value="<?php echo $vetor['datacad']; ?>" class="form-control" required>
            </div>
        </div>


                
                <button type="submit" class="btn btn-ionc-4"  style="    float: left;">Alterar</button>
      </form>

      </br>
      </br>

      <h3>Conta Corrente</h3>

      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">

            <li class="active"><a href="#tab_1_conta" data-toggle="tab">Contas a Pagar</a></li>
            <li><a href="#tab_2_conta" data-toggle="tab">Contas a Receber</a></li>
            <li><a href="#tab_3_recibo" data-toggle="tab">Recibo</a></li>

            </ul>
            <div class="tab-content">
           
            <div class="tab-pane active" id="tab_1_conta">
            <table width="100%" id="table-sm" class="table table-bordered table-hover table-sm">
                      <tr>
                        <td width="15%" bgcolor="e8e8e8">Descri&ccedil;&atilde;o</td>
                        <td bgcolor="e8e8e8">Data Entrada</td>
                        <td bgcolor="e8e8e8">Vencimento</td>
                        <td bgcolor="e8e8e8">Valor Docto</td>
                        <td bgcolor="e8e8e8">Desc / Abat.</td>
                        <td bgcolor="e8e8e8">Juros e Acrescimos</td>
                        <td bgcolor="e8e8e8">Valor Pago</td>
                        <td bgcolor="e8e8e8">Data Pagamento</td>
                        <td bgcolor="e8e8e8">Forma de Pagamento</td>
                        <td bgcolor="e8e8e8">Status</td>
                      </tr>
                      <?php 

                      $sql_cp = mysqli_query($con, "select * from cp01 where id_cli = '$id'");

                      while ($vetor1=mysqli_fetch_array($sql_cp)) {

                      $sql_itens = mysqli_query($con, "select * from cp02 where id_cp01 = '$vetor1[id_cp01]' order by datavencimento ASC");

                      while($vetor_itens = mysqli_fetch_array($sql_itens)) { 

                      ?>
                      <tr>
                        <td><?php echo $vetor1['descricao']; ?></td>
                        <td><span class="style17"><?php echo date('d/m/Y', strtotime($vetor1['datalancamento'])); ?></span></td>
                        <td><span class="style17"><?php 

                        if (!empty($vetor_itens['datavencimento'])) {
                          echo date('d/m/Y', strtotime($vetor_itens['datavencimento'])); 
                        }else{
                          echo 'NÃO INFORMADO!';
                        }

                        ?></span></td>
                        <td><span class="style17"><?php echo $num2 = number_format($vetor_itens['valor'],2,',','.');  ?></span></td>
                        <td><span class="style17"><?php echo $num2 = number_format($vetor_itens['desconto'],2,',','.'); ?></span></td>
                        <td><span class="style17"><?php echo $num2 = number_format($vetor_itens['multa'],2,',','.'); ?></span></td>
                        <td><span class="style17"><?php echo $num_pago = number_format($vetor_itens['vlpago'],2,',','.');  ?></span></td>
                        
                        <td><span class="style17">
                          <?php if($vetor_itens['datapagamento'] == NULL) { 
                              echo 'NÃO INFORMADO.';
                          } else { 
                            if (!empty($vetor['datapagamento'])) {
                                echo date('d/m/Y', strtotime($vetor['datapagamento'])); 
                            }else{
                                echo 'NÃO INFORMADO.';
                            }
                                
                          } ?></span></td>

                        <td><?php echo $vetor_itens['formapag']; ?></td>

                        <td><?php if($vetor_itens['pago'] == 1) { echo "Pendente"; } else { echo "Pago"; } ?></td>
                      </tr>
                      <?php } } ?>
                </table>
            </div>

            <div class="tab-pane" id="tab_2_conta">
            <table width="100%" id="table-sm" class="table table-bordered table-hover table-sm">
                      <tr>
                        <td width="15%" bgcolor="e8e8e8">Descri&ccedil;&atilde;o</td>
                        <td bgcolor="e8e8e8">Data Entrada</td>
                        <td bgcolor="e8e8e8">Vencimento</td>
                        <td bgcolor="e8e8e8">Valor Docto</td>
                        <td bgcolor="e8e8e8">Desc / Abat.</td>
                        <td bgcolor="e8e8e8">Juros e Acrescimos</td>
                        <td bgcolor="e8e8e8">Valor Pago</td>
                        <td bgcolor="e8e8e8">Data Pagamento</td>
                        <td bgcolor="e8e8e8">Forma de Pagamento</td>
                        <td bgcolor="e8e8e8">Status</td>
                      </tr>
                      <?php 

                      $sql_cp = mysqli_query($con, "select * from cr where id_cliente = '$id' order by datavencimento ASC");

                      while ($vetor=mysqli_fetch_array($sql_cp)) {


                      ?>
                      <tr>
                        <td><?php echo $vetor['descricao']; ?></td>
                        <td><span class="style17"><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></span></td>
                        <td><span class="style17"><?php 
                                if (!empty($vetor['datavencimento'])) {
                                  # code...
                                  echo date('d/m/Y', strtotime($vetor['datavencimento'])); 
                                }else{
                                  echo 'NÃO INFORMADO!';
                                }
                        ?></span></td>
                        <td><span class="style17"><?php echo $num2 = number_format($vetor['valor'],2,',','.');  ?></span></td>
                        <td><span class="style17"><?php echo $num2 = number_format($vetor['desconto'],2,',','.'); ?></span></td>
                        <td><span class="style17"><?php echo $num2 = number_format($vetor['multa'],2,',','.'); ?></span></td>
                        <td><span class="style17"><?php echo $num_pago = number_format($vetor['valorpago'],2,',','.');  ?></span></td>
                        <td><span class="style17"><?php if($vetor['datapagamento'] == NULL) { } else { echo date('d/m/Y', strtotime($vetor['datapagamento'])); } ?></span></td>
                        <td><?php echo $vetor['formapag']; ?></td>
                        <td><?php if($vetor['status'] == 1) { echo "Pendente"; } else { echo "Pago"; } ?></td>
                      </tr>
                      <?php } ?>
                </table>
            </div>

            <div class="tab-pane" id="tab_3_recibo">
            <table width="100%" id="table-sm" class="table table-bordered table-hover table-sm">
                      <tr>
                        <td bgcolor="e8e8e8">Nome: </td>
                        <td bgcolor="e8e8e8">Recebi de</td>
                        <td bgcolor="e8e8e8">Valor</td>
                        <td bgcolor="e8e8e8">Referente</td>
                        <td bgcolor="e8e8e8" colspan="2">AÇÃO</td>
                      </tr>
                      <?php
                          $sql = $conn->prepare("SELECT id_cli, nome, cpfcnpj, rg, recebide, valorrec, porextenso, referenterec FROM clientes WHERE id_cli = ?");
                          $sql->execute([$id]);
                          $fetch = $sql->fetchAll();
                      ?>
                      <?php foreach ($fetch as $value): ?>
                        <tr>
                            <td><span class="style17"><?php echo hasContent($value['nome']); ?></span></td>
                            <td><span class="style17"><?php echo hasContent($value['recebide']); ?></span></td>
                            <td><span class="style17"><?php 

                                if (isset($value['valorrec']) and !empty($value['valorrec'])) {
                                    echo number_format(hasContent($value['valorrec']), 2);
                                }else{
                                    echo 'NÃO INFORMADO!';
                                }

                             ?></span></td>
                            <td><span class="style17"><?php echo hasContent($value['referenterec']); ?></span></td>
                            <td><a title="Imprimir" href="pdfRecibo.php?idenrec=<?php echo hasContent($value['id_cli']) ?>"><i class="fa fa-print"></i> Imprimir </a></td>

                            <td><a title="Alterar" href="alterarrecibo.php?id=<?php echo hasContent($value['id_cli']) ?>"><i class="fa fa-edit"></i> Atualizar</a></td>
                        </tr>
                      <?php endforeach ?>
                </table>
            </div>


            </div>
            </div>

        </div>

        <div class="tab-pane" id="tab_2">
          <form name="excluir_todos" method="post" action="excluirSel.php"/>
          
          <?php

            function format_valor($num){
              $valor = number_format($num,2,',','.');
        
              return $valor;
            }

            $sql_quantidade = mysqli_query($con, "SELECT id_pcld FROM pcld WHERE id_cliente = '$id' ");
            $sql_pcld = mysqli_query($con, "SELECT SUM(valor) FROM pcld WHERE id_cliente = '$id' ");
            $vetor_valor = mysqli_fetch_array($sql_pcld);
            $valor_tot = $vetor_valor['SUM(valor)'];
            $valor_tot = format_valor($valor_tot);

            $quantidade = mysqli_num_rows($sql_quantidade);

            if ($quantidade > 0){
          ?>
            <div class="alert alert-danger" role="alert">
              <p style="margin-left: 6px">Este cliente tem <?php echo $quantidade; ?> mensalidade(s) em atraso! Com valor total de: <?php echo $valor_tot; ?></p>

              <input type="hidden" name="id" value="<?php echo $id;?>">

              <input style="float: right; position: relative; top: -25px; margin-left: 3px" type="submit" class="btn btn-danger mesmo-tamanho" value="Excluir" name="btn_sel">

              <input style="float: right; position: relative; top: -25px; margin-left: 3px" type="submit" class="btn btn-ionc-4 mesmo-tamanho" value="P.C.L.D" name="btn_sel">

              <button onclick="ativar_inputs()" type="button" class="glyphicon glyphicon-check btn btn-warning mesmo-tamanho" title="Selecionar tudo" style="float: right; position: relative; top: -25px;"></button>

              <a id="url33" href="#" target="_blank" onclick="url_dinamica();"><button type="button" class="glyphicon glyphicon-print btn btn-warning" title="PDF/Download Selecionados" style="float: right; position: relative; top: -25px; right: 3px;"></button></a>



              <a target="_blank" href="imprimir_faturas.php?id=<?php echo $id; ?>"><button type="button" class="glyphicon glyphicon-print btn btn-sucess" title="PDF/Download Todos" style="float: left; position: relative; right: 3px; top: -25px;"></button></a>

            </div>

          <?php }else{ ?>

          <div style="width: 100%; height: 40px;">
            <input style="float: right; position: relative; top: 3px; right: 6px;" type="submit" class="btn btn-danger mesmo-tamanho" value="Excluir" name="btn_sel">

            <input type="hidden" name="id" value="<?php echo $id;?>">

            <input style="float: right; position: relative; top: 3px; right: 9px;" type="submit" class="btn btn-ionc-4 mesmo-tamanho" value="P.C.L.D" name="btn_sel">

            <button onclick="ativar_inputs()" type="button" class="glyphicon glyphicon-check btn btn-warning" title="Selecionar tudo" style="float: right; position: relative; top: 3px; right: 12px"></button>

            <a id="url22" target="_blank" onclick="url_dinamica();"><button type="button" class="glyphicon glyphicon-print btn btn-warning" title="PDF/Download Selecionados" style="float: right; position: relative; top: 3px; right: 16px;"></button></a>



            <a target="_blank" href="imprimir_faturas.php?id=<?php echo $id; ?>"><button type="button" class="glyphicon glyphicon-print btn btn-sucess" title="PDF/Download Todos" style="float: left; position: relative; top: 3px;"></button></a>

          </div>

        <?php } ?>

          <table id="example1" data-page-length='50' class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="10%">Código</th>
                    <th>Descrição</th>
                    <th>Vencimento</th>
                    <th>Valor Conta</th>
                    <th>Data Pagamento</th>
                    <th>Valor Recebido</th>
                    <th>Status</th>
                    <th>Ação</th>
                    <th></th>
                  </tr>
                </thead>

                <tbody>
                <?php 
        
        
          $mes_atual = date('m');
          $sql_atual = mysqli_query($con, "select * from cr where id_cliente = '$id' order by datavencimento ASC");

          $sql_total_a_pagar = mysqli_query($con, "SELECT SUM(valor) FROM cr where id_cliente = '$id' AND status = 1");
          $vetor_a_pagar = mysqli_fetch_array($sql_total_a_pagar);
          $total_a_pagar = $vetor_a_pagar['SUM(valor)'];

          $sql_total_pago = mysqli_query($con, "SELECT SUM(valorpago) FROM cr where id_cliente = '$id' AND status = 2");
          $vetor_pago = mysqli_fetch_array($sql_total_pago);
          $total_pago = $vetor_pago['SUM(valorpago)'];

          
        
        while ($vetor=mysqli_fetch_array($sql_atual)) {
        
        $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
        $vetor_cliente = mysqli_fetch_array($sql_cliente);
        
         ?>
                <tr>
                  <td><?php echo $vetor['id_cr']; ?></td>
                  <td><?php echo $vetor['descricao']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['datavencimento'])); ?></td>
                  <td><?php $num = $vetor['valor'];
  $num = number_format($num,2,',','.');
  echo $num;
   ?></td>
                  <td><?php $date =  date('d/m/Y', strtotime($vetor['datapagamento']));  if($date == "01/01/1970" OR $date == "31/12/1969"){echo " ";}else{echo $date;}  ?></td>
                   <td>
                    <?php
                      $num1 = $vetor['valorpago'];
                      $num1 = number_format($num1,2,',','.');
                      echo $num1;
                     ?>
                    </td>

                    <td>

                      <?php
                        if($vetor['status'] == 1) { echo "Pendente"; } if($vetor['status'] == 2) { echo "Pago"; } if($vetor['status'] == 3) { echo "Cancelado"; } ?>  
                    </td>


                  <td>
                    <a href="recebercontasareceber.php?id=<?php echo $vetor['id_cr']; ?>&tipo=2">
                      <button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Visualizar / Alterar Conta">
                        <i class="fa fa-edit"></i>
                      </button>

                      <a href='alterarcr.php?id=<?php echo $vetor['id_cr']; ?>&id_cli=<?php echo $id;?>'>
                        <button type='button' class='btn btn-success mesmo-tamanho' title='Alterar Cadastro'>
                          <i class='fa fa-edit'></i>
                        </button>
                      </a>

                  </td>



                  <?php

                    $codigo = $vetor['id_cr'];

                    echo"
                      <td> 
                        <input style='width: 25px; height: 25px; cursor: pointer;' type='checkbox' style='width: 30px; height: 30px;' value='$codigo' name='sel[]' onclick='guardar_id(this.value)'>
                      </td>
                    ";
                  ?>


                </tr>
                <?php } ?>

              

               
                <tr>
                    <td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='6'> VALOR PAGO</td>
                    <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='3'>
                    R$ <?php echo format_valor($total_pago);?></td>
                </tr>

                
                <tr>
                    <td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='6'> VALOR A RECEBER</td>
                    <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='3'>
                    R$ <?php echo format_valor($total_a_pagar);?></td>
                </tr>

                

                
                </tbody>
                
              </table>

            </form>

        </div>

        <div class="tab-pane" id="tab_3">

          <table id="example2" data-page-length='50' class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">Código</th>
                  <th>Data Pedido</th>
                  <th>Valor Total</th>
                  <th>Status</th>
                  <th width="17%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php 
        
          $sql_atual = mysqli_query($con, "select * from pedidos where id_cliente = '$id' order by id_pedido DESC");
        
          while ($vetor=mysqli_fetch_array($sql_atual)) {

          $mensalidade = $vetor['mensalidade'] * 12;

          $total = $vetor['adesao'] + $mensalidade + $vetor['vacina'] + $vetor['instalacao'] + $vetor['assistencia'] + $vetor['assistenciaterceiro'];

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);
        
         ?>
                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($total,2,',','.'); ?></td>
                  <td><?php if($vetor['status'] == 1) { echo "Realizado"; } if($vetor['status'] == 2) { echo "Cancelado"; } ?></a></td>
                  <td><a href="alterarpedido.php?id=<?php echo $vetor['id_pedido']; ?>"><button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Alterar Cadastro"><i class="fa fa-edit"></i></button></a> <a href="telaimpressoes.php?id=<?php echo $vetor['id_pedido']; ?>"><button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Alterar Cadastro"><i class="fa fa-eye"></i></button></a> <?php if($vetor['status'] == 1) { ?> <a href="confexcluirpedido.php?id=<?php echo $vetor['id_pedido']; ?>" ><button type="button" class="btn btn-danger mesmo-tamanho" title="Excluir Cadastro"><i class="fa fa-close"></i></button></a> <a href="confcancpedido.php?id=<?php echo $vetor['id_pedido']; ?>" ><button type="button" class="btn btn-warning mesmo-tamanho" title="Cancelar Pedido"><i class="fa fa-times-circle-o"></i></button></a><?php } ?></td> 
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th width="10%">Código</th>
                  <th>Data Pedido</th>
                  <th>Valor Total</th>
                  <th>Status</th>
                  <th width="17%">Ação</th>
                </tr>
                </tfoot>
              </table>

        </div>
        
        <div class="tab-pane" id="tab_4">

          <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">Código</th>
                  <th>Pedido</th>
                  <th>Data Pedido / OS</th>
                  <th width="15%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php 
        


          $sql_atual = mysqli_query($con, "select * from os a, pedidos b where a.id_pedido = b.id_pedido AND b.id_cliente = '$id' order by id_os DESC");

                  
          while ($vetor_os=mysqli_fetch_array($sql_atual)) {

          $sql_os = mysqli_query($con, "select * from os where id_os = '$vetor_os[id_os]'");
          $vetor=mysqli_fetch_array($sql_os);

          $sql_pedido = mysqli_query($con, "select * from pedidos where id_pedido = '$vetor[id_pedido]'");
          $vetor_pedido = mysqli_fetch_array($sql_pedido);

          if($vetor['tipocad'] == 1) { 

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);

          } else { 

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);

          }
        
         ?>
                <tr>
                  <td><?php echo $vetor['id_os']; ?></td>
                  <td><?php echo $vetor['id_pedido']; ?></td> 
                  <td><?php if($vetor['tipocad'] == 1) { echo date('d/m/Y', strtotime($vetor_pedido['data'])); } if($vetor['tipocad'] == 2) { echo date('d/m/Y', strtotime($vetor['data'])); } ?></td>
                  <td><a href="alteraros.php?id=<?php echo $vetor['id_os']; ?>"><button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Alterar Cadastro"><i class="fa fa-edit"></i></button></a> <a href="enviarfotos.php?id=<?php echo $vetor['id_os']; ?>" ><button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Enviar Cadastro"><i class="fa fa-file-photo-o"></i></button></a> <a href="imprimiros.php?id_pedido=<?php echo $vetor['id_os']; ?>&tipo=<?php echo $vetor['tipocad']; ?>" ><button type="button" class="btn btn-warning mesmo-tamanho" title="Imprimir Cadastro"><i class="fa fa-print"></i></button></a></td> 
                </tr>
                <?php  } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th width="10%">Código</th>
                  <th>Pedido</th>
                  <th>Data Pedido / OS</th>
                  <th width="15%">Ação</th>
                </tr>
                </tfoot>
              </table>

        </div>

        <div class="tab-pane" id="tab_5">

          <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">Código</th>
                  <th>Data</th>
                  <th>Data Retorno</th>
                  <th width="13%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php 
        
        
          
          $sql_atual = mysqli_query($con, "select * from crm where id_cliente = '$id' order by id_crm DESC");
          
        
          while ($vetor=mysqli_fetch_array($sql_atual)) {

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);
        
         ?>
                <tr>
                
                  <td><?php echo $vetor['id_crm']; ?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php if($vetor['dataretorno'] != NULL) { echo date('d/m/Y', strtotime($vetor['dataretorno'])); } ?></td>
                  <td><a href="alterarcrm.php?id=<?php echo $vetor['id_crm']; ?>"><button type="button" class="btn btn-ionc-4 mesmo-tamanho" title="Visualizar / Alterar CRM"><i class="fa fa-edit"></i></button></a> <a href="confexcluircrm.php?id=<?php echo $vetor['id_crm']; ?>" ><button type="button" class="btn btn-danger mesmo-tamanho" title="Excluir Cadastro"><i class="fa fa-close"></i></button></a></td> 
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th width="10%">Código</th>
                  <th>Data</th>
                  <th>Data Retorno</th>
                  <th width="13%">Ação</th>
                </tr>
                </tfoot>
              </table>

        </div>

        <div class="tab-pane" id="tab_6">

        <?php

        $sql_produtos = mysqli_query($con, "select * from pedidos where id_cliente = '$id' order by id_pedido DESC limit 0,1");
        $vetor_produto = mysqli_fetch_array($sql_produtos);

        if(mysqli_num_rows($sql_produtos) == 0) { 

        ?>

        <div class="row">
              <div class="col-lg-12">
                <div class="alert alert-danger">
                 Cliente sem pedido.
                </div>
              </div>
              </div>

        <?php


        } else {

        ?>

        <table width="100%" id="table-sm" class="table table-bordered table-hover table-sm">
          <?php if($vetor_produto['assistencia'] != '0.00') { ?>
          <tr>
            <td><span class="style12">Assistência 24 horas </span></td>
          </tr>
          <?php } if($vetor_produto['assistenciaterceiro'] != '0.00') { ?>
          <tr>
            <td><span class="style12">Assistência terceiro </span></td>
          </tr>
          <?php } if($vetor_produto['instalacao'] != '0.00') { ?>
          <tr>
            <td><span class="style12">Instalação </span></td>
          </tr>
          <?php } if($vetor_produto['carroreserva'] != '0.00') { ?>
          <tr>
            <td><span class="style12">Carro Reserva </span></td>
          </tr>
          <?php } if($vetor_produto['carroapp'] != '0.00') { ?>
          <tr>
            <td><span class="style12">Seguro APP </span></td>
          </tr>
          <?php } ?>
          

        </table>

        <?php } 
        
        
        ?>

        </div>

        <div class="tab-pane" id="tab_8">

        

              
             <div id="origem">

              
                <div class="row">
              
             
              
             </div> 
             
            </div>
            <div id="destino">
            </div>
             </br>
             <h2 style="background-color:#2c3e50; text-align:center; color:white; font-family:Arial, Helvetica, sans-serif;">Cadastro de Documentos</h2>
          
            <br>
            <div>
              <form class="row" name="jdForm_Upload_Arquivo" action="upload.php" method="post" enctype="multipart/form-data">
                 

                  <div class="col-lg-4">
                    <label>Data :</label>
                    <input type="date" id="datavenc" onkeyup="formatar_ano(this.id)" required name="data_vencimento">

                  </div>
                                      
                  <div class="col-lg-4">
                    <input type="file" name="Arquivo" />
                  </div>
                  <input name="id_cliente" type="hidden" value="<?php echo $id ?>"> 
                  <div class="col-lg-4">
                  <input type="submit" value="Cadastrar Documento"/>
                  
                  </div>
 
                </form>

            </div>
              
              <br> 

            <form action="excluirSel_arquivo.php" method="POST">

             

              <input type="hidden" name="id_cliente" value='<?php echo $id; ?>'>

              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th >Documentos</th>
                      <th>Data</th>
                      <th>Ação</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php

                    function data($data){
                      return date("d/m/Y", strtotime($data));
                    }

                    $consulta = mysqli_query($con, "SELECT * FROM arquivos where id_cliente = '$id' ");

                     while($resultado = mysqli_fetch_array($consulta)){

                      
                              echo "<tr>";
                              echo "<td><a target='_blank' href=\"" . $resultado["arquivo_local"] . $resultado["arquivo_nome"] . "\">" . $resultado["arquivo_nome"] . "</a></td>";

                              $sql_data = mysqli_query($con, "SELECT data_arquivo FROM datas_arquivos WHERE id_arquivo = $resultado[arquivo_id]");
                              $feat = mysqli_fetch_array($sql_data);
                              $feat = $feat['data_arquivo'];

                              $feat = data($feat);

                              echo "<td> $feat</td>";

                              echo "<td> <input style='width: 25px; height: 25px; cursor: pointer;' type='checkbox' style='width: 30px; height: 30px;' value='$resultado[arquivo_id]' name='sel[]'> </td>";
                              echo "</tr>";
                            
                    }

                    ?>
                 
                  </tbody>

                 
                </table>

                 <div class="row">

                <div class="col-lg-4">
                  <input type="submit" class="btn btn-danger" value="Excluir Doc.">
                </div>
                
              </div>
              </form>


          
      </div>
       <!-- <div class="tab_pane" id="tab_8">

        SSS
</div>-->
        <div class="tab-pane" id="tab_7">

          <div id="panel-button">
            <a href="add_carro.php?id=<?php echo $id;?>"><button title="Adicionar Carro à Cliente" type="button" class="btn glyphicon glyphicon-plus"></button></a>
          </div>

          <table id="example5" data-page-length='50' class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Contr.</th>
                  <th>Data</th>
                  <th>Rastreador</th>

                  <th>Chip Num.</th>
                  <th>Operadora</th>

                  <th>Marca</th>
                  <th>Cor</th>
                  <th>Modelo</th>
                  <th>Ano Modelo</th>
                  <th>Combustivel</th>
                  <th>Placa</th>
                  <th>Renavan</th>
                  <th>Chassi</th>
                  <th>Valor</th>
                  <th width="8%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php 
        
          $sql_veiculo = mysqli_query($con, "select * from cliente_veiculo where id_cliente = '$id'");
        
          while ($vetor_veiculo=mysqli_fetch_array($sql_veiculo)) {

          $sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$vetor_veiculo[marca]'");
          $vetor_marca = mysqli_fetch_array($sql_marca);

          $sql_ano = mysqli_query($con, "select * from fp_ano where id_ano = '$vetor_veiculo[anomod]'");
          $vetor_ano = mysqli_fetch_array($sql_ano);

          $sql_cliente_veiculo = mysqli_query($con, "select * from clientes where id_cli = '$id'");
          $vetor_cliente_veiculo = mysqli_fetch_array($sql_cliente_veiculo);

          $id_veiculo = $vetor_veiculo['id_veiculo'];

          if ($vetor_veiculo['id_pedido'] == null) {
            
            $sql_pedido = mysqli_query($con, "select * from pedidos where id_veiculo = '$id_veiculo'");
            $vetor_pedido = mysqli_fetch_array($sql_pedido);
          
            $id_pedido = $vetor_pedido['id_pedido'];
          
          }else{

            $id_pedido = $vetor_veiculo['id_pedido'];
          }

          

            $sql_os = mysqli_query($con, "select * from os where id_pedido = '$id_pedido'");
            $vetor_os = mysqli_fetch_array($sql_os);

            $datasolicitacao  = $vetor_os['datasolicitacao'];
            $nrastreador      = $vetor_os['nrastreador'];

            $sql_vinculos  = mysqli_query($con, "SELECT * FROM vinculo_rastreador WHERE id_pedido = '$id_pedido'");
            $vetor_vinculo = mysqli_fetch_array($sql_vinculos);

            $id_chip = $vetor_vinculo['id_chip'];

            $sql_chip = mysqli_query($con, "SELECT * FROM chips wHERE id_chip = '$id_chip' ");
            $vetor_chip = mysqli_fetch_array($sql_chip);

            $linha = $vetor_chip['linha'];
            $operadora = $vetor_chip['operadora'];
          

         ?>
                <tr>
                  <td><?php echo $id_pedido ?></td>
                  <td><?php if($id_pedido != NULL AND $id_pedido != 0){ echo date('d/m/Y', strtotime($datasolicitacao)); } ?></td>
                  <td><?php if($id_pedido != NULL AND $id_pedido != 0){ echo $nrastreador; } ?></td>

                  <td><?php if($id_pedido != NULL AND $id_pedido != 0){ echo $linha; } ?></td>
                  <td><?php if($id_pedido != NULL AND $id_pedido != 0){ echo $operadora; } ?></td>

                  <td><?php echo $vetor_marca['marca']; ?></td>
                  <td><?php echo $vetor_veiculo['cor']; ?></td>
                  <td><?php echo $vetor_veiculo['modelo']; ?></td>
                  <td><?php echo $vetor_ano['ano']; ?></td> 
                  <td><?php echo $vetor_veiculo['combustivel']; ?></td> 
                  <td><?php echo $vetor_veiculo['placa']; ?></td> 
                  <td><?php echo $vetor_veiculo['renavan']; ?></td>  
                  <td><?php echo $vetor_veiculo['chassi']; ?></td> 
                  <td><?php echo $num1 = number_format($vetor_veiculo['valor'],2,',','.'); ?></td>
                  <td>
                    <a href="alterar_veiculo.php?id_veiculo=<?php echo $id_veiculo;?>">
                      <button type="button" class="btn btn-primary">
                        <i class="glyphicon glyphicon-pencil"></i>
                      </button>
                    </a>

                    <a href="escolher_manutencao.php?id_pedido=<?php echo $id_pedido;?>&id_veiculo=<?php echo $id_veiculo;?>&datasolicitacao=<?php echo $datasolicitacao;?>&nrastreador=<?php echo $nrastreador;?>&linha=<?php echo $linha;?>&operadora=<?php echo $operadora;?>&marca=<?php echo $vetor_marca['marca']; ?>&cor=<?php echo $vetor_veiculo['cor']; ?>&modelo=<?php echo $vetor_veiculo['modelo']; ?>&ano=<?php echo $vetor_ano['ano']; ?>&combustivel=<?php echo $vetor_veiculo['combustivel']; ?>&placa=<?php echo $vetor_veiculo['placa']; ?>&renavan=<?php echo $vetor_veiculo['renavan']; ?>&chassi=<?php echo $vetor_veiculo['chassi']; ?>&valor=<?php echo $vetor_veiculo['valor']; ?>&id_cliente=<?php echo $id; ?>">
                      <button type="button" class="btn btn-primary" title="Manutenção do Pedido <?php echo $id_pedido;?>">
                        <i class="glyphicon glyphicon-cog"></i>
                      </button>
                    </a>

                  </td>



                </tr>
                <?php } ?>
                </tbody>
                
              </table>


              <br>
              <br>

              <div style="padding: 15px; background-color: rgb(241, 222, 278); border-radius: 10px;">
                <div style="text-align: center;">
                  <h5 class="box-title">Hístórico de Manutenções</h5>
                </div>
              </div>

              <table id="example6" data-page-length='50' class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Data Man.</th>
                  <th>Contr.</th>
                  <th>Data</th>
                  <th>Rastreador</th>
                  <th>Chip Num.</th>
                  <th>Operadora</th>
                  <th>Marca</th>
                  <th>Cor</th>
                  <th>Modelo</th>
                  <th>Ano Modelo</th>
                  <th>Combustivel</th>
                  <th>Placa</th>
                  <th>Renavan</th>
                  <th>Chassi</th>
                  <th>Valor</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php 
        
                  $sql_ve = mysqli_query($con, "SELECT * from historico where id_cliente = '$id' AND visivel = '2' ");
        
                  while ($veto=mysqli_fetch_array($sql_ve)) {

                  $id_pedido = $veto['id_pedido'];
                  $id_veiculo = $veto['id_veiculo'];
                  $id_cliente = $veto['id_cliente'];
                  $datasolicitacao = $veto['datasolicitacao'];
                  $nrastreador = $veto['nrastreador'];
                  $linha = $veto['linha'];
                  $operadora = $veto['operadora'];
                  $marca = $veto['marca'];
                  $cor = $veto['cor'];
                  $modelo = $veto['modelo'];
                  $ano = $veto['ano'];
                  $combustivel = $veto['combustivel'];
                  $placa = $veto['placa'];
                  $renavan = $veto['renavan'];
                  $chassi = $veto['chassi'];
                  $valor = $veto['valor'];
                  $data_manutencao = $veto['data_manutencao'];
                ?>

                <tr>
                  <td><?php echo date('d/m/Y', strtotime($data_manutencao)); ?></td>
                  <td><?php echo $id_pedido ?></td>
                  <td><?php echo date('d/m/Y', strtotime($datasolicitacao)); ?></td>
                  <td><?php echo $nrastreador ?></td>
                  <td><?php echo $linha ?></td>
                  <td><?php echo $operadora ?></td>
                  <td><?php echo $marca; ?></td>
                  <td><?php echo $cor; ?></td>
                  <td><?php echo $modelo; ?></td>
                  <td><?php echo $ano ?></td> 
                  <td><?php echo $combustivel ?></td> 
                  <td><?php echo $placa?></td> 
                  <td><?php echo $renavan; ?></td>  
                  <td><?php echo $chassi; ?></td> 
                  <td><?php echo $num1 = number_format($valor,2,',','.'); ?></td>
                  
                </tr>

              <?php } ?>

              </tbody>
            </table>

        </div>

        </div>
        </div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versão</b> 1.0    </div>
    <strong>Todos direitos reservados JL Seguro.
  </footer>
</div>
<!-- ./wrapper -->


<script type="text/javascript">
  var cont = 1;

  function ativar_inputs(){
    if (cont == 1) {
      
      ativar_todos_inputs();
      inserir_ids_array();

      cont = 2;
    }else{
      $('input:checkbox').prop("checked", false);

      remover_ids_array();
      cont = 1;
    }
  }

  function ativar_todos_inputs(){
    $('input:checkbox').prop("checked", true);
  }

</script>

<script type="text/javascript">

  $(function(){
            $('html').css('background-color', '#F1FCFF');
            $('.container').css('background-color', '#F1FCFF');
            $('.content').css('background-color', '#F1FCFF');
            $('.box').css('background-color', '#F1FCFF');
            $('.tab-content').css('background-color', '#F1FCFF');
       });

  codigos = [];

  var retorno = false;
  var index;

  function guardar_id(id_cr){

    retorno = verifica_cod(id_cr);

    if (retorno == true) {
      inserir_cod(id_cr);
    }else{
      remover_cod(id_cr);
    }
    
  }

  function inserir_ids_array(){

    <?php

    $sql_id_do_cr = mysqli_query($con, "SELECT * FROM cr WHERE id_cliente = '$id' ");

      while ($vetor_cr=mysqli_fetch_array($sql_id_do_cr)):
        $id_cr22 = $vetor_cr['id_cr'];
    ?>

    inserir_cod(<?php echo "'".$id_cr22."'"; ?>);

   <?php endwhile ?>

  }

  function remover_ids_array(){
    var tamanho_array = codigos.length;

    codigos.splice(0, tamanho_array);
  }

  function inserir_cod(id_cr){
    codigos.push(id_cr);
  }

  function remover_cod(id_cr){

    index = codigos.indexOf(id_cr);

    if (index > -1) {
      codigos.splice(index, 1);
    }

  }

  function verifica_cod(id_cr1){

    if (codigos.indexOf(id_cr1) > -1) {
      console.log("o id " + id_cr1 + " já existe no array!, removendo...");

      return false;
    }else{
      console.log("o id " + id_cr1 + " não existe no array, adicionando...");

      return true;
    }
  }

  function url_dinamica(){

    var url = "imprimir_selecionados.php?codigos=";
    

    for( l = 0; l < codigos.length; l++){

      codigo_atual = codigos[l];

      url += codigo_atual;
      url += "-";

    }

   window.open(
    url,
  '_blank' // <- This is what makes it open in a new window.
);

  }

</script>

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
    $('#example3').DataTable()
    $('#example4').DataTable()
    $('#example5').DataTable()
  })
</script>

<script type="text/javascript">

function formatar_ano(id){
    var array_data = [];

      var data = $("#"+id).val();

      if (data.length > 10) {

        array_data = data.split("-");

        var ano = array_data[0];

        ano = ano.substring(0,(ano.length - 1));

        var dia = array_data[2];
        var mes = array_data[1];

        var data_final = ano + "-" + mes + "-" + dia;

        $("#"+id).val(data_final);

      }
}

</script>

</body>
</html>
<?php } } ?>