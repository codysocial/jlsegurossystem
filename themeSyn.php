<?php

session_start();

include 'includes/conexao.php';

$sql = "SELECT theme FROM usuarios WHERE id_user = ?";
$sql = $conn->prepare($sql);
$sql->execute([$_SESSION['id']]);

if ($sql->rowCount() > 0) {
	$theme = $sql->fetch(PDO::FETCH_OBJ);
	echo  json_encode(['tema' => $theme->theme]);
}else{
	echo  json_encode(['tema' => 0]);
}