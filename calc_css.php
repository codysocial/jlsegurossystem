<style type="text/css">
    
/*calculadora style*/

@font-face {
  font-family: "calculadora";
  src: url("Calculator.ttf") format("truetype");
}

#button_on{
  position: fixed;
  bottom: 15px;
  right: 25px;
  z-index: 1003300;
}

.btn_vermelho{
  background: rgba(169,3,41,1);
background: -moz-linear-gradient(-45deg, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 25%, rgba(224,33,78,1) 60%, rgba(109,0,25,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(169,3,41,1)), color-stop(25%, rgba(143,2,34,1)), color-stop(60%, rgba(224,33,78,1)), color-stop(100%, rgba(109,0,25,1)));
background: -webkit-linear-gradient(-45deg, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 25%, rgba(224,33,78,1) 60%, rgba(109,0,25,1) 100%);
background: -o-linear-gradient(-45deg, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 25%, rgba(224,33,78,1) 60%, rgba(109,0,25,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 25%, rgba(224,33,78,1) 60%, rgba(109,0,25,1) 100%);
background: linear-gradient(135deg, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 25%, rgba(224,33,78,1) 60%, rgba(109,0,25,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a90329', endColorstr='#6d0019', GradientType=1 );
}

.btn_verde{
  background: rgba(210,255,82,1);
  background: -moz-linear-gradient(-45deg, rgba(210,255,82,1) 0%, rgba(210,255,82,1) 20%, rgba(145,179,41,1) 44%, rgba(145,232,66,1) 100%);
  background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(210,255,82,1)), color-stop(20%, rgba(210,255,82,1)), color-stop(44%, rgba(145,179,41,1)), color-stop(100%, rgba(145,232,66,1)));
  background: -webkit-linear-gradient(-45deg, rgba(210,255,82,1) 0%, rgba(210,255,82,1) 20%, rgba(145,179,41,1) 44%, rgba(145,232,66,1) 100%);
  background: -o-linear-gradient(-45deg, rgba(210,255,82,1) 0%, rgba(210,255,82,1) 20%, rgba(145,179,41,1) 44%, rgba(145,232,66,1) 100%);
  background: -ms-linear-gradient(-45deg, rgba(210,255,82,1) 0%, rgba(210,255,82,1) 20%, rgba(145,179,41,1) 44%, rgba(145,232,66,1) 100%);
  background: linear-gradient(135deg, rgba(210,255,82,1) 0%, rgba(210,255,82,1) 20%, rgba(145,179,41,1) 44%, rgba(145,232,66,1) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d2ff52', endColorstr='#91e842', GradientType=1 );
}

#button_onon{
  width: 50px;
  height: 50px;
  border-radius: 50%;
  font-size: 20px;
  font-weight: bold;
  color: black;

  -webkit-box-shadow: -2px 10px 9px 1px rgba(0,0,0,0.75);
  -moz-box-shadow: -2px 10px 9px 1px rgba(0,0,0,0.75);
  box-shadow: -2px 10px 9px 1px rgba(0,0,0,0.75);

}

#calculadora{
   position: fixed;
   width: 0px;
   bottom: 2px;
   right: 20px;
   border: solid black 1px;
   height: 0px;
   border-radius: 10px;

  -webkit-box-shadow: 0px 0px 22px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 0px 22px 0px rgba(0,0,0,0.75);
  box-shadow: 0px 0px 22px 0px rgba(0,0,0,0.75);

  background: rgba(196,196,196,1);
background: -moz-linear-gradient(45deg, rgba(196,196,196,1) 0%, rgba(191,191,191,1) 30%, rgba(222,222,222,1) 49%, rgba(181,181,181,1) 77%, rgba(181,181,181,1) 99%, rgba(181,181,181,1) 100%);
background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(196,196,196,1)), color-stop(30%, rgba(191,191,191,1)), color-stop(49%, rgba(222,222,222,1)), color-stop(77%, rgba(181,181,181,1)), color-stop(99%, rgba(181,181,181,1)), color-stop(100%, rgba(181,181,181,1)));
background: -webkit-linear-gradient(45deg, rgba(196,196,196,1) 0%, rgba(191,191,191,1) 30%, rgba(222,222,222,1) 49%, rgba(181,181,181,1) 77%, rgba(181,181,181,1) 99%, rgba(181,181,181,1) 100%);
background: -o-linear-gradient(45deg, rgba(196,196,196,1) 0%, rgba(191,191,191,1) 30%, rgba(222,222,222,1) 49%, rgba(181,181,181,1) 77%, rgba(181,181,181,1) 99%, rgba(181,181,181,1) 100%);
background: -ms-linear-gradient(45deg, rgba(196,196,196,1) 0%, rgba(191,191,191,1) 30%, rgba(222,222,222,1) 49%, rgba(181,181,181,1) 77%, rgba(181,181,181,1) 99%, rgba(181,181,181,1) 100%);
background: linear-gradient(45deg, rgba(196,196,196,1) 0%, rgba(191,191,191,1) 30%, rgba(222,222,222,1) 49%, rgba(181,181,181,1) 77%, rgba(181,181,181,1) 99%, rgba(181,181,181,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c4c4c4', endColorstr='#b5b5b5', GradientType=1 );

  z-index: 10000000;
}

#visor {
  font-family: "calculadora";
  font-weight: bold;
  font-size: 37px;

  background: rgba(251,251,254,1);
  background: -moz-linear-gradient(45deg, rgba(251,251,254,1) 0%, rgba(230,225,245,1) 32%, rgba(215,209,239,1) 67%, rgba(203,201,238,1) 100%);
  background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(251,251,254,1)), color-stop(32%, rgba(230,225,245,1)), color-stop(67%, rgba(215,209,239,1)), color-stop(100%, rgba(203,201,238,1)));
  background: -webkit-linear-gradient(45deg, rgba(251,251,254,1) 0%, rgba(230,225,245,1) 32%, rgba(215,209,239,1) 67%, rgba(203,201,238,1) 100%);
  background: -o-linear-gradient(45deg, rgba(251,251,254,1) 0%, rgba(230,225,245,1) 32%, rgba(215,209,239,1) 67%, rgba(203,201,238,1) 100%);
  background: -ms-linear-gradient(45deg, rgba(251,251,254,1) 0%, rgba(230,225,245,1) 32%, rgba(215,209,239,1) 67%, rgba(203,201,238,1) 100%);
  background: linear-gradient(45deg, rgba(251,251,254,1) 0%, rgba(230,225,245,1) 32%, rgba(215,209,239,1) 67%, rgba(203,201,238,1) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fbfbfe', endColorstr='#cbc9ee', GradientType=1 );

   text-align: right;
   height: 50px;
   width: 90%;
   margin-left: 5%;
   border-radius: 5px;
}

.es{
  margin-left: 3px;
}

table {
   margin: 0 auto 0 auto;
}

  
input[type=button]:hover{
  background: rgba(179,220,237,1);
background: -moz-linear-gradient(-45deg, rgba(179,220,237,1) 0%, rgba(179,220,237,1) 3%, rgba(41,184,229,1) 47%, rgba(188,224,238,1) 96%, rgba(188,224,238,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(179,220,237,1)), color-stop(3%, rgba(179,220,237,1)), color-stop(47%, rgba(41,184,229,1)), color-stop(96%, rgba(188,224,238,1)), color-stop(100%, rgba(188,224,238,1)));
background: -webkit-linear-gradient(-45deg, rgba(179,220,237,1) 0%, rgba(179,220,237,1) 3%, rgba(41,184,229,1) 47%, rgba(188,224,238,1) 96%, rgba(188,224,238,1) 100%);
background: -o-linear-gradient(-45deg, rgba(179,220,237,1) 0%, rgba(179,220,237,1) 3%, rgba(41,184,229,1) 47%, rgba(188,224,238,1) 96%, rgba(188,224,238,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(179,220,237,1) 0%, rgba(179,220,237,1) 3%, rgba(41,184,229,1) 47%, rgba(188,224,238,1) 96%, rgba(188,224,238,1) 100%);
background: linear-gradient(135deg, rgba(179,220,237,1) 0%, rgba(179,220,237,1) 3%, rgba(41,184,229,1) 47%, rgba(188,224,238,1) 96%, rgba(188,224,238,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3dced', endColorstr='#bce0ee', GradientType=1 );
}

#visor:hover{
  background-color: white;
  color: black;
}

/*acabou aqui a calculadora */

.button {
  display: block;
  width: 50px;
  height: 50px;
  background-color: #eef;
  font-size: 20pt;
  cursor: pointer;
  background-color: rgb(169, 169, 169);
  transition: 100ms ease;
}

.textView {
    width: 206px;
    font-size: 25pt;
    padding: 5px;
    margin: 5px;
    border: 1px solid rgb(0, 0, 0, 0.25);
}
.yl {
    background-color: orange;
    transition: 100ms ease;
    margin-bottom: 2px;
}
.yl:hover{
    background-color: rgb(255, 165, 0, 0.700);
}
.backspace {
    background-color: rgb(253, 38, 0);
    color: white;
    transition: 100ms ease;
}

</style>