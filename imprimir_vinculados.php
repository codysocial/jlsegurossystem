<!DOCTYPE html>
<html>
<head>

    <style type="text/css">
    .mesmo-tamanho{
      padding-right: 10px;
      padding-left: 1%;
      margin-left: 5px;
      width: 100px;
    }
    .btn_restaurar{
      border: 0px;
      border-radius: 3px;
      width: 100px;
      height: 33px;
      margin-left: 2px;
      background-color: green;
    }
    .min{
      width: 5%;
    }
    .alinhar{
      float: left; 
      margin-top: 3px; 
      margin-right: auto;
    }
    .alinhar2{
      float: left; 
      margin-top: 3px; 
      margin-right: auto;
      margin-left: 3px;
    }
    .alinhar3{
      float: right; 
      margin-top: 3px; 
      margin-right: auto;
      margin-right: 3px;
    }
    #ball{
      width: 15px;
      height: 15px;
      border-radius: 50%;
      margin-left: 50%;
    }
    #legenda{
      width: 210px;
      height: 100px;
      float: right;
      margin-right: 5%;
      display: flex;
      flex-direction: column;
      background-color: #eee;
      border-radius: 8px;
    }
    .linha_legen{
      width: 100%;
      height: 50%;
      display: flex;
      flex-direction: row;
    }
    .ball_lege{
      position: relative;
      width: 15px;
      height: 15px;
      border-radius: 50%;
      margin-top: 10px;
      margin-left: 5px;
    }
    .legend{

      margin-top: 8px;
      margin-left: 4px;

    }
  </style>

</head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>

<?php include"includes/conexao.php"; ?>
<div style="width: 90%; margin-left: 5%;">
  <h3 style="text-align: center; margin-bottom: 25px;">Relatório - Clientes Vinculados</h3>
<table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>IMEI</th>
                  <th>Fabricante</th>
                  <th>Modelo</th>
                  <th>Data</th>
                  <th>Cliente</th>
                  <th>Datacad</th>
                  <th>Chip</th>
                  <th>Operadora</th>
                  <th>ISMC</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  
                  $sql_vinculo = mysqli_query($con, "SELECT * FROM vinculo_rastreador");

                  while ($vetor = mysqli_fetch_array($sql_vinculo)) {

                    $id_vinculo = $vetor['id_vinculo'];
                    $id_rastreador = $vetor['id_rastreador'];
                    $id_chip = $vetor['id_chip'];
                    $id_cliente = $vetor['id_cliente'];
                    $id_pedido = $vetor['id_pedido'];

                    $sql_cliente = mysqli_query($con, "SELECT situacao, id_cli, nome, nomefant, datacad from clientes where id_cli = '$id_cliente' ");
                    $vetor_cliente = mysqli_fetch_array($sql_cliente);

                    $sql_rastreador = mysqli_query($con, "SELECT * FROM rastreador where id_rastreador = '$id_rastreador' ");
                    $vetor_rastreador = mysqli_fetch_array($sql_rastreador);

                    $sql_chip = mysqli_query($con, "SELECT * FROM chips where id_chip = '$id_chip' ");
                    $vetor_chip = mysqli_fetch_array($sql_chip);

                    if ($vetor_cliente['situacao'] == 1) {
                      $color = "green";
                    }
                    elseif ($vetor_cliente['situacao'] == 2) {
                      $color = "black";
                    }elseif ($vetor_cliente['situacao'] == 3) {
                      $color = "red";
                    } elseif ($vetor_cliente['situacao'] == 4) {
                      $color = "firebrick";
                    }else{
                      $color = "silver";
                    }

                    $nomecli = $vetor_cliente['nomefant'];

                    if (!$nomecli) {
                      $nomecli = $vetor_cliente['nome'];
                    }
                ?>

                <tr>
                  <td><?php echo $vetor_rastreador['imei'];?></td>
                  <td><?php echo $vetor_rastreador['fabricante'];?></td>
                  <td><?php echo $vetor_rastreador['modelo'];?></td>
                  <td><?php echo $vetor_rastreador['data'];?></td>
                  <td><?php echo $nomecli; ?></td>
                  <td><?php echo $vetor_cliente['datacad'];?></td>
                  <td><?php echo $vetor_chip['linha'];?></td>
                  <td><?php echo $vetor_chip['operadora'];?></td>
                  <td><?php echo $vetor_chip['ismc'];?></td>
                  
                </tr>

                <?php  } ?>
                </tbody>
                
              </table>

</body>

<script type="text/javascript">
  print();
</script>

</html>