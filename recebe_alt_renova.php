<?php

session_start();

include"includes/conexao.php";

function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
}


$id = $_GET['id'];

$id_cliente = $_POST['id_cliente'];

$data = date('Y-m-d');

$tipoindenizacao = $_POST['tipoindenizacao'];
//contrato
$id_produto = $_POST['rastreadormod'];
$chipop_busca = $_POST['chipop'];

$sql_modelo_gel = mysqli_query($con, "select * from produtos where id_produto = '$id_produto'");
$vetor_modelo_gel = mysqli_fetch_array($sql_modelo_gel);

$sql_modelo_chip = mysqli_query($con, "select * from produtos where id_produto = '$chipop_busca'");
$vetor_modelo_chip = mysqli_fetch_array($sql_modelo_chip);

$nrastreador = $_POST['nrastreador'];
$datainstalacao = $_POST['datainstalacao'];
$fabricante = $_POST['fabricante'];
$chipop = $vetor_modelo_chip['chipprimario'];
$rastreadormod = $vetor_modelo_gel['rastreadormod'];
$periodo = $_POST['periodo'];
$vigenciainicio = $_POST['datainstalacao'];
$timestamp1 = strtotime($vigenciainicio . "+1 year");
$vigenciafim1 = date('Y-m-d', $timestamp1);
$timestamp = strtotime($vigenciainicio . "+2 year");
$vigenciafim = date('Y-m-d', $timestamp);
$id_corretor = $_POST['id_corretor'];
$id_indicador = $_POST['id_indicador'];

$sql_cliente = mysqli_query($con, "update clientes SET vendedor = '$id_corretor' where id_cli = '$id_cliente'");

$sql_consulta = mysqli_query($con, "select * from renovacoes where id_cliente = '$id_cliente'");

if(mysqli_num_rows($sql_consulta) == 0) { 

$sql_vigencia = mysqli_query($con, "insert into renovacoes (id_cliente, data1, data2) VALUES ('$id_cliente', '$vigenciafim1', '$vigenciafim')");

}

//cadastro endereço instalação

$id_endereco = $_POST['id_endereco'];
$cep1 = $_POST['cep1'];
$endereco1 = strtoupper($_POST['endereco1']);
$numero1 = $_POST['numero1'];
$complemento1 = strtoupper($_POST['complemento1']);
$bairro1 = strtoupper($_POST['bairro1']);
$cidade1 = strtoupper($_POST['cidade1']);
$estado1 = strtoupper($_POST['estado1']);
$observacoes1 = strtoupper($_POST['observacoes1']);

if(!empty($id_endereco)) {

	$sql_endereco_instalacao = mysqli_query($con, "update cliente_instalacao SET id_cliente='$id_cliente', cep='$cep1', endereco='$endereco1', numero='$numero1', complemento='$complemento1', bairro='$bairro1', cidade='$cidade1', estado='$estado1', observacoes='$observacoes1' where id_instalacao = '$id_endereco'") or die (mysqli_error($con));
} else { 

	$sql_endereco_instalacao = mysqli_query($con, "insert into cliente_instalacao (id_cliente, cep, endereco, numero, complemento, bairro, cidade, estado, observacoes) VALUES ('$id_cliente', '$cep1', '$endereco1', '$numero1', '$complemento1', '$bairro1', '$cidade1', '$estado1', '$observacoes1')") or die (mysqli_error($con));

}

//cadastro referencia cliente

$id_referencia1 = $_POST['id_referencia1'];
$id_referencia2 = $_POST['id_referencia2'];
$nomep = strtoupper($_POST['nomep']);
$parentesco = strtoupper($_POST['parentesco']);
$telefonep = $_POST['telefonep'];
$nomep1 = strtoupper($_POST['nomep1']);
$parentesco1 = strtoupper($_POST['parentesco1']);
$telefonep1 = $_POST['telefonep1'];

if(!empty($nomep)) {

	$sql_parentesco = mysqli_query($con, "update cliente_referencias SET id_cliente='$id_cliente', nome='$nomep', parentesco='$parentesco', telefone='$telefonep' where id_referencia = '$id_referencia1'") or die (mysqli_error($con));

} 

if($id_referencia1 == '') { 

	$sql_parentesco = mysqli_query($con, "insert into cliente_referencias (id_cliente, nome, parentesco, telefone) VALUES ('$id_cliente', '$nomep', '$parentesco', '$telefonep')") or die (mysqli_error($con));

}

if(!empty($nomep1)) {

	$sql_parentesco1 = mysqli_query($con, "update cliente_referencias SET id_cliente='$id_cliente', nome='$nomep1', parentesco='$parentesco1', telefone='$telefonep1' where id_referencia = '$id_referencia2'") or die (mysqli_error($con));

}

if($id_referencia2 == '') { 

	$sql_parentesco1 = mysqli_query($con, "insert into cliente_referencias (id_cliente, nome, parentesco, telefone) VALUES ('$id_cliente', '$nomep1', '$parentesco1', '$telefonep1')") or die (mysqli_error($con));

}

//cadastro veiculo

	$marca = $_POST['marca'];

	$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
	$vetor_marca = mysqli_fetch_array($sql_marca);

	$modelos = $_POST['modelos_alt'];

	$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
	$vetor_modelo = mysqli_fetch_array($sql_modelo);

	$anomod = $_POST['anomod'];

	$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
	$vetor_anomod = mysqli_fetch_array($sql_anomod);

	$valor = moeda($_POST['valor']);

	if($valor == NULL) {

		//$valorcarro = $vetor_anomod['valor'];

	} else {

		$valorcarro = $valor;

	}

	$cor = strtoupper($_POST['cor']);
	$placa = strtoupper($_POST['placa']);
	$combustivel = strtoupper($_POST['combustivel']);
	$renavan = strtoupper($_POST['renavan']);
	$chassi = strtoupper($_POST['chassi']);

	$id_veiculo = $_POST['id_veiculo'];

	if(!empty($id_veiculo)) { 

	$sql_veiculo = mysqli_query($con, "update cliente_veiculo SET id_cliente='$id_cliente', modelo='$vetor_modelo[modelo]', marca='$marca', valor='$valorcarro', cor='$cor', combustivel='$combustivel', placa='$placa', renavan='$renavan', chassi='$chassi', anomod='$anomod' where id_veiculo = '$id_veiculo'") or die (mysqli_error($con));

	} else { 

		insert('cliente_veiculo', [
			'id_cliente' => $id_cliente,
			'tipo' => $tipo_veiculo,
			'modelo' => $vetor_modelo['modelo'],
			'marca' => $marca,
			'valor' => $valorcarro,
			'cor' => $cor,
			'combustivel' => $combustivel,
			'placa' => $placa,
			'renavan' => $renavan,
			'chassi' => $chassi,
			'anomod' => $anomod
		], $conn); 

	}


$adesao = moeda($_POST['adesao']);
$parcelar = $_POST['parcelar'];
$formapag = $_POST['formapag'];
$vencimentoadesao = $_POST['vencimentoadesao'];
$formapag_1 = $_POST['formapag_1'];
$vencimentoadesao_1 = $_POST['vencimentoadesao_1'];
$formapag_2 = $_POST['formapag_2'];
$vencimentoadesao2 = $_POST['vencimentoadesao2'];
$formapag_3 = $_POST['formapag_3'];
$vencimentoadesao3 = $_POST['vencimentoadesao3'];
$mensalidade = moeda($_POST['mensalidade']);
$mensalidade1 = $_POST['1mensalidade'];
$demaismensalidade = $_POST['demaismensalidade'];
$formapagmensalidade = $_POST['formapagmensalidade'];
$vacina = moeda($_POST['vacina']);
$vencimentovicina = $_POST['vencimentovicina'];
$formapagvacina = $_POST['formapagvacina'];
$instalacao = moeda($_POST['instalacao']);
$vencimentoinstalacao = $_POST['vencimentoinstalacao'];
$formapaginst = $_POST['formapaginst'];
$assistencia = moeda($_POST['assistencia']);
$parcelara24 = $_POST['parcelara24'];

$formapaga24 = $_POST['formapaga24'];
$vencimentoa24 = $_POST['vencimentoa24'];
$formapag_a24 = $_POST['formapag_a241'];
$vencimentoa241 = $_POST['vencimentoa24_1'];
$formapag_a242 = $_POST['formapag_a242'];
$vencimentoa242 = $_POST['vencimentoa242'];
$formapag_a243 = $_POST['formapag_a243'];
$vencimentoa243 = $_POST['vencimentoa243'];

$assistenciaterceiro = moeda($_POST['assistenciaterceiro']);
$parcelarat = $_POST['parcelarat'];

$formapagat = $_POST['formapagat'];
$vencimentoat = $_POST['vencimentoat'];
$formapag_at1 = $_POST['formapag_at1'];
$vencimentoat_1 = $_POST['vencimentoat_1'];
$formapag_at2 = $_POST['formapag_at2'];
$vencimentoat2 = $_POST['vencimentoat2'];
$formapag_at3 = $_POST['formapag_at3'];
$vencimentoat3 = $_POST['vencimentoat3'];

$carroreserva = moeda($_POST['carroreserva']);
$parcelascarro = $_POST['parcelascarro'];

$formapagcarro = $_POST['formapagcarro'];
$vencimentocarro1 = $_POST['vencimentocarro1'];
$formapagcarro1 = $_POST['formapagcarro1'];
$vencimentocarro2 = $_POST['vencimentocarro2'];
$formapagcarro2 = $_POST['formapagcarro2'];
$vencimentocarro3 = $_POST['vencimentocarro3'];
$formapagcarro3 = $_POST['formapagcarro3'];
$vencimentocarro4 = $_POST['vencimentocarro4'];

$carroapp = moeda($_POST['carroapp']);
$parcelascarroapp = $_POST['parcelascarroapp'];

$formapagcarroapp = $_POST['formapagcarroapp'];
$vencimentocarroapp1 = $_POST['vencimentocarroapp1'];
$formapagcarroapp1 = $_POST['formapagcarroapp1'];
$vencimentocarroapp2 = $_POST['vencimentocarroapp2'];
$formapagcarroapp2 = $_POST['formapagcarroapp2'];
$vencimentocarroapp3 = $_POST['vencimentocarroapp3'];
$formapagcarroapp3 = $_POST['formapagcarroapp3'];
$vencimentocarroapp4 = $_POST['vencimentocarroapp4'];

$outras = strtoupper($_POST['outras']);
$observacoes = strtoupper($_POST['observacoes']);

$qtdmensalidade = 12;

$valoradesao = $adesao / $parcelar;
$valora24 = $assistencia / $parcelara24;
$valoraat = $assistenciaterceiro / $parcelarat;
$valorcarrofinal = $carroreserva / $parcelascarro;
$valorcarroapp = $carroapp / $parcelascarroapp;

$valormensalidade = $mensalidade * 12;

if($parcelar == 12) { 

	$valormensalidade += $adesao;

}

if($parcelara24 == 12) { 

	$valormensalidade += $assistencia;

}

if($parcelarat == 12) { 

	$valormensalidade += $assistenciaterceiro;

}

if($parcelascarro == 12) { 

	$valormensalidade += $carroreserva;

}

if($parcelascarroapp == 12) { 

	$valormensalidade += $carroapp;

}

$sql_produto = mysqli_query($con, "select * from produtos where id_produto = '$id_produto'") or die (mysqli_error($con));
$vetor_produto = mysqli_fetch_array($sql_produto);

update('pedidos', [
	`tipo` => $tipoindenizacao, 
	`vigenciainicio` => $vigenciainicio, 
	`vigenciafim` => $vigenciafim, 
	`id_cliente` => $id_cliente, 
	`id_corretor` => $id_corretor, 
	`id_indicador` => $id_indicador, 
	`id_veiculo` => $id_veiculo, 
	`adesao` => $adesao, 
	`parcelar` => $parcelar, 
	`formapag` => $formapag, 
	`vencimentoadesao` => $vencimentoadesao, 
	`formapag1` => $formapag_1, 
	`vencimentoadesao1` => $vencimentoadesao_1, 
	`formapag2` => $formapag_2, 
	`vencimentoadesao2` => $vencimentoadesao2, 
	`formapag3` => $formapag_3, 
	`vencimentoadesao3` => $vencimentoadesao3, 
	`mensalidade` => $mensalidade, 
	`1mensalidade` => $mensalidade1, 
	`demaismensalidade` => $demaismensalidade, 
	`formapagmensalidade` => $formapagmensalidade, 
	`vacina` => $vacina, 
	`vencimentovicina` => $vencimentovicina, 
	`formapagvacina` => $formapagvacina, 
	`instalacao` => $instalacao, 
	`vencimentoinstalacao` => $vencimentoinstalacao, 
	`formapaginst` => $formapaginst, 
	`assistencia` => $assistencia, 
	`parcelara24h` => $parcelara24, 
	`vencimentoassistencia` => $vencimentoassistencia, 
	`assistenciaterceiro` => $assistenciaterceiro, 
	`parcelarat` => $parcelarat, 
	`vencimentoassistenciaterceiro` => $vencimentoassistenciaterceiro, 
	`observacoes` => $observacoes, 
	`carroreserva` => $carroreserva, 
	`parcelascarro` => $parcelascarro, 
	`formapagcarro` => $formapagcarro, 
	`vencimentocarro1` => $vencimentocarro1, 
	`formapagcarro1` => $formapagcarro1, 
	`vencimentocarro2` => $vencimentocarro2, 
	`formapagcarro2` => $formapagcarro2, 
	`vencimentocarro3` => $vencimentocarro3, 
	`formapagcarro3` => $formapagcarro3, 
	`vencimentocarro4` => $vencimentocarro4, 
	`carroapp` => $carroapp, 
	`parcelascarroapp` => $parcelascarroapp, 
	`formapagcarroapp` => $formapagcarroapp, 
	`vencimentocarroapp1` => $vencimentocarroapp1, 
	`formapagcarroapp1` => $formapagcarroapp1, 
	`vencimentocarroapp2` => $vencimentocarroapp2, 
	`formapagcarroapp2` => $formapagcarroapp2, 
	`vencimentocarroapp3` => $vencimentocarroapp3, 
	`formapagcarroapp3` => $formapagcarroapp3, 
	`vencimentocarroapp4` => $vencimentocarroapp4, 
	`outras` => $outras
], [
	'id_pedido' => $id
], "",$conn);

$sql_os = mysqli_query($con, "update os SET periodo='$periodo', datainstalacao='$datainstalacao', rastreador='$id_produto', nrastreador='$nrastreador', contrato='$id_pedido', chipop='$chipop', rastreadormod='$rastreadormod', fabricante='$fabricante' where id_pedido = '$id'") or die (mysqli_error($con));

//gerar financeiro

$DataInicial = new \DateTime($demaismensalidade);

// Intervalo de 1 mês
$Intervalo = new \DateInterval( 'P1M' );

// Data daqui a 2 anos, apenas para aumentar a amostragem
$DataFinal = new \DateTime( 'today + 11 month' );

$Periodo = new \DatePeriod( $DataInicial, $Intervalo, $DataFinal );

$valorcontrato = $valormensalidade / 12;

		insert('cr', [
			'tipo' => '1',
			'id_cliente' => $id_cliente,
			'id_pedido' => $id,
			'descricao' => '1º Parcela da renovação Pedido: '.$id,
			'formapag' => $formapagmensalidade,
			'data' => $data,
			'datavencimento' => $mensalidade1,
			'valor' => $valorcontrato,
			'status' => 1,
			'ordem' => 1
		], $conn); 

$i = 1;

foreach ( $Periodo as $Data ) {

   $datagerada = $Data->format( 'Y-m-d' );

   $diasemana_numero = date('w', strtotime($datagerada));

   if($diasemana_numero == 0) {

    $datafinal = date('Y-m-d', strtotime('+1 days', strtotime($datagerada)));

    } if($diasemana_numero == 1) {

    $datafinal = $datagerada;

    } if($diasemana_numero == 2) {

    $datafinal = $datagerada;

    } if($diasemana_numero == 3) {

    $datafinal = $datagerada;

    } if($diasemana_numero == 4) {

    $datafinal = $datagerada;

    } if($diasemana_numero == 5) {

    $datafinal = $datagerada;

    } if($diasemana_numero == 6) {

    $datafinal = date('Y-m-d', strtotime('+2 days', strtotime($datagerada)));

    }
    /*
   $sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('1', '$id_cliente', '$id', 'Renovação do Pedido: $id', '$formapagmensalidade', '$data', '$datafinal', '$valorcontrato', '1')");
	*/
   $i++;
			
}

if($vacina != '0.00') {

	insert('cr', [
		'tipo' => '6',
		'id_cliente' => $id_cliente,
		'id_pedido' => $id,
		'descricao' => 'Vacina do Pedido: '.$id,
		'formapag' => $formapagvacina,
		'data' => $data,
		'datavencimento' => $vencimentovicina,
		'valor' => $vacina,
		'status' => 1
	], $conn); 

}

/*if($instalacao != '0.00') {

	$sql_vacina = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('7', '$id_cliente', '$id', 'Instalação do Pedido: $id', '$formapaginst', '$data', '$vencimentoinstalacao', '$instalacao', '1')");

}
*/

echo"<script language=\"JavaScript\">
location.href=\"validamensalidadealt_renova.php?id=$id\";
</script>";

?>