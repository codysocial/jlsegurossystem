<?php
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'cr_view';
 
// Table's primary key
$primaryKey = 'id_cr';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id_cr', 'dt' => 0 ),
    array( 'db' => 'nome',  'dt' => 1 ),
    array( 'db' => 'descricao',  'dt' => 2 ),
    
    array( 'db' => 'datavencimento',
      'dt' => 3,
      'formatter' => function($db){

      $data =  date('d/m/Y', strtotime($db));

      return $data;
      }
    ),
    
    array( 
      'db' => 'valor',
        'dt' => 4,
        'formatter' => function($db){
      $num = $db;
            $num = number_format($num,2,',','.');
            return $num;
        }
    ),
    
    array( 'db' => 'datapagamento',
      'dt' => 5,
      'formatter' => function($db){

      $data =  date('d/m/Y', strtotime($db));

      if ($data == "31/12/1969" OR $data == "01/01/1970") {
          $data = "";
      }


      return $data;
      }
    ),

    array( 
      'db' => 'valorpago',
        'dt' => 6,
        'formatter' => function($db){
      $num = $db;
            $num = number_format($num,2,',','.');
            return $num;
        }
    ),
    
    array( 
      'db' => 'status',
        'dt' => 7,
        'formatter' => function($db){
          if($db == 1) { $db = 'Pendente'; }
            else if($db == 2) { $db = 'Pago'; } 
            else if($db == 3) { $db = 'Cancelado'; }
        
        return $db;

      }
  ),
    
    array( 
      'db' => 'id_cr',
      'dt' => 8,
      'formatter' => function($db){
        return "
        <a href='recebercontasareceber.php?id={$db}&tipo=1'>
          <button type='button' class='btn btn-info mesmo-tamanho' title='Visualizar / Alterar Conta'>
                  <i class='fa fa-edit'></i>
                </button>
        </a>

        <a href='alterarcr.php?id={$db}'>
                <button type='button' class='btn btn-success mesmo-tamanho' title='Alterar Cadastro'>
                  <i class='fa fa-edit'></i>
              </button>
            </a>

        ";
      }
    ),
    
    array( 
      'db' => 'id_cr',
        'dt' => 9,
        'formatter' => function($db){
        return "
          <input style='width: 25px; height: 25px; cursor: pointer;' type='checkbox' style='width: 30px; height: 30px;' value='{$db}' name='sel[]'>
        ";
      }
      ),
);

// SQL server connection information
$sql_details = array(
    'user' => 'jlsegu62_sistema',
    'pass' => '8512wedfr',
    'db'   => 'jlsegu62_sistema',
    'host' => 'localhost'
);


/*


$sql_details = array(
    'user' => 'root',
    'pass' => '',
    'db'   => 'jlsegu62_sistema',
    'host' => 'localhost'
);
*/

 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);