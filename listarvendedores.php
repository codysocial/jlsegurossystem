<?php

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

	 include"includes/conexao.php";
	 
	 session_start();

   $id_pagina = '3';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>

<?php include "includes/menu_sistema.php"; ?>

<div class="container">

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Vendedores</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <a href="cadastrovendedor.php"><button class="btn btn-ionc-2"  style="float: left;">Cadastrar Novo Vendedor</button></a>
            </br>
            </br>
           	</br>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">Código</th>
                  <th>Nome do Vendedor</th>
                  <th>CPF</th>
                  <th>Telefone</th>
                  <th width="13%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php 
				
				
				  
				  $sql_atual = mysqli_query($con, "select * from clientes where tipocad = '3' order by nome ASC");
				
				while ($vetor=mysqli_fetch_array($sql_atual)) {
				
				 ?>
                <tr>
                  <td><?php echo $vetor['id_cli']; ?></td>
                  <td><?php echo $vetor['nome']; ?></td> 
                  <td><?php 
                
                $qtddoc = strlen($vetor['cpfcnpj']);

                if($qtddoc == 14) {
                
                echo Mask("##.###.###/####-##",$vetor['cpfcnpj']); 
                
                }
                
                if($qtddoc == 11) {
                
                echo Mask("###.###.###-##",$vetor['cpfcnpj']); 
                
                }
                
                
                ?></td>
                  <td><?php echo $vetor['telefone']; ?></td>
                  <td><a href="alterarvendedor.php?id=<?php echo $vetor['id_cli']; ?>"><button type="button" class="btn btn-ionc-2 mesmo-tamanho" title="Alterar Cadastro"><i class="fa fa-edit"></i></button></a> <a href="confexcluirvendedor.php?id=<?php echo $vetor['id_cli']; ?>" ><button type="button" class="btn btn-danger mesmo-tamanho" title="Excluir Cadastro"><i class="fa fa-close"></i></button></a></td> 
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Versão</b> 1.0    </div>
      <strong>Todos direitos reservados JL Seguro.
    </footer>


<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable({
      "pageLength": 50
    });
    $('#example2').DataTable({
      "pageLength": 50,
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });


    $(function(){
        $('html').css('background-color', '#ecffec');
        $('body').css('background-color', '#ecffec');
        $('.container').css('background-color', '#ecffec');
        $('.content').css('background-color', '#ecffec');
        $('.box').css('background-color', '#ecffec');
        $('.tab-content').css('background-color', '#ecffec');
        $('.box-title').addClass('text-ionc-2');
    });

  })
</script>
</div>
</body>
</html>
<?php } } ?>