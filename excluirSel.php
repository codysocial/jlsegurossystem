<?php

function moeda($get_valor) { 
    $source = array('.', ',');  
    $replace = array('', '.'); 
    $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
    return $valor; //retorna o valor formatado para gravar no banco 
}

function reverse_date( $date ){
    return ( strstr( $date, '-' ) ) ? implode( '/', array_reverse( explode( '-', $date ) ) ) : implode( '-', array_reverse( explode(                '/', $date ) )      );
}

include"includes/conexao.php";

if ($_POST['sel']) {

	if ($_POST['btn_sel'] == "PCLD") {

		foreach ($_POST['sel'] as $codigos) {
	
			$sqlFeatch = mysqli_query($con, "select * from cr where id_cr = '$codigos'");

			$sql_ultimo_id = mysqli_query($con, "SELECT id_pcld FROM pcld order by id_pcld DESC");

			if (mysqli_num_rows($sql_ultimo_id) > 0) {
				$vetor = mysqli_fetch_array($sql_ultimo_id);
				$ultimo_id = $vetor['id_pcld'];
				$ultimo_id = $ultimo_id + 1;
			}else{
				$ultimo_id = 1;
			}
			

			$vetor = mysqli_fetch_array($sqlFeatch);

			$id_cr 			=	$vetor['id_cr'];
			$tipo 			=	$vetor['tipo'];
			$id_cliente 	=	$vetor['id_cliente'];
			$id_pedido 		=	$vetor['id_pedido'];
			
			if ($id_pedido == null) {
				$id_pedido = 0;
			}

			$descricao 		=	$vetor['descricao'];
			$formapag 		=	$vetor['formapag'];
			$data 			=	$vetor['data'];
		    $datavencimento = 	$vetor['datavencimento'];
		    $valor 			=	$vetor['valor'];
		    $valorpago 		=	$vetor['valorpago'];
		    $status 		=	$vetor['status'];
			
			
			$query = "insert into pcld (id_pcld, id_cr, tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('$ultimo_id', '$id_cr' , '$tipo', '$id_cliente', '$id_pedido', '$descricao', '$formapag', '$data', '$datavencimento', '$valor', '$status')";

		    $sql = mysqli_query($con, $query) or die (mysqli_error($con));

			//aqui é a exclusão da linha na tabela
			$sql_exclui = "delete FROM cr where id_cr = '$codigos'";
			$res2 = mysqli_query($con, $sql_exclui);

		}

		echo"<script language=\"JavaScript\">
			location.href=\"listarcr.php\";
			</script>";
		
	}

	if ($_POST['btn_sel'] == "P.C.L.D") {

		foreach ($_POST['sel'] as $codigos) {
	
			$sqlFeatch = mysqli_query($con, "select * from cr where id_cr = '$codigos'");

			$sql_ultimo_id = mysqli_query($con, "SELECT id_pcld FROM pcld order by id_pcld DESC");

			if (mysqli_num_rows($sql_ultimo_id) > 0) {
				$vetor = mysqli_fetch_array($sql_ultimo_id);
				$ultimo_id = $vetor['id_pcld'];
				$ultimo_id = $ultimo_id + 1;
			}else{
				$ultimo_id = 1;
			}

			$id_hehe = $_POST['id'];
			

			$vetor = mysqli_fetch_array($sqlFeatch);

			$id_cr 			=	$vetor['id_cr'];
			$tipo 			=	$vetor['tipo'];
			$id_cliente 	=	$vetor['id_cliente'];
			$id_pedido 		=	$vetor['id_pedido'];
			
			if ($id_pedido == null) {
				$id_pedido = 0;
			}

			$descricao 		=	$vetor['descricao'];
			$formapag 		=	$vetor['formapag'];
			$data 			=	$vetor['data'];
		    $datavencimento = 	$vetor['datavencimento'];
		    $valor 			=	$vetor['valor'];
		    $valorpago 		=	$vetor['valorpago'];
		    $status 		=	$vetor['status'];
			
			
			$query = "insert into pcld (id_pcld, id_cr, tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('$ultimo_id', '$id_cr' , '$tipo', '$id_cliente', '$id_pedido', '$descricao', '$formapag', '$data', '$datavencimento', '$valor', '$status')";

		    $sql = mysqli_query($con, $query) or die (mysqli_error($con));

			//aqui é a exclusão da linha na tabela
			$sql_exclui = "delete FROM cr where id_cr = '$codigos'";
			$res2 = mysqli_query($con, $sql_exclui);

		}

		echo"<script language=\"JavaScript\">
			location.href=\"alterarcliente.php?id=". $id_hehe ."\"</script>";
		
	}

	if ($_POST['btn_sel'] == "EXCLUIR") {

		foreach ($_POST['sel'] as $codigos) {
	
			$sqlFeatch = mysqli_query($con, "select * from cr where id_cr = '$codigos'");

			$vetor = mysqli_fetch_array($sqlFeatch);

			$id_cr 			=	$vetor['id_cr'];
			$tipo 			=	$vetor['tipo'];
			$id_cliente 	=	$vetor['id_cliente'];
			$id_pedido 		=	$vetor['id_pedido'];
			
			if ($id_pedido == null) {
				$id_pedido = 0;
			}

			$descricao 		=	$vetor['descricao'];
			$formapag 		=	$vetor['formapag'];
			$data 			=	$vetor['data'];
		    $datavencimento = 	$vetor['datavencimento'];
		    $valor 			=	$vetor['valor'];
		    $valorpago 		=	$vetor['valorpago'];
		    $status 		=	$vetor['status'];
		    $data_exclusao  =   date('Y-m-d');
			
			
			$query = "insert into temporario (id_cr, tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status, data_exclusao) VALUES ('$id_cr' , '$tipo', '$id_cliente', '$id_pedido', '$descricao', '$formapag', '$data', '$datavencimento', '$valor', '$status', '$data_exclusao')";

		    $sql = mysqli_query($con, $query) or die (mysqli_error($con));

			//aqui é a exclusão da linha na tabela
			$sql_exclui = "delete FROM cr where id_cr = '$codigos'";
			$res2 = mysqli_query($con, $sql_exclui);

			
		}

		echo"<script language=\"JavaScript\">
			location.href=\"listarcr.php\";
			</script>";
	}


	if ($_POST['btn_sel'] == "Excluir") {

		foreach ($_POST['sel'] as $codigos) {

			$id_hehe = $_POST['id'];
	
			$sqlFeatch = mysqli_query($con, "select * from cr where id_cr = '$codigos'");

			$vetor = mysqli_fetch_array($sqlFeatch);

			$id_cr 			=	$vetor['id_cr'];
			$tipo 			=	$vetor['tipo'];
			$id_cliente 	=	$vetor['id_cliente'];
			$id_pedido 		=	$vetor['id_pedido'];
			
			if ($id_pedido == null) {
				$id_pedido = 0;
			}

			$descricao 		=	$vetor['descricao'];
			$formapag 		=	$vetor['formapag'];
			$data 			=	$vetor['data'];
		    $datavencimento = 	$vetor['datavencimento'];
		    $valor 			=	$vetor['valor'];
		    $valorpago 		=	$vetor['valorpago'];
		    $status 		=	$vetor['status'];
		    $data_exclusao  =   date('Y-m-d');
			
			
			$query = "insert into temporario (id_cr, tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status, data_exclusao) VALUES ('$id_cr' , '$tipo', '$id_cliente', '$id_pedido', '$descricao', '$formapag', '$data', '$datavencimento', '$valor', '$status', '$data_exclusao')";

		    $sql = mysqli_query($con, $query) or die (mysqli_error($con));

			//aqui é a exclusão da linha na tabela
			$sql_exclui = "delete FROM cr where id_cr = '$codigos'";
			$res2 = mysqli_query($con, $sql_exclui);

			
		}


		echo"<script language=\"JavaScript\">
			location.href=\"alterarcliente.php?id=". $id_hehe ."\"</script>";
	}
	

/*

echo "<script> alert('Excluido com sucesso!')</script>";
echo "<script> window.location.href='listarcr.php'</script>";

*/

}else{

	echo"<script language=\"JavaScript\">
	location.href=\"listarcr.php\";
	</script>";
}