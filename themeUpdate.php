<?php

session_start();

include 'includes/conexao.php';

$theme = ltrim(addslashes($_POST['theme']));

$sql = "UPDATE usuarios SET theme = ? WHERE id_user = ?";
$sql = $conn->prepare($sql);
$sql->execute([$theme, $_SESSION['id']]);

header("Location: index.php");