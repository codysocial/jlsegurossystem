<?php

error_reporting(0);
session_start();
include"includes/conexao.php";

function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
}

$tipocad = $_POST['tipocad'];
$data = date('Y-m-d');

$cpfcnpj = $_POST['cpfcnpj'];
$cpfcnpj = str_replace('.', '', $cpfcnpj);
$cpfcnpj = str_replace('-', '', $cpfcnpj);
$cpfcnpj = str_replace('/', '', $cpfcnpj);

// VERIFICAR SE EXISTE O CPF/CNPJ
$sql = $conn->prepare("SELECT * FROM clientes WHERE cpfcnpj = ?");
$sql->execute([$cpfcnpj]);

if ($sql->rowCount() > 0) {
	echo json_encode(array('error' => 404, 'tipocad' => 404));
}else{

if($tipocad == 1) {

		$tipoindenizacao = $_POST['tipoindenizacao'];
		//contrato

		$datainstalacao = $_POST['datainstalacao'];
		$periodo = $_POST['periodo'];
		$vigenciainicio = $_POST['datainstalacao'];
		$timestamp1 = strtotime($vigenciainicio . "+1 year");
		$vigenciafim1 = date('Y-m-d', $timestamp1);
		$timestamp = strtotime($vigenciainicio . "+2 year");
		$vigenciafim = date('Y-m-d', $timestamp);

		//cadastro cliente
		$id_corretor = $_POST['id_corretor'];
		$id_indicador = $_POST['id_indicador'];
		$nome = strtoupper($_POST['nome']);
		$rg = $_POST['rg'];
		$cep = $_POST['cep'];
		$endereco = strtoupper($_POST['endereco']);
		$numero = $_POST['numero'];
		$complemento = strtoupper($_POST['complemento']);
		$bairro = strtoupper($_POST['bairro']);
		$cidade = strtoupper($_POST['cidade']);
		$estado = strtoupper($_POST['estado']);
		$telefone = $_POST['telefone'];
		$celular = $_POST['celular'];
		$email = strtoupper($_POST['email']);
		$empresa = strtoupper($_POST['empresa']);
		$telempresa = $_POST['telempresa'];
		$datanasc = $_POST['datanasc'];
		$sexo = $_POST['sexo'];
		$nomesocial = $_POST['nomesocial'];

	$sql_cliente = mysqli_query($con, "insert into clientes (nomefant, genero, nome, cpfcnpj, rg, cep, endereco, numero, complemento, bairro, cidade, estado, telefone, celular, email, empresa, telempresa, datanasc, tipocad, vendedor, datacad) VALUES ('$nomesocial', '$sexo', '$nome', '$cpfcnpj', '$rg', '$cep', '$endereco', '$numero', '$complemento', '$bairro', '$cidade', '$estado', '$telefone', '$celular', '$email', '$empresa', '$telempresa', '$datanasc', '1', '$id_corretor', '$data')") or die (mysqli_error($con));

			$id_cliente = $con->insert_id;

			$sql_vigencia = mysqli_query($con, "insert into renovacoes (id_cliente, data1, data2) VALUES ('$id_cliente', '$vigenciafim1', '$vigenciafim')");

			//cadastro endereço instalação

			$cep1 = $_POST['cep1'];
			$endereco1 = strtoupper($_POST['endereco1']);
			$numero1 = $_POST['numero1'];
			$complemento1 = strtoupper($_POST['complemento1']);
			$bairro1 = strtoupper($_POST['bairro1']);
			$cidade1 = strtoupper($_POST['cidade1']);
			$estado1 = strtoupper($_POST['estado1']);
			$observacoes1 = strtoupper($_POST['observacoes1']);

			if(!empty($cep1)) {

				$sql_endereco_instalacao = mysqli_query($con, "insert into cliente_instalacao (id_cliente, cep, endereco, numero, complemento, bairro, cidade, estado, observacoes) VALUES ('$id_cliente', '$cep1', '$endereco1', '$numero1', '$complemento1', '$bairro1', '$cidade1', '$estado1', '$observacoes1')") or die (mysqli_error($con));
			}

			//cadastro referencia cliente

			$nomep = strtoupper($_POST['nomep']);
			$parentesco = strtoupper($_POST['parentesco']);
			$telefonep = $_POST['telefonep'];
			$nomep1 = strtoupper($_POST['nomep1']);
			$parentesco1 = strtoupper($_POST['parentesco1']);
			$telefonep1 = $_POST['telefonep1'];

			if(!empty($nomep)) {

				$sql_parentesco = mysqli_query($con, "insert into cliente_referencias (id_cliente, nome, parentesco, telefone) VALUES ('$id_cliente', '$nomep', '$parentesco', '$telefonep')") or die (mysqli_error($con));

			}

			if(!empty($nomep1)) {

				$sql_parentesco1 = mysqli_query($con, "insert into cliente_referencias (id_cliente, nome, parentesco, telefone) VALUES ('$id_cliente', '$nomep1', '$parentesco1', '$telefonep1')") or die (mysqli_error($con));

			}

			//cadastro veiculo

			$tipo_veiculo = $_POST['tipo_veiculo'];

			if($tipo_veiculo == 1) {

				$marca = $_POST['marca'];

				$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
				$vetor_marca = mysqli_fetch_array($sql_marca);

				$modelos = $_POST['modelos'];

				$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
				$vetor_modelo = mysqli_fetch_array($sql_modelo);

				$anomod = $_POST['anomod'];

				$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
				$vetor_anomod = mysqli_fetch_array($sql_anomod);

				$valor = moeda($_POST['valor']);

				if($valor == NULL) {

					//$valorcarro = $vetor_anomod['valor'];

				} else {

					$valorcarro = $valor;

				}

				$cor = strtoupper($_POST['cor']);
				$placa = strtoupper($_POST['placa']);
				$combustivel = strtoupper($_POST['combustivel']);
				$renavan = strtoupper($_POST['renavan']);
				$chassi = strtoupper($_POST['chassi']);

				$sql_veiculo = mysqli_query($con, "insert into cliente_veiculo (id_cliente, tipo, modelo, marca, valor, cor, combustivel, placa, renavan, chassi, anomod) VALUES ('$id_cliente', '$tipo_veiculo', '$vetor_modelo[modelo]', '$marca', '$valorcarro', '$cor', '$combustivel', '$placa', '$renavan', '$chassi', '$anomod')") or die (mysqli_error($con));

				$id_veiculo = $con->insert_id;

			} if($tipo_veiculo == 2) {

				$marca = $_POST['marca1'];

				$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
				$vetor_marca = mysqli_fetch_array($sql_marca);

				$modelos = $_POST['modelos1'];

				$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
				$vetor_modelo = mysqli_fetch_array($sql_modelo);

				$anomod = $_POST['anomod1'];

				$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
				$vetor_anomod = mysqli_fetch_array($sql_anomod);

				$valor = moeda($_POST['valor1']);

				if($valor == NULL) {

					//$valorcarro = $vetor_anomod['valor'];

				} else {

					$valorcarro = $valor;

				}

				$cor = strtoupper($_POST['cor1']);
				$placa = strtoupper($_POST['placa1']);
				$combustivel = strtoupper($_POST['combustivel1']);
				$renavan = strtoupper($_POST['renavan1']);
				$chassi = strtoupper($_POST['chassi1']);

				$sql_veiculo = mysqli_query($con, "insert into cliente_veiculo (id_cliente, tipo, modelo, marca, valor, cor, combustivel, placa, renavan, chassi, anomod) VALUES ('$id_cliente', '$tipo_veiculo', '$vetor_modelo[modelo]', '$marca', '$valorcarro', '$cor', '$combustivel', '$placa', '$renavan', '$chassi', '$anomod')") or die (mysqli_error($con));

				$id_veiculo = $con->insert_id;

			} if($tipo_veiculo == 3) {

				$marca = $_POST['marca2'];

				$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
				$vetor_marca = mysqli_fetch_array($sql_marca);

				$modelos = $_POST['modelos2'];

				$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
				$vetor_modelo = mysqli_fetch_array($sql_modelo);

				$anomod = $_POST['anomod2'];

				$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
				$vetor_anomod = mysqli_fetch_array($sql_anomod);

				$valor = moeda($_POST['valor2']);

				if($valor == NULL) {

					//$valorcarro = $vetor_anomod['valor'];

				} else {

					$valorcarro = $valor;

				}

				$cor = strtoupper($_POST['cor2']);
				$placa = strtoupper($_POST['placa2']);
				$combustivel = strtoupper($_POST['combustivel2']);
				$renavan = strtoupper($_POST['renavan2']);
				$chassi = strtoupper($_POST['chassi2']);

				$sql_veiculo = mysqli_query($con, "insert into cliente_veiculo (id_cliente, tipo, modelo, marca, valor, cor, combustivel, placa, renavan, chassi, anomod) VALUES ('$id_cliente', '$tipo_veiculo', '$vetor_modelo[modelo]', '$marca', '$valorcarro', '$cor', '$combustivel', '$placa', '$renavan', '$chassi', '$anomod')") or die (mysqli_error($con));

				$id_veiculo = $con->insert_id;

			}

			$adesao = moeda($_POST['adesao']);
			$parcelar = $_POST['parcelar'];
			$formapag = $_POST['formapag'];
			$vencimentoadesao = $_POST['vencimentoadesao'];
			$formapag_1 = $_POST['formapag_1'];
			$vencimentoadesao_1 = $_POST['vencimentoadesao_1'];
			$formapag_2 = $_POST['formapag_2'];
			$vencimentoadesao2 = $_POST['vencimentoadesao2'];
			$formapag_3 = $_POST['formapag_3'];
			$vencimentoadesao3 = $_POST['vencimentoadesao3'];
			$mensalidade = moeda($_POST['mensalidade']);
			$mensalidade1 = $_POST['1mensalidade'];
			$demaismensalidade = $_POST['demaismensalidade'];
			$formapagmensalidade = $_POST['formapagmensalidade'];
			$vacina = moeda($_POST['vacina']);
			$vencimentovicina = $_POST['vencimentovicina'];
			$formapagvacina = $_POST['formapagvacina'];
			$instalacao = moeda($_POST['instalacao']);
			$vencimentoinstalacao = $_POST['vencimentoinstalacao'];
			$formapaginst = $_POST['formapaginst'];
			$assistencia = moeda($_POST['assistencia']);
			$parcelara24 = $_POST['parcelara24'];

			$formapaga24 = $_POST['formapaga24'];
			$vencimentoa24 = $_POST['vencimentoa24'];
			$formapag_a24 = $_POST['formapag_a241'];
			$vencimentoa241 = $_POST['vencimentoa24_1'];
			$formapag_a242 = $_POST['formapag_a242'];
			$vencimentoa242 = $_POST['vencimentoa242'];
			$formapag_a243 = $_POST['formapag_a243'];
			$vencimentoa243 = $_POST['vencimentoa243'];

			$assistenciaterceiro = moeda($_POST['assistenciaterceiro']);
			$parcelarat = $_POST['parcelarat'];

			$formapagat = $_POST['formapagat'];
			$vencimentoat = $_POST['vencimentoat'];
			$formapag_at1 = $_POST['formapag_at1'];
			$vencimentoat_1 = $_POST['vencimentoat_1'];
			$formapag_at2 = $_POST['formapag_at2'];
			$vencimentoat2 = $_POST['vencimentoat2'];
			$formapag_at3 = $_POST['formapag_at3'];
			$vencimentoat3 = $_POST['vencimentoat3'];

			$carroreserva = moeda($_POST['carroreserva']);
			$parcelascarro = $_POST['parcelascarro'];

			$formapagcarro = $_POST['formapagcarro'];
			$vencimentocarro1 = $_POST['vencimentocarro1'];
			$formapagcarro1 = $_POST['formapagcarro1'];
			$vencimentocarro2 = $_POST['vencimentocarro2'];
			$formapagcarro2 = $_POST['formapagcarro2'];
			$vencimentocarro3 = $_POST['vencimentocarro3'];
			$formapagcarro3 = $_POST['formapagcarro3'];
			$vencimentocarro4 = $_POST['vencimentocarro4'];

			$carroapp = moeda($_POST['carroapp']);
			$parcelascarroapp = $_POST['parcelascarroapp'];

			$formapagcarroapp = $_POST['formapagcarroapp'];
			$vencimentocarroapp1 = $_POST['vencimentocarroapp1'];
			$formapagcarroapp1 = $_POST['formapagcarroapp1'];
			$vencimentocarroapp2 = $_POST['vencimentocarroapp2'];
			$formapagcarroapp2 = $_POST['formapagcarroapp2'];
			$vencimentocarroapp3 = $_POST['vencimentocarroapp3'];
			$formapagcarroapp3 = $_POST['formapagcarroapp3'];
			$vencimentocarroapp4 = $_POST['vencimentocarroapp4'];

			$outras = strtoupper($_POST['outras']);
			$observacoes = strtoupper($_POST['observacoes']);

			$qtdmensalidade = 12;

			$valoradesao = $adesao / $parcelar;
			$valora24 = $assistencia / $parcelara24;
			$valoraat = $assistenciaterceiro / $parcelarat;
			$valorcarrofinal = $carroreserva / $parcelascarro;
			$valorcarroapp = $carroapp / $parcelascarroapp;

			$valormensalidade = $mensalidade * 12;

			if($parcelar == 12) { 

				$valormensalidade += $adesao;

			}

			if($parcelara24 == 12) { 

				$valormensalidade += $assistencia;

			}

			if($parcelarat == 12) { 

				$valormensalidade += $assistenciaterceiro;

			}

			if($parcelascarro == 12) { 

				$valormensalidade += $carroreserva;

			}

			if($parcelascarroapp == 12) { 

				$valormensalidade += $carroapp;

			}

			$sql_produto = mysqli_query($con, "select * from produtos where id_produto = '$id_produto'") or die (mysqli_error($con));
			$vetor_produto = mysqli_fetch_array($sql_produto);

			$sql_pedido = mysqli_query($con, "insert into pedidos (tipo, vigenciainicio, vigenciafim, id_cliente, id_corretor, id_indicador, id_veiculo, data, adesao, parcelar, formapag, vencimentoadesao, formapag1, vencimentoadesao1, formapag2, vencimentoadesao2, formapag3, vencimentoadesao3, mensalidade, 1mensalidade, demaismensalidade, formapagmensalidade, vacina, vencimentovicina, formapagvacina, instalacao, vencimentoinstalacao, formapaginst, assistencia, parcelara24h, vencimentoassistencia, assistenciaterceiro, parcelarat, vencimentoassistenciaterceiro, observacoes, carroreserva, parcelascarro, formapagcarro, vencimentocarro1, formapagcarro1, vencimentocarro2, formapagcarro2, vencimentocarro3, formapagcarro3, vencimentocarro4, carroapp, parcelascarroapp, formapagcarroapp, vencimentocarroapp1, formapagcarroapp1, vencimentocarroapp2, formapagcarroapp2, vencimentocarroapp3, formapagcarroapp3, vencimentocarroapp4, outras, comissao, comissaoind, comissaotec, status) VALUES ('$tipoindenizacao', '$vigenciainicio', '$vigenciafim', '$id_cliente', '$id_corretor', '$id_indicador', '$id_veiculo', '$data', '$adesao', '$parcelar', '$formapag', '$vencimentoadesao', '$formapag_1', '$vencimentoadesao_1', '$formapag_2', '$vencimentoadesao2', '$formapag_3', '$vencimentoadesao3', '$mensalidade', '$mensalidade1', '$demaismensalidade', '$formapagmensalidade', '$vacina', '$vencimentovicina', '$formapagvacina', '$instalacao', '$vencimentoinstalacao', '$formapaginst', '$assistencia', '$parcelara24', '$vencimentoassistencia', '$assistenciaterceiro', '$parcelarat', '$vencimentoassistenciaterceiro', '$observacoes', '$carroreserva', '$parcelascarro', '$formapagcarro', '$vencimentocarro1', '$formapagcarro1', '$vencimentocarro2', '$formapagcarro2', '$vencimentocarro3', '$formapagcarro3', '$vencimentocarro4', '$carroapp', '$parcelascarroapp', '$formapagcarroapp', '$vencimentocarroapp1', '$formapagcarroapp1', '$vencimentocarroapp2', '$formapagcarroapp2', '$vencimentocarroapp3', '$formapagcarroapp3', '$vencimentocarroapp4', '$outras', '1', '1', '1', '1')") or die (mysqli_error($con));

			$id_pedido = $con->insert_id;

			$consulta_os = mysqli_query($con, "select * from os order by id_os DESC limit 0,1") or die (mysqli_error($con));
			$vetor_consulta = mysqli_fetch_array($consulta_os);

			$nos = $vetor_consulta['nos'] + 1;

			/*
				$sql_os = mysqli_query($con, "insert into os (tipocad, id_pedido, nos, datasolicitacao, periodo, datainstalacao, rastreador, nrastreador, contrato, chipop, rastreadormod, fabricante) VALUES ('1', '$id_pedido', '$nos', '$data', '$periodo', '$datainstalacao', '$id_produto', '$nrastreador', '$id_pedido', '$chipop', '$rastreadormod', '$fabricante')") or die (mysqli_error($con));
			*/
			/// $id_produto , $nrastreador, $chipop, $rastreadormod, $fabricante

			$id_rastreador = $_POST['id_rastreador1'];

			$sql_rastreador1 = mysqli_query($con, "SELECT * from rastreador where id_rastreador = $id_rastreador");
			$vetor_rastreador1 = mysqli_fetch_array($sql_rastreador1);

			$nrastreador = $vetor_rastreador1['imei'];
			$rastreadormod = $vetor_rastreador1['modelo'];
			$fabricante = $vetor_rastreador1['fabricante'];

			$sql_vinculado = mysqli_query($con, "SELECT * from rastreador_chip WHERE id_rastreador = '$id_rastreador' ");
			$vetor_vinculado = mysqli_fetch_array($sql_vinculado);

			$id_chip1 = $vetor_vinculado['id_chip'];

			$sql_chip1 = mysqli_query($con, "SELECT * from chips where id_chip = $id_chip1");
			$vetor_chip1 = mysqli_fetch_array($sql_chip1);

			$chipop = $vetor_chip1['operadora'];

			$sql_atualiza_estoque = mysqli_query($con, "update produtos SET estoque = estoque - 1 where id_produto = '$rastreadormod'");

			$sql_atualiza_estoque1 = mysqli_query($con, "update produtos SET estoque = estoque - 1 where id_produto = '$chipop'");


			$sql_os = mysqli_query($con, "insert into os (tipocad, id_pedido, nos, datasolicitacao, periodo, datainstalacao, rastreador, nrastreador, contrato, chipop, rastreadormod, fabricante) VALUES ('1', '$id_pedido', '$nos', '$data', '$periodo', '$datainstalacao', '$id_rastreador', '$nrastreador', '$id_pedido', '$chipop', '$rastreadormod', '$fabricante')") or die (mysqli_error($con));

			$sql_vinculacao1 = mysqli_query($con, "INSERT into vinculo_rastreador (id_rastreador, id_chip, id_cliente, id_pedido) VALUES ('$id_rastreador', '$id_chip1', '$id_cliente', '$id_pedido')") or die (mysqli_error($con));

			$sql_updatesitu = mysqli_query($con, "UPDATE chips SET situacao = '3' WHERE id_chip = '$id_chip1'");
			$sql_updatesitu = mysqli_query($con, "UPDATE rastreador SET situacao = '3' WHERE id_rastreador = '$id_rastreador'");

			date_default_timezone_set('America/Sao_Paulo');
			$data_vinculo = date('Y/m/d H:i:s');

			$update_datapedido = mysqli_query($con, "UPDATE rastreador_chip SET data_pedido = '$data_vinculo' WHERE id_rastreador = '$id_rastreador' ");


			//gerar financeiro

			if($parcelar != 12) {

				if($formapag != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '1° Parcela Adesão Pedido: $id_pedido', '$formapag', '$data', '$vencimentoadesao', '$valoradesao', '1')");

				}

				if($formapag_1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '2° Parcela Adesão Pedido: $id_pedido', '$formapag_1', '$data', '$vencimentoadesao_1', '$valoradesao', '1')");

				}

				if($formapag_2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '3° Parcela Adesão Pedido: $id_pedido', '$formapag_2', '$data', '$vencimentoadesao2', '$valoradesao', '1')");

				}

				if($formapag_3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '4° Parcela Adesão Pedido: $id_pedido', '$formapag_3', '$data', '$vencimentoadesao3', '$valoradesao', '1')");

				}

			}

			if($parcelara24 != 12) {

				if($formapaga24 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '1° Parcela Assistencia 24H Pedido: $id_pedido', '$formapaga24', '$data', '$vencimentoa24', '$valora24', '1')");

				}

				if($formapag_a24 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '2° Parcela Assistencia 24H Pedido: $id_pedido', '$formapag_a24', '$data', '$vencimentoa241', '$valora24', '1')");

				}

				if($formapag_a242 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '3° Parcela Assistencia 24H Pedido: $id_pedido', '$formapag_a242', '$data', '$vencimentoa242', '$valora24', '1')");

				}

				if($formapag_a243 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '4° Parcela Assistencia 24H Pedido: $id_pedido', '$formapag_a243', '$data', '$vencimentoa243', '$valora24', '1')");

				}

			}

			if($parcelarat != 12) {

				if($formapagat != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '1° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapagat', '$data', '$vencimentoat', '$valoraat', '1')");

				}

				if($formapag_at1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '2° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapag_at1', '$data', '$vencimentoat_1', '$valoraat', '1')");

				}

				if($formapag_at2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '3° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapag_at2', '$data', '$vencimentoat2', '$valoraat', '1')");

				}

				if($formapag_at3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '4° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapag_at3', '$data', '$vencimentoat3', '$valoraat', '1')");

				}

			}

			if($parcelascarro != 12) {

				if($formapagcarro != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '1° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro', '$data', '$vencimentocarro1', '$valorcarrofinal', '1')");

				}

				if($formapagcarro1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '2° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro1', '$data', '$vencimentocarro2', '$valorcarrofinal', '1')");

				}

				if($formapagcarro2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '3° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro2', '$data', '$vencimentocarro3', '$valorcarrofinal', '1')");

				}

				if($formapagcarro3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '4° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro3', '$data', '$vencimentocarro4', '$valorcarrofinal', '1')");

				}

			}

			if($parcelascarroapp != 12) {

				if($formapagcarroapp != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '1° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarroapp', '$data', '$vencimentocarroapp1', '$valorcarroapp', '1')");

				}

				if($formapagcarroapp1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '2° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarro1', '$data', '$vencimentocarroapp2', '$valorcarroapp', '1')");

				}

				if($formapagcarroapp2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '3° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarroapp2', '$data', '$vencimentocarroapp3', '$valorcarroapp', '1')");

				}

				if($formapagcarroapp3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '4° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarroapp3', '$data', '$vencimentocarroapp4', '$valorcarroapp', '1')");

				}

			}

			$DataInicial = new \DateTime($demaismensalidade);

			// Intervalo de 1 mês
			$Intervalo = new \DateInterval( 'P1M' );

			// Data daqui a 2 anos, apenas para aumentar a amostragem
			$DataFinal = new \DateTime( 'today + 11 month' );

			$Periodo = new \DatePeriod( $DataInicial, $Intervalo, $DataFinal );

			$valorcontrato = $valormensalidade / 12;

			$sql_cr_mensalidade_inicio = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('1', '$id_cliente', '$id_pedido', '1° Parcela Mensalidade Pedido: $id_pedido', '$formapagmensalidade', '$data', '$mensalidade1', '$valorcontrato', '1')");


			if($vacina != NULL) {

				$sql_vacina = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('6', '$id_cliente', '$id_pedido', 'Vacina do Pedido: $id_pedido', '$formapagvacina', '$data', '$vencimentovicina', '$vacina', '1')");

			}

			if($instalacao != NULL) {

				$sql_vacina = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('7', '$id_cliente', '$id_pedido', 'Instalação do Pedido: $id_pedido', '$formapaginst', '$data', '$vencimentoinstalacao', '$instalacao', '1')");

			}

			} else { 

			$tipoindenizacao = $_POST['tipoindenizacao1'];

			//contrato
			$id_corretor = $_POST['id_corretor1'];
			$id_indicador = $_POST['id_indicador1'];

			$datainstalacao = $_POST['datainstalacao1'];
			$chipop = $vetor_modelo_chip['chipprimario'];
			$periodo = $_POST['periodo1'];
			$vigenciainicio = $_POST['datainstalacao1'];
			$timestamp1 = strtotime($vigenciainicio . "+1 year");
			$vigenciafim1 = date('Y-m-d', $timestamp1);
			$timestamp = strtotime($vigenciainicio . "+2 year");
			$vigenciafim = date('Y-m-d', $timestamp);

			$id_cliente = $_POST['id_cliente'];

			//cadastro endereço instalação

			$cep1 = $_POST['cep11'];
			$endereco1 = strtoupper($_POST['endereco11']);
			$numero1 = $_POST['numero11'];
			$complemento1 = strtoupper($_POST['complemento11']);
			$bairro1 = strtoupper($_POST['bairro11']);
			$cidade1 = strtoupper($_POST['cidade11']);
			$estado1 = strtoupper($_POST['estado11']);
			$observacoes1 = strtoupper($_POST['observacoes11']);

			if(!empty($cep1)) {

				$sql_endereco_instalacao = mysqli_query($con, "insert into cliente_instalacao (id_cliente, cep, endereco, numero, complemento, bairro, cidade, estado, observacoes) VALUES ('$id_cliente', '$cep1', '$endereco1', '$numero1', '$complemento1', '$bairro1', '$cidade1', '$estado1', '$observacoes1')") or die (mysqli_error($con));
			}

			$sql_vigencia = mysqli_query($con, "insert into renovacoes (id_cliente, data1, data2) VALUES ('$id_cliente', '$vigenciafim1', '$vigenciafim')");

			//cadastro referencia cliente

			$nomep = strtoupper($_POST['nomep1']);
			$parentesco = strtoupper($_POST['parentesco1']);
			$telefonep = $_POST['telefonep1'];
			$nomep1 = strtoupper($_POST['nomep11']);
			$parentesco1 = strtoupper($_POST['parentesco11']);
			$telefonep1 = $_POST['telefonep11'];

			if(!empty($nomep1)) {

				$sql_parentesco = mysqli_query($con, "insert into cliente_referencias (id_cliente, nome, parentesco, telefone) VALUES ('$id_cliente', '$nomep', '$parentesco', '$telefonep')") or die (mysqli_error($con));

			}

			if(!empty($nomep1)) {

				$sql_parentesco1 = mysqli_query($con, "insert into cliente_referencias (id_cliente, nome, parentesco, telefone) VALUES ('$id_cliente', '$nomep1', '$parentesco1', '$telefonep1')") or die (mysqli_error($con));

			}

			//cadastro veiculo

			$tipo_veiculo = $_POST['tipo_veiculo1'];

			if($tipo_veiculo == 1) {

				$marca = $_POST['marca1'];

				$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
				$vetor_marca = mysqli_fetch_array($sql_marca);

				$modelos = $_POST['modelos1'];

				$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
				$vetor_modelo = mysqli_fetch_array($sql_modelo);

				$anomod = $_POST['anomod1'];

				$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
				$vetor_anomod = mysqli_fetch_array($sql_anomod);

				$valor = moeda($_POST['valor1']);

				if($valor == NULL) {

					$valorcarro = $vetor_anomod['valor'];

				} else {

					$valorcarro = $valor;

				}

				$cor = strtoupper($_POST['cor1']);
				$placa = strtoupper($_POST['placa1']);
				$combustivel = strtoupper($_POST['combustivel1']);
				$renavan = strtoupper($_POST['renavan1']);
				$chassi = strtoupper($_POST['chassi1']);

				$sql_veiculo = mysqli_query($con, "insert into cliente_veiculo (id_cliente, tipo, modelo, marca, valor, cor, combustivel, placa, renavan, chassi, anomod) VALUES ('$id_cliente', '$tipo_veiculo', '$vetor_modelo[modelo]', '$vetor_marca[marca]', '$valorcarro', '$cor', '$combustivel', '$placa', '$renavan', '$chassi', '$vetor_anomod[ano]')") or die (mysqli_error($con));

				$id_veiculo = $con->insert_id;

			} if($tipo_veiculo == 2) {

				$marca = $_POST['marca11'];

				$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
				$vetor_marca = mysqli_fetch_array($sql_marca);

				$modelos = $_POST['modelos11'];

				$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
				$vetor_modelo = mysqli_fetch_array($sql_modelo);

				$anomod = $_POST['anomod11'];

				$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
				$vetor_anomod = mysqli_fetch_array($sql_anomod);

				$valor = moeda($_POST['valor11']);

				if($valor == NULL) {

					$valorcarro = $vetor_anomod['valor'];

				} else {

					$valorcarro = $valor;

				}

				$cor = strtoupper($_POST['cor11']);
				$placa = strtoupper($_POST['placa11']);
				$combustivel = strtoupper($_POST['combustivel11']);
				$renavan = strtoupper($_POST['renavan11']);
				$chassi = strtoupper($_POST['chassi11']);

				$sql_veiculo = mysqli_query($con, "insert into cliente_veiculo (id_cliente, tipo, modelo, marca, valor, cor, combustivel, placa, renavan, chassi, anomod) VALUES ('$id_cliente', '$tipo_veiculo', '$vetor_modelo[modelo]', '$vetor_marca[marca]', '$valorcarro', '$cor', '$combustivel', '$placa', '$renavan', '$chassi', '$vetor_anomod[ano]')") or die (mysqli_error($con));

				$id_veiculo = $con->insert_id;

			} if($tipo_veiculo == 3) {

				$marca = $_POST['marca21'];

				$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
				$vetor_marca = mysqli_fetch_array($sql_marca);

				$modelos = $_POST['modelos21'];

				$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
				$vetor_modelo = mysqli_fetch_array($sql_modelo);

				$anomod = $_POST['anomod21'];

				$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
				$vetor_anomod = mysqli_fetch_array($sql_anomod);

				$valor = moeda($_POST['valor21']);

				if($valor == NULL) {

					$valorcarro = $vetor_anomod['valor'];

				} else {

					$valorcarro = $valor;

				}

				$cor = strtoupper($_POST['cor21']);
				$placa = strtoupper($_POST['placa21']);
				$combustivel = strtoupper($_POST['combustivel21']);
				$renavan = strtoupper($_POST['renavan21']);
				$chassi = strtoupper($_POST['chassi21']);

				$sql_veiculo = mysqli_query($con, "insert into cliente_veiculo (id_cliente, tipo, modelo, marca, valor, cor, combustivel, placa, renavan, chassi, anomod) VALUES ('$id_cliente', '$tipo_veiculo', '$vetor_modelo[modelo]', '$vetor_marca[marca]', '$valorcarro', '$cor', '$combustivel', '$placa', '$renavan', '$chassi', '$vetor_anomod[ano]')") or die (mysqli_error($con));

				$id_veiculo = $con->insert_id;

			}

			$adesao = moeda($_POST['adesao1']);
			$parcelar = $_POST['parcelar1'];
			$formapag = $_POST['formapag1'];
			$vencimentoadesao = $_POST['vencimentoadesao1'];
			$formapag_1 = $_POST['formapag_11'];
			$vencimentoadesao_1 = $_POST['vencimentoadesao_11'];
			$formapag_2 = $_POST['formapag_21'];
			$vencimentoadesao2 = $_POST['vencimentoadesao21'];
			$formapag_3 = $_POST['formapag_31'];
			$vencimentoadesao3 = $_POST['vencimentoadesao31'];
			$mensalidade = moeda($_POST['mensalidade1']);
			$mensalidade1 = $_POST['1mensalidade1'];
			$demaismensalidade = $_POST['demaismensalidade1'];
			$formapagmensalidade = $_POST['formapagmensalidade1'];
			$vacina = moeda($_POST['vacina1']);
			$vencimentovicina = $_POST['vencimentovicina1'];
			$formapagvacina = $_POST['formapagvacina1'];
			$instalacao = moeda($_POST['instalacao1']);
			$vencimentoinstalacao = $_POST['vencimentoinstalacao1'];
			$formapaginst = $_POST['formapaginst1'];
			$assistencia = moeda($_POST['assistencia1']);
			$parcelara24 = $_POST['parcelara241'];

			$formapaga24 = $_POST['formapaga241'];
			$vencimentoa24 = $_POST['vencimentoa241'];
			$formapag_a24 = $_POST['formapag_a2411'];
			$vencimentoa241 = $_POST['vencimentoa24_11'];
			$formapag_a242 = $_POST['formapag_a2421'];
			$vencimentoa242 = $_POST['vencimentoa2421'];
			$formapag_a243 = $_POST['formapag_a2431'];
			$vencimentoa243 = $_POST['vencimentoa2431'];

			$assistenciaterceiro = moeda($_POST['assistenciaterceiro1']);
			$parcelarat = $_POST['parcelarat1'];

			$formapagat = $_POST['formapagat1'];
			$vencimentoat = $_POST['vencimentoat1'];
			$formapag_at1 = $_POST['formapag_at11'];
			$vencimentoat_1 = $_POST['vencimentoat_11'];
			$formapag_at2 = $_POST['formapag_at21'];
			$vencimentoat2 = $_POST['vencimentoat21'];
			$formapag_at3 = $_POST['formapag_at31'];
			$vencimentoat3 = $_POST['vencimentoat31'];

			$carroreserva = moeda($_POST['carroreserva_1']);
			$parcelascarro = $_POST['parcelascarro_1'];

			$formapagcarro = $_POST['formapagcarro_1'];
			$vencimentocarro1 = $_POST['vencimentocarro1_1'];
			$formapagcarro1 = $_POST['formapagcarro1_1'];
			$vencimentocarro2 = $_POST['vencimentocarro2_1'];
			$formapagcarro2 = $_POST['formapagcarro2_1'];
			$vencimentocarro3 = $_POST['vencimentocarro3_1'];
			$formapagcarro3 = $_POST['formapagcarro3_1'];
			$vencimentocarro4 = $_POST['vencimentocarro4_1'];

			$carroapp = moeda($_POST['carroapp_1']);
			$parcelascarroapp = $_POST['parcelascarroapp_1'];

			$formapagcarroapp = $_POST['formapagcarroapp_1'];
			$vencimentocarroapp1 = $_POST['vencimentocarroapp1_1'];
			$formapagcarroapp1 = $_POST['formapagcarroapp1_1'];
			$vencimentocarroapp2 = $_POST['vencimentocarroapp2_1'];
			$formapagcarroapp2 = $_POST['formapagcarroapp2_1'];
			$vencimentocarroapp3 = $_POST['vencimentocarroapp3_1'];
			$formapagcarroapp3 = $_POST['formapagcarroapp3_1'];
			$vencimentocarroapp4 = $_POST['vencimentocarroapp4_1'];

			$outras = strtoupper($_POST['outras1']);
			$observacoes = strtoupper($_POST['observacoes1']);

			$qtdmensalidade = 12;

			$valoradesao = $adesao / $parcelar;
			$valora24 = $assistencia / $parcelara24;
			$valoraat = $assistenciaterceiro / $parcelarat;
			$valorcarrofinal = $carroreserva / $parcelascarro;
			$valorcarroapp = $carroapp / $parcelascarroapp;

			$valormensalidade = $mensalidade * 12;

			if($parcelar == 12) { 

				$valormensalidade += $adesao;

			}

			if($parcelara24 == 12) { 

				$valormensalidade += $assistencia;

			}

			if($parcelarat == 12) { 

				$valormensalidade += $assistenciaterceiro;

			}

			if($parcelascarro == 12) { 

				$valormensalidade += $carroreserva;

			}

			if($parcelascarroapp == 12) { 

				$valormensalidade += $carroapp;

			}

			$sql_produto = mysqli_query($con, "select * from produtos where id_produto = '$id_produto'") or die (mysqli_error($con));
			$vetor_produto = mysqli_fetch_array($sql_produto);

			$sql_pedido = mysqli_query($con, "insert into pedidos (tipo, vigenciainicio, vigenciafim, id_cliente, id_corretor, id_indicador, id_veiculo, data, adesao, parcelar, formapag, vencimentoadesao, formapag1, vencimentoadesao1, formapag2, vencimentoadesao2, formapag3, vencimentoadesao3, mensalidade, 1mensalidade, demaismensalidade, formapagmensalidade, vacina, vencimentovicina, formapagvacina, instalacao, vencimentoinstalacao, formapaginst, assistencia, parcelara24h, vencimentoassistencia, assistenciaterceiro, parcelarat, vencimentoassistenciaterceiro, observacoes, carroreserva, parcelascarro, formapagcarro, vencimentocarro1, formapagcarro1, vencimentocarro2, formapagcarro2, vencimentocarro3, formapagcarro3, vencimentocarro4, carroapp, parcelascarroapp, formapagcarroapp, vencimentocarroapp1, formapagcarroapp1, vencimentocarroapp2, formapagcarroapp2, vencimentocarroapp3, formapagcarroapp3, vencimentocarroapp4, outras, comissao, comissaoind, comissaotec, status) VALUES ('$tipoindenizacao', '$vigenciainicio', '$vigenciafim', '$id_cliente', '$id_corretor', '$id_indicador', '$id_veiculo', '$data', '$adesao', '$parcelar', '$formapag', '$vencimentoadesao', '$formapag_1', '$vencimentoadesao_1', '$formapag_2', '$vencimentoadesao2', '$formapag_3', '$vencimentoadesao3', '$mensalidade', '$mensalidade1', '$demaismensalidade', '$formapagmensalidade', '$vacina', '$vencimentovicina', '$formapagvacina', '$instalacao', '$vencimentoinstalacao', '$formapaginst', '$assistencia', '$parcelara24', '$vencimentoassistencia', '$assistenciaterceiro', '$parcelarat', '$vencimentoassistenciaterceiro', '$observacoes', '$carroreserva', '$parcelascarro', '$formapagcarro', '$vencimentocarro1', '$formapagcarro1', '$vencimentocarro2', '$formapagcarro2', '$vencimentocarro3', '$formapagcarro3', '$vencimentocarro4', '$carroapp', '$parcelascarroapp', '$formapagcarroapp', '$vencimentocarroapp1', '$formapagcarroapp1', '$vencimentocarroapp2', '$formapagcarroapp2', '$vencimentocarroapp3', '$formapagcarroapp3', '$vencimentocarroapp4', '$outras', '1', '1', '1', '1')") or die (mysqli_error($con));

			$id_pedido = $con->insert_id;

			$consulta_os = mysqli_query($con, "select * from os order by id_os DESC limit 0,1") or die (mysqli_error($con));
			$vetor_consulta = mysqli_fetch_array($consulta_os);

			$nos = $vetor_consulta['nos'] + 1;

			/*
				$sql_os = mysqli_query($con, "insert into os (tipocad, id_pedido, nos, datasolicitacao, periodo, datainstalacao, rastreador, nrastreador, contrato, chipop, rastreadormod, fabricante) VALUES ('1', '$id_pedido', '$nos', '$data', '$periodo', '$datainstalacao', '$id_produto', '$nrastreador', '$id_pedido', '$chipop', '$rastreadormod', '$fabricante')") or die (mysqli_error($con));
			*/
			/// $id_produto , $nrastreador, $chipop, $rastreadormod, $fabricante

			$id_rastreador = $_POST['id_rastreador2'];

			$sql_rastreador1 = mysqli_query($con, "SELECT * from rastreador where id_rastreador = $id_rastreador");
			$vetor_rastreador1 = mysqli_fetch_array($sql_rastreador1);

			$nrastreador = $vetor_rastreador1['imei'];
			$rastreadormod = $vetor_rastreador1['modelo'];
			$fabricante = $vetor_rastreador1['fabricante'];

			$sql_vinculado = mysqli_query($con, "SELECT * from rastreador_chip WHERE id_rastreador = '$id_rastreador' ");
			$vetor_vinculado = mysqli_fetch_array($sql_vinculado);

			$id_chip1 = $vetor_vinculado['id_chip'];

			$sql_chip1 = mysqli_query($con, "SELECT * from chips where id_chip = $id_chip1");
			$vetor_chip1 = mysqli_fetch_array($sql_chip1);

			$chipop = $vetor_chip1['operadora'];

			$sql_atualiza_estoque = mysqli_query($con, "update produtos SET estoque = estoque - 1 where id_produto = '$rastreadormod'");

			$sql_atualiza_estoque1 = mysqli_query($con, "update produtos SET estoque = estoque - 1 where id_produto = '$chipop'");


			$sql_os = mysqli_query($con, "insert into os (tipocad, id_pedido, nos, datasolicitacao, periodo, datainstalacao, rastreador, nrastreador, contrato, chipop, rastreadormod, fabricante) VALUES ('1', '$id_pedido', '$nos', '$data', '$periodo', '$datainstalacao', '$id_rastreador', '$nrastreador', '$id_pedido', '$chipop', '$rastreadormod', '$fabricante')") or die (mysqli_error($con));

			$sql_vinculacao1 = mysqli_query($con, "INSERT into vinculo_rastreador (id_rastreador, id_chip, id_cliente, id_pedido) VALUES ('$id_rastreador', '$id_chip1', '$id_cliente', '$id_pedido')") or die (mysqli_error($con));

			$sql_updatesitu = mysqli_query($con, "UPDATE chips SET situacao = '3' WHERE id_chip = '$id_chip1'");
			$sql_updatesitu = mysqli_query($con, "UPDATE rastreador SET situacao = '3' WHERE id_rastreador = '$id_rastreador'");

			date_default_timezone_set('America/Sao_Paulo');
			$data_vinculo1 = date('Y/m/d H:i:s');

			$update_datapedido = mysqli_query($con, "UPDATE rastreador_chip SET data_pedido = '$data_vinculo1' WHERE id_rastreador = '$id_rastreador' ");

			//gerar financeiro

			if($parcelar != 12) {

				if($formapag != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '1° Parcela Adesão Pedido: $id_pedido', '$formapag', '$data', '$vencimentoadesao', '$valoradesao', '1')");

				}

				if($formapag_1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '2° Parcela Adesão Pedido: $id_pedido', '$formapag_1', '$data', '$vencimentoadesao_1', '$valoradesao', '1')");

				}

				if($formapag_2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '3° Parcela Adesão Pedido: $id_pedido', '$formapag_2', '$data', '$vencimentoadesao2', '$valoradesao', '1')");

				}

				if($formapag_3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('2', '$id_cliente', '$id_pedido', '4° Parcela Adesão Pedido: $id_pedido', '$formapag_3', '$data', '$vencimentoadesao3', '$valoradesao', '1')");

				}

			}

			if($parcelara24 != 12) {

				if($formapaga24 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '1° Parcela Assistencia 24H Pedido: $id_pedido', '$formapaga24', '$data', '$vencimentoa24', '$valora24', '1')");

				}

				if($formapag_a24 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '2° Parcela Assistencia 24H Pedido: $id_pedido', '$formapag_a24', '$data', '$vencimentoa241', '$valora24', '1')");

				}

				if($formapag_a242 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '3° Parcela Assistencia 24H Pedido: $id_pedido', '$formapag_a242', '$data', '$vencimentoa242', '$valora24', '1')");

				}

				if($formapag_a243 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('3', '$id_cliente', '$id_pedido', '4° Parcela Assistencia 24H Pedido: $id_pedido', '$formapag_a243', '$data', '$vencimentoa243', '$valora24', '1')");

				}

			}

			if($parcelarat != 12) {

				if($formapagat != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '1° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapagat', '$data', '$vencimentoat', '$valoraat', '1')");

				}

				if($formapag_at1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '2° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapag_at1', '$data', '$vencimentoat_1', '$valoraat', '1')");

				}

				if($formapag_at2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '3° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapag_at2', '$data', '$vencimentoat2', '$valoraat', '1')");

				}

				if($formapag_at3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('4', '$id_cliente', '$id_pedido', '4° Parcela Assistencia A Terceiros Pedido: $id_pedido', '$formapag_at3', '$data', '$vencimentoat3', '$valoraat', '1')");

				}

			}

			if($parcelascarro != 12) {

				if($formapagcarro != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '1° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro', '$data', '$vencimentocarro1', '$valorcarrofinal', '1')");

				}

				if($formapagcarro1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '2° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro1', '$data', '$vencimentocarro2', '$valorcarrofinal', '1')");

				}

				if($formapagcarro2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '3° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro2', '$data', '$vencimentocarro3', '$valorcarrofinal', '1')");

				}

				if($formapagcarro3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('5', '$id_cliente', '$id_pedido', '4° Parcela CARRO RESERVA Pedido: $id_pedido', '$formapagcarro3', '$data', '$vencimentocarro4', '$valorcarrofinal', '1')");

				}

			}

			if($parcelascarroapp != 12) {

				if($formapagcarroapp != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '1° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarroapp', '$data', '$vencimentocarroapp1', '$valorcarroapp', '1')");

				}

				if($formapagcarroapp1 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '2° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarro1', '$data', '$vencimentocarroapp2', '$valorcarroapp', '1')");

				}

				if($formapagcarroapp2 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '3° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarroapp2', '$data', '$vencimentocarroapp3', '$valorcarroapp', '1')");

				}

				if($formapagcarroapp3 != NULL) {

				$sql_cr_adesao = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('8', '$id_cliente', '$id_pedido', '4° Parcela CARRO APP Pedido: $id_pedido', '$formapagcarroapp3', '$data', '$vencimentocarroapp4', '$valorcarroapp', '1')");

				}

			}


			$DataInicial = new \DateTime($demaismensalidade);

			// Intervalo de 1 mês
			$Intervalo = new \DateInterval( 'P1M' );

			// Data daqui a 2 anos, apenas para aumentar a amostragem
			$DataFinal = new \DateTime( 'today + 11 month' );

			$Periodo = new \DatePeriod( $DataInicial, $Intervalo, $DataFinal );

			$valorcontrato = $valormensalidade / 12;

			$sql_cr_mensalidade_inicio = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status, ordem) VALUES ('1', '$id_cliente', '$id_pedido', '1° Parcela Mensalidade Pedido: $id_pedido', '$formapagmensalidade', '$data', '$mensalidade1', '$valorcontrato', '1', '1')");


			if($vacina != NULL) {

				$sql_vacina = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('6', '$id_cliente', '$id_pedido', 'Vacina do Pedido: $id_pedido', '$formapagvacina', '$data', '$vencimentovicina', '$vacina', '1')");

			}

			if($instalacao != NULL) {

				$sql_vacina = mysqli_query($con, "insert into cr (tipo, id_cliente, id_pedido, descricao, formapag, data, datavencimento, valor, status) VALUES ('7', '$id_cliente', '$id_pedido', 'Instalação do Pedido: $id_pedido', '$formapaginst', '$data', '$vencimentoinstalacao', '$instalacao', '1')");

			}

			}

			$qtd = $_POST['quantidade_parcelas'];


			echo json_encode(array(
				'success' => 1, 
				'id' => $id_pedido,
				'qtd' => $qtd,
				'tipocad' => $tipocad
			));

			// echo"<script language=\"JavaScript\">
			//location.href=\"validamensalidade.php?id=$id_pedido&qtdparcelas=$qtd\";
			// </script>";

}

?>