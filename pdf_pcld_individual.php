<?php

    function reverse_date( $date )
        {
      return ( strstr( $date, '/' ) ) ? implode( '-', array_reverse( explode( '/', $date ) ) ) : implode( '/', array_reverse( explode(                '-', $date ) )      );
        }

	$id = $_GET['id'];

	include"includes/conexao.php";

    /// PRESCISA DA PASTA DO DOMMPDF
	
	$sql_atual = mysqli_query($con, "SELECT * from pcld where id_cliente = '$id' order by datavencimento ASC");

    $sql_sub_total = mysqli_query($con, "SELECT SUM(valor) FROM pcld WHERE id_cliente = '$id' ");
    $vetor_sub_total = mysqli_fetch_array($sql_sub_total);
    $subtotal = $vetor_sub_total['SUM(valor)'];



    function format_valor($num){
        $valor = number_format($num,2,',','.');
        
        return $valor;
    }

    function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
    }

    
   


    $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$id' ");
    $vetor_cliente = mysqli_fetch_array($sql_cliente);

	$tabela = "<table class='table table-bordered table-striped'><thead>
                        <tr>
                            <th>Contr.</th>
                            <th>Descrição</th>
                            <th>Vencimento</th>
                            <th>Valor Princ.</th>

                            <th>Dias em Atras.</th>

                            

                            <th>Juros/Multas</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        $return = "$tabela";


        $total_juros = 0;

        $j = 0;

        $num_rows = mysqli_num_rows($sql_atual);
        // Captura os dados da consulta e inseri na tabela HTML
        while ($linha = mysqli_fetch_array($sql_atual)) {

            $data1 = $linha['datavencimento'];
            $data_inicio = new DateTime($data1);
            $dataAtual = date("Y-m-d");
            $data_fim = new DateTime($dataAtual);

            // Resgata diferença entre as datas
            $dateInterval = $data_inicio->diff($data_fim);
            $dataFinal = $dateInterval->days;

            $porcentagem = (round($dataFinal) / 30) + 10;

            if(strtotime($data1) > strtotime($dataAtual)){ 
                $porcentagem = 0;
                $dataFinal = 0;
            }




            $j = $j + 1;

        	$valor = $linha["valor"];

            $valorF = format_valor($valor);

        	$id_cr = $linha["id_cr"];

            $juros = $valor;

            $juros = ($juros * $porcentagem) / 100;
            
            $jurosF = $juros;

            $total_juros = $total_juros + $juros;

        	$status = $linha["status"];

            $data = reverse_date($linha["datavencimento"]);

        	if ($status == 1) {
        		$status = "Pendente";
        	}elseif ($status == 2) {
        		$status = "Pago";
        	}elseif ($status == 3) {
        		$status = "Cancelado";
        	}
            

            $return.= "<tr>";
            $return.= "<td>" . utf8_encode($linha["id_pedido"]) . "</td>";
            $return.= "<td>" . $linha["descricao"] . "</td>";
            $return.= "<td>" . utf8_encode($data) . "</td>";
            $return.= "<td>" . utf8_encode($valorF) . "</td>";

            $return.= "<td>" . $dataFinal . "</td>";

            
           

            $return.= "<td>" . format_valor($jurosF) . "</td>";
            $return.= "<td>" . utf8_encode($status) . "</td>";
            

            $return.= "</tr>";
        }


        $total_tudo = $total_juros + $subtotal;

        $return.= "
        <tr>
            <td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='4'> VALOR TOTAL</td>
            <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='3'>
            R$ " . format_valor($total_tudo) . " </td>
        </tr>";

       	$return.= "</tbody></table>";


	//referenciar o DomPDF com namespace
	use Dompdf\Dompdf;

	// include autoloader
	require("vendor/autoload.php");

	//Criando a Instancia
	$dompdf = new DOMPDF();
	
	// Carrega seu HTML
	$dompdf->load_html('

        <h2 style="text-align: center"> CLIENTE '. $vetor_cliente['nome'] .' </h2>

         <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">

         <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">

         <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

         <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">

         <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        '. $return .'

		');


	//Renderizar o html
	$dompdf->render();

	//Exibibir a página
	$dompdf->stream(
		"PCLD", 
		array(
			"Attachment" => false //Para realizar o download somente alterar para true
		)
	);
?>