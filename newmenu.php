<?php /*!! <*$exec(pushContentsServerLaraFluks())\$/> !! */

include "includes/conexao.php";
include 'components/helps/helps.php';

if (isset($_GET['id']) and !empty($_GET['id'])) {
	$fetch_menu = pushMenuOne($conn, htmlspecialchars(addslashes($_GET['id'])));
}

function Mask($mask, $str) {

	$str = str_replace(" ", "", $str);

	for ($i = 0; $i < strlen($str); $i++) {
		$mask[strpos($mask, "#")] = $str[$i];
	}

	return $mask;

}

session_start();

$id_pagina = '1';

if ($_SESSION['id'] == NULL) {

	echo "<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";

} else {

	$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
	$res = mysqli_query($con, $sql_permissao);
	$num_busca = mysqli_num_rows($res);

	if ($num_busca == 0) {

		echo "<script language=\"JavaScript\">
  location.href=\"sempermissao.php\";
  </script>";

	} else {

		$sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
		$res_cadastro = mysqli_query($con, $sql_cadastro);
		$vetor_cadastro = mysqli_fetch_array($res_cadastro);

		?>
<!DOCTYPE html>
<html>
<?php include 'includes/head.php'?>

<style type="text/css">
      #ball{
      width: 15px;
      height: 15px;
      border-radius: 50%;
      margin-left: 50%;
    }

    .min{
      width: 5%;
    }
</style>
<script>
            // $(function(){
            //   $('html').css('background-color', '#F1FCFF');
            //   $('body').css('background-color', '#F1FCFF');
            //   $('.container').css('background-color', '#F1FCFF');
            //   $('.content').css('background-color', '#F1FCFF');
            //   $('.box').css('background-color', '#F1FCFF');
            //   $('.tab-content').css('background-color', '#F1FCFF');
            //   $('.box-title').addClass('text-ionc-4');
            // });
   </script>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">

              <?php if (isset($_GET['em']) && $_GET['em'] == 1): ?>
                <h3 class="box-title">Editar Menu</h3>
              <?php else: ?>
                <h3 class="box-title">Novo Menu</h3>
              <?php endif;?>



              <a href="navigation.php"><button class="btn btn-primary">Todos os Menu</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div id="formResult"></div>

            <?php if (isset($_GET['em']) && $_GET['em'] == 1): ?>
              <form method="POST" id="formEditMenu">
            <?php else: ?>
              <form method="POST" id="formAddMenu">
            <?php endif;?>

                <table class="table">

                  <tr>

                    <td width="150">
                      <label for="">Tornar o Menu Dropdown ?</label>
                      <ol style="list-style: none;display: flex;flex-direction: row;padding: 0;">

                        <?php if (isset($_GET['em']) && $_GET['em'] == 1): ?>

                          <li style="margin-right: 20px;">
                            <label>
                              <input type="radio" name="isdropdown" value="1" <?php echo $fetch_menu['is_dropdown'] == 1 ? 'checked="true"' : ''; ?> />Sim
                            </label>
                          </li>

                          <li>
                            <label>
                              <input type="radio" name="isdropdown" value="0" <?php echo $fetch_menu['is_dropdown'] == 0 ? 'checked="true"' : ''; ?> />Não
                            </label>
                          </li>

                        <?php else: ?>

                          <li style="margin-right: 20px;">
                            <label>
                              <input type="radio" name="isdropdown" value="1" />Sim
                            </label>
                          </li>

                          <li>
                            <label>
                              <input type="radio" name="isdropdown" value="0" checked="true" />Não
                            </label>
                          </li>

                        <?php endif;?>

                      </ol>
                    </td>

                    <td>
                      <label for="">Nome do Menu</label>
                      <input type="text" class="form-control" name="menu_name"
                      value="<?php echo $component->old('menu_name', $fetch_menu['menu_name']); ?>" placeholder="Nome do Menu" maxlength="100">
                    </td>

                    <td>
                      <label for="">Link do Menu</label>
                      <input type="text" class="form-control" value="<?php echo $component->old('menu_link', $fetch_menu['menu_link']); ?>" name="menu_link" placeholder="Link do Menu" maxlength="255">
                    </td>

                  </tr>

                  <?php if (isset($_GET['em']) && $_GET['em'] == 1): ?>
                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
                  <?php endif;?>

                  <tr>

                    <td colspan="2">
                      <label for="">Icone do Menu</label>
                      <input type="text" value="<?php echo $component->old('menu_icon', $fetch_menu['menu_icon']); ?>" class="form-control" name="menu_icon" placeholder="Icone do Menu" maxlength="100">
                    </td>

                    <td>
                      <label for="">Status do Menu</label>
                      <select name="menu_status" id="menu_status" class="form-control">
                        <option value="0">Manter Desativado</option>
                        <option value="1" selected="true">Manter Ativado</option>
                      </select>
                    </td>

                  </tr>

                </table>

                <div class="form-group">
                  <?php if (isset($_GET['em']) && $_GET['em'] == 1): ?>
                    <button type="submit" class="btn btn-primary" style="float: right;">Alterar</button>
                  <?php else: ?>
                    <button type="submit" class="btn btn-primary" style="float: right;">Cadastar</button>
                  <?php endif;?>
                </div>

              </form>

              <script>

                $(function(){

                    // sm
                    $('#formAddMenu').on('submit', function(e) {

                      e.preventDefault();

                      $.ajax({
                        url: "http://v2.systemjl.com.br/envDataMenu.php",
                        method: "POST",
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: "json",
                        success: function(data){
                          var html = '';

                          if(parseInt(data.success) == 1){
                              html = `<div class="alert alert-success">Menu Salvo com Sucesso!</div>`;
                          }else{
                              html = `<div class="alert alert-danger">Ops :(, algo deu errado tente novamente mais tarde!</div>`;
                          }


                          $("#formAddMenu")[0].reset();
                          $("#formResult").html(html);
                        }
                      });

                    });

                    // em
                    $('#formEditMenu').on('submit', function(e) {

                      e.preventDefault();

                      $.ajax({
                        url: "http://v2.systemjl.com.br/envDataEditMenu.php",
                        method: "POST",
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: "json",
                        success: function(data){
                          var html = '';

                          if(parseInt(data.success) == 1){
                              html = `<div class="alert alert-success">Menu Alterado com Sucesso!</div>`;
                          }else{
                              html = `<div class="alert alert-danger">Ops :(, algo deu errado tente novamente mais tarde!</div>`;
                          }


                          $("#formEditMenu")[0].reset();
                          $("#formResult").html(html);
                        }
                      });

                    });

                });

              </script>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php';?>


</div>
<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->

</body>
</html>
<?php }}?>