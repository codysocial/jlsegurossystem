<?php

function formatarCPF_CNPJ($campo, $formatado = true){
  //retira formato
  $codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
  // pega o tamanho da string menos os digitos verificadores
  $tamanho = (strlen($codigoLimpo) -2);
  //verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
  if ($tamanho != 9 && $tamanho != 12){
    return false; 
  }
 
  if ($formatado){ 
    // seleciona a mÃ¡scara para cpf ou cnpj
    $mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
    $indice = -1;
    for ($i=0; $i < strlen($mascara); $i++) {
      if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
    }
    //retorna o campo formatado
    $retorno = $mascara;
 
  }else{
    //se nÃ£o quer formatado, retorna o campo limpo
    $retorno = $codigoLimpo;
  }
 
  return $retorno;
 
}

   include"includes/conexao.php";
   
   session_start();

   $id_pagina = '9';

  if($_SESSION['id'] == NULL) {
  
  echo"<script language=\"JavaScript\">
  location.href=\"index.html\";
  </script>";
  
  } else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
    
  $sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
  $res_cadastro = mysqli_query($con, $sql_cadastro);
  $vetor_cadastro = mysqli_fetch_array($res_cadastro);



        $datainicio = $_POST['datainicio'];
        $datafim = $_POST['datafim'];
        $tipo = $_POST['tipo'];
        $id_cliente = $_POST['id_cliente'];
        $centrodecusto = $_POST['centrodecusto'];
        $despesa = $_POST['despesa'];
        $evento = $_POST['evento'];
  
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<div class="container">

  <header class="main-header">

    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="imgs/logo.png" width="100px"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="imgs/logo.png" width="100px"></span>    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <?php include "includes/topo.php"; ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Seja bem-vindo,
        <small> <?php echo $vetor_cadastro['nome']; ?></small>      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Financeiro</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title text-ionc-3">Contas a Pagar</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table width="100%">

              <tr>

                <td width="15%"><a href="cadastrocp.php" target="_blank"><button class="btn btn-primary"  style="    float: left;">Cadastrar Nova Conta</button></a></td>

                <td width="5%">
                  <a href="imprimirbuscacp.php?datainicio=<?php echo $datainicio; ?>&datafim=<?php echo $datafim; ?>&tipo=<?php echo $tipo; ?>&id_cliente=<?php echo $id_cliente; ?>&centrodecusto=<?php echo $centrodecusto; ?>&despesa=<?php echo $despesa; ?>&evento=<?php echo $evento; ?>" target="_blank">
                    <button type="button" class="btn btn-warning mesmo-tamanho" title="Imprimir Cadastro">
                      <i class="fa fa-print"></i>
                    </button>
                  </a>
                </td>


                <td width="85%">
                  <table width="100%">
                    <form action="recebe_buscacp.php" method="post" name="busca">
                    <tr>
                      <td><input type="date" name="datainicio" class="form-control"></td>
                      <td width="2%"></td>
                      <td><input type="date" name="datafim" class="form-control"></td>
                      <td width="2%"></td>
                      <td><select name="id_cliente" id="exampleSelect" class="form-control">
                              <option value="" selected="selected">Selecione o Fornecedor</option>
                              <?php 
                              $sql_clientes = mysqli_query($con, "select * from clientes where tipocad = '2' order by nome ASC");
                              while ($vetor_cliente=mysqli_fetch_array($sql_clientes)) { ?>
                              <option value="<?php echo $vetor_cliente['id_cli']; ?>"><?php echo $vetor_cliente['nome'] ?></option>
                              <?php } ?>
                            </select></td>
                      <td width="2%"></td>
                      <td><select name="centrodecusto" id="exampleSelect" class="form-control">
                              <option value="" selected="selected">Selecione o Centro de Custo</option>
                              <?php 
                              $sql_centros = mysqli_query($con, "select * from centrodecustos order by nome ASC");
                              while ($vetor_centros=mysqli_fetch_array($sql_centros)) { ?>
                              <option value="<?php echo $vetor_centros['id_centro']; ?>"><?php echo $vetor_centros['nome'] ?></option>
                              <?php } ?>
                            </select></td>
                      <td width="2%"></td>
                      <td><select name="despesa" id="exampleSelect" class="form-control">
                              <option value="" selected="selected">Selecione a Despesa</option>
                              <?php 
                              $sql_despesas = mysqli_query($con, "select * from despesa order by nome ASC");
                              while ($vetor_despesas=mysqli_fetch_array($sql_despesas)) { ?>
                              <option value="<?php echo $vetor_despesas['id_despesa']; ?>"><?php echo $vetor_despesas['nome'] ?></option>
                              <?php } ?>
                            </select></td>
                      <td width="2%"></td>
                      <td><select name="evento" id="exampleSelect" class="form-control">
                              <option value="" selected="selected">Selecione o Projeto / Finalidade</option>
                              <?php 
                              $sql_eventos = mysqli_query($con, "select * from eventos order by nome ASC");
                              while ($vetor_eventos=mysqli_fetch_array($sql_eventos)) { ?>
                              <option value="<?php echo $vetor_eventos['id_evento']; ?>"><?php echo $vetor_eventos['nome'] ?></option>
                              <?php } ?>
                            </select></td>
                      <td width="2%"></td>
                      <td><select name="tipo" id="tipobusca" class="form-control">
                        <option value="" selected="">Selecione...</option>
                        <option value="1">Em Aberto</option>
                        <option value="3">Recebidos</option>
                      </select></td>
                      <td width="2%"></td>
                      <td><button type="submit" class="btn btn-primary"  style="float: left;">Buscar</button></td>
                    </tr>
                    </form>
                  </table>
                </td>
              </tr>
            </table>

            <br>
            <br>
            
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Código</th>
                  <th>Fornecedor</th>
                  <th>Descrição</th>
                  <th>Centro de Custo</th>
                  <th>Despesa</th>
                  <th>Projeto / Finalidade</th>
                  <th>Forma de Pagamento</th>
                  <th>Data Entrada</th>
                  <th>Vencimento</th>
                  <th>Data Pagamento</th>
                  <th>Valor a Pagar</th>
                  <th>Valor Pago</th>
                  <th>Status</th>
                  <th width="13%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php 
        
        
        

        if(!empty($datainicio) && !empty($datafim)) { $where .= " AND datavencimento BETWEEN '".$datainicio."' AND '".$datafim."'"; }
        if(!empty($tipo)) { $where .= " AND pagamento = '".$tipo."'"; }
        if(!empty($id_cliente)) { $where .= " AND id_cli = '".$id_cliente."'"; }
        if(!empty($centrodecusto)) { $where .= " AND centrodecusto = '".$centrodecusto."'"; }
        if(!empty($despesa)) { $where .= " AND despesa = '".$despesa."'"; }
        if(!empty($evento)) { $where .= " AND evento = '".$evento."'"; }

        $valor_a_pagar = 0;

        $valor_pago = 0;

          
        $sql_atual = mysqli_query($con, "select * from cp01 where 1".$where." order by datavencimento ASC");
        
        while ($vetor=mysqli_fetch_array($sql_atual)) {
        
        $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cli]'");
        $vetor_cliente = mysqli_fetch_array($sql_cliente);
        
        $selec = mysqli_query($con, "SELECT SUM(vlpago) as vlpago FROM cp02 where id_cp01 = '$vetor[id_cp01]' and pago = '2'");
        $vetorPag = mysqli_fetch_array($selec);
        
        $selec1 = mysqli_query($con, "SELECT SUM(valor) as valor FROM cp02 where id_cp01 = '$vetor[id_cp01]' and pago = '1'");
        $vetoraPag = mysqli_fetch_array($selec1);

        $sqlpapa = mysqli_query($con, "SELECT datapagamento FROM cp02 WHERE id_cp01 = '$vetor[id_cp01]' ");
        $vetorpapa = mysqli_fetch_array($sqlpapa);

        $sql_centro = mysqli_query($con, "select * from centrodecustos where id_centro = '$vetor[centrodecusto]'");
        $vetor_centro = mysqli_fetch_array($sql_centro);

        $sql_despesa = mysqli_query($con, "select * from despesa where id_despesa = '$vetor[despesa]'");
        $vetor_despesa = mysqli_fetch_array($sql_despesa);

        $sql_evento = mysqli_query($con, "select * from eventos where id_evento = '$vetor[evento]'");
        $vetor_evento = mysqli_fetch_array($sql_evento);
        
         ?>
                 <tr>
                  <td><?php echo $vetor['id_cp01']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></a></td>
                  <td><?php echo $vetor['descricao']; ?></td>
                  <td><?php echo $vetor_centro['nome']; ?></a></td>
                  <td><?php echo $vetor_despesa['nome']; ?></a></td>
                  <td><?php echo $vetor_evento['nome']; ?></a></td> 
                  <td><?php echo $vetor['formapag']; ?></a></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['datalancamento'])); ?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['datavencimento'])); ?></td>
                  <td><?php $datapag = date('d/m/Y', strtotime($vetorpapa['datapagamento']));
                            if($datapag != "01/01/1970" &&  $datapag != "31/12/1969"){
                              echo $datapag;
                            }
                      ?></td>
                  <td><?php $num = $vetoraPag['valor'];
  $num = number_format($num,2,',','.');
  echo $num;
   ?></td>
                   <td><?php $num1 = $vetorPag['vlpago'];
                    $num1 = number_format($num1,2,',','.');
                    echo $num1;
                     ?></td>
                     <td><?php if($vetor['pagamento'] == 3) { echo "Pago"; } if($vetor['pagamento'] == 1) { echo "Pendente"; } if($vetor['pagamento'] == 2) { echo "Pagamento Parcial"; } if($vetor['pagamento'] == 4) { echo "Cancelado"; } ?></a></td>
                  <td><a href="recebercontasapagar.php?id=<?php echo $vetor['id_cp01']; ?>"><button type="button" class="btn btn-info mesmo-tamanho" title="Visualizar / Alterar Conta"><i class="fa fa-edit"></i></button></a> <a href="imprimircp.php?id=<?php echo $vetor['id_cp01']; ?>" target="_blank"><button type="button" class="btn btn-warning mesmo-tamanho" title="Imprimir Cadastro"><i class="fa fa-print"></i></button></a> <?php if($vetor['pagamento'] == 3 || $vetor['pagamento'] == 4) { } else { ?><a href="confexcluircp.php?id=<?php echo $vetor['id_cp01']; ?>" ><button type="button" class="btn btn-danger mesmo-tamanho" title="Excluir Cadastro"><i class="fa fa-close"></i></button></a><?php } ?></td> 
                
                 <?php

                 $valor_a_pagar = $valor_a_pagar + $vetoraPag['valor'];

                 $valor_pago = $valor_pago + $vetorPag['vlpago'];

                 ?>
                

                </tr>
                <?php } ?>



                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->

            <div style="background-color: silver">
              <div style="display: flex; flex-direction: row">
                <div style="width: 90%; margin-left: 10px;">
                  <h4 style="font-weight: bold;">Total a pagar: </h4>
                </div>
                <div style="width: 10%;">
                  <h4 style="font-weight: bold;">R$ <?php $num = $valor_a_pagar;
                    $num = number_format($num,2,',','.');
                    echo $num;?></h4>
                </div>
              </div>
            </div>
            <div style="background-color: silver; border-top: 1px solid white; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px;">
              <div style="display: flex; flex-direction: row">
                <div style="width: 90%; margin-left: 10px;">
                  <h4 style="font-weight: bold;">Total Pago: </h4>
                </div>
                <div style="width: 10%;">
                  <h4 style="font-weight: bold;">R$ <?php $num = $valor_pago;
                    $num = number_format($num,2,',','.');
                    echo $num; ?></h4>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box -->
          <!--<div class="box">
            <div class="box-body">
                <table>
                  <tr>
                    <td width="95%"></td>
                    <td>
                      <p>aqui mesmo</p>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versão</b> 1.0    </div>
    <strong>Todos direitos reservados JL Seguro.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });

  $(function(){
      $('html').css('background-color', '#F9EBFF');
      $('body').css('background-color', '#F9EBFF');
      $('.container').css('background-color', '#F9EBFF');
      $('.content').css('background-color', '#F9EBFF');
      $('.box').css('background-color', '#F9EBFF');
      $('.tab-content').css('background-color', '#F9EBFF');
    });
</script>
</body>
</html>
<?php } } ?>