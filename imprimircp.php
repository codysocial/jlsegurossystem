<?php

function reverse_date( $date )
        {
      return ( strstr( $date, '/' ) ) ? implode( '-', array_reverse( explode( '/', $date ) ) ) : implode( '/', array_reverse( explode(                '-', $date ) )      );
        }

include"includes/conexao.php";

$id = $_GET['id'];
   //essa query pega dados da tabela cp01, somente direcionado ao id informado!
   $sql = "select * from cp01 where id_cp01 = '$id'";
   
   //esse executa a query e guarda na variavel $res1
   $res1 = mysqli_query($con, $sql);
   
   /* encapsula todo o resultado da query e poem numa variavel chamada vetor, os indices do vetor é o nome da coluna da tabela */
   $vetor = mysqli_fetch_array($res1);

   $sql1 = "select * from cp02 where id_cp01 = '$id'";

   $res2 = mysqli_query($con, $sql1);
   $data = reverse_date($vetor['datavencimento']);
   $data1 = reverse_date($vetor['datalancamento']);

  $sql_cliente = "select * from clientes where id_cli = '$vetor[id_cli]'";
  $res3 = mysqli_query($con, $sql_cliente);
  $vetor_cliente = mysqli_fetch_array($res3);

  $sql_comissao = mysqli_query($con, "select * from comissao where id_tipo = '$vetor[id_comissao]'");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <style type="text/css">
    #local-banco{
      margin-left: 0px;
    }
    .flexBox{
      display: flex;
      flex-direction: row;
    }
    .espaco-left{
      margin-left: 50px;
    }
    .espaco-left-2{
      margin-left: 25px;
    }
    .no-margin-top{
      position: relative;
      top: 1px;
      margin-top: 0px;
      font-weight: normal;
      font-family: monospace;
    }
    .espaco-right{
      position: relative;
      margin-right: 5px;
      font-weight: bold;
      font-size: 15px;
    }
    .title-cp{
      font-weight: bold;
      font-size: 25px;
    }
  </style>

</head>
<body>
<section class="content">
    <div class="box-header">
      <h3 class="title-cp">CONTAS A PAGAR</h3>
    </div>
  
  <div>
    <div class="flexBox">
      <label class="form-label semibold espaco-right" for="exampleInput">Fornecedor: </label>
      <!--   aqui faz a verificação para saber se a variavel esta vazia   -->
      <p class="no-margin-top">
        <?php if (!empty($vetor_cliente['nomefant'])) {
        echo $vetor_cliente['nomefant'];
        }else{
        echo $vetor['cliente'];
        }; ?>
      </p>
    </div>

    <div class="flexBox">
      <label class="form-label semibold espaco-right" for="exampleInput">CPF/CNPJ: </label>
      <p  class="no-margin-top">
        <?php if (!empty($vetor_cliente['cpfcnpj'])) {
        echo $vetor_cliente['cpfcnpj'];
        }else{
        echo "";
        }; ?>
      </p>
    </div>

  </div>

  <div class="box-header">
    <h3 class="box-title title-cp">FORMA DE PAGAMENTO</h3>
  </div>

  <div class="flexBox">
    <div id="local-banco" class="espaco-left flexBox">
      <label class="espaco-right">Banco: </label>
      <p class="no-margin-top"><?php if (!empty($vetor_cliente['banco'])) {
        echo $vetor_cliente['banco'];
        }else{
        echo "";
        }; ?>
      </p>
    </div>
    <div id="local-agencia" class="espaco-left flexBox">
      <label class="espaco-right">Agencia: </label>
      <p class="no-margin-top"><?php if (!empty($vetor_cliente['agencia'])) {
        echo $vetor_cliente['agencia'];
        }else{
        echo "";
        }; ?>
      </p>
    </div>
    <div id="local-conta" class="espaco-left flexBox">
      <label class="espaco-right">Conta: </label>
      <p class="no-margin-top"><?php if (!empty($vetor_cliente['conta'])) {
        echo $vetor_cliente['conta'];
        }else{
        echo "";
        }; ?>
      </p>
    </div>
    <div id="local-tipo-conta" class="espaco-left flexBox">
      <label class="espaco-right">Tipo: </label>
      <p class="no-margin-top">
        <?php if (!empty($vetor_cliente['tipoconta'])){
          if ($vetor_cliente['tipoconta'] == 1) {
            echo "Corrente";
          }else{
            echo "Popança";
          }
        }else{
          echo "";
        }; ?>
      </p>              
    </div>

  </div>

  <div class="flexBox">
    <label  class="espaco-right">Descri&ccedil;&atilde;o: </label>
    <p class="no-margin-top"><?php echo $vetor['descricao']; ?></p>
  </div>

  <div class="flexBox">
    <div id="local-banco" class="espaco-left flexBox">
      <label class="espaco-right">Data de Vencimento: </label>
      <p class="no-margin-top"><?php echo $data; ?></p>
    </div>
    <div id="local-agencia" class="espaco-left-2 flexBox">
      <label class="espaco-right">Data de Lan&ccedil;amento: </label>
      <p class="no-margin-top"><?php echo $data1; ?></p>
    </div>
    <div id="local-conta" class="espaco-left-2 flexBox">
      <label class="espaco-right">Valor Total: </label>
      <p class="no-margin-top"><?php $num = $vetor['valor'];
  $num = number_format($num,2,',','.');
  echo $num;
   ?>
   </p>
    </div>
  </div>

  <div style="width: 100%; height: 30px;">
    
  </div>
        
        <!-- ESSA PARTE NÃO SERÁ MAIS NECESSARIO --------------------------------------------
        -------------------------------------------------------------------------------------
        <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Quantidade de Parcelas</label>
              </br>
              <?php echo $vetor['parcelas']; ?>
            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Intervalo</label>
              </br>
              <?php echo $vetor['intervalo']; ?>
            </fieldset>
          </div>
        </div>.row
        ---------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------
        -->

        <?php

        if(mysqli_num_rows($sql_comissao) > 0){ 

        ?>

        <h3>Comissões</h3>

        <table width="100%" id="table-sm" class="table table-bordered table-hover table-sm">
      <tr>
        <tr>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">N° Pedido</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Nome do Cliente</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Data Pedido</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Valor Pedido</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Valor Comissão</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Data Pagamento</span></td>
        </tr>
        <?php 

        while ($vetor_comissao=mysqli_fetch_array($sql_comissao)) {

        $sql_cliente_comissao = mysqli_query($con, "select * from clientes where id_cli = '$vetor_comissao[id_cli]'");
        $vetor_cliente_comissao = mysqli_fetch_array($sql_cliente_comissao);

        
        $sql_pedido = mysqli_query($con, "select * from pedidos where id_pedido = '$vetor_comissao[id_pedido]'");
        $vetor_pedido = mysqli_fetch_array($sql_pedido);

        $sql_cliente_pedido = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
        $vetor_cliente_pedido = mysqli_fetch_array($sql_cliente_pedido);

        $mensalidade = $vetor_pedido['mensalidade'] * 12;

        $total = $vetor_pedido['adesao'] + $mensalidade + $vetor_pedido['vacina'] + $vetor_pedido['instalacao'] + $vetor_pedido['assistencia'] + $vetor_pedido['assistenciaterceiro'];

        ?>
      <tr>
        <td><?php echo $vetor_pedido['id_pedido']; ?></td>
        <td><?php echo $vetor_cliente_pedido['nome']; ?></td>
        <td><?php echo date('d/m/Y', strtotime($vetor_pedido['data'])); ?></td>
        <td><?php echo number_format($total,2,',','.'); ?></td>
        <td><?php echo number_format($vetor_comissao['valor'],2,',','.'); ?></td>
        <td><?php echo date('d/m/Y', strtotime($vetor_comissao['datavencimento'])); ?></td>
        </tr>
      <?php } ?>
    </table>

  <?php } ?>
                
        <table width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
            
            <table width="100%" id="table-sm" class="table table-bordered table-hover table-sm">
      <tr>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Situa&ccedil;&atilde;o</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Valor da Fatura</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Data Vencimento</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Data Pagamento</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Valor Pagamento</span></td>
        <td bgcolor="e8e8e8" class="style22"><span class="style12">Forma de Pagamento</span></td>
        </tr>
      <tr><?php while ($parcela=mysqli_fetch_array($res2)) { 
    $data2 = reverse_date($parcela['datapagamento']);
    $datavenc = reverse_date($parcela['datavencimento']);
    ?>
        <td><?php if ($parcela['pago'] == '1') { echo "Em Aberto"; } else { echo "Pago"; } ?></td>
        <td><?php $num = $parcela['valor'];
  $num = number_format($num,2,',','.');
  echo $num;
   ?></td>
        <td><?php echo $datavenc; ?></td>
        <td><?php echo $data2; ?></td>
        <td><?php $num = $parcela['vlpago'];
  $num = number_format($num,2,',','.');
  echo $num;
   ?></td>
     <td><?php if($parcela['formapag'] == 'Cartão') { echo "Cartão"; } if($parcela['formapag'] == 'Boleto') { echo "Boleto"; } if($parcela['formapag'] == 'Dinheiro') { echo "Dinheiro"; } if($parcela['formapag'] == 'Outras') { echo "Outras"; } if($parcela['formapag'] == 'Créd. Conta') { echo "Créd. Conta"; } ?></td>
        </tr>
        </form>
      <?php } ?>
    </table>
    </br>

    <!-- ESSA PARTE NÃO SERÁ MAIS NECESSARIO --------------------------------------------
        ---------------------------------------------------------------------------------
              <?php
              if(!empty($vetor_cliente['banco'])) { 
              ?>

              <div class="row">
                    <div class="col-lg-3">
                      <fieldset class="form-group">
                        <label class="form-label semibold" for="exampleInput">Banco</label>
                        </br>
                        <?php echo $vetor_cliente['banco']; ?>
                      </fieldset>
                    </div>
                    <div class="col-lg-3">
                      <fieldset class="form-group">
                        <label class="form-label" for="exampleInputEmail1">Agencia</label>
                        </br>
                        <?php echo $vetor_cliente['agencia']; ?>
                      </fieldset>
                    </div>
                    <div class="col-lg-3">
                      <fieldset class="form-group">
                        <label class="form-label" for="exampleInputPassword1">Conta</label>
                        </br>
                        <?php echo $vetor_cliente['conta']; ?>
                      </fieldset>
                    </div>
                    <div class="col-lg-3">
                      <fieldset class="form-group">
                        <label class="form-label" for="exampleInputPassword1">Tipo da Conta</label>
                        </br>
                        <?php if ($vetor_cliente['tipoconta'] == '1') { echo "Corrente"; } else { echo"Poupança"; } ?>
                      </fieldset>
                    </div>
                  </div> row
              <?php } ?>
        -------------------------------------------------------------------------------------
        -------------------------------------------------------------------------------------
        -->

      </div>
      <!-- /.row -->
    </section>
</body>
</html>

<script type="text/javascript">
        print();
</script> 