<?php

	try {
		
		include"includes/conexao.php";
 
session_start();

function removeAcentos($string, $slug = false) {
  $string = strtolower($string);
  // Código ASCII das vogais
  $ascii['a'] = range(224, 230);
  $ascii['e'] = range(232, 235);
  $ascii['i'] = range(236, 239);
  $ascii['o'] = array_merge(range(242, 246), array(240, 248));
  $ascii['u'] = range(249, 252);
  // Código ASCII dos outros caracteres
  $ascii['b'] = array(223);
  $ascii['c'] = array(231);
  $ascii['d'] = array(208);
  $ascii['n'] = array(241);
  $ascii['y'] = array(253, 255);
  foreach ($ascii as $key=>$item) {
    $acentos = '';
    foreach ($item AS $codigo) $acentos .= chr($codigo);
    $troca[$key] = '/['.$acentos.']/i';
  }
  $string = preg_replace(array_values($troca), array_keys($troca), $string);
  // Slug?
  if ($slug) {
    // Troca tudo que não for letra ou número por um caractere ($slug)
    $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
    // Tira os caracteres ($slug) repetidos
    $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
    $string = trim($string, $slug);
  }
  return $string;
}

function reverse_date( $date )
        {
    return ( strstr( $date, '-' ) ) ? implode( '/', array_reverse( explode( '-', $date ) ) ) : implode( '-', array_reverse( explode(                '/', $date ) )      );
        }
		
		function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
     }

  		
		$map = array(
    'á' => 'a',
    'à' => 'a',
    'ã' => 'a',
    'â' => 'a',
    'é' => 'e',
    'ê' => 'e',
    'í' => 'i',
    'ó' => 'o',
    'ô' => 'o',
    'õ' => 'o',
    'ú' => 'u',
    'ü' => 'u',
    'ç' => 'c',
    'Á' => 'A',
    'À' => 'A',
    'Ã' => 'A',
    'Â' => 'A',
    'É' => 'E',
    'Ê' => 'E',
    'Í' => 'I',
    'Ó' => 'O',
    'Ô' => 'O',
    'Õ' => 'O',
    'Ú' => 'U',
    'Ü' => 'U',
    'Ç' => 'C'
);

  		
		$genero = isset($_POST['genero']) && !empty($_POST['genero']) ? $_POST['genero']:"";
		$seguimento = isset($_POST['seguimento']) && !empty($_POST['seguimento']) ? $_POST['seguimento']:"";
		$representante = isset($_POST['representante']) && !empty($_POST['representante']) ? $_POST['representante']:"";
	
		$indicador = $_POST['indicador'];
		$contratoparc = isset($_POST['contratoparc']) && !empty($_POST['contratoparc']) ? $_POST['contratoparc']:"";
		$nome = removeAcentos($_POST['nome']);
		$nomefinal = strtr($nome, $map);
		$nomefinal1 = strtoupper($nomefinal);
		$nomefant = isset($_POST['nomefant']) && !empty($_POST['nomefant']) ? $_POST['nomefant']:"";
		$nomefantfinal = strtr($nomefant, $map);
		$nomefantfinal1 = strtoupper($nomefantfinal);
		$cpfcnpj = $_POST['cpfcnpj'];
		$cpfcnpj = str_replace('.', '', $cpfcnpj);
		$cpfcnpj = str_replace('-', '', $cpfcnpj);
		$cpfcnpj = str_replace('/', '', $cpfcnpj);
		$rg = $_POST['rg'];
		$datanasc = $_POST['datanasc'];
		$nomeresp = isset($_POST['nomeresp']) && !empty($_POST['nomeresp']) ? $_POST['nomeresp']:"";
		$nomerespfinal = strtr($nomeresp, $map);
		$nomerespfinal1 = strtoupper($nomerespfinal);
		$inscmunicipal = isset($_POST['inscmunicipal']) && !empty($_POST['inscmunicipal']) ? $_POST['inscmunicipal']:"";
		$contribuinte = isset($_POST['contribuinte']) && !empty($_POST['contribuinte']) ? $_POST['contribuinte']:"";
		
		if (empty($contribuinte)) {
		$isento = 2;
		}
		else {
		$isento = 1;
		}
		
		$inscestadual = isset($_POST['inscestadual']) && !empty($_POST['inscestadual']) ? $_POST['inscestadual']:"";
		$inscsubst = isset($_POST['inscsubst']) && !empty($_POST['inscsubst']) ?  $_POST['inscsubst']:"";
		$inscsuframa = isset($_POST['inscsuframa']) && !empty($_POST['inscsuframa']) ? $_POST['inscsuframa']:"";
		$cep = $_POST['cep'];
		$endereco = removeAcentos($_POST['endereco']);
		$enderecofinal = strtr($endereco, $map);
		$enderecofinal1 = strtoupper($enderecofinal);
		$numero = $_POST['numero'];
		$complemento = removeAcentos($_POST['complemento']);
		$complementofinal = strtr($endereco, $map);
		$complementofinal1 = strtoupper($complementofinal);
		$bairro = $_POST['bairro'];
		$bairrofinal = strtr($bairro, $map);
		$bairrofinal1 = strtoupper($bairrofinal);
		$cidade =  removeAcentos($_POST['cidade']);
		$cidadefinal = strtr($cidade, $map);
		$cidadefinal1 = strtoupper($cidadefinal);
		$estado = $_POST['estado'];
		$estadofinal = strtr($estado, $map);
		$estadofinal1 = strtoupper($estadofinal);
		$codigoibge = isset($_POST['codigoibge']) && !empty($_POST['codigoibge']) ? $_POST['codigoibge']:"";
		$pais = isset($_POST['pais']) && !empty($_POST['pais']) ? $_POST['pais']:"";
		$telefone = $_POST['telefone'];
		$celular = $_POST['celular'];
		$email = $_POST['email'];
		$tipocad = $_POST['tipocad'];
		$contrato = isset($_POST['contrato']) && !empty($_POST['contrato']) ? $_POST['contrato']:"";
		$diavenc = isset($_POST['diavenc']) && !empty($_POST['diavenc']) ? $_POST['diavenc']:"";
		$dataprimeiro = isset($_POST['dataprimeiro']) && !empty($_POST['dataprimeiro']) ? $_POST['dataprimeiro']:"";
		$boleto = isset($_POST['boleto']) && !empty($_POST['boleto']) ? $_POST['boleto']:"";
		$valorcontrato =  moeda(isset($_POST['valorcontrato']) && !empty($_POST['valorcontrato']) ? $_POST['valorcontrato']:"0.00");
		$anotacoes = $_POST['anotacoes'];  
		$empresa = $_POST['empresa'];
		$telempresa = $_POST['telempresa'];
		$comissao = moeda(isset($_POST['comissao']) && !empty($_POST['comissao']) ? $_POST['comissao']:"0.00");  
		$comissaop = isset($_POST['comissaop']) && !empty($_POST['comissaop']) ? $_POST['comissaop']:"";
		$vendedor = $_POST['vendedor'];
		$banco = $_POST['banco'];
		$agencia = $_POST['agencia'];
		$conta = $_POST['conta'];
		$tipoconta = $_POST['tipoconta'];
		$data = date('Y-m-d');

		$recebide     = empty($_POST['recebide']) ? 'JL SEGUROS SYSTEM DO BRASIL':$_POST['recebide'];
		$valorrec	  = empty($_POST['valorrec']) ? '0,00':$_POST['valorrec'];
		$porextenso   = empty($_POST['porextenso']) ? 'ZERO REAIS':$_POST['porextenso'];
		$referenterec = empty($_POST['referenterec']) ? 'NÃO INFORMADO!':$_POST['referenterec'];


		// VERIFICAR SE EXISTE O CPF/CNPJ
		$sql = $conn->prepare("SELECT * FROM clientes WHERE cpfcnpj = ?");
		$sql->execute([$cpfcnpj]);

		if ($sql->rowCount() > 0) {
			echo json_encode(array('error' => 404, 'tipocad' => 404));
		}else{
		
			$sql = "insert into clientes (genero, seguimento, representante, indicador, contratoparc, nome, nomefant, cpfcnpj, rg, datanasc, nomeresp, inscmun, isento, inscest, inscestsubst, inscsuframa, cep, endereco, numero, complemento, bairro, cidade, codigoibge, estado, pais, telefone, celular, email, tipocad, anotacoes, empresa, telempresa, contrato, diavenc, dataprimeiro, boleto, valorcontrato, comissao, comissaop, vendedor, banco, agencia, conta, tipoconta, datacad, recebide, valorrec, porextenso, referenterec) 


				VALUES ('$genero', '$seguimento', '$representante', '$indicador', '$contratoparc', '$nomefinal1', '$nomefantfinal1', '$cpfcnpj', '$rg', '$datanasc', '$nomeresp', '$inscmunicipal', '$isento', '$inscestadual', '$inscsubst', '$inscsuframa', '$cep', '$enderecofinal1', '$numero', '$complementofinal1', '$bairrofinal1', '$cidadefinal1', '$codigoibge', '$estadofinal1', '$pais', '$telefone', '$celular', '$email', '$tipocad', '$anotacoes', '$empresa', '$telempresa', '$contrato', '$diavenc', '$dataprimeiro', '$boleto', '$valorcontrato', '$comissao', '$comissaop', '$vendedor', '$banco', '$agencia', '$conta', '$tipoconta', '$data', '$recebide', '$valorrec', '$porextenso', '$referenterec')";
			
			$res = mysqli_query($con, $sql) or die (mysqli_error($con));
			$id_last_insert = $con->insert_id;

			if (!empty($valorrec)) {

			$sql ="insert into cp01 SET 
					descricao = '$referenterec', 
					datalancamento = '$data',datavencimento = null, 
					valor = $valorrec, tipo = null, nrodoc = null, 
					id_cli = $id_last_insert, cliente = '$nome', 
					parcelas = '1', intervalo = null, observacao = 'Não Informado.', pagamento = '3', centrodecusto = null, 
					despesa = null, evento = null, excluido = null;";
			
				$res = mysqli_query($con, $sql);
				$lastcontpaga_id = $con->insert_id;

			$sql02 = "insert into cp02 SET id_cp01 = '$lastcontpaga_id',
						datapagamento = '$data',
						datavencimento = null,
						num_mens = null,
						pago = 2,
						valor = '$valorrec',
						desconto = null,
						multa = null,
						vlpago = '$valorrec',
						formapag = 'Boleto',
						comissao = null";
			$res = mysqli_query($con, $sql02);

			}

			if($tipocad == 1) {

			echo json_encode(array('success' => 1, 'tipocad' => 1));
		
			// echo"<script language=\"JavaScript\">
			// location.href=\"listarclientes.php\";
			// </script>";
			
			}
			
			if($tipocad == 2) {

				echo json_encode(array('success' => 1, 'tipocad' => 2));
			
				// echo"<script language=\"JavaScript\">
				// location.href=\"listarfornecedores.php\";
				// </script>";
				
			}
			
			if($tipocad == 3) {

				echo json_encode(array('success' => 1, 'tipocad' => 3));
			
				// echo"<script language=\"JavaScript\">
				// location.href=\"listarvendedores.php\";
				// </script>";
				
			}

			if($tipocad == 4) {

				echo json_encode(array('success' => 1, 'tipocad' => 4));
			
				// echo"<script language=\"JavaScript\">
				// location.href=\"listarprestadores.php\";
				// </script>";
				
			}

			if($tipocad == 5) {

				echo json_encode(array('success' => 1, 'tipocad' => 5));
			
				// echo"<script language=\"JavaScript\">
				// location.href=\"listarparceiros.php\";
				// </script>";
				
			}

			if($tipocad == 6) {

				echo json_encode(array('success' => 1, 'tipocad' => 6));
			
				// echo"<script language=\"JavaScript\">
				// location.href=\"listarrepresentantes.php\";
				// </script>";
				
			}

			if($tipocad == 7) {
				
				echo json_encode(array('success' => 1, 'tipocad' => 7));

				// echo"<script language=\"JavaScript\">
				// location.href=\"listarparcerias.php\";
				// </script>";
				
			}

		}// END VERIFICAR CPF


	} catch (Exception $e) {
		echo $e->getLine();
	}


?>