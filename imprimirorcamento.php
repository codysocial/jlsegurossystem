<?php

include"includes/conexao.php";

$id = $_GET['id'];

$sql = mysqli_query($con, "select * from orcamento where id_orcamento = '$id'");
$vetor = mysqli_fetch_array($sql);

$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$vetor[marca]'") or die (mysqli_error($con));
$vetor_marca = mysqli_fetch_array($sql_marca);

$sql_modelo = mysqli_query($con, "select * from fp_modelo where codigo_modelo = '$vetor[modelo]'") or die (mysqli_error($con));
$vetor_modelo = mysqli_fetch_array($sql_modelo);

$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$vetor[ano]'") or die (mysqli_error($con));
$vetor_anomod = mysqli_fetch_array($sql_anomod);

?>
<!DOCTYPE html>
<html>
<meta charset="utf-8">
<head>
<title></title>
</head>
<body>

<table width="100%">
	<tr>
		<td width="60%"><img src="imgs/logoimpressao.png" width="70%"></td>
		<td valign="center">
			<div style="font-size: small">
			<p align="right">
			Data: <?php echo date('d/m', strtotime($vetor['data'])); ?>

			</br>
			VÁLIDO ATÉ: 
			<?php

			$diasemana_numero = date('w', strtotime($vetor['data']));

			if($diasemana_numero == 0) {

            $datafinal = date('d/m/Y', strtotime('+3 days', strtotime($vetor['data'])));

            } if($diasemana_numero == 1) {

            $datafinal = date('d/m/Y', strtotime('+3 days', strtotime($vetor['data'])));

            } if($diasemana_numero == 2) {

            $datafinal = date('d/m/Y', strtotime('+3 days', strtotime($vetor['data'])));

            } if($diasemana_numero == 3) {

            $datafinal = date('d/m/Y', strtotime('+5 days', strtotime($vetor['data'])));

            } if($diasemana_numero == 4) {

            $datafinal = date('d/m/Y', strtotime('+5 days', strtotime($vetor['data'])));

            } if($diasemana_numero == 5) {

            $datafinal = date('d/m/Y', strtotime('+5 days', strtotime($vetor['data'])));

            } if($diasemana_numero == 6) {

            $datafinal = date('d/m/Y', strtotime('+4 days', strtotime($vetor['data'])));

            }

            echo $datafinal;

			?>
			</p>
			</div>
		</td>
	</tr>
</table>

</br>
<table width="100%">
	<tr>
		<td align="justify">
			Prezado(a) Cliente, <?php echo $vetor['nome']; ?> Você acaba de receber o orçamento solicitado. Lembrando que, a JL Seguro System esta entre as melhores empresas de proteção veicular do Brasil. Temos uma grande variedade de benefícios que deixa os nossos clientes muito mais tranquilos, é um prazer ter você conosco.

		</td>
	</tr>
</table>
</br>
<div style="font-size: x-small">
<table width="100%" BORDER="0" style="border-collapse: collapse" BGCOLOR="#FFFFFF">
	<tr>
		<td>

			<table width="100%">
				<tr>
					<td width="3%"></td>
					<td width="94%">
						
						<table width="100%" BORDER="1" style="border-collapse: collapse" BGCOLOR="#FFFFFF">
							<tr>
								<td>
									<table width="100%" bgcolor="#FFFFFF">
										<tr>
											<td align="center">ORÇAMENTO</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%">
										<tr>
											<td width="25%"><strong>TIPO DE INDENIZAÇÃO:</td>
											<td width="25%"><?php if($vetor['tipoindenizacao'] == 1) { ?>100% TABELA FIPE  ( X )<?php } ?></td>
											<td width="25%"><?php if($vetor['tipoindenizacao'] == 2) { ?>IDENIZAÇÃO PARCIAL   ( X )<?php } ?></td>
											<td width="25%"><?php if($vetor['tipoindenizacao'] == 3) { ?>SEM INDENIZAÇÃO  ( X )<?php } ?></strong></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td>
									<table width="100%">
										<tr>
											<td width="30%">
												<strong>MARCA</strong>
											</td>
											<td width="70%">
												<?php echo $vetor_marca['marca']; ?>
											</td>
										</tr>
										<tr>
											<td width="30%">
												<strong>MODELO</strong>
											</td>
											<td width="70%">
												<?php echo $vetor_modelo['modelo']; ?>
											</td>
										</tr>
										<tr>
											<td width="30%">
												<strong>ANO MODELO</font>
											</td>
											<td width="70%">
												<?php echo $vetor_anomod['ano']; ?>
											</td>
										</tr>
										<tr>
											<td width="30%">
												<strong>VALOR FIPE<strong>
											</td>
											<td width="70%">
												<?php echo number_format($vetor['valorfipe'],2,',','.'); ?>
											</td>
										</tr>
										<tr>
											<td width="30%">
												<strong>INDENIZAÇÃO ATÉ<strong>
											</td>
											<td width="70%">
												<?php 

												if($vetor['indenizacao'] == 1) { 

												echo number_format($vetor['valorindenizacao'],2,',','.'); 

												} else{

													if($vetor['indenizacao'] == 2) { echo "FIPE"; } if($vetor['indenizacao'] == 3) { echo ""; }
												}


												?>
											</td>
										</tr>
										<?php if($vetor['valorassistencia24'] != '0.00') { ?>
										<tr>
											<td width="30%">
												<strong>COM ASSITENCIA  24 HRS</strong>
											</td>
											<td width="70%">
												( <?php if($vetor['valorassistencia24'] != '0.00') { ?>X<?php } ?> ) Sim  ( <?php if($vetor['valorassistencia24'] == '0.00') { ?>X<?php } ?> ) Não
											</td>
										</tr>
										<?php } ?>
										<?php if($vetor['valorassistenciaterceiros'] != '0.00') { ?>
										<tr>
											<td width="30%">
												<strong>COM ASSITENCIA TERCEIRO</strong>
											</td>
											<td width="70%">
												( <?php if($vetor['valorassistenciaterceiros'] != '0.00') { ?>X<?php } ?> ) Sim  ( <?php if($vetor['valorassistenciaterceiros'] == '0.00') { ?>X<?php } ?> ) Não
											</td>
										</tr>
										<?php } ?>
										<?php if($vetor['valorseguroapp'] != '0.00') { ?>
										<tr>
											<td width="30%">
												<strong>COM SEGURO APP</strong>
											</td>
											<td width="70%">
												( <?php if($vetor['valorseguroapp'] != '0.00') { ?>X<?php } ?> ) Sim  ( <?php if($vetor['valorseguroapp'] == '0.00') { ?>X<?php } ?> ) Não
											</td>
										</tr>
										<?php } ?>
										<?php if($vetor['valorcarroreserva'] != '0.00') { ?>
										<tr>
											<td width="30%">
												<strong>COM CARRO RESERVA</strong>
											</td>
											<td width="70%">
												( <?php if($vetor['valorcarroreserva'] != '0.00') { ?>X<?php } ?> ) Sim  ( <?php if($vetor['valorcarroreserva'] == '0.00') { ?>X<?php } ?> ) Não
											</td>
										</tr>
										<?php } ?>
										<?php if($vetor['outros'] != '0.00') { ?>
										<tr>
											<td width="30%">
												<strong>VACINA</strong>
											</td>
											<td width="70%">
												( <?php if($vetor['outros'] != '0.00') { ?>X<?php } ?> ) Sim  ( <?php if($vetor['outros'] == '0.00') { ?>X<?php } ?> ) Não
											</td>
										</tr>
										<?php } ?>
									</table>
									
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%">
										<tr>
											<td width="30%">
												<font color="#FF0000">CONTRATAÇÃO</font>
											</td>
											<td width="20%">
												<font color="#FF0000">VALOR PLANO</font>
											</td>
											<td width="15%">
												<font color="#FF0000">QTDE DE PARCELA</font>
											</td>
											<td width="15%">
												<font color="#FF0000">PARCELA</font>
											</td>
											<td width="20%">
												<font color="#FF0000">FORMA DE PAGAMENTO</font>
											</td>
										</tr>
										<tr>
											<td>
												<strong>ADESÃO</strong>
											</td>
											<td>
												<?php echo number_format($vetor['adesao'],2,',','.'); ?>
											</td>
											<td>
												<?php echo $vetor['qtdparcadesao']; ?>
											</td>
											<td>
												<?php if($vetor['qtdparcadesao'] >= 2) { $valorparcadesao = $vetor['adesao'] / $vetor['qtdparcadesao']; echo number_format($valorparcadesao,2,',','.'); } ?>
											</td>
											<td>
												<?php echo $vetor['formapagadesao']; ?>
											</td>
										</tr>
										<tr>
											<td>
												<strong>Mensalidade</strong>
											</td>
											<td>
												<?php 

												$valortodasparcfinal = $vetor['mensalidade'];

												if($vetor['qtdparcadesao'] == 12) { 

													$valorparcelaadesao = $vetor['adesao'] / $vetor['qtdparcadesao'];

													$valortodasparcfinal += $valorparcelaadesao;

												}

												if($vetor['qtdparcinst'] == 12) { 

													$valorparcinstalacao = $vetor['instalacao'] / $vetor['qtdparcinst'];

													$valortodasparcfinal += $valorparcinstalacao;

												}

												if($vetor['qtdparc24'] == 12) { 

													$valorparcelaa24 = $vetor['valorassistencia24'] / $vetor['qtdparc24'];

													$valortodasparcfinal += $valorparcelaa24;

												}

												if($vetor['qtdparcterc'] == 12) { 

													$valorparcelaater = $vetor['valorassistenciaterceiros'] / $vetor['qtdparcterc'];

													$valortodasparcfinal += $valorparcelaater;

												} 

												if($vetor['qtdparcoutros'] == 12) { 

													$valorparcelaoutros = $vetor['valorassioutrosstenciaterceiros'] / $vetor['qtdparcoutros'];

													$valortodasparcfinal += $valorparcelaoutros;

												}

												if($vetor['qtdparccarroreserva'] == 12) { 

													$valorparcelacr = $vetor['valorcarroreserva'] / $vetor['qtdparccarroreserva'];

													$valortodasparcfinal += $valorparcelacr;

												}

												if($vetor['qtdparcseguroapp'] == 12) { 

													$valorparcelseguroapp = $vetor['valorseguroapp'] / $vetor['qtdparcseguroapp'];

													$valortodasparcfinal += $valorparcelseguroapp;

												}

												echo number_format($valortodasparcfinal,2,',','.'); ?>
											</td>
											<td>
											</td>
											<td>
											</td>
											<td>
												<?php echo $vetor['formapagmens']; ?>
											</td>
										</tr>
										<tr>
											<td>
												<strong>Instalação a domicilio</strong>
											</td>
											<td>
												<?php echo number_format($vetor['instalacao'],2,',','.'); ?>
											</td>
											<td>
											</td>
											<td>
												
											</td>
											<td>
												<?php echo $vetor['formapaginst']; ?>
											</td>
										</tr>
										
									</table>
									
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" bgcolor="#FFFFFF">
										<tr>
											<td align="center">BENEFÍCIOS</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td width="90%">
												<?php if($vetor['tipoindenizacao'] == 1) { ?>
												Indenização por não recuperação em caso de roubo 100% valor da tabela FIPE;
												<br>
												Colisão acima de 75% do veiculo danificado em função do roubo válido para plano c/indenização 100% FIPE;
												<br>
												Incêndio  acima de 75% do veiculo danificado  em função do roubo válido para plano c/indenização 100% FIPE;
												<br>
												<?php } if($vetor['tipoindenizacao'] == 2) { ?>
												Indenização de acordo com plano escolhido
												<?php } ?>
												Sem análise de perfil de condutor e sem restrição de condutor; 
												<br>
												Sem restrição com veiculo para uso em aplicativos (UBER, 99 e etc.);
												<br>
												Aplicativo da nossa central totalmente gratuito e pode baixar em mais de um celular;
												<br>
												Visualização liberada do rastreador pela internet; (Baixe o APP);
												<br>
												Não tem botão de pânico para não colocar a vida dos ocupantes em risco;
												<br>
												Não tem sirene para não colocar a vida dos ocupantes em risco;
												<br>
												Histórico dos percursos rodados dos últimos 180 dias;
												<br>
												Central de Furto e Roubo 24 horas por dia 7 dias por semana 365 dias por ano no 0800;
												<br>
												Serviço de Âncora (você deixa seu veiculo estacionado caso ele se movimenta você será notificado);
												<br>
												Nossa cobertura e em todo território Nacional;
												<br>
												Instalação em domicílio ( Trabalho/ residência ou onde for conveniente).
											</td>
											<td width="10%" valign="top"><img src="imgs/orcamento.png" height="180px"></td>
										</tr>
									</table>
								</td>
							</tr>
							<?php if($vetor['valorassistencia24'] != '0.00') { ?>
							<tr>
								<td>
									<table width="100%" bgcolor="#FFFFFF">
										<tr>
											<td align="center">ASSISTÊNCIA 24 HORAS COMPLETA</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td width="100%">
												•	Guincho para PANE ELÉTRICA ou MECÂNICA - 1X POR MÊS 12X POR ANO (Na hipótese de pane, que impossibilite a locomoção do veículo, será providenciado de um S.O.S. para realizar o paliativo no local, Caso não seja possível efetuar o paliativo será rebocado para o local do cliente)
												<br>
												•	Guincho para PANE SECA – FALTA DE COMBUSTÍVEL (GASOLINA, ÁLCOOL, DIESEL E GNV) 1X POR MÊS 12X POR ANO (Será providenciado o reboque até o posto de abastecimento mais próximo para que o usuário possa abastecê-lo e seguir sua viagem)
												<br>
												•	Guincho para TROCA DE PNEUS (EXCLUÍDOS VEÍCULOS PESADOS/EXTRA PESADOS) 1X POR MÊS 12X POR ANO (será enviado um prestador para efetuar a troca do pneu danificado desde que você tenha um step e as ferramentas, caso contrario o veículo será rebocado até a borracharia mais próxima.)
												<br>
												•	Guincho para CHAVEIRO 1X POR MÊS 12X POR ANO (Em caso de perda, extravio ou esquecimento das chaves dentro do veiculo será enviado um chaveiro para abertura do veículo (somente abertura do veiculo)
												<br>
												•	Guincho em caso ACIDENTE, ROUBO ou FURTO, INCÊNDIO 1x POR MÊS 12X POR ANO (Em caso de evento previsto (acidente/colisão, incêndio, roubo ou furto)
												<br>
												**	OUTROS BENEFICIOS DA ASISTENCIA:
												<br>
												•	ENVIO DE TÁXI 1X POR MÊS 12X POR ANO (Em caso de remoção do veículo assistido, a Assistência 24h providenciará o transporte para o retorno do usuário até a outro destino, limitando a 25 (vinte e cinco) quilômetros de raio)
												</br>
												•	MEIO DE TRANSPORTE ALTERNATIVO – MTA. 1X POR MÊS 12X POR ANO (Em caso de evento previsto (acidente/colisão, incêndio, roubo ou furto) a Assistência 24h colocará à disposição do usuário assistido o Meio de Transporte mais adequado limite R$350,00
												</br>
												•	HOSPEDAGEM (Este serviço só será disponibilizado em decorrência de um evento previsto, caso não seja possível fornecer o Meio de Transporte Alternativo – MTA terão direito usuários e acompanhantes a duas diárias de hotel em rede credenciada limitado a R$ 500,00 (quinhentos reais) Obs.: 1) Serviço não extensivo a atendimento de pane 2) (Somente as diárias.)
												</br>
												•	MOTORISTA AMIGO 2X POR ANO( Se o usuário assistido não se sentir em condições físicas ou psicológicas de conduzir o veículo. ESTE SERVIÇO PODERÁ SER SOLICITADO TAMBÉM PARA DEIXAR O (USUÁRIO MAIS TRANQUILO AO VOLTAR PARA A CASA, RESPEITANDO A LEI SECA )
 
												</br>
												•	TRANSPORTE PARA RETIRADA DO VEÍCULO (Sendo o veículo assistido localizado pós roubo ou furto, a Assistência 24h coloca à disposição um Táxi para que o proprietário do veículo possa recuperá-lo. Limite: Até 25 (vinte e cinco) km de raio (uma utilização por mês).
												</br>
												•	REMOÇÃO HOSPITALAR APÓS ACIDENTE; ENVIO DE ACOMPANHANTE E TRANSMISSÃO DE MENSAGENS EM CASO DE ACIDENTE;
												</br>
												•	TRASLADO DE CORPO EM CASO DE FALECIMENTO; REGRESSO ANTECIPADO (Maiores informações sobre este itens consulte o manual da assistência 24 horas)
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php } if($vetor['valorassistenciaterceiros'] != '0.00') { ?>
							<tr>
								<td>
									<table width="100%" bgcolor="#FFFFFF">
										<tr>
											<td align="center">DANOS MATERIAS E DANOS PESSOAIS TERCEIROS</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td width="100%" valign="top" align="center">
												DANOS MATERIAS TERCEIROS (cobertura de até R$ 50, 000,00 em caso de prejuízo a terceiro em função colidir um bem de terceiro). 
												<br>
												DANOS PESSOAIS A TERCEIROS (Cobertura de até R$ 50, 000,00 para despesas médicas e hospitalares em função de acidente com terceiros). 
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php } ?>
							<?php if($vetor['valorcarroreserva'] != '0.00') { ?>
							<tr>
								<td>
									<table width="100%" bgcolor="#FFFFFF">
										<tr>
											<td align="center">CARRO RESERVA</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td width="100%" valign="top" align="center">
												O Carro Reserva e liberado quando o veiculo do proprietário estiver sem condições de utilização em função de batidas, roubo, furto, incediado.
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php } ?>
							<?php if($vetor['valorseguroapp'] != '0.00') { ?>
							<tr>
								<td>
									<table width="100%" bgcolor="#FFFFFF">
										<tr>
											<td align="center">SEGURO APP ( ACIDENTE PARA PARA PASSAGEIROS) </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td width="100%" valign="top" align="center">
												Evento ocorrido exclusivamente no veiculo descriminado na apólice, externo, involuntário, violento, e causador de lesão física
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php } ?>
							<tr>
								<td>
									<table width="100%" bgcolor="#FFFFFF">
										<tr>
											<td align="center">PARA APROVAR O ORÇAMENTO ENVIE OS DOCUMENTOS ABAIXO PELO  WHATSAPP (11) 94996-0826</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td width="100%" valign="top">
												Foto da CNH ou RG
												<br>
												Foto do documento do veiculo ou nota fiscal (veículo zero km)
												<br>
												Endereço completo para instalação 
												<br>
												Endereço de e-mail
												<br>
												Nome de duas pessoas para contato em emergencia
												<br>
												<br>
												<?php

												$sql_vendedor = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_vendedor]'");
												$vetor_vendedor = mysqli_fetch_array($sql_vendedor);

												?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td width="100%" valign="top" bgcolor="#CCCCCC">
												Eu sou (a)o seu(ua) corretor(a): <?php echo $vetor_vendedor['nome']; ?> <?php echo $vetor['telefonerepresentante']; ?> seja  bem vindo(a) a JL SEGURO SYSTEM DO BRASIL 
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						

					</td>
					<td width="3%"></td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</div>
</br>
</br>
<div align="center">

	Rua Olivia Guedes Penteado, 156 cj – 02 – Socorro – São Paulo/SP (11) 4171-5781 Whatsapp (11) 94996-0826
	</br>
	site: www.jlsegurosystem.com.br email contato@jlsegurosystem.com.br

</div>
</body>
</html>
<script type="text/javascript">
<!--
        print();
-->
</script>