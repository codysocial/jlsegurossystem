<?php

require 'vendor/autoload.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

class Monetary {
    private static $unidades = array("um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze",
                                     "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove");
    private static $dezenas = array("dez", "vinte", "trinta", "quarenta","cinqüenta", "sessenta", "setenta", "oitenta", "noventa");
    private static $centenas = array("cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", 
                                     "seiscentos", "setecentos", "oitocentos", "novecentos");
    private static $milhares = array(
        array("text" => "mil", "start" => 1000, "end" => 999999, "div" => 1000),
        array("text" => "milhão", "start" =>  1000000, "end" => 1999999, "div" => 1000000),
        array("text" => "milhões", "start" => 2000000, "end" => 999999999, "div" => 1000000),
        array("text" => "bilhão", "start" => 1000000000, "end" => 1999999999, "div" => 1000000000),
        array("text" => "bilhões", "start" => 2000000000, "end" => 2147483647, "div" => 1000000000)        
    );
    const MIN = 0.01;
    const MAX = 2147483647.99;
    const MOEDA = " real ";
    const MOEDAS = " reais ";
    const CENTAVO = " centavo ";
    const CENTAVOS = " centavos ";    
     
    static function numberToExt($number, $moeda = true) {
        if ($number >= self::MIN && $number <= self::MAX) {
            $value = self::conversionR((int)$number);       
            if ($moeda) {
                if (floor($number) == 1) {
                    $value .= self::MOEDA;
                }
                else if (floor($number) > 1) $value .= self::MOEDAS;
            }
 
            $decimals = self::extractDecimals($number);            
            if ($decimals > 0.00) {
                $decimals = round($decimals * 100);
                $value .= " e ".self::conversionR($decimals);
                if ($moeda) {
                    if ($decimals == 1) {
                        $value .= self::CENTAVO;
                    }   
                    else if ($decimals > 1) $value .= self::CENTAVOS;
                }
            }
        }
        return trim($value);
    }
     
    private static function extractDecimals($number) {
        return $number - floor($number);
    }
     
    static function conversionR($number) {
        if (in_array($number, range(1, 19))) {
            $value = self::$unidades[$number-1];
        }
        else if (in_array($number, range(20, 90, 10))) {
             $value = self::$dezenas[floor($number / 10)-1]." ";           
        }     
        else if (in_array($number, range(21, 99))) {
             $value = self::$dezenas[floor($number / 10)-1]." e ".self::conversionR($number % 10);           
        }     
        else if (in_array($number, range(100, 900, 100))) {
             $value = self::$centenas[floor($number / 100)-1]." ";           
        }          
        else if (in_array($number, range(101, 199))) {
             $value = ' cento e '.self::conversionR($number % 100);         
        }   
        else if (in_array($number, range(201, 999))) {
             $value = self::$centenas[floor($number / 100)-1]." e ".self::conversionR($number % 100);        
        }  
        else {
            foreach (self::$milhares as $item) {
                if ($number >= $item['start'] && $number <= $item['end']) {
                    $value = self::conversionR(floor($number / $item['div']))." ".$item['text']." ".self::conversionR($number % $item['div']);
                    break;
                }
            }
        }        
        return $value;
    }
}

require('GExtenso.php');

include"includes/conexao.php";

$id = $_GET['id'];

$sql = mysqli_query($con, "select * from clientes where id_cli = '$id'");
$vetor = mysqli_fetch_array($sql);


$qtddoc = strlen($vetor['cpfcnpj']);

if($qtddoc == 14) {
                
$cpfcnpj = Mask("##.###.###/####-##",$vetor['cpfcnpj']); 
                
}
                
if($qtddoc == 11) {
                
$cpfcnpj = Mask("###.###.###-##",$vetor['cpfcnpj']); 
                
}

if($vetor['comissao'] == NULL || $vetor['comissao'] == '0.00') { 

$valor = $vetor['comissaop'];
$valorextenso = GExtenso::numero($valor).' por centro';

} else { 

$valor = $vetor['comissao'];
$valorextenso = strtoupper(Monetary::numberToExt($vetor['comissao']));

}

$dompdf->loadHtml('<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
.style2 {
	font-size: 12
}
.style3 {font-size: 12px}

@page {
                margin: 100px 25px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                color: white;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                color: #000000;
                text-align: center;
                line-height: 35px;
            }

            footer .pagenum:before {
		      	content: counter(page);
			}
-->
    </style>
</head>
<body>
		<header>
            <img src="imgs/logo.png" width="150px">
            <hr style="height:1px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
        </header>

        <footer>
            <hr style="height:1px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
            <table width="100%">

            	<tr><td width="100%">
				<div class="style3">
            	Rua Olívia Guedes Penteado,156 - Socorro
            	</br>
				São Paulo/SP – CEP 04764-000
				</br>
				www.jlsegurosystem.com.br
				</div>

            	</td></tr>


            </table> 
            <div class="pagenum-container style3"><span class="pagenum"></span></div>
        </footer>

<div class="style3">

<table width="100%">
	<tr>
		<td align="center"><span class="style3">Contrato de Parceria Comercial</span></td>
	</tr>

	<tr>
		<td>
		Pelo presente instrumento particular de parceria comercial sem vínculo empregatício, de um lado JL SEGURO SYSTEM DO BRASIL, inscrita no CNPJ nº. 29.940.383/0001-60 e isenta de Inscrição estadual, estabelecida na Rua Olivia Guedes Penteado, 156 cj 02, Socorro, São Paulo/SP, neste ato denominada de <strong>PARCEIRO OUTORGANTE</strong>, e de outro lado, '.$vetor["nome"].', inscrita no CNPJ nº. '.$cpfcnpj.', estabelecida no endereço: '.$vetor["endereco"].' Nº '.$vetor["numero"].', '.$vetor["complemento"].' Bairro: '.$vetor["bairro"].' na cidade de '.$vetor["cidade"].' no estado de '.$vetor["estado"].', ora em diante denominado de <strong>PARCEIRO OUTORGADO</strong>, têm entre si, justos e contratados o seguinte:
		</td>
	</tr>
	<tr>
		<td>
		<strong>CLÁUSULA PRIMEIRA: DA AUTONOMIA DESSE CONTRATO</strong> 
		</td>
	</tr>
	<tr>
		<td>
		
		O presente contrato é autêntico contrato autônomo de PARCERIA COMERCIAL sem vínculo empregatício ou Societário apenas comercial no qual os contratantes se obrigam ao que se segue nas cláusulas seguintes: 

		</td>
	</tr>
	<tr>

		<td>
		
		<strong>CLÁUSULA SEGUNDA - DO OBJETO DO CONTRATO</strong>
</td>
</tr>

<tr>

<td>
O presente contrato tem como objeto a parceria de <strong>INDICAÇÃO DE CLIENTE</strong> E/OU <strong>APROXIMAÇÃO DE CLIENTE A OUTORGANTE</strong>, serviços e produtos fornecidos pela PARCEIRA OUTORGANTE aos clientes da PARCEIRA OUTORGADA, no seguinte: 
</td>
</tr>
<tr>

<td>
Plano de rastreadores para veiculo e viabilizar propostas através de indicações de clientes potenciais. 

	</td>

	</tr>

	<tr>

	<td>

		<strong>PARÁGRAFO 1º PRIMEIRO</strong>

	</td>

	</tr>

	<tr>

	<td>

	A parceira OUTORGANTE assume inteiramente todas as responsabilidades civis e criminais perante os clientes que forem indicados pela Outorgada.

	</td>

	</tr>

	<tr>

	<td>

	<strong>PARÁGRAFO 2º SEGUNDO</strong>

	</td>

	</tr>

	<tr>

	<td>

	Os contratos de venda dos planos de rastreamento com indenização serão feitos sempre pela OUTORGANTE (JL SEGURO SYSTEM DO BRASIL) para o(a) cliente da Outorgada (cliente indicado pela outorgada com sucesso no fechamento do plano).

	</td>

	</tr>

	<tr>

	<td>

	<strong>PARÁGRAFO 3º TERCEIRO</strong>

	</td>

	</tr>

	<tr>

	<td>

	A PARCEIRA OUTORGADA fica inteiramente isenta de qualquer responsabilidade cível, criminal e trabalhista perante terceiros.

	</td>

	</tr>

	<tr>

	<td>

	<strong>CLÁUSULA TERCEIRA - DA FORMA DE PAGAMENTO</strong>

	</td>

	</tr>

	<tr>

	<td>

	A PARCEIRA OUTORGANTE (JL SEGURO SYSTEM DO BRASIL) pagará pelas indicações realizadas, sob forma de comissão por cada indicação ao PARCEIRO OUTORGADO, o valor de R$ '.$valor.' ('.$valorextenso.')  por cada contrato plenamente realizado ou seja, assinado e instalado o rastreador pelo PARCEIRO OUTORGANTE (JL SEGURO SYSTEM DO BRASIL).

	</td>

	</tr>

	<tr>

	<td>

	<strong>CLÁUSULA QUARTA – DAS OBRIGAÇOES DA PARCEIRA</strong>

	</td>

	</tr>

	<tr>

	<td>

	a) A PARCEIRA OUTORGANTE (JL SEGURO SYSTEM DO BRASIL) compromete-se a colocar à disposição do PARCEIRO OUTORGADO seus assessores e consultores (conforme negociação) especializados, em assuntos referentes aos serviços inerentes para contato direto com o cliente final, sempre que se fizer necessário e assegurar a boa execução das tarefas. 

	</td>

	</tr>

	<tr>

	<td>

	b) A PARCEIRA OUTORGANTE (JL SEGURO SYSTEM DO BRASIL) se responsabiliza por todo e qualquer prejuízo que possa ser acarretado a PARCEIRA OUTORGADO pelo não cumprimento de dispositivos legais relativos aos produtos e serviços acima enumerados, desde que não lhe possam os mesmos ser atribuídos, por motivos estranhos à sua vontade, tais como força maior comprovada, isentando a parceira OUTORGADA quaisquer responsabilidade referente ao produto e serviço ofertado pela parceira OUTORGANTE.

	</td>

	</tr>

	<tr>

	<td>

	c) A Parceira Outorgante (JL SEGURO SYSTEM DO BRASIL) se responsabiliza ainda por qualquer danos na parte elétrica do veiculo pertencente ao cliente indicado pelo outorgado, bem como perlas obrigações impostas por clausulas contratuais dos serviços de rastreamento e indenizações aos clientes indicados pelo outorgado. Para tanto irá cobrir inclusive de formar pecúnia/ financeira a garantia dada na parte elétrica do veiculo pela empresa outorgada parceira.

	</td>

	</tr>

	<tr>

	<td>

	<strong>CLÁUSULA QUINTA- DO PRAZO DO CONTRATO</strong>

	</td>

	</tr>

	<tr>

	<td>

	O presente contrato terá validade indeterminada, a contar de sua assinatura. Em caso de rescisão a parte interessada deve se manifestar em 30 (trinta dias) de antecedência. 

	</td>

	</tr>

	<tr>

	<td>

	<strong>CLÁUSULA DÉCIMA - DO FORO</strong>

	</td>

	</tr>

	<tr>

	<td>

	As partes elegem o foro da Cidade de São Paulo estado de SP como o competente para dirimir quaisquer outro, por mais privilegiado que seja ou se apresente inclusive à mudança de domicílio das partes. 

	</td>

	</tr>

	<tr>

	<td>

	E por estarem assim justas e contratadas, assim as partes em 02 (duas) vias de igual teor e para um só efeito jurídico, juntamente com as 02 (duas) testemunhas, abaixo indicadas.

	</td>

	</tr>

	<tr>

	<td>

	</td>

	</tr>

	<tr>

	<td>

	<div align="right">Sã Paulo,  '.date("d").'/'.date("m").'/'.date("Y").'</div>

	</td>

	</tr>

	<tr>

	<td>

	<table width="100%">
	<tr>
		<td width="50%" valign="top" align="center">
			
			_______________________________
			<p>
			JL SEGURO SYSTEM
			</p>
			CNPJ:29.940.383/0001-60		</td>
		<td width="50%" valign="top" align="center">
			
			_______________________________
			<p>
			'.$vetor["nome"].'
			</p>
			CPF.: '.$cpfcnpj.'</td>
	</tr>
</table>

	</td>

	</tr>

	<tr>

	<td align="center">

	TESTEMUNHAS:

	<img src="imgs/transp.png">
	<p><img src="imgs/transp.png"></p>

	</td>

	</tr>

	<tr>

	<td align="center">

	<table width="100%">

	<tr>

	<td width="50%" align="center">
	_______________________________________
	</td>

	<td width="50%" align="center">
	_______________________________________
	</td>

	</tr>

	</table>


	</td>

	</tr>

	

</table>
</div>
</body>
</html>');

$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$pdf = $dompdf->output();
$dompdf->stream();

?>