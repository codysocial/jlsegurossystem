<?php

require 'includes/conexao.php';

if (isset($_POST['dropmenu_name']) and !empty($_POST['dropmenu_name'])) {

	if (!empty($_POST['id_menu'])) {

		if (!empty($_POST['dropmenu_name']) and !empty($_POST['dropdown_link'])) {

			$dropmenu_name = addslashes(htmlspecialchars($_POST['dropmenu_name']));
			$dropdown_icon = addslashes(htmlspecialchars($_POST['dropdown_icon']));
			$id_menu = addslashes(htmlspecialchars($_POST['id_menu']));
			$dropdown_link = addslashes(htmlspecialchars($_POST['dropdown_link']));
			$id = addslashes(htmlspecialchars($_POST['id']));

			if (empty($dropdown_link)) {
				$dropdown_link = '#';
			} else {
				$dropdown_link = $dropdown_link;
			}

			if (updateMenuDropDown($dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $id, $isdropdown, $conn)) {
				echo json_encode(array('success' => 1));
			} else {
				echo json_encode(array('success' => 0));
			}

		} else {
			echo json_encode(array('success' => 0));
		}

	} else {
		echo json_encode(array('success' => 3));
	}

} else {
	echo json_encode(array('success' => 0));
}