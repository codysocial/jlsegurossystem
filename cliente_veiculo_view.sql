-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 21-Fev-2020 às 20:42
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `jlsegu62_sistema`
--

-- --------------------------------------------------------

--
-- Estrutura para vista `cliente_veiculo_view`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cliente_veiculo_view`  AS  select `a`.`id_veiculo` AS `id_veiculo`,`a`.`tipo` AS `tipo`,`a`.`id_cliente` AS `id_cliente`,`b`.`id_pedido` AS `id_pedido`,`a`.`modelo` AS `modelo`,`a`.`marca` AS `marca`,`a`.`valor` AS `valor`,`a`.`cor` AS `cor`,`a`.`combustivel` AS `combustivel`,`a`.`placa` AS `placa`,`a`.`renavan` AS `renavan`,`a`.`chassi` AS `chassi`,`a`.`anomod` AS `anomod` from (`cliente_veiculo` `a` join `pedidos` `b`) where `a`.`id_veiculo` = `b`.`id_veiculo` ;

--
-- VIEW  `cliente_veiculo_view`
-- Dados: Nenhum
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
