<?php

	include "includes/conexao.php";
        require "vendor/autoload.php";

    /// PRESCISA DA PASTA DO DOMMPDF
	
	$sql_distinct = mysqli_query($con, "SELECT DISTINCT id_cliente FROM pcld");

    $sql_total = mysqli_query($con, "SELECT SUM(valor) FROM pcld");
    $vetor_total = mysqli_fetch_array($sql_total);
    $total = $vetor_total['SUM(valor)'];

    $total = format_valor($total);


    function format_valor($num){
        $valor1 = number_format($num,2,',','.');
        
        return $valor1;
    }

    

	$tabela = "<table class='table table-bordered table-striped'><thead>
                        <tr>
                            <th>Contr.</th>
                            <th>nome</th>
                            <th>Descrição</th>
                            <th>Vencimento</th>
                            <th>Valor Conta</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        $return = "$tabela";

        // Captura os dados da consulta e inseri na tabela HTML
        while ($linha = mysqli_fetch_array($sql_distinct)) {


            $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$linha[id_cliente]' ");
            $vetor_cliente = mysqli_fetch_array($sql_cliente);

            
            $sql_subtotal = mysqli_query($con, "SELECT SUM(valor) FROM pcld where id_cliente = '$linha[id_cliente]' ");
            $vetor_subtotal = mysqli_fetch_array($sql_subtotal);
            $subtotal = $vetor_subtotal['SUM(valor)'];

            $subtotal = format_valor($subtotal);



            $nome = $vetor_cliente['nome'];


            $sql_pcld_cliente_atual = mysqli_query($con, "SELECT * from pcld where id_cliente = '$linha[id_cliente]' ");

            foreach ($sql_pcld_cliente_atual as $linha_atual) {

                $valor = $linha_atual["valor"];

                $id_cr = $linha_atual["id_cr"];

                $valor = number_format($valor,2,",",".");

                $status = $linha_atual["status"];

                if ($status == 1) {
                    $status = "Pendente";
                }elseif ($status == 2) {
                    $status = "Pago";
                }elseif ($status == 3) {
                    $status = "Cancelado";
                }
                

                $return.= "<tr>";
                $return.= "<td>" . utf8_encode($linha_atual["id_pedido"]) . "</td>";
                $return.= "<td>" . utf8_encode($nome) . "</td>";
                $return.= "<td>" . $linha_atual["descricao"] . "</td>";
                $return.= "<td>" . utf8_encode($linha_atual["datavencimento"]) . "</td>";
                $return.= "<td>" . utf8_encode($valor) . "</td>";
                $return.= "<td>" . utf8_encode($status) . "</td>";
                

                $return.= "</tr>";
                
            }

        	

            $return.= "<tr><td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='4'> SUB-TOTAL</td>
                        <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='2'>
                          R$ " . $subtotal . "   
                        </td></tr>";

            $return.= "<tr><td colspan='5'></td></tr>";
        }

        $return.= "<tr><td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='4'> VALOR TOTAL</td>
                        <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='2'>
                          R$ " . $total . "   
                        </td></tr>";

       	$return.= "</tbody></table>";

        

        

	
	//referenciar o DomPDF com namespace
	use Dompdf\Dompdf;

	// include autoloader
	//require_once("dompdf/autoload.inc.php");

	//Criando a Instancia
	$dompdf = new DOMPDF();
	
	// Carrega seu HTML
	$dompdf->load_html('

         <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">

         <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">

         <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

         <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">

         <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        '. $return .'

		');


	//Renderizar o html
	$dompdf->render();

	//Exibibir a página
	$dompdf->stream(
		"PCLD", 
		array(
			"Attachment" => true //Para realizar o download somente alterar para true
		)
	);
?>