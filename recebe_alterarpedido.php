<?php

session_start();
include"includes/conexao.php";
$id = $_GET['id'];
$id_cliente = $_POST['id_cliente'];

$sql_consultando = $conn->prepare("SELECT * FROM cr WHERE id_pedido = ?");
$sql_consultando->execute([$id]);

// CR INSTALADOR
$cr_instalador = $conn->prepare("SELECT * FROM cr WHERE id_pedido = ? AND descricao = ?");
$cr_instalador->execute([$id, 'Instalação do Pedido: '.$id]);
$cr_instalador_fetch = $cr_instalador->fetch(PDO::FETCH_ASSOC);

// CR ADESÃO
$cr_adesao = $conn->prepare("SELECT * FROM cr WHERE id_pedido = ? AND descricao = ?");
$cr_adesao->execute([$id, '1° Parcela Adesão Pedido: '.$id]);
$cr_adesao_fetch = $cr_adesao->fetch(PDO::FETCH_ASSOC);

// CR 1. MENSALIDADE
$cr_umMensal = $conn->prepare("SELECT * FROM cr WHERE id_pedido = ? AND descricao = ?");
$cr_umMensal->execute([$id, '1º Parcela Mensalidade Pedido: '.$id]);
$cr_umMensal_fetch = $cr_umMensal->fetch(PDO::FETCH_ASSOC);

if ($sql_consultando->rowCount() == 0) {
	$delete_financeiro = mysqli_query($con, "DELETE FROM cr WHERE id_pedido = $id");
}

function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
}

// -

	$sqlnumrenova = mysqli_query($con, "SELECT ordem FROM cr WHERE id_pedido = $id ORDER BY id_cr DESC LIMIT 1");
	$numdeordem_featch = mysqli_fetch_array($sqlnumrenova);

	if($numdeordem_featch['ordem'] == NULL){
	    $numdeordem = 1;
	}else{
	    $numdeordem = $numdeordem_featch['ordem'] + 1;
	}	


		// -
		$data = date('Y-m-d');

		$tipoindenizacao = $_POST['tipoindenizacao'];
		//contrato
		$datainstalacao = $_POST['datainstalacao'];
		$periodo = $_POST['periodo'];
		$vigenciainicio = $_POST['datainstalacao'];
		$timestamp1 = strtotime($vigenciainicio . "+1 year");
		$vigenciafim1 = date('Y-m-d', $timestamp1);
		$timestamp = strtotime($vigenciainicio . "+2 year");
		$vigenciafim = date('Y-m-d', $timestamp);
		$id_corretor = $_POST['id_corretor'];
		$id_indicador = $_POST['id_indicador'];

		$sql_cliente = $conn->prepare("update clientes SET vendedor = ? where id_cli = ?");
		$sql_cliente->execute([$id_corretor, $id_cliente]);

		$sql_consulta = $conn->prepare("select * from renovacoes where id_cliente = ?");
		$sql_consulta->execute([$id_cliente]);

		if($sql_consulta->rowCount() == 0){ 
			insert('renovacoes', [
				'id_cliente' => $id_cliente, 
				'data1' => $vigenciafim1, 
				'data2' => $vigenciafim
			], $conn);
		}

		// //cadastro endereço instalação
		$id_endereco = $_POST['id_endereco'];
		$cep1 = $_POST['cep1'];
		$endereco1 = strtoupper($_POST['endereco1']);
		$numero1 = $_POST['numero1'];
		$complemento1 = strtoupper($_POST['complemento1']);
		$bairro1 = strtoupper($_POST['bairro1']);
		$cidade1 = strtoupper($_POST['cidade1']);
		$estado1 = strtoupper($_POST['estado1']);			
		$observacoes1 = strtoupper($_POST['observacoes1']);

		if(!empty($id_endereco)) {
			update('cliente_instalacao', [
				'id_cliente' => $id_cliente, 
				'cep' => $cep1, 
				'endereco' => $endereco1, 
				'numero' => $numero1, 
				'complemento' => $complemento1, 
				'bairro' => $bairro1, 
				'cidade' => $cidade1, 
				'estado' => $estado1, 
				'observacoes' => $observacoes1
			], [
				'id_instalacao' => $id_endereco
			], "",$conn);
		} else {
			insert('cliente_instalacao', [
				'id_cliente' => $id_cliente, 
				'cep' => $cep1, 
				'endereco' => $endereco1, 
				'numero' => $numero1, 
				'complemento' => $complemento1, 
				'bairro' => $bairro1, 
				'cidade' => $cidade1, 
				'estado' => $estado1, 
				'observacoes' => $observacoes1
			], $conn); 
		}

		// //cadastro referencia cliente
		$id_referencia1 = $_POST['id_referencia1'];
		$id_referencia2 = $_POST['id_referencia2'];
		$nomep = strtoupper($_POST['nomep']);
		$parentesco = strtoupper($_POST['parentesco']);
		$telefonep = $_POST['telefonep'];
		$nomep1 = strtoupper($_POST['nomep1']);
		$parentesco1 = strtoupper($_POST['parentesco1']);
		$telefonep1 = $_POST['telefonep1'];

		if(!empty($nomep)) {
			update('cliente_referencias', [
				'id_cliente' => $id_cliente, 
				'nome' => $nomep, 
				'parentesco' => $parentesco, 
				'telefone' => $telefonep, 
			], [
				'id_referencia' => $id_referencia1
			], "",$conn);
		} 

		if($id_referencia1 == '') { 
			insert('cliente_referencias', [
				'id_cliente' => $id_cliente, 
				'nome' => $nomep, 
				'parentesco' => $parentesco, 
				'telefone' => $telefonep
			], $conn); 
		}

		if(!empty($nomep1)) {
			update('cliente_referencias', [
				'id_cliente' => $id_cliente, 
				'nome' => $nomep, 
				'parentesco' => $parentesco, 
				'telefone' => $telefonep, 
			], [
				'id_referencia' => $id_referencia2
			], "",$conn);
		}

		if($id_referencia2 == '') { 
			insert('cliente_referencias', [
				'id_cliente' => $id_cliente, 
				'nome' => $nomep1, 
				'parentesco' => $parentesco1, 
				'telefone' => $telefonep1
			], $conn); 
		}


		// //cadastro veiculo

			$marca = $_POST['marca'];
			$sql_marca = $conn->prepare("SELECT * FROM fp_marca WHERE codigo_marca = ?");
			$sql_marca->execute([$marca]);
			$vetor_marca = $sql_marca->fetchAll();

			$modelos = $_POST['modelos_alt'];

			$sql_modelo = $conn->prepare("SELECT * FROM `fp_modelo` WHERE codigo_modelo = ?");
			$sql_modelo->execute([$modelos]);
			$vetor_modelo = $sql_modelo->fetch();

			$anomod = $_POST['anomod'];
			$sql_anomod = $conn->prepare("SELECT * FROM fp_ano WHERE id_ano = ?");
			$sql_anomod->execute([$anomod]);
			$vetor_anomod = $sql_marca->fetchAll();

			$valor = moeda($_POST['valor']);
			if($valor == NULL) {
				//$valorcarro = $vetor_anomod['valor'];
			} else {
				$valorcarro = $valor;
			}

			$cor = strtoupper($_POST['cor']);
			$placa = strtoupper($_POST['placa']);
			$combustivel = strtoupper($_POST['combustivel']);
			$renavan = strtoupper($_POST['renavan']);
			$chassi = strtoupper($_POST['chassi']);

			$id_veiculo = $_POST['id_veiculo'];

			if(!empty($id_veiculo)) {
				update('cliente_veiculo', [
					'id_cliente' => $id_cliente, 
					'modelo' => $vetor_modelo["modelo"],
					'marca' =>  $marca,
					'valor' =>  $valorcarro,
					'cor' => $cor,
					'combustivel' => $combustivel,
					'placa' => $placa,
					'renavan' => $renavan,
					'chassi' => $chassi,
					'anomod' => $anomod
				], [
					'id_cliente' => $id_cliente
				], "",$conn);

				update('cliente_veiculo_view', [
					'id_cliente' => $id_cliente, 
					'modelo' => $vetor_modelo["modelo"],
					'marca' =>  $marca,
					'valor' =>  $valorcarro,
					'cor' => $cor,
					'combustivel' => $combustivel,
					'placa' => $placa,
					'renavan' => $renavan,
					'chassi' => $chassi,
					'anomod' => $anomod
				], [
					'id_cliente' => $id_cliente
				], "",$conn);

			} else { 

				insert('cliente_referencias', [
					'id_cliente' => $id_cliente, 
					'tipo' => $tipo_veiculo, 
					'modelo' => $vetor_modelo["modelo"], 
					'marca' => $marca, 
					'valor' => $valorcarro, 
					'cor' => $cor, 
					'combustivel' => $combustivel, 
					'placa' => $placa, 
					'renavan' => $renavan, 
					'chassi' => $chassi, 
					'anomod' => $anomod
				], $conn); 

			}

			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		$adesao = moeda($_POST['adesao']);
		$parcelar = $_POST['parcelar'];
		$formapag = $_POST['formapag'];
		$vencimentoadesao = $_POST['vencimentoadesao'];
		$formapag_1 = $_POST['formapag_1'];
		$vencimentoadesao_1 = $_POST['vencimentoadesao_1'];
		$formapag_2 = $_POST['formapag_2'];
		$vencimentoadesao2 = $_POST['vencimentoadesao2'];
		$formapag_3 = $_POST['formapag_3'];
		$vencimentoadesao3 = $_POST['vencimentoadesao3'];
		$mensalidade = moeda($_POST['mensalidade']);
		$mensalidade1 = $_POST['1mensalidade'];
		$demaismensalidade = $_POST['demaismensalidade'];

		$formapagmensalidade = $_POST['formapagmensalidade'];
		$vacina = moeda($_POST['vacina']);
		$vencimentovicina = $_POST['vencimentovicina'];
		$formapagvacina = $_POST['formapagvacina'];
		$instalacao = moeda($_POST['instalacao']);
		$vencimentoinstalacao = $_POST['vencimentoinstalacao'];
		$formapaginst = $_POST['formapaginst'];
		$assistencia = moeda($_POST['assistencia']);
		$parcelara24 = $_POST['parcelara24'];

		$formapaga24 = $_POST['formapaga24'];
		$vencimentoa24 = $_POST['vencimentoa24'];
		$formapag_a24 = $_POST['formapag_a241'];
		$vencimentoa241 = $_POST['vencimentoa24_1'];
		$formapag_a242 = $_POST['formapag_a242'];
		$vencimentoa242 = $_POST['vencimentoa242'];
		$formapag_a243 = $_POST['formapag_a243'];
		$vencimentoa243 = $_POST['vencimentoa243'];

		$assistenciaterceiro = moeda($_POST['assistenciaterceiro']);
		$parcelarat = $_POST['parcelarat'];

		$formapagat = $_POST['formapagat'];
		$vencimentoat = $_POST['vencimentoat'];
		$formapag_at1 = $_POST['formapag_at1'];
		$vencimentoat_1 = $_POST['vencimentoat_1'];
		$formapag_at2 = $_POST['formapag_at2'];
		$vencimentoat2 = $_POST['vencimentoat2'];
		$formapag_at3 = $_POST['formapag_at3'];
		$vencimentoat3 = $_POST['vencimentoat3'];

		$carroreserva = moeda($_POST['carroreserva']);
		$parcelascarro = $_POST['parcelascarro'];

		$formapagcarro = $_POST['formapagcarro'];
		$vencimentocarro1 = $_POST['vencimentocarro1'];
		$formapagcarro1 = $_POST['formapagcarro1'];
		$vencimentocarro2 = $_POST['vencimentocarro2'];
		$formapagcarro2 = $_POST['formapagcarro2'];
		$vencimentocarro3 = $_POST['vencimentocarro3'];
		$formapagcarro3 = $_POST['formapagcarro3'];
		$vencimentocarro4 = $_POST['vencimentocarro4'];

		$carroapp = moeda($_POST['carroapp']);
		$parcelascarroapp = $_POST['parcelascarroapp'];

		$formapagcarroapp = $_POST['formapagcarroapp'];
		$vencimentocarroapp1 = $_POST['vencimentocarroapp1'];
		$formapagcarroapp1 = $_POST['formapagcarroapp1'];
		$vencimentocarroapp2 = $_POST['vencimentocarroapp2'];
		$formapagcarroapp2 = $_POST['formapagcarroapp2'];
		$vencimentocarroapp3 = $_POST['vencimentocarroapp3'];
		$formapagcarroapp3 = $_POST['formapagcarroapp3'];
		$vencimentocarroapp4 = $_POST['vencimentocarroapp4'];

		$outras = strtoupper($_POST['outras']);
		$observacoes = strtoupper($_POST['observacoes']);

		$qtdmensalidade = 12;

		$valoradesao = $adesao / $parcelar;
		$valora24 = $assistencia / $parcelara24;
		$valoraat = $assistenciaterceiro / $parcelarat;
		$valorcarrofinal = $carroreserva / $parcelascarro;
		$valorcarroapp = $carroapp / $parcelascarroapp;

		$valormensalidade = $mensalidade * 12;

		if($parcelar == 12) { 

			$valormensalidade += $adesao;

		}

		if($parcelara24 == 12) { 

			$valormensalidade += $assistencia;

		}

		if($parcelarat == 12) { 

			$valormensalidade += $assistenciaterceiro;

		}

		if($parcelascarro == 12) { 

			$valormensalidade += $carroreserva;

		}

		if($parcelascarroapp == 12) { 

			$valormensalidade += $carroapp;

		}

		$sql_produto = $conn->prepare("SELECT * FROM produtos WHERE id_produto = ?");
		$sql_produto->execute([$id_produto]);
		$vetor_produto = $sql_produto->fetchAll();

		$sql_pedido2 = $conn->prepare("UPDATE pedidos 
			SET 
			demaismensalidade = ?,
			`1mensalidade` = ?
		WHERE id_pedido = ?");
		$sql_pedido2->execute([$demaismensalidade, $mensalidade1, $id]);
		
		$sql_pedido = $conn->prepare(
			"UPDATE pedidos SET 
			`tipo` = '$tipoindenizacao', 
			`vigenciainicio` = '$vigenciainicio', 	
			`vigenciafim` = '$vigenciafim', 
			`id_cliente` = '$id_cliente', 
			`id_corretor` = '$id_corretor', 
			`id_indicador` = '$id_indicador', 
			`id_veiculo` = '$id_veiculo', 
			`adesao` = '$adesao', 
			`parcelar` = '$parcelar', 
			`formapag` = '$formapag', 
			`vencimentoadesao` = '$vencimentoadesao', 
			`formapag1` = '$formapag_1', 
			`vencimentoadesao1` = '$vencimentoadesao_1', 
			`formapag2` = '$formapag_2', 
			`vencimentoadesao2` = '$vencimentoadesao2', 
			`formapag3` = '$formapag_3', 
			`vencimentoadesao3` = '$vencimentoadesao3', 
			`mensalidade` = '$mensalidade', 
			`1mensalidade` = '$mensalidade1',
			`demaismensalidade` = '$demaismensalidade',
			`formapagmensalidade` ='$formapagmensalidade', 
			`vacina` = '$vacina', 
			`vencimentovicina` = '$vencimentovicina', 
			`formapagvacina` = '$formapagvacina', 
			`instalacao` = '$instalacao', 
			`vencimentoinstalacao` = '$vencimentoinstalacao', 
			`formapaginst` = '$formapaginst', 
			`assistencia` = '$assistencia', 
			`parcelara24h` = '$parcelara24', 
			`vencimentoassistencia` = '$vencimentoassistencia', 
			`assistenciaterceiro` = '$assistenciaterceiro', 
			`parcelarat` = '$parcelarat', 
			`vencimentoassistenciaterceiro` = '$vencimentoassistenciaterceiro', 
			`observacoes` = '$observacoes', 
			`carroreserva` = '$carroreserva', 
			`parcelascarro` = '$parcelascarro', 
			`formapagcarro` = '$formapagcarro', 
			`vencimentocarro1` = '$vencimentocarro1', 
			`formapagcarro1` = '$formapagcarro1', 
			`vencimentocarro2` = '$vencimentocarro2', 
			`formapagcarro2` = '$formapagcarro2', 
			`vencimentocarro3` = '$vencimentocarro3', 
			`formapagcarro3` = '$formapagcarro3', 
			`vencimentocarro4` = '$vencimentocarro4', 
			`carroapp` = '$carroapp', 
			`parcelascarroapp` = '$parcelascarroapp', 
			`formapagcarroapp` = '$formapagcarroapp', 
			`vencimentocarroapp1` = '$vencimentocarroapp1', 
			`formapagcarroapp1` = '$formapagcarroapp1', 
			`vencimentocarroapp2` = '$vencimentocarroapp2',
			`formapagcarroapp2` = '$formapagcarroapp2', 
			`vencimentocarroapp3` = '$vencimentocarroapp3', 
			`formapagcarroapp3` = '$formapagcarroapp3', 
			`vencimentocarroapp4` = '$vencimentocarroapp4', 
			`outras` = '$outras' WHERE id_pedido = '$id'"
		);
		$sql_pedido->execute();


		$id_produto1 = $_POST['id_rastreador1'];

		$sql_rastreador1 = mysqli_query($con, "SELECT * from rastreador where id_rastreador = $id_produto1");
		$vetor_rastreador1 = mysqli_fetch_array($sql_rastreador1);

		$nrastreador = $vetor_rastreador1['imei'];
		$rastreadormod = $vetor_rastreador1['modelo'];
		$fabricante = $vetor_rastreador1['fabricante'];

		$sql_vinculo = mysqli_query($con, "SELECT * FROM rastreador_chip WHERE id_rastreador = '$id_produto1' ");

		$vetor_vinculo = mysqli_fetch_array($sql_vinculo);


		$id_chip1 = $vetor_vinculo['id_chip'];

		$sql_chip1 = mysqli_query($con, "SELECT * from chips where id_chip = $id_chip1");
		$vetor_chip1 = mysqli_fetch_array($sql_chip1);

		$chipop = $vetor_chip1['operadora'];

		$sql_os = mysqli_query($con, "update os SET periodo='$periodo', datainstalacao='$datainstalacao', rastreador='$id_produto1', nrastreador='$nrastreador', contrato='$id_pedido', chipop='$chipop', rastreadormod='$rastreadormod', fabricante='$fabricante' where id_pedido = '$id'") or die (mysqli_error($con));

		$sql_desvincula = mysqli_query($con, "SELECT * from vinculo_rastreador where id_pedido = '$id'");

		$linhas = $sql_desvincula->num_rows;



		if ($linhas > 0) {

			$vetor_vinculo = mysqli_fetch_array($sql_desvincula);

			$id_chipantigo = $vetor_vinculo['id_chip'];
			$id_rastreadorantigo = $vetor_vinculo['id_rastreador'];

			$sql_desvinculachip = mysqli_query($con, "UPDATE chips SET situacao = '2' where id_chip = '$id_chipantigo' ");
			$sql_desvincularas = mysqli_query($con, "UPDATE rastreador SET situacao = '2' where id_rastreador = '$id_rastreadorantigo' ");

			$sql_updatesitu = mysqli_query($con, "UPDATE chips SET situacao = '3' WHERE id_chip = '$id_chip1'");
			$sql_updatesitu = mysqli_query($con, "UPDATE rastreador SET situacao = '3' WHERE id_rastreador = '$id_produto1'");

			$sql_vinculacao = mysqli_query($con, "UPDATE vinculo_rastreador SET id_rastreador = '$id_produto1', id_chip = '$id_chip1' where id_pedido = '$id' ") or die (mysqli_error($con));

			$desvincula_datapedido = mysqli_query($con, "UPDATE rastreador_chip SET data_pedido = '' WHERE id_rastreador = '$id_rastreadorantigo' ");
		}else{

			$insert_vinculo = mysqli_query($con, "INSERT INTO vinculo_rastreador (id_rastreador, id_chip, id_cliente, id_pedido) VALUES ('$id_produto1', '$id_chip1', '$id_cliente', '$id')") or die (mysqli_error($con));

			$sql_updatesitu = mysqli_query($con, "UPDATE chips SET situacao = '3' WHERE id_chip = '$id_chip1'");
			$sql_updatesitu = mysqli_query($con, "UPDATE rastreador SET situacao = '3' WHERE id_rastreador = '$id_produto1'");
		}

		date_default_timezone_set('America/Sao_Paulo');
		$data_vinculo = date('Y/m/d H:i:s');

		$update_datapedido = mysqli_query($con, "UPDATE rastreador_chip SET data_pedido = '$data_vinculo' WHERE id_rastreador = '$id_produto1' ");


if($sql_consultando->rowCount() == 0){

		// ========================================= // =================================================

		// 12 PARCELAS
		if($parcelar != 12) {

			if($formapag != NULL) {

				insert('cr', [
					'tipo' => '2', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '1° Parcela Adesão Pedido: '.$id, 
					'formapag' => $formapag, 
					'data' => $data, 
					'datavencimento' => $vencimentoadesao, 
					'valor' => $valoradesao, 
					'ordem' => $numdeordem
				], $conn); 

			}

			if($formapag_1 != NULL) {

				insert('cr', [
					'tipo' => '2', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '2° Parcela Adesão Pedido: '.$id, 
					'formapag' => $formapag_1, 
					'data' => $data, 
					'datavencimento' => $vencimentoadesao_1, 
					'valor' => $valoradesao, 
					'ordem' => $numdeordem
				], $conn); 

			}

			if($formapag_2 != NULL) {

				insert('cr', [
					'tipo' => '2', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '3° Parcela Adesão Pedido: '.$id, 
					'formapag' => $formapag_2, 
					'data' => $data, 
					'datavencimento' => $vencimentoadesao2, 
					'valor' => $valoradesao, 
					'ordem' => $numdeordem
				], $conn); 

			}

			if($formapag_3 != NULL) {

				insert('cr', [
					'tipo' => '2', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '4° Parcela Adesão Pedido: '.$id, 
					'formapag' => $formapag_3, 
					'data' => $data, 
					'datavencimento' => $vencimentoadesao3, 
					'valor' => $valoradesao, 
					'ordem' => $numdeordem
				], $conn); 

			}

		}

		//  24

		if($parcelara24 != 12) {

			if($formapaga24 != NULL) {

				insert('cr', [
					'tipo' => '3', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '1° Parcela Assistencia 24H Pedido: '.$id, 
					'formapag' => $formapaga24, 
					'data' => $data, 
					'datavencimento' => $vencimentoa24, 
					'valor' => $valora24, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapag_a24 != NULL) {

				insert('cr', [
					'tipo' => '3', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '2° Parcela Assistencia 24H Pedido: '.$id, 
					'formapag' => $formapag_a24, 
					'data' => $data, 
					'datavencimento' => $vencimentoa241, 
					'valor' => $valora24, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapag_a242 != NULL) {

				insert('cr', [
					'tipo' => '3', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '3° Parcela Assistencia 24H Pedido: '.$id, 
					'formapag' => $formapag_a242, 
					'data' => $data, 
					'datavencimento' => $vencimentoa242, 
					'valor' => $valora24, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapag_a243 != NULL) {

				insert('cr', [
					'tipo' => '3', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '4° Parcela Assistencia 24H Pedido: '.$id, 
					'formapag' => $formapag_a243, 
					'data' => $data, 
					'datavencimento' => $vencimentoa243, 
					'valor' => $valora24, 
					'ordem' => $numdeordem
				], $conn);

			}

		}

		if($parcelarat != 12) {

			if($formapagat != NULL) {

				insert('cr', [
					'tipo' => '4', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '1° Parcela Assistencia A Terceiros Pedido: '.$id, 
					'formapag' => $formapagat, 
					'data' => $data, 
					'datavencimento' => $vencimentoat, 
					'valor' => $valoraat, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapag_at1 != NULL) {

				insert('cr', [
					'tipo' => '4', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '2° Parcela Assistencia A Terceiros Pedido: '.$id, 
					'formapag' => $formapag_at1, 
					'data' => $data, 
					'datavencimento' => $vencimentoat_1, 
					'valor' => $valoraat, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapag_at2 != NULL) {

				insert('cr', [
					'tipo' => '4', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '3° Parcela Assistencia A Terceiros Pedido: '.$id, 
					'formapag' => $formapag_at2, 
					'data' => $data, 
					'datavencimento' => $vencimentoat2, 
					'valor' => $valoraat, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapag_at3 != NULL) {

				insert('cr', [
					'tipo' => '4', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '4° Parcela Assistencia A Terceiros Pedido: '.$id, 
					'formapag' => $formapag_at3, 
					'data' => $data, 
					'datavencimento' => $vencimentoat3, 
					'valor' => $valoraat, 
					'ordem' => $numdeordem
				], $conn);

			}

		}

		if($parcelascarro != 12) {

			if($formapagcarro != NULL) {

				insert('cr', [
					'tipo' => '5', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '1° Parcela CARRO RESERVA Pedido: '.$id, 
					'formapag' => $formapagcarro, 
					'data' => $data, 
					'datavencimento' => $vencimentocarro1, 
					'valor' => $valorcarrofinal, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapagcarro1 != NULL) {

				insert('cr', [
					'tipo' => '5', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '2° Parcela CARRO RESERVA Pedido: '.$id, 
					'formapag' => $formapagcarro1, 
					'data' => $data, 
					'datavencimento' => $vencimentocarro2, 
					'valor' => $valorcarrofinal, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapagcarro2 != NULL) {

				insert('cr', [
					'tipo' => '5', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '3° Parcela CARRO RESERVA Pedido: '.$id, 
					'formapag' => $formapagcarro2, 
					'data' => $data, 
					'datavencimento' => $vencimentocarro3, 
					'valor' => $valorcarrofinal, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapagcarro3 != NULL) {

				insert('cr', [
					'tipo' => '5', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '4° Parcela CARRO RESERVA Pedido: '.$id, 
					'formapag' => $formapagcarro3, 
					'data' => $data, 
					'datavencimento' => $vencimentocarro4, 
					'valor' => $valorcarrofinal, 
					'ordem' => $numdeordem
				], $conn);

			}

		}

		if($parcelascarroapp != 12) {

			if($formapagcarroapp != NULL) {

				insert('cr', [
					'tipo' => '8', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '1° Parcela CARRO APP Pedido: '.$id, 
					'formapag' => $formapagcarroapp, 
					'data' => $data, 
					'datavencimento' => $vencimentocarroapp1, 
					'valor' => $valorcarroapp, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapagcarroapp1 != NULL) {

				insert('cr', [
					'tipo' => '8', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '2° Parcela CARRO APP Pedido: '.$id, 
					'formapag' => $formapagcarro1, 
					'data' => $data, 
					'datavencimento' => $vencimentocarroapp2, 
					'valor' => $valorcarroapp, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapagcarroapp2 != NULL) {

				insert('cr', [
					'tipo' => '8', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '3° Parcela CARRO APP Pedido: '.$id, 
					'formapag' => $formapagcarroapp2, 
					'data' => $data, 
					'datavencimento' => $vencimentocarroapp3, 
					'valor' => $valorcarroapp, 
					'ordem' => $numdeordem
				], $conn);

			}

			if($formapagcarroapp3 != NULL) {

				insert('cr', [
					'tipo' => '8', 
					'id_cliente' => $id_cliente, 
					'id_pedido' => $id, 
					'descricao' => '4° Parcela CARRO APP Pedido: '.$id, 
					'formapag' => $formapagcarroapp3, 
					'data' => $data, 
					'datavencimento' => $vencimentocarroapp4, 
					'valor' => $valorcarroapp, 
					'ordem' => $numdeordem
				], $conn);

			}

		}

		// ===============================================================================================

		$DataInicial = new \DateTime($demaismensalidade);
		// Intervalo de 1 mês
		$Intervalo = new \DateInterval( 'P1M' );
		// Data daqui a 2 anos, apenas para aumentar a amostragem
		$DataFinal = new \DateTime( 'today + 11 month' );
		$Periodo = new \DatePeriod( $DataInicial, $Intervalo, $DataFinal );
		$valorcontrato = $valormensalidade / 12;

		if ($cr_umMensal_fetch['status'] != 2) {
			insert('cr', [
				'tipo' => '1', 
				'id_cliente' => $id_cliente, 
				'id_pedido' => $id, 
				'descricao' => '1º Parcela Mensalidade Pedido: '.$id, 
				'formapag' => $formapagmensalidade, 
				'data' => $data, 
				'datavencimento' => $demaismensalidade, 
				'valor' => $valorcontrato, 
				'ordem' => $numdeordem
			], $conn);
		}
		
		// VACINA
		if($vacina != '0.00') {

			insert('cr', [
			'tipo' => '6', 
			'id_cliente' => $id_cliente, 
			'id_pedido' => $id, 
			'descricao' => 'Vacina do Pedido: '.$id, 
			'formapag' => $formapagvacina, 
			'data' => $data, 
			'datavencimento' => $vencimentovicina, 
			'valor' => $vacina, 
			'ordem' => $numdeordem
			], $conn);

		}

		if($cr_instalador_fetch['status'] != 2){
			// INSTALAÇÃO
			if($instalacao != '0.00') {
				insert('cr', [
				'tipo' => '7', 
				'id_cliente' => $id_cliente, 
				'id_pedido' => $id, 	
				'descricao' => 'Instalação do Pedido: '.$id, 
				'formapag' => $formapaginst, 
				'data' => $data, 
				'datavencimento' => $vencimentoinstalacao, 
				'valor' => $instalacao, 
				'ordem' => $numdeordem
				], $conn);
			}
		}
}

header("Location: validamensalidadealt.php?id=$id&ummensalidade=$mensalidade1&rows=".$sql_consultando->rowCount());

?>