<?php

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

require_once 'vendor/autoload.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

include"includes/conexao.php";

$id_pedido = $_GET['id_pedido'];

$sql_pedido = mysqli_query($con, "select * from pedidos where id_pedido = '$id_pedido'");
$vetor_pedido = mysqli_fetch_array($sql_pedido);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$sql_ref1 = mysqli_query($con, "select * from cliente_referencias where id_cliente = '$vetor_pedido[id_cliente]' order by id_referencia ASC limit 0,1");
$vetor_ref1 = mysqli_fetch_array($sql_ref1);

$sql_ref2 = mysqli_query($con, "select * from cliente_referencias where id_cliente = '$vetor_pedido[id_cliente]' order by id_referencia DESC limit 0,1");
$vetor_ref2 = mysqli_fetch_array($sql_ref2);

$sql_end = mysqli_query($con, "select * from cliente_instalacao where id_cliente = '$vetor_pedido[id_cliente]' order by id_instalacao DESC limit 0,1");
$vetor_end = mysqli_fetch_array($sql_end);

$sql_veiculo = mysqli_query($con, "select * from cliente_veiculo where id_cliente = '$vetor_pedido[id_cliente]' order by id_veiculo DESC limit 0,1");
$vetor_veiculo = mysqli_fetch_array($sql_veiculo);

$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$vetor_veiculo[marca]'") or die (mysqli_error($con));
$vetor_marca = mysqli_fetch_array($sql_marca);

$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$vetor_veiculo[anomod]'") or die (mysqli_error($con));
$vetor_anomod = mysqli_fetch_array($sql_anomod);

$sql_os = mysqli_query($con, "select * from os where id_pedido = '$id_pedido'");
$vetor_os = mysqli_fetch_array($sql_os);

$sql_rastreador = mysqli_query($con, "select * from produtos where id_produto = '$vetor_os[rastreador]'");
$vetor_rastreador = mysqli_fetch_array($sql_rastreador);

$sql_corretor = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_corretor]'");
$vetor_corretor = mysqli_fetch_array($sql_corretor);

$sql_indicador = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_indicador]'");
$vetor_indicador = mysqli_fetch_array($sql_indicador);

$cpf = Mask("###.###.###-##",$vetor_cliente['cpfcnpj']);
$datanasc = date('d/m/Y', strtotime($vetor_cliente['datanasc']));
$datainstalacao = date('d/m/Y', strtotime($vetor_os['datainstalacao']));

$dataadesao = date('d/m/Y', strtotime($vetor_pedido['vencimentoadesao']));
$datamensalidade = date('d/m/Y', strtotime($vetor_pedido['1mensalidade']));

if($vetor_veiculo['valor'] != NULL) { $valorveiculo = number_format($vetor_veiculo['valor'],2,',','.'); } else { $valorveiculo = number_format($vetor_anomod['valor'],2,',','.'); }

//financeiro

$mensalidade = number_format($vetor_pedido['mensalidade'],2,',','.');
$totalmensalidade = $vetor_pedido['mensalidade'] * 12;
$totalizadormensalidade = number_format($totalmensalidade,2,',','.');

$adesao = number_format($vetor_pedido['adesao'],2,',','.');
$totaladesao = $vetor_pedido['mensalidade'] / $vetor_pedido['parcelar'];
$totaladesao1 = number_format($totaladesao,2,',','.');
$totalizadoradesao = number_format($adesao,2,',','.');

$assistenciatec = number_format($vetor_pedido['assistencia'],2,',','.');

$assistenciaterceiro = number_format($vetor_pedido['assistenciaterceiro'],2,',','.');

$vacina = number_format($vetor_pedido['vacina'],2,',','.');

$instalacao = number_format($vetor_pedido['instalacao'],2,',','.');

$carroreserva = number_format($vetor_pedido['carroreserva'],2,',','.');
$carroreservaparc = $vetor_pedido['carroreserva'] / $vetor_pedido['parcelascarro'];
$totalizadorcarroreserva = number_format($carroreservaparc,2,',','.');

$seguroapp = number_format($vetor_pedido['carroapp'],2,',','.');
$seguroappparc = $vetor_pedido['carroapp'] / $vetor_pedido['parcelascarroapp'];
$totalizadorseguroapp = number_format($seguroappparc,2,',','.');

$somatotal = $totalmensalidade + $vetor_pedido['adesao'] + $vetor_pedido['assistencia'] + $vetor_pedido['assistenciaterceiro'] + $vetor_pedido['vacina'] + $vetor_pedido['instalacao'] + $vetor_pedido['carroreserva'] + $vetor_pedido['carroapp'];
$somatotal1 = number_format($somatotal,2,',','.');

$html = '<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div style="font-size: x-small">
<table>
	<tr>
		<td><img src="imgs/logoimpressao.png" width="100%" height="200px"></td>
	</tr>
</table>
</br>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>CONTRATO Nº</strong></td>
		<td BGCOLOR="#e8e8e8">'.$id_pedido.'</td>
		<td><strong>RASTREADOR Nº</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_os[nrastreador].'</td>
		<td><strong>DATA: INSTALAÇÃO</strong></td>
		<td BGCOLOR="#e8e8e8">'.$datainstalacao.'</td>
	</tr>
</table>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>CHIP OP.:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_os[chipop].'</td>
		<td><strong>RASTREADOR</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_rastreador[modelo].'</td>
	</tr>
</table>
<p align="center"><FONT COLOR="#FF0000"><strong>DADOS DO CLIENTE</strong></FONT></p>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>CLIENTE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[nome].'</td>
		<td><strong>NASCTO.:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$datanasc.'</td>
	</tr>
	<tr>
		<td width="16%"><strong>CPF:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$cpf.'</td>
		<td><strong>RG:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[rg].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>ENDEREÇO::</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[endereco].'</td>
		<td><strong>Nº:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[numero].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>BAIRRO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[bairro].'</td>
		<td><strong>CEP:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[cep].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>CIDADE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[cidade].'</td>
		<td><strong>ESTADO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[estado].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>E-MAIL:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[email].'</td>
		<td><strong>CELULAR:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[celular].'</td>
	</tr>
</table>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>REF. PESSOAL(1)</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref1[nome].'</td>
		<td><strong>TELEFONE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref1[telefone].'</td>
		<td><strong>PARENTESCO</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref1[parentesco].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>REF. PESSOAL(1)</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref2[nome].'</td>
		<td><strong>TELEFONE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref2[telefone].'</td>
		<td><strong>PARENTESCO</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref2[parentesco].'</td>
	</tr>
</table>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>EMPRESA </strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[empresa].'</td>
		<td><strong>TELEFONE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[telempresa].'</td>
	</tr>
</table>';
if(mysqli_num_rows($sql_end) == 0) { } else { if($vetor_end['endereco'] == '') { } else {
$html .= '<p align="center"><FONT COLOR="#FF0000"><strong>ENDEREÇO DE INSTALAÇÃO</strong></FONT></p>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>ENDEREÇO::</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[endereco].'</td>
		<td><strong>Nº:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[numero].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>BAIRRO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[bairro].'</td>
		<td><strong>CEP:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[cep].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>CIDADE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[cidade].'</td>
		<td><strong>ESTADO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[estado].'</td>
	</tr>
</table>';
} }
$html .= '<p align="center"><FONT COLOR="#FF0000"><strong>DADOS DO VEICULO</strong></FONT></p>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>MARCA:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_marca[marca].'</td>
		<td><strong>MODELO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[modelo].'</td>
		<td><strong>VALOR R$</strong></td>
		<td BGCOLOR="#e8e8e8">'.$valorveiculo.'</td>
	</tr>
	<tr>
		<td width="16%"><strong>COR:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[cor].'</td>
		<td><strong>COMB.:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[combustivel].'</td>
		<td><strong>PLACA:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[placa].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>RENAVAN:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[renavan].'</td>
		<td><strong>CHASSI:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[chassi].'</td>
		<td><strong>ANO MOD:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_anomod[ano].'</td>
	</tr>
</table>
<p align="center"><FONT COLOR="#FF0000"><strong>INFORMAÇÕES DO PAGAMENTO</strong></FONT></p>
<table width="100%">
	<tr>
		<td>

			<table width="100%">';
				if($vetor_pedido['adesao'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">ADESÃO:</td>
					<td width="15%">'.$adesao.'</td>
					<td width="62%">PARCELAR EM: '.$vetor_pedido[parcelar].'</td>
				</tr>
				<tr>
					<td width="23%">FORMA DE PAGAMENTO:</td>
					<td width="15%">Cartão ( '; 
					if($vetor_pedido['formapag'] == 'Cartão') {  

						$html .= ' X'; } $html .= ' )</td>
					<td width="62%">Boleto ( '; 
					if($vetor_pedido['formapag'] == 'Boleto') {  

						$html .= ' X'; } $html .= ' )</td>
				</tr>
				<tr>
					<td width="23%">VENCIMENTO ADESÃO:</td>
					<td width="15%">'.$dataadesao.'</td>
					<td width="62%"></td>
				</tr>';
				} if($vetor_pedido['mensalidade'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">MENSALIDADE R$:</td>
					<td width="15%">'.$mensalidade.'</td>
					<td width="62%">1ª MENSALIDADE PARA: '.$datamensalidade.'</td>
				</tr>';
				} if($vetor_pedido['vacina'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">TX ADM R$:</td>
					<td width="15%">'.$vacina.'</td>
					<td width="62%"></td>
				</tr>';
				} if($vetor_pedido['instalacao'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">INSTALAÇÃO R$:</td>
					<td width="15%">'.$instalacao.'</td>
					<td width="62%"></td>
				</tr>';
				} if($vetor_pedido['assistencia'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">ASSITÊNCIA  24HS R$:</td>
					<td width="15%">'.$assistenciatec.'</td>
					<td width="62%"></td>
				</tr>';
				} if($vetor_pedido['assistenciaterceiro'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">ASSIST. TERCEIROS R$:</td>
					<td width="15%">'.$assistenciaterceiro.'</td>
					<td width="62%"></td>
				</tr>';
				} if($vetor_pedido['carroreserva'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">CARRO RESERVA R$:</td>
					<td width="15%">'.$carroreserva.'</td>
					<td width="62%"></td>
				</tr>';
				} if($vetor_pedido['carroapp'] != '0.00') { 
				$html .= '<tr>
					<td width="23%">SEGURO APP R$:</td>
					<td width="15%">'.$seguroapp.'</td>
					<td width="62%"></td>
				</tr>';
				}
			$html .= '<table>

		</td>
	</tr>
</table>
</br>
<table width="100%">
	<tr width="100%">
		<td width="50%"></td>
		<td width="25%" align="left">Assinatura do Cliente</td>
		<td width="25%" align="center">

			____________________________________

					</br>'.$vetor_cliente[nome].' 

		</td>
	<tr>
</table>
<table width="100%">
	<tr width="100%">
		<td width="50%"></td>
		<td width="50%" align="left">São Paulo ________/________/________.</td>
	<tr>
</table>
<hr style="height:2px; border:none; color:#000; background-color:#000; margin-top: 0px; margin-bottom: 0px;"/>
<table width="100%">
	<tr width="100%">
		<td width="100%">Observações</td>
	<tr>
	<tr width="100%">
		<td width="100%">'.$vetor_pedido[observacoes].'</td>
	<tr>
</table>
<br>
<p align="center"><FONT COLOR="#FF0000"><strong>DOCUMENTOS NECESSÁRIOS DO CLIENTE</strong></FONT></p>
<table width="100%">
	<tr>
		<td BGCOLOR="#e8e8e8" align="center">

			<FONT COLOR="#FF0000">
			Cópia de CNH ou RG, E CÓPIA DO DOCUMENTO DO VEICULO.

			<p>E-mail do cliente e nome de duas pessoas com referencia com telefone e grau de parentesco.</p>
			</FONT>

		</td>
	</tr>
</table>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<div align="center">'.$vetor_corretor[nome].' - '.$vetor_indicador[nome].'</div>
</div>
</body>
</html>';

echo $html;

?>

<script type="text/javascript">
<!--
        print();
-->
</script>