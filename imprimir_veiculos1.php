<!DOCTYPE html>
<html>
<head>

  <style type="text/css">
    .mesmo-tamanho{
      padding-right: 10px;
      padding-left: 1%;
      margin-left: 5px;
      width: 100px;
    }
    .btn_restaurar{
      border: 0px;
      border-radius: 3px;
      width: 100px;
      height: 33px;
      margin-left: 2px;
      background-color: green;
    }
  </style>

  <?php
    include"includes/conexao.php";
    
  ?>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">


<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body>
    <div style="width: 100%; display: flex; justify-content: center;"><h2>RELATÓRIO DOS VEÍCULOS CADASTRADOS</h2></div>
    <div style="width: 80%; margin-left: 10%; margin-top: 2%;">
<table id="example1" class="table table-bordered table-striped" style="overflow-y:hidden; overflow-x:hidden;">
                <thead style="overflow-y:hidden; overflow-x:hidden;">
                <tr>
                  <th>Modelo</th>
                  <th>Quantidade de Veiculos</th>
                  <th>Tipo Veicular</th>
                </tr>
                </thead>
                <tbody style="overflow-y:hidden; overflow-x:hidden;">
                <?php



                /// INIT VARS
                $quant_total_veicular = 0;

                $quant_carros = 0;
                $quant_motos = 0;
                $quant_onibus = 0;
                $quant_indefined = 0;

                $sql_distinct = mysqli_query($con, "SELECT DISTINCT modelo FROM cliente_veiculo");

                while ($vetor = mysqli_fetch_array($sql_distinct)) {

                  $nome_modelo = $vetor['modelo'];
                  $sql_modelo = mysqli_query($con, "SELECT modelo, marca from cliente_veiculo WHERE modelo = '$nome_modelo' ");
                  $vetor_infos = mysqli_fetch_array($sql_modelo);
                  $Quantidade_carro = mysqli_num_rows($sql_modelo);
                  $cod_marca = $vetor_infos['marca'];
                  $sql_marca = mysqli_query($con, "SELECT tipo FROM fp_marca WHERE codigo_marca = '$cod_marca' ");
                  $vetor_marca = mysqli_fetch_array($sql_marca);
                  $marca = $vetor_marca['tipo'];

                  if ($marca == 1) {
                    $tipo_v = "CARRO";
                    $quant_carros += $Quantidade_carro;

                  }elseif($marca == 2){
                    $tipo_v = "MOTO";
                    $quant_motos += $Quantidade_carro;

                  }elseif ($marca == 3) {
                    $tipo_v = "ONIBUS";
                    $quant_onibus += $Quantidade_carro;
                  }else{
                    $tipo_v = "INDEFINIDO";
                    $quant_indefined += $Quantidade_carro;
                  }

                  $quant_total_veicular += $Quantidade_carro;

              ?>
                
                <?php  if($marca == 1) { ?>
        
         
                <tr>
                  <td><?php echo $vetor['modelo']; ?></td>
                  <td><?php echo $Quantidade_carro; ?></td>
                  <td><?php echo $tipo_v; ?></td>
                </tr>

              <?php } ?>
                <?php  } ?>
                </tbody>
                
              </table>
              </form>
          </div>

<script type="text/javascript">
  $(function () {
    $('#example1').DataTable({
      "order": [[1, "desc"]],
      "searching": false,
      "paging":   false,
        "info":     false
    });
   
  })
</script>

<script type="text/javascript">

    setTimeout(function(){ print(); }, 2000);

</script>
</body>