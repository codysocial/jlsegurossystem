<?php

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
 
}

	 include"includes/conexao.php";
	 
	 session_start();

   $id_pagina = '7';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);

  $datainicio = $_POST['datainicio'];
  $datafim = $_POST['datafim'];
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <script type="text/javascript" src="aplicacoes/aplicjava.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<div class="container">

  <header class="main-header">

    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="imgs/logo.png" width="100px"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="imgs/logo.png" width="100px"></span>    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <?php include"includes/topo.php"; ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include"includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Seja bem-vindo,
        <small> <?php echo $vetor_cadastro['nome']; ?></small>      </h1>
      <ol class="breadcrumb">
        <li><a href="imprimircomissaoapagar.php?datainicio=<?php echo $datainicio; ?>&datafim=<?php echo $datafim; ?>" target="_blank"><button type="button" class="btn btn-warning mesmo-tamanho" title="Imprimir Cadastro"><i class="fa fa-print"></i></button></a></li>
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pedidos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pedidos com comissão a Pagar</h3>
              <a href="imprimircomissaoapagar.php?datainicio=<?php echo $datainicio; ?>&datafim=<?php echo $datafim; ?>" target="_blank"><button type="button" class="btn btn-warning mesmo-tamanho" title="Imprimir Cadastro"><i class="fa fa-print"></i></button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <font color="#FF0000"><strong>Período de <?php echo date('d/m/Y', strtotime($datainicio)); ?> a <?php echo date('d/m/Y', strtotime($datafim)); ?>.</strong></font>

            </br>
            </br>

              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">N° Pedido</th>
                  <th>Tipo</th>
                  <th>Nome</th>
                  <th>Nome do Cliente</th>
                  <th>Data Pedido</th>
                  <th>Valor Total</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <?php 
								  
				  $sql_atual = mysqli_query($con, "select a.id_pedido, a.id_corretor, a.id_cliente, a.data, a.mensalidade, a.adesao, a.vacina, a.instalacao, a.assistencia, a.assistenciaterceiro, a.parcelara24h, a.parcelascarro, a.carroreserva, a.carroapp, a.parcelascarroapp, b.id_pedido, b.datainstalacao from pedidos a, os b where a.id_pedido = b.id_pedido and a.comissao = '1' and b.datainstalacao BETWEEN '$datainicio' AND '$datafim'");
				
				  while ($vetor=mysqli_fetch_array($sql_atual)) {

          $mensalidade = $vetor['mensalidade'] * 12;

          $total = $vetor['adesao'] + $mensalidade + $vetor['vacina'] + $vetor['instalacao'] + $vetor['assistencia'] + $vetor['assistenciaterceiro'];

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);

          $sql_representante = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_corretor]'");
          $vetor_representante = mysqli_fetch_array($sql_representante);
				
				 ?>
                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td>Representante</td>
                  <td><?php echo $vetor_representante['nome']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($total,2,',','.'); ?></td>
                  <td></td>
                </tr>
                <?php

                if($vetor['assistencia'] != '0.00') { 

                  if($vetor['parcelara24h'] == '1') { 

                      $percentual = 10/100;

                      $valorpercentual = $percentual * $vetor['assistencia'];

                  } else { 

                    $percentual = 8/100;

                    $valorpercentual = $percentual * $vetor['assistencia'];

                  }

                ?>

                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td>Representante</td>
                  <td><?php echo $vetor_representante['nome']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($valorpercentual,2,',','.'); ?></td>
                  <td>ASSISTÊNCIA 24H</td> 
                </tr>
                <?php } ?>
                <?php

                if($vetor['carroreserva'] != '0.00') { 

                  if($vetor['parcelascarro'] == '1') { 

                      $percentual1 = 10/100;

                      $valorpercentual1 = $percentual1 * $vetor['carroreserva'];

                  } else { 

                    $percentual1 = 8/100;

                    $valorpercentual1 = $percentual1 * $vetor['carroreserva'];

                  }

                ?>

                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td>Representante</td>
                  <td><?php echo $vetor_representante['nome']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($valorpercentual1,2,',','.'); ?></td>
                  <td>CARRO RESERVA</td> 
                </tr>
                <?php } ?>
                <?php

                if($vetor['carroapp'] != '0.00') { 

                  if($vetor['parcelascarroapp'] == '1') { 

                      $percentual2 = 10/100;

                      $valorpercentual2 = $percentual2 * $vetor['carroapp'];

                  } else { 

                    $percentual2 = 8/100;

                    $valorpercentual2 = $percentual2 * $vetor['carroapp'];

                  }

                ?>

                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td>Representante</td>
                  <td><?php echo $vetor_representante['nome']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($valorpercentual2,2,',','.'); ?></td>
                  <td>SEGURO APP</td> 
                </tr>
                <?php } } ?>
                </tbody>
              </table>

            </br>
            </br>

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">N° Pedido</th>
                  <th>Tipo</th>
                  <th>Nome</th>
                  <th>Nome do Cliente</th>
                  <th>Data Pedido</th>
                  <th>Valor Total</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>

                <?php 
				
				//indicador
				  
				  $sql_atual = mysqli_query($con, "select a.id_pedido, a.id_cliente, a.data, a.mensalidade, a.adesao, a.vacina, a.instalacao, a.assistencia, a.assistenciaterceiro, a.parcelara24h, a.parcelascarro, a.carroreserva, a.carroapp, a.parcelascarroapp, a.id_indicador, b.id_pedido, b.datainstalacao, c.id_cli from pedidos a, os b, clientes c where a.id_pedido = b.id_pedido and a.id_indicador = c.id_cli and a.comissaoind = '1' and b.datainstalacao BETWEEN '$datainicio' AND '$datafim' and (a.id_indicador != '0' and a.id_indicador != '598')");
				
				  while ($vetor=mysqli_fetch_array($sql_atual)) {

          $mensalidade = $vetor['mensalidade'] * 12;

          $total = $vetor['adesao'] + $mensalidade + $vetor['vacina'] + $vetor['instalacao'] + $vetor['assistencia'] + $vetor['assistenciaterceiro'];

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);

          $sql_representante = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_indicador]'");
          $vetor_representante = mysqli_fetch_array($sql_representante);

          if($vetor['id_indicador'] != 0 || $vetor['id_indicador'] != 598) { 
				
				 ?>
                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td><?php if($vetor_representante['tipocad'] == 1) { echo "Cliente"; } if($vetor_representante['tipocad'] == 2) { echo "Fornecedor"; } if($vetor_representante['tipocad'] == 3) { echo "Vendedor"; } if($vetor_representante['tipocad'] == 4) { echo "Técnico - Instalador"; } if($vetor_representante['tipocad'] == 5) { echo "Indicador"; } if($vetor_representante['tipocad'] == 6) { echo "Representante"; } if($vetor_representante['tipocad'] == 7) { echo "Parceiro"; } ?></td>
                  <td><?php echo $vetor_representante['nome']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($total,2,',','.'); ?></td>
                  <td></td>
                </tr>
                <?php } } ?>

                </tbody>
              </table>

            </br>
            </br>

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">N° Pedido</th>
                  <th>Tipo</th>
                  <th>Nome</th>
                  <th>Nome do Cliente</th>
                  <th>Data Pedido</th>
                  <th>Valor Total</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>

                <?php 
				
				//tecnico
				  
				  $sql_atual = mysqli_query($con, "select a.id_pedido, a.id_cliente, a.data, a.mensalidade, a.adesao, a.vacina, a.instalacao, a.assistencia, a.assistenciaterceiro, a.parcelara24h, a.parcelascarro, a.carroreserva, a.carroapp, a.parcelascarroapp, b.id_pedido, b.datainstalacao, b.nometecnico, c.id_cli from pedidos a, os b, clientes c where a.id_pedido = b.id_pedido and b.nometecnico = c.id_cli and a.comissaotec = '1' and b.datainstalacao BETWEEN '$datainicio' AND '$datafim'");
				
				  while ($vetor=mysqli_fetch_array($sql_atual)) {

          $mensalidade = $vetor['mensalidade'] * 12;

          $total = $vetor['adesao'] + $mensalidade + $vetor['vacina'] + $vetor['instalacao'] + $vetor['assistencia'] + $vetor['assistenciaterceiro'];

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);

          $sql_representante = mysqli_query($con, "select * from clientes where id_cli = '$vetor[nometecnico]'");
          $vetor_representante = mysqli_fetch_array($sql_representante);

          if($vetor['nometecnico'] != '' || $vetor['nometecnico'] != NULL) {
				
				 ?>
                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td>Técnico - Instalador</td>
                  <td><?php echo $vetor_representante['nome']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($total,2,',','.'); ?></td>
                  <td></td>
                </tr>
                <?php } } ?>
                
                </tbody>
              </table>
              </br>

              

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });

  $(function(){
      $('html').css('background-color', '#F9EBFF');
      $('body').css('background-color', '#F9EBFF');
      $('.container').css('background-color', '#F9EBFF');
      $('.content').css('background-color', '#F9EBFF');
      $('.box').css('background-color', '#F9EBFF');
      $('.tab-content').css('background-color', '#F9EBFF');
      $('.box-title').addClass('text-ionc-3');
    });
</script>
</body>
</html>
<?php } } ?>