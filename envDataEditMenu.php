<?php

require 'includes/conexao.php';

if (isset($_POST['menu_name']) and !empty($_POST['menu_name'])) {

	if (!empty($_POST['menu_name']) and !empty($_POST['menu_link'])) {

		$menu_name = addslashes(htmlspecialchars($_POST['menu_name']));
		$menu_link = addslashes(htmlspecialchars($_POST['menu_link']));
		$menu_icon = addslashes(htmlspecialchars($_POST['menu_icon']));
		$menu_status = addslashes(htmlspecialchars($_POST['menu_status']));
		$isdropdown = addslashes(htmlspecialchars($_POST['isdropdown']));
		$id = addslashes(htmlspecialchars($_POST['id']));

		if (empty($isdropdown)) {
			$isdropdown = 0;
		} else {
			$isdropdown = $isdropdown;
		}

		if (editMenu($menu_name, $menu_link, $menu_icon, $menu_status, $isdropdown, $id, $conn)) {
			echo json_encode(array('success' => 1));
		} else {
			echo json_encode(array('success' => 0));
		}

	} else {
		echo json_encode(array('success' => 0));
	}

} else {
	echo json_encode(array('success' => 0));
}