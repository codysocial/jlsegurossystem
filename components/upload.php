<?php 

class upload
{
    // Propiedades
    
    private $imgName;
    
    # Inserir Foto

    public function save_photo()
    {
        if (isset($_FILES['myfile']['tmp_name']) && !empty($_FILES['myfile']['tmp_name'])) {

            // extensoes permitidos
            $img_exts_permitidos = array('image/png',
                'image/jpeg','image/jpg', 'image/gif','video/mp4','video/webm','video/ogg'
            );

            if (in_array($_FILES['myfile']['type'], $img_exts_permitidos)) {
                // pegando a extensao da midia
                $img_extensao = pathinfo($_FILES['myfile']['name']);
                // configurando um novo nome para a midia
                $this->setNameImg("PROFILE_".time()."_".rand(0,99999)."_".date('Y').".".$img_extensao['extension']);

                // Escolhendo o diretório e movendo a midia
                $upload_dir = "imgs/profile/";

                move_uploaded_file($_FILES['myfile']['tmp_name'], $upload_dir.$this->getNameImg());
            }else{
                $_SESSION['tipo_erro_img'] = "Isso não é uma imagem!";
            }

            return $this->getNameImg();

        }else{
            $this->setNameImg(null);
            return $this->getNameImg();
        }

    }
    
    // Getter e Setter

    function getNameImg() {
        return $this->imgName;
    }



    function setNameImg($imgName) {
        $this->imgName = $imgName;
    }
    
}