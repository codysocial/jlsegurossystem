function formatReal(int){
	var tmp = int+'';
	tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
	if( tmp.length > 6 )
	        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

	return tmp;
}

function mesInvert(obj){
	var item =  obj.split("-");

	var mon    =  item[1];
	var year   =  item[0];
	var html   =  'M'+mon.substr(1)+'Y'+year.substr(2);

	return html;
}

$(function(){

	$(window).resize(function(){

       if ($(window).width() <= 1005) {  

              $('.table').addClass('table-responsive');

       }     

	});

	$('#home-tab').on('click', function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});

	$('#profile-tab').on('click', function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});

	$('#contact-tab').on('click', function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});

	$('.dropdown-toggle').dropdown();
	$('[data-toggle="tooltip"]').tooltip();

	// MASCARAS - INPUTS
	$('#fieldWhatsapptTable').mask('(00) 0000-0000');
	$('#cpf_cnpj').mask('000.000.000-00', {reverse: true});
	$('#cnpj').mask('000.000.000-00', {reverse: true});
	$('#rg').mask('99.999.999-9');
	$('#nascimento').mask('00/00/0000');
	$('#vencimento').mask('00/00/0000');
	$("#barcode").mask('99999.99999 99999.999999 99999.999999 9 99999999999999');

	$('.tag_money').mask('000.000.000.000.000,00', {reverse: true});
	$('#inputValor').mask('000.000.000.000.000,00', {reverse: true});

	// mascara telefones

	$('#telefone').mask('(00) 0000-0000');
	$('#celular1').mask('(00) 0000-0000');
	$('#celular2').mask('(00) 0000-0000');
	$('#zap').mask('(00) 0000-0000');

	// mascara cep
	$('#cep').mask('00000-000');

	// Exibir ou Ocultar Informações - Cliente
	$('#hideInformationsClient').bind('click', function(){

		if ($('#inforsClientShowTable').hasClass('tableInforClientsNone') == false) {

			$('#inforsClientShowTable').hide('fast').addClass('tableInforClientsNone');
			$('#hideInformationsClient').html('<i class="far fa-eye"></i> Mostrar Informações');

		}else if ($('#inforsClientShowTable').hasClass('tableInforClientsNone') == true) {

			$('#inforsClientShowTable').show('fast').removeClass('tableInforClientsNone');
			$('#hideInformationsClient').html('<i class="far fa-eye-slash"></i> Ocultar Informações');
		}

	});

	$('#ieInsento').on('click', function() {
		if ($('#ieInsento').val() == 1) {
			$('#ie').attr('disabled', 'true');
		}
	});

	$('#refreshAssocBrand').bind('click', function(){
		$('#marca_select').load(location.href + " #marca_select>*", "");
		alert('Lista atualizada com sucesso!');
	});

	$('#refreshAssocCate').bind('click', function(){
		$('#categoriaf5').load(location.href + " #categoriaf5>*", "");
		$('#categoriaf5sub').load(location.href + " #categoriaf5sub>*", "");
		alert('Lista atualizada com sucesso!');
	});

	$('#user_photo').bind('click', function(){
		$("#dropdown-menu-sys").toggle(300);
	});

});



// preview image
function PreverImg() {
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("customFileImageProduct").files[0]);

	oFReader.onload = function (oFREvent) {

		document.getElementById("imgProductupload").src = oFREvent.target.result;

	};
};


$(function(){

	$('#formGetClients').keyup(function(e) {

		e.preventDefault();
		var item = $('#formGetClients').val();
		var totalpage = $('#totalPage').val();
		var tipo = $('#tipocad').val();

		$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
		$.ajax({
			url: url_sistema+'/qryclients/get',
			type: 'POST',
			data: {texto:item, total:totalpage, tipocad:tipo},
			dataType: 'json',
			success: function(json) {

				var tr = ``;
				for(var i in json){

					if (tipo == 1) {
						tr += `<tr>
					    <td class="field-value-table">${json[i].id_cli}</td>
					    <td class="field-value-table">${json[i].datacad}</td>
					    <td class="field-value-table">${json[i].nome}</td>
					    <td class="field-value-table">${json[i].cpfcnpj}</td>
					    <td class="field-value-table">${json[i].telefone}</td>
					    <td class="field-value-table">${json[i].celular}</td>
					    <td class="field-value-table">${json[i].email}</td>
					    <td>
							
							<a href="alterarcliente.php?id={{ $e->id_cli }}">
									<button type="button" class="btn btn-info btn-edit" title="Alterar Cadastro"><i class="fa fa-edit"></i>
								</button></a> 

								<a href="confexcluircliente.php?id={{ $e->id_cli }}" >
									<button type="button" class="btn btn-danger btn-del" title="Excluir Cadastro"><i class="far fa-trash-alt"></i>
									</button>
							</a>
			
					    </td>
						</tr>`;
					}else if(tipo == 2 || tipo == 7 || tipo == 3 || tipo == 5 || tipo == 6){
						tr += `<tr>
									<td class="field-value-table">${json[i].id_cli}</td>
									<td class="field-value-table">${json[i].nome}</td>
									<td class="field-value-table">${json[i].cpfcnpj}</td>
									<td class="field-value-table">${json[i].telefone}</td>
									<td class="field-value-table">
										<a href="{{ route('fornecedor.edit', $e->id_cli) }}">
											<button type="button" class="btn btn-info btn-edit" title="Alterar Cadastro"><i class="fa fa-edit"></i>
										</button></a> 

										<a href="{{ route('fornecedor.destroy', $e->id_cli) }}" >
											<button type="button" class="btn btn-danger btn-del" title="Excluir Cadastro"><i class="far fa-trash-alt"></i>
											</button>
										</a>
									</td>
								</tr>`;
					}

					
				}

				$('.pagination').css('display', 'none');
				$("#listarClientesAjax").html(tr);

			}
		});

	});

	

});

// conta a receber 
function getClient(id_client, id_cr){

    $.ajax({
        url: url_sistema+'/crajax/'+id_client,
        type: 'GET',
        dataType:'json',
        
        data: {id_cli:id_client},
        success: function(json){
            $('#id_cli'+id_cr).html(json[0].nome);
        }
    });

}

function formatDate(data, formato) {
  if (formato == 'pt-br') {
    return (data.substr(0, 10).split('-').reverse().join('/'));
  } else {
    return (data.substr(0, 10).split('/').reverse().join('-'));
  }
}

// pega data - analitico
function getDaysWeek(id_cr, tipo, status, datavencimento, datapagamento){

    $.ajax({
        url: `${url_sistema}/getdaysweek`,
        type: 'POST',
        dataType:'json',
        data: {
        	tipo:tipo, 
        	status:status, 
        	datavencimento:datavencimento, 
        	datapagamento:datapagamento
        },
        success: function(json){
            $('#ra_datavencimento'+id_cr).html(formatDate(json.datavencimento, 'pt-br'));
            $('#ra_datapagamento'+id_cr).html(formatDate(json.datapagamento, 'pt-br'));
        }
    });

}