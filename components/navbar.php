<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
    <title>gg</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Styles -->
    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/appSystem.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/appsystem-responsive.css" media="screen" />
    <!-- Scripts -->
    
    <script src="assets/js/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

    <script src="assets/js/jquery.mask.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/chart.min.js"></script>
    <script src="assets/js/appSystem.js"></script>
    <script src="assets/js/ajax/login.js"></script>
	
</head>
<body>

	<nav class="navbar fixed-top navbar-expand-lg navbar-light shadow p-3 mb-5" id="navbar">
  <a class="navbar-brand" href="./">
    <img src="assets/images/empresa/logo-white.png" id="logotipo-menu">
  </a>

  <button class="navbar-togglerr" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars"></i>
  </button>

  <div class="collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">

      <li class="nav-item active">
        <a class="nav-link" href="">CONFIGURAÇÕES</a>
      </li>
      

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bars"></i> CADASTROS GERAIS
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown3">

          <a class="dropdown-item" href="#">Cliente</a>
          <a class="dropdown-item" href="#">Fornecedor</a>
          <a class="dropdown-item" href="#">Parceiro</a>
          <a class="dropdown-item" href="#">Pedido</a>
          <a class="dropdown-item" href="#">Vendedor</a>
          <a class="dropdown-item" href="#">Indicador</a>
          <a class="dropdown-item" href="#">Representante</a>
          <a class="dropdown-item" href="#">Técnico - Instalador</a>

        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-plus-square"></i> NOVO
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown3">

          <a class="dropdown-item" href="#">Cliente</a>
          <a class="dropdown-item" href="#">Fornecedor</a>
          <a class="dropdown-item" href="#">Parceiro</a>
          <a class="dropdown-item" href="#">Pedido</a>
          <a class="dropdown-item" href="#">Vendedor</a>
          <a class="dropdown-item" href="#">Indicador</a>
          <a class="dropdown-item" href="#">Representante</a>
          <a class="dropdown-item" href="#">Técnico - Instalador</a>

          <a class="dropdown-item" href="#">ANIVERSARIANTES</a>

        </div>
      </li>

    </ul>


    <div class="user_photo" id="user_photo">

        <img src="{{ asset('assets/images/svg/002-user-1.svg') }}" />

        <div class="dropdown-menu-sys" id="dropdown-menu-sys">

          <a class="dropdown-item-sys" href=""><i class="fas fa-user"></i> Meu Perfil</a>
          <a class="dropdown-item-sys" href=""><i class="fas fa-cog"></i> Configurações</a>

          <div class="dropdown-divider-sys"></div>

          <a class="dropdown-item-sys" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> DESCONECTAR</a>

                                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    </form>

        </div>
        
    </div>

  </div>
</nav>

	
</body>
</html>