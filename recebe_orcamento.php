<?php

session_start();

include"includes/conexao.php";

function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
}

$nome = strtoupper($_POST['nome']);
$tipoindenizacao = $_POST['tipoindenizacao'];
$indenizacao = $_POST['indenizacao'];
$indenizacaoexplode = explode('_', $indenizacao);
$id_indenizacao = $indenizacaoexplode[0];
$valorindenizacao = moeda($_POST['valorindenizacao']);
$id_vendedor = $_POST['id_vendedor'];
$telefonerepresentante = $_POST['telefonerepresentante'];
$tipo_veiculo = $_POST['tipo_veiculo'];
$marca = $_POST['marca'];
$modelos = $_POST['modelos'];
$anomod = $_POST['anomod'];
$valorassistencia24 = moeda($_POST['valorassistencia24']);
$qtdparc24 = $_POST['qtdparc24'];
$formapag24 = strtoupper($_POST['formapag24']);
$valorassistenciaterceiros = moeda($_POST['valorassistenciaterceiros']);
$qtdparcterc = $_POST['qtdparcterc'];
$formapagterc = strtoupper($_POST['formapagterc']);
$adesao = moeda($_POST['adesao']);
$qtdparcadesao = $_POST['qtdparcadesao'];
$formapagadesao = strtoupper($_POST['formapagadesao']);
$mensalidade = moeda($_POST['mensalidade']);
$qtdparcmens = $_POST['qtdparcmens'];
$formapagmens = strtoupper($_POST['formapagmens']);
$valorseguroapp = moeda($_POST['valorseguroapp']);
$qtdparcseguroapp = $_POST['qtdparcseguroapp'];
$formapagseguroapp = strtoupper($_POST['formapagseguroapp']);
$valorcarroreserva = moeda($_POST['valorcarroreserva']);
$qtdparccarroreserva = $_POST['qtdparccarroreserva'];
$formapcarroreserva = strtoupper($_POST['formapcarroreserva']);
$instalacao = moeda($_POST['instalacao']);
$qtdparcinst = $_POST['qtdparcinst'];
$formapaginst = strtoupper($_POST['formapaginst']);
$outros = moeda($_POST['outros']);
$qtdparcoutros = $_POST['qtdparcoutros'];
$formapagoutros = strtoupper($_POST['formapagoutros']);

$data = date('Y-m-d');

if($valorassistencia24 != NULL) {

	$assistencia24 = "Sim";
}

if($valorassistenciaterceiros != NULL) {

	$assistenciaterceiros = "Sim";
}

if($valorcarroreserva != NULL) {

	$carroreserva = "Sim";
}

if($valorseguroapp != NULL) {

	$seguroapp = "Sim";
}

if($outros != NULL) {

	$outrossim = "Sim";
}

if($tipo_veiculo == 1) {

	$marca = $_POST['marca'];

	$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
	$vetor_marca = mysqli_fetch_array($sql_marca);

	$modelos = $_POST['modelos'];

	$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
	$vetor_modelo = mysqli_fetch_array($sql_modelo);

	$anomod = $_POST['anomod'];

	$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
	$vetor_anomod = mysqli_fetch_array($sql_anomod);

} if($tipo_veiculo == 2) {

	$marca = $_POST['marca1'];

	$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
	$vetor_marca = mysqli_fetch_array($sql_marca);

	$modelos = $_POST['modelos1'];

	$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
	$vetor_modelo = mysqli_fetch_array($sql_modelo);

	$anomod = $_POST['anomod1'];

	$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
	$vetor_anomod = mysqli_fetch_array($sql_anomod);

} if($tipo_veiculo == 3) {

	$marca = $_POST['marca2'];

	$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$marca'") or die (mysqli_error($con));
	$vetor_marca = mysqli_fetch_array($sql_marca);

	$modelos = $_POST['modelos2'];

	$sql_modelo = mysqli_query($con, "SELECT * FROM fp_modelo WHERE codigo_modelo = '$modelos'") or die (mysqli_error($con));
	$vetor_modelo = mysqli_fetch_array($sql_modelo);

	$anomod = $_POST['anomod2'];

	$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$anomod'") or die (mysqli_error($con));
	$vetor_anomod = mysqli_fetch_array($sql_anomod);

}

$sql = mysqli_query($con, "insert into orcamento (nome, tipoindenizacao, indenizacao, valorindenizacao, tipo, data, id_vendedor, telefonerepresentante, marca, modelo, ano, versao, valorfipe, assistencia24, qtdparc24, formapag24, valorassistencia24, assistenciaterceiros, valorassistenciaterceiros, qtdparcterc, formapagterc, adesao, qtdparcadesao, formapagadesao, mensalidade, qtdparcmens, formapagmens, instalacao, qtdparcinst, formapaginst, outrossim, outros, qtdparcoutros, formapagoutros, seguroapp, valorseguroapp, qtdparcseguroapp, formapagseguroapp, carroreserva, valorcarroreserva, qtdparccarroreserva, formapcarroreserva) VALUES ('$nome', '$tipoindenizacao', '$id_indenizacao', '$valorindenizacao',  '$tipo_veiculo', '$data', '$id_vendedor', '$telefonerepresentante', '$marca', '$modelos', '$anomod', '$vetor_modelo[modelo]', '$vetor_anomod[valor]', '$assistencia24', '$qtdparc24', '$formapag24', '$valorassistencia24', '$assistenciaterceiros', '$valorassistenciaterceiros', '$qtdparcterc', '$formapagterc', '$adesao', '$qtdparcadesao', '$formapagadesao', '$mensalidade', '$qtdparcmens', '$formapagmens', '$instalacao', '$qtdparcinst', '$formapaginst', '$outrossim', '$outros', '$qtdparcoutros', '$formapagoutros', '$seguroapp', '$valorseguroapp', '$qtdparcseguroapp', '$formapagseguroapp', '$carroreserva', '$valorcarroreserva', '$qtdparccarroreserva', '$formapcarroreserva')")  or die (mysqli_error($con));

echo"<script language=\"JavaScript\">
location.href=\"listarorcamentos.php\";
</script>";

?>