<?php

	function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
     }


class Monetary {
    private static $unidades = array("um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze",
                                     "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove");
    private static $dezenas = array("dez", "vinte", "trinta", "quarenta","cinqüenta", "sessenta", "setenta", "oitenta", "noventa");
    private static $centenas = array("cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", 
                                     "seiscentos", "setecentos", "oitocentos", "novecentos");
    private static $milhares = array(
        array("text" => "mil", "start" => 1000, "end" => 999999, "div" => 1000),
        array("text" => "milhão", "start" =>  1000000, "end" => 1999999, "div" => 1000000),
        array("text" => "milhões", "start" => 2000000, "end" => 999999999, "div" => 1000000),
        array("text" => "bilhão", "start" => 1000000000, "end" => 1999999999, "div" => 1000000000),
        array("text" => "bilhões", "start" => 2000000000, "end" => 2147483647, "div" => 1000000000)        
    );
    const MIN = 0.01;
    const MAX = 2147483647.99;
    const MOEDA = " real ";
    const MOEDAS = " reais ";
    const CENTAVO = " centavo ";
    const CENTAVOS = " centavos ";    
     
    static function numberToExt($number, $moeda = true) {
        if ($number >= self::MIN && $number <= self::MAX) {
            $value = self::conversionR((int)$number);       
            if ($moeda) {
                if (floor($number) == 1) {
                    $value .= self::MOEDA;
                }
                else if (floor($number) > 1) $value .= self::MOEDAS;
            }
 
            $decimals = self::extractDecimals($number);            
            if ($decimals > 0.00) {
                $decimals = round($decimals * 100);
                $value .= " e ".self::conversionR($decimals);
                if ($moeda) {
                    if ($decimals == 1) {
                        $value .= self::CENTAVO;
                    }   
                    else if ($decimals > 1) $value .= self::CENTAVOS;
                }
            }
        }
        return trim($value);
    }
     
    private static function extractDecimals($number) {
        return $number - floor($number);
    }
     
    static function conversionR($number) {
        if (in_array($number, range(1, 19))) {
            $value = self::$unidades[$number-1];
        }
        else if (in_array($number, range(20, 90, 10))) {
             $value = self::$dezenas[floor($number / 10)-1]." ";           
        }     
        else if (in_array($number, range(21, 99))) {
             $value = self::$dezenas[floor($number / 10)-1]." e ".self::conversionR($number % 10);           
        }     
        else if (in_array($number, range(100, 900, 100))) {
             $value = self::$centenas[floor($number / 100)-1]." ";           
        }          
        else if (in_array($number, range(101, 199))) {
             $value = ' cento e '.self::conversionR($number % 100);         
        }   
        else if (in_array($number, range(201, 999))) {
             $value = self::$centenas[floor($number / 100)-1]." e ".self::conversionR($number % 100);        
        }  
        else {
            foreach (self::$milhares as $item) {
                if ($number >= $item['start'] && $number <= $item['end']) {
                    $value = self::conversionR(floor($number / $item['div']))." ".$item['text']." ".self::conversionR($number % $item['div']);
                    break;
                }
            }
        }        
        return $value;
    }
}

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

include"includes/conexao.php";

$id = $_GET['id'];
$npedido = $_POST['npedido'];
$datapedido = $_POST['datapedido'];
$valor =  moeda($_POST['valor']);
$referencia = $_POST['referencia'];


$sql_os = mysqli_query($con, "select * from os where id_pedido = '$id'");
$vetor_os = mysqli_fetch_array($sql_os);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$id'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$sql_pedido = mysqli_query($con, "select * from pedidos where id_cliente = '$id' order by id_pedido DESC limit 0,1");
$vetor_pedido = mysqli_fetch_array($sql_pedido);

$sql_os = mysqli_query($con, "select * from os where id_pedido = '$vetor_pedido[id_pedido]'");
$vetor_os = mysqli_fetch_array($sql_os);

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
.style2 {
	font-size: 12
}
.style3 {font-size: 12px}

            .pontilhada {
			border: 2px dotted #999; /*Definindo o estilo da borda*/
			background-color: #FFFFFF; /*Cor de fundo para melhor visualização do exemplo*/
			height: 200px; /*Altura para melhor visualização do exemplo*/
			width: 420px; /*Largura para melhor visualização do exemplo*/
			}

-->
    </style>
</head>
<body>
<div class="style3">
<img src="imgs/logo.png" width="180px">
</br>
</br>
<table width="100%">
	<tr>
		<td>
		<div align="justify">
		<p align="center">
		<font color="#FF0000" size="6">NOTIFICAÇÃO EXTRAJUDICIAL</font>
		</p>
		</br>
        <font size="3">
		<p align="justify">
			Vimos por meio desta, informar a Vossa Senhoria, <?php echo $vetor_cliente['nome']; ?>, portador do  CPF nº <?php 
                
                $qtddoc = strlen($vetor_cliente['cpfcnpj']);

                if($qtddoc == 14) {
                
                echo Mask("##.###.###/####-##",$vetor_cliente['cpfcnpj']); 
                
                }
                
                if($qtddoc == 11) {
                
                echo Mask("###.###.###-##",$vetor_cliente['cpfcnpj']); 
                
                }
                
                
                ?>, que consta em nosso sistema a existência de débito em seu nome no valor de R$<?php echo $_POST['valor']; ?> (<?php echo strtoupper(Monetary::numberToExt($valor)); ?>), valor este ainda não atualizado, referente ao contrato de monitoramento veicular nº  <?php echo $npedido; ?>  conforme documentos assinados na data. <?php echo $datainstalacao = date('d/m/Y', strtotime($datapedido)); ?>, Após várias tentativas e todas frustradas por celular e whatsapp, tudo registrados no intuito de solucionarmos, amigável e extrajudicialmente, a questão, pedimos que o referido débito possa ser sanado da forma mais célere possível. Para tanto, pedimos que entre em contato conosco o mais rápido possível no prazo máximo de 72 (setenta e duas horas) a contar desta data no telefone (11) 4171-5781. Caso isso não ocorra estaremos acionando o poder judiciário conforme informa o contrato, bem como anotando o CPF junto aos órgão de proteção de crédito.
		</p>
		<p align="justify">

			O não cumprimento acarreta ao DEVEDOR, em consequência, a responsabilidade por perdas e danos. é o que preceitua o art. 389 do Código Civil: “Não cumprida a obrigação, responde o devedor por perdas e danos, mais juros e atualizações monetária segundo índices oficiais regularmente estabelecidos, e honorários de advogados”. Sobre a mora/juros é o retardamento ou imperfeito cumprimento da obrigação. 

		</p>

		<p align="justify">

			Caso o referido débito já tenha sido quitado ao tempo do recebimento desta, favor desconsiderar esta notificação e nos enviar comprovantes certos de que seremos prontamente atendidos nesse cordial pedido, desde já agradecemos sua compreensão. 
			
		</p>

        </font>

		</td>
	</tr>
	
	<tr>

	<td align="center">

	</br>
	</br>
	</br>
	</br>
	</br>
	</br>
	</td>

	</tr>

	<tr>

	<td>

	<img src="imgs/assinatura.png">

	</br>
	</br>

	</td>

	</tr>

	<tr>
		<td align="Right">São Paulo, <?php echo date('d/m/Y'); ?></td>
	</tr>

	
</table>
</div>
</body>
</html>

<script type="text/javascript">
<!--
        print();
-->
</script>