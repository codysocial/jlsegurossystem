-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 05-Fev-2020 às 21:06
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `jlsegu62_sistema`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastrobanco`
--

CREATE TABLE `cadastrobanco` (
  `id` int(11) NOT NULL,
  `cod_banco` int(25) NOT NULL,
  `nome_banco` varchar(40) NOT NULL,
  `nagencia` varchar(30) NOT NULL,
  `nconta` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `cadastrobanco`
--

INSERT INTO `cadastrobanco` (`id`, `cod_banco`, `nome_banco`, `nagencia`, `nconta`) VALUES
(12, 20, 'NET INTERNET E TELEFONIA ', '504845515', 51520515),
(13, 40, 'PLAY ARTES ', '584821-848', 45120890),
(14, 40, 'PLAY ARTES ', '6595256', 540548415),
(15, 20, 'NET INTERNET E TELEFONIA ', '5105415841', 540165418),
(16, 40, 'PLAY ARTES ', '51520315', 541121),
(17, 40, 'PLAY ARTES ', '55152850', 840584180),
(18, 1000, 'BRADESCO', '45445844', 2147483647);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cadastrobanco`
--
ALTER TABLE `cadastrobanco`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cadastrobanco`
--
ALTER TABLE `cadastrobanco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
