<?php

include"includes/conexao.php";

$codigos_no_total = $_GET['codigos'];


function format_valor($num){
    $valor1 = number_format($num,2,',','.');
        
    return $valor1;
}

$codigos_array = explode("-", $codigos_no_total);
array_pop($codigos_array);

$primeiro_cod = $codigos_array[0];

$sql_cr = mysqli_query($con, "SELECT id_cliente from cr where id_cr = '$primeiro_cod' ");
$vetor_id = mysqli_fetch_array($sql_cr);
$id_cliente = $vetor_id['id_cliente'];

$sql_cliente = mysqli_query($con, "SELECT * from clientes where id_cli = '$id_cliente' ");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$nome = $vetor_cliente['nome'];




$tabela = "<table class='table table-bordered table-striped'><thead>
                        <tr>
                            <th>Contr.</th>
                            <th>nome</th>
                            <th>Descrição</th>
                            <th>Vencimento</th>
                            <th>Valor Conta</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
$return = "$tabela";

$total_a_pagar = 0;
$total_pago = 0;

foreach ($codigos_array as $codigo_atual) {

	$sql = mysqli_query($con, "SELECT * from cr where id_cr = '$codigo_atual' ");

	while ($linha_atual = mysqli_fetch_array($sql)) {


	    $valor = $linha_atual["valor"];

	    $id_cr = $linha_atual["id_cr"];

	    $valor = number_format($valor,2,",",".");

	    $status = $linha_atual["status"];

	    if ($status == 1) {
	        $status = "Pendente";
	        $total_a_pagar += $linha_atual['valor'];

	    }elseif ($status == 2) {
	        $status = "Pago";
	        $total_pago += $linha_atual['valor'];

	    }elseif ($status == 3) {
	        $status = "Cancelado";
	    }
	                

	    $return.= "<tr>";
	    $return.= "<td>" . utf8_encode($linha_atual["id_pedido"]) . "</td>";
	    $return.= "<td>" . utf8_encode($nome) . "</td>";
	    $return.= "<td>" . $linha_atual["descricao"] . "</td>";
	    $return.= "<td>" . utf8_encode($linha_atual["datavencimento"]) . "</td>";
	    $return.= "<td>" . utf8_encode($valor) . "</td>";
	    $return.= "<td>" . utf8_encode($status) . "</td>";
	                

	    $return.= "</tr>";
	}
}

		$total_pago = format_valor($total_pago);
		$total_a_pagar = format_valor($total_a_pagar);

		$return.= "<tr>
                    <td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='5'> VALOR PAGO</td>
                    <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='2'>
                    R$ " . $total_pago . "</td>
                </tr>

                
                <tr>
                    <td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='5'> VALOR A RECEBER</td>
                    <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='2'>
                    R$ " . $total_a_pagar . "</td>
                </tr>";

    

    $return.= "</tbody></table>";

        

	//referenciar o DomPDF com namespace
	use Dompdf\Dompdf;

	// include autoloader
	require("vendor/autoload.php");

	//Criando a Instancia
	$dompdf = new DOMPDF();
	
	// Carrega seu HTML
	$dompdf->load_html('

         <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">

         <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">

         <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

         <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">

         <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        '. $return .'

		');


	//Renderizar o html
	$dompdf->render();

	//Exibibir a página
	$dompdf->stream(
		"Faturamento do Cliente " . $nome . "", 
		array(
			"Attachment" => false //Para realizar o download somente alterar para true
		)
	);


