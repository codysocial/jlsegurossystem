<?php

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

require_once 'vendor/autoload.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

include "includes/conexao.php";

$id_pedido = $_GET['id_pedido'];

$sql_pedido = mysqli_query($con, "select * from pedidos where id_pedido = '$id_pedido'");
$vetor_pedido = mysqli_fetch_array($sql_pedido);

$sql_corretor = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_corretor]'");
$vetor_corretor = mysqli_fetch_array($sql_corretor);

$sql_indicador = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_indicador]'");
$vetor_indicador = mysqli_fetch_array($sql_indicador);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$sql_ref1 = mysqli_query($con, "select * from cliente_referencias where id_cliente = '$vetor_pedido[id_cliente]' order by id_referencia ASC limit 0,1");
$vetor_ref1 = mysqli_fetch_array($sql_ref1);

$sql_ref2 = mysqli_query($con, "select * from cliente_referencias where id_cliente = '$vetor_pedido[id_cliente]' order by id_referencia DESC limit 0,1");
$vetor_ref2 = mysqli_fetch_array($sql_ref2);

$sql_end = mysqli_query($con, "select * from cliente_instalacao where id_cliente = '$vetor_pedido[id_cliente]' order by id_instalacao DESC limit 0,1");
$vetor_end = mysqli_fetch_array($sql_end);

$sql_veiculo = mysqli_query($con, "select * from cliente_veiculo where id_cliente = '$vetor_pedido[id_cliente]' order by id_veiculo DESC limit 0,1");
$vetor_veiculo = mysqli_fetch_array($sql_veiculo);

$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$vetor_veiculo[marca]'") or die (mysqli_error($con));
$vetor_marca = mysqli_fetch_array($sql_marca);

$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$vetor_veiculo[anomod]'") or die (mysqli_error($con));
$vetor_anomod = mysqli_fetch_array($sql_anomod);

$sql_os = mysqli_query($con, "select * from os where id_pedido = '$id_pedido'");
$vetor_os = mysqli_fetch_array($sql_os);

$sql_rastreador = mysqli_query($con, "select * from produtos where id_produto = '$vetor_os[rastreador]'");
$vetor_rastreador = mysqli_fetch_array($sql_rastreador);

$cpf = Mask("###.###.###-##",$vetor_cliente['cpfcnpj']);
$datanasc = date('d/m/Y', strtotime($vetor_cliente['datanasc']));
$datainstalacao = date('d/m/Y', strtotime($vetor_os['datainstalacao']));

$vigenciainicio = date('d/m/Y', strtotime($vetor_pedido['vigenciainicio']));
$vigenciafim = date('d/m/Y', strtotime($vetor_pedido['vigenciafim']));

if($vetor_veiculo['valor'] != NULL) { $valorveiculo = number_format($vetor_veiculo['valor'],2,',','.'); } else { $valorveiculo = number_format($vetor_anomod['valor'],2,',','.'); }

//financeiro

$mensalidade = number_format($vetor_pedido['mensalidade'],2,',','.');
$totalmensalidade = $vetor_pedido['mensalidade'] * 12;
$totalizadormensalidade = number_format($totalmensalidade,2,',','.');

$adesao = number_format($vetor_pedido['adesao'],2,',','.');
$totaladesao = $vetor_pedido['adesao'] / $vetor_pedido['parcelar'];
$totaladesao1 = number_format($totaladesao,2,',','.');
$totalizadoradesao = number_format($adesao,2,',','.');

$assistenciatec = number_format($vetor_pedido['assistencia'],2,',','.');
$totalassistenciatec = $vetor_pedido['assistencia'] / $vetor_pedido['parcelara24h'];
$totalizadorassistenciatec = number_format($totalassistenciatec,2,',','.');

$assistenciaterceiro = number_format($vetor_pedido['assistenciaterceiro'],2,',','.');
$totalassistenciaterc = $vetor_pedido['assistenciaterceiro'] / $vetor_pedido['parcelarat'];
$totalizadorassistenciaterc = number_format($totalassistenciaterc,2,',','.');

$vacina = number_format($vetor_pedido['vacina'],2,',','.');

$instalacao = number_format($vetor_pedido['instalacao'],2,',','.');

$carroreserva = number_format($vetor_pedido['carroreserva'],2,',','.');
$carroreservaparc = $vetor_pedido['carroreserva'] / $vetor_pedido['parcelascarro'];
$totalizadorcarroreserva = number_format($carroreservaparc,2,',','.');

$seguroapp = number_format($vetor_pedido['carroapp'],2,',','.');
$seguroappparc = $vetor_pedido['carroapp'] / $vetor_pedido['parcelascarroapp'];
$totalizadorseguroapp = number_format($seguroappparc,2,',','.');

$somatotal = $totalmensalidade + $vetor_pedido['adesao'] + $vetor_pedido['assistencia'] + $vetor_pedido['assistenciaterceiro'] + $vetor_pedido['vacina'] + $vetor_pedido['instalacao'] + $vetor_pedido['carroreserva'] + $vetor_pedido['carroapp'];
$somatotal1 = number_format($somatotal,2,',','.');

$html = '<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div style="font-size: x-small">
<table>
	<tr>
		<td><img src="imgs/logoimpressao.png" width="100%"></td>
	</tr>
	<tr>
		<td BGCOLOR="#e8e8e8" align="center">

			A JL SEGURO SYSTEM  proteção via satélite, inscrita CNPJ: 29.940.383/0001-60, com sede na Rua Olívia Guedes Penteado, N° 156 – CJ 2 - Socorro - São Paulo/SP - Cep: 04766-000, Empresa que opera serviços de Sistema de rastreamento para veiculos automotores ativado a distancia.  Vendas: (11) 4171-5781  CENTRAL 24 HORAS  0800 030 6672   ou    0800 777 9740  Opção 2   www.jlsegurosystem.com.br

			<p>A CONTRATADA OFERECE SERVIÇO DE RASTREADOR MONITORADO A DISTANCIA ANT-ROUBO, COM PACTO INDENIZÁTORIO, GARANTINDO A RECUPERAÇÃO OU A INDENIZAÇÃO DO SEU VEÍCULO.</p>

		</td>
	</tr>
</table>
</br>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>CONTRATO Nº</strong></td>
		<td BGCOLOR="#e8e8e8">'.$id_pedido.'</td>
		<td><strong>RASTREADOR Nº</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_os[nrastreador].'</td>
		<td><strong>DATA: INSTALAÇÃO</strong></td>
		<td BGCOLOR="#e8e8e8">'.$datainstalacao.'</td>
	</tr>
</table>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>CHIP OP.:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_os[chipop].'</td>
		<td><strong>RASTREADOR</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_rastreador[modelo].'</td>
	</tr>
</table>
<p align="center"><FONT COLOR="#FF0000"><strong>DADOS DO CLIENTE</strong></FONT></p>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>CLIENTE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[nome].'</td>
		<td><strong>NASCTO.:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$datanasc.'</td>
	</tr>
	<tr>
		<td width="16%"><strong>CPF:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$cpf.'</td>
		<td><strong>RG:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[rg].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>VIGÊNCIA INICIO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vigenciainicio.'</td>
		<td><strong>VIGÊNCIA FIM::</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vigenciafim.'</td>
	</tr>
	<tr>
		<td width="16%"><strong>ENDEREÇO::</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[endereco].'</td>
		<td><strong>Nº:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[numero].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>BAIRRO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[bairro].'</td>
		<td><strong>CEP:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[cep].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>CIDADE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[cidade].'</td>
		<td><strong>ESTADO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[estado].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>E-MAIL:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[email].'</td>
		<td><strong>CELULAR:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[celular].'</td>
	</tr>
</table>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>REF. PESSOAL(1)</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref1[nome].'</td>
		<td><strong>TELEFONE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref1[telefone].'</td>
		<td><strong>PARENTESCO</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref1[parentesco].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>REF. PESSOAL(1)</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref2[nome].'</td>
		<td><strong>TELEFONE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref2[telefone].'</td>
		<td><strong>PARENTESCO</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_ref2[parentesco].'</td>
	</tr>
</table>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>EMPRESA </strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[empresa].'</td>
		<td><strong>TELEFONE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_cliente[telempresa].'</td>
	</tr>
</table>';
if(mysqli_num_rows($sql_end) == 0) { } else { 

	if($vetor_end['cep'] == '') { } else {
$html .= '<p align="center"><FONT COLOR="#FF0000"><strong>ENDEREÇO DE INSTALAÇÃO</strong></FONT></p>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>ENDEREÇO::</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[endereco].'</td>
		<td><strong>Nº:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[numero].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>BAIRRO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[bairro].'</td>
		<td><strong>CEP:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[cep].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>CIDADE:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[cidade].'</td>
		<td><strong>ESTADO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_end[estado].'</td>
	</tr>
</table>';
} }
$html .= '<p align="center"><FONT COLOR="#FF0000"><strong>DADOS DO VEICULO</strong></FONT></p>
<table width="100%" BORDER="1" style="border-collapse: collapse">
	<tr>
		<td width="16%"><strong>MARCA:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_marca[marca].'</td>
		<td><strong>MODELO:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[modelo].'</td>
		<td><strong>VALOR R$</strong></td>
		<td BGCOLOR="#e8e8e8">'.$valorveiculo.'</td>
	</tr>
	<tr>
		<td width="16%"><strong>COR:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[cor].'</td>
		<td><strong>COMB.:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[combustivel].'</td>
		<td><strong>PLACA:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[placa].'</td>
	</tr>
	<tr>
		<td width="16%"><strong>RENAVAN:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[renavan].'</td>
		<td><strong>CHASSI:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_veiculo[chassi].'</td>
		<td><strong>ANO MOD:</strong></td>
		<td BGCOLOR="#e8e8e8">'.$vetor_anomod[ano].'</td>
	</tr>
</table>
<p align="center"><FONT COLOR="#FF0000"><strong>DESCRIÇÃO DO PRODUTO E NEGOCIAÇÃO DE PAGAMENTO</strong></FONT></p>
<table width="100%">
	<tr>
		<td>

		<table width="100%">
				<tr>
					<td width="20%"><strong>ÍTEM</strong></td>
					<td width="15%"><strong>VALOR</strong></td>
					<td width="15%"><strong>QTD PARC</strong></td>
					<td width="15%"><strong>TOTAL</strong></td>
					<td width="15%"><strong>VLR TOTAL</strong></td>
					<td width="20%"><strong>OUTRAS INFORMAÇÕES</strong></td>
				</tr>
				<tr>
					<td>PLANO:</td>
					<td>'.$mensalidade.'</td>
					<td>12</td>
					<td>'.$totalizadormensalidade.'</td>
					<td>'.$totalizadormensalidade.'</td>
					<td>CONTRATO POR 24 MESES SEGUE AS PRIMEIRAS 12 MENSALIDADES</td>
				</tr>';
				if($vetor_pedido['adesao'] != '0.00') { 
				$html .= '<tr>
					<td>ADESÃO:</td>
					<td>'.$adesao.'</td>
					<td>'.$vetor_pedido[parcelar].'</td>
					<td>'.$totaladesao1.'</td>
					<td>'.$adesao.'</td>
					<td>'.$vetor_pedido[outras].'</td>
				</tr>';
				} if($vetor_pedido['assistencia'] != '0.00') {
				$html .= '<tr>
					<td>ASSITÊNCIA  24HS:</td>
					<td>'.$assistenciatec.'</td>
					<td>'.$vetor_pedido[parcelara24h].'</td>
					<td>'.$totalizadorassistenciatec.'</td>
					<td>'.$assistenciatec.'</td>
					<td></td>
				</tr>';
				} if($vetor_pedido['assistenciaterceiro'] != '0.00') {
				$html .= '<tr>
					<td>ASSIST. TERC(DP+DM):</td>
					<td>'.$assistenciaterceiro.'</td>
					<td>'.$vetor_pedido[parcelarat].'</td>
					<td>'.$totalizadorassistenciaterc.'</td>
					<td>'.$assistenciaterceiro.'</td>
					<td></td>
				</tr>';
				} if($vetor_pedido['vacina'] != '0.00') {
				$html .= '<tr>
					<td>TX ADM:</td>
					<td>'.$vacina.'</td>
					<td>1</td>
					<td>'.$vacina.'</td>
					<td>'.$vacina.'</td>
					<td></td>
				</tr>';
				} if($vetor_pedido['instalacao'] != '0.00') {
				$html .= '<tr>
					<td>INSTALAÇÃO:</td>
					<td>'.$instalacao.'</td>
					<td>1</td>
					<td>'.$instalacao.'</td>
					<td>'.$instalacao.'</td>
					<td></td>
				</tr>';
				} if($vetor_pedido['carroreserva'] != '0.00') {
				$html .= '<tr>
					<td>CARRO RESERVA:</td>
					<td>'.$carroreserva.'</td>
					<td>'.$vetor_pedido[parcelascarro].'</td>
					<td>'.$carroreservaparc.'</td>
					<td>'.$totalizadorcarroreserva.'</td>
					<td></td>
				</tr>';
				} if($vetor_pedido['carroapp'] != '0.00') {
				$html .= '<tr>
					<td>SEGURO APP:</td>
					<td>'.$seguroapp.'</td>
					<td>'.$vetor_pedido[parcelascarroapp].'</td>
					<td>'.$seguroappparc.'</td>
					<td>'.$totalizadorseguroapp.'</td>
					<td></td>
				</tr>';
				}
				$html .= '<tr>
					<td></td>
					<td></td>
					<td></td>
					<td><strong>Total Contrato:</strong></td>
					<td><strong>'.$somatotal1.'</strong></td>
				</tr>
			<table>	

		</td>
	</tr>
</table>
</br>
</br>
<table>
	<tr>
	<td><img src="imgs/transp.png"></td>
	</tr>
<table>
<table width="100%">
	<tr>
		<td width="33%" align="center">
		_____________________________________
		<br>JL SEGURO  SYSTEM DO BRASIL 
		<br>CNPJ.: 29.940.383/0001-60
		</td>
		<td width="33%" align="center">
		_____________________________________
		<br>'.$vetor_cliente[nome].' 
		<br>CPF.: '.$cpf.'
		</td>
		<td width="33%" align="center">
		Data ______/______/______.
		</td>
	<tr>
</table>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
<table width="100%">
	<tr>
		<td>
			<div align="center">'.$vetor_corretor[nome].' - '.$vetor_indicador[nome].'</div>
		</td>
	<tr>
</table>

</div>
</body>
</html>';

echo $html;

?>

<script type="text/javascript">
<!--
        print();
-->
</script>