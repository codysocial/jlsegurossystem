<meta charset=utf-8><meta content="IE=edge"http-equiv=X-UA-Compatible><title>JL Seguro</title><meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"name=viewport><link href=layout/bower_components/bootstrap/dist/css/bootstrap.min.css rel=stylesheet>

<link href=layout/bower_components/font-awesome/css/font-awesome.min.css rel=stylesheet><link href=layout/bower_components/Ionicons/css/ionicons.min.css rel=stylesheet>

<link href=layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css rel=stylesheet>

<link id="alttheme" href=layout/dist/css/AdminLTE.css rel=stylesheet>

<link href=layout/dist/css/skins/_all-skins.min.css rel=stylesheet>

<link href=favicon-32x32.png rel=icon sizes=32x32 type=image/png>

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"rel=stylesheet><script src=//code.jquery.com/jquery-2.1.4.min.js></script><script>$(document).ready(function(){function l(){$("#rua").val(""),$("#bairro").val(""),$("#cidade").val(""),$("#uf").val(""),$("#ibge").val("")}$("#cep").blur(function(){var a=$(this).val().replace(/\D/g,"");if(""!=a){/^[0-9]{8}$/.test(a)?($("#rua").val("..."),$("#bairro").val("..."),$("#cidade").val("..."),$("#uf").val("..."),$("#ibge").val("..."),$.getJSON("//viacep.com.br/ws/"+a+"/json/?callback=?",function(a){"erro"in a?(l(),alert("CEP não encontrado.")):($("#rua").val(a.logradouro),$("#bairro").val(a.bairro),$("#cidade").val(a.localidade),$("#uf").val(a.uf),$("#ibge").val(a.ibge))})):(l(),alert("Formato de CEP inválido."))}else l()})})</script><script>function mascara(e,n){v_obj=e,v_fun=n,setTimeout("execmascara()",1)}function execmascara(){v_obj.value=v_fun(v_obj.value)}function mtel(e){return e=(e=(e=e.replace(/\D/g,"")).replace(/^(\d{2})(\d)/g,"($1) $2")).replace(/(\d)(\d{4})$/,"$1-$2")}function id(e){return document.getElementById(e)}window.onload=function(){id("telefone").onkeypress=function(){mascara(this,mtel)},id("telefone2").onkeypress=function(){mascara(this,mtel)}}</script>