<div id="contentModal">
<div id="regReciboClient" class="regReciboClient">
  <h6>Recibo Cliente  <a href="#" id="closeModal"><i class="fa fa-close"></i></a></h6>

  <div class="form-group">
      <label for="">RECEBI DE: </label>
      <input type="text" value="JL SEGUROS SYSTEM DO BRASIL" name="recebide" class="form-control" />
  </div>

  <div class="form-group" style="display: flex;flex-direction: row;">
      <div>
        <label for="">VALOR: </label>
        <input type="text" id="valorRec" name="valorrec" class="form-control" placeholder="0,00(Valor)" />
      </div>
      
      <div>
        <label for="">POR EXTENSO: </label>
      <input type="text" name="porextenso" class="form-control" placeholder="Valor por Extenso" />
      </div>
  </div>

  <div class="form-group">
    <label for="">REFERENTE: </label>
      <textarea name="referenterec" placeholder="Referente" id="referente" class="form-control"></textarea>
  </div>


  <input id="confirmRecYes" type="hidden" name="reciboyes" value="1">
  <input id="confirmRecNot" type="hidden" name="reciboyes" value="0">

</div>
</div>