<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8"/>
    <title>Untitled Document</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <script src="http://v2.systemjl.com.br/jquery-1.3.2.min.js"></script>
    <script>
        var url_sys = 'http://v2.systemjl.com.br/';
    </script>
</head>
<body>

    <script type="text/javascript">
        $(function(){

            $.ajax({
                url: 'http://v2.systemjl.com.br/themeSyn.php',
                type: 'GET',
                dataType: 'json',
                success: function(json){
                    if (Number(json.tema) != 1) {

                        $('#alttheme').attr('href', 'layout/dist/css/AdminLTE.css');
                        $('#logomarca').attr('src', 'http://v2.systemjl.com.br/components/assets/images/empresa/logo.png');
                        $('.iconmobileopen').attr('src', 'includes/menu.svg');

                    }else{
                        $('#alttheme').attr('href', 'layout/dist/css/dark.css');
                        $('#logomarca').attr('src', 'http://v2.systemjl.com.br/components/assets/images/empresa/logo-white.png');
                        $('.iconmobileopen').attr('src', 'includes/menu_white.svg');
                    }
                }
            });

        });
    </script>

    <nav id="sidebar">

        <a href="index"><img src="http://v2.systemjl.com.br/components/assets/images/empresa/logo.png" id="logomarca" alt="JL SEGUROS SYSTEM" /></a>

        <ul class="sidebar-menu" id="sidebar-menu" data-widget="tree">

            <li class="treeview">

                <a href="#" class="menu-ionc-3">
                <i class="fa fa-pie-chart text-ionc-3"></i>
                <span class="text-ionc-3">Cadastros Gerais</span>
                <span class="pull-right-container  hideToFromMovel">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu cadastros-gerais-pop">
                    <li><a href="listarclientes"><i class="far fa-circle"></i> Cliente</a></li>
                    <li><a href="listarfornecedores"><i class="far fa-circle"></i> Fornecedor</a></li>
                    <li><a href="listarparcerias"><i class="far fa-circle"></i> Parceiro</a></li>
                    <li><a href="listarvendedores"><i class="far fa-circle"></i> Vendedor</a></li>
                    <li><a href="listarparceiros"><i class="far fa-circle"></i> Indicador</a></li>
                    <li><a href="listarrepresentantes"><i class="far fa-circle"></i> Representante</a></li>
                    <li><a href="listarprestadores"><i class="far fa-circle"></i> Técnico - Instalador</a></li>
                    <li><a href="aniversariantes"><i class="far fa-circle"></i> Aniversariantes</a></li>
                    <li><a href="listarEstatisticas"><i class="far fa-circle"></i> Estatisticas</a></li>

                </ul>

            </li>

            <li class="treeview">

                <a href="#" class="menu-ionc-5">
                <i class="fa fa-money text-ionc-5"></i>
                <span class="text-ionc-5">Financeiro</span>
                <span class="pull-right-container  hideToFromMovel">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu cadastros-gerais-pop">
                    <li><a href="extratoconta"><i class="far fa-circle"></i> Fluxo de Caixa</a></li>
                    <li><a href="cadastro_conta"><i class="far fa-circle"></i> Cadastro de Contas</a></li>
                    <li><a href="cadastrosaldo"><i class="far fa-circle"></i> Cadastro de Saldos</a></li>
                    <li><a href="contascorrente"><i class="far fa-circle"></i> Histórico de Saldos</a></li>
                </ul>

            </li>

            <li class="treeview" onclick="window.location.href = 'listarfornecedores.php'">
                <a href="http://v2.systemjl.com.br/listarfornecedores.php" class="menu-ionc-9">
                    <i class="fa fa-cube text-ionc-9"></i> <span class="text-ionc-9">Fornecedor</span>
                </a>
            </li>   


            <li class="treeview" onclick="window.location.href = 'listarclientes.php'">
                <a href="http://v2.systemjl.com.br/listarclientes.php" class="menu-ionc-1">
                <i class="fa fa-cube text-ionc-1"></i> <span class="text-ionc-1">Clientes</span>
                </a>
            </li>          

            <li class="treeview hideToFromMovel">
                <a href="#">
                <i class="fa fa-money"></i> <span>Financeiro</span>
                <span class="pull-right-container  hideToFromMovel">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu financeiro-dropdown">

                    <li><a href="extratoconta"><i class="far fa-circle"></i> Fluxo de Caixa</a></li>
                    <li><a href="cadastro_conta"><i class="far fa-circle"></i> Cadastro de Contas</a></li>
                    <li><a href="cadastrosaldo"><i class="far fa-circle"></i> Cadastro de Saldos</a></li>
                    <li><a href="contascorrente"><i class="far fa-circle"></i> Histórico de Saldos</a></li>

                </ul>
            </li>

            <li class="treeview">
            <a href="#" class="menu-ionc-3">
            <i class="fas fa-cart-arrow-down text-ionc-3"></i> <span class="text-ionc-3">Contas a Pagar</span>
            <span class="pull-right-container  hideToFromMovel">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
            </a>
            <ul class="treeview-menu cp-dropdown">
            <li><a href="listarcp"><i class="far fa-circle"></i> Todos</a></li>
            <li><a href="gerarcomissoes"><i class="far fa-circle"></i> Gerar Comissões</a></li>
            
            <li class="treeview">
            <a href="#"><i class="far fa-circle"></i> Relatórios C. a Pagar
            </a>

            <ul class="treeview-menu">
            <li><a href="relcpanalitico"><i class="far fa-circle"></i> Analítico</a></li>
            <li><a href="relcpsintetico"><i class="far fa-circle"></i> Sintético</a></li>
            <li><a href="relcomissoesapagar"><i class="far fa-circle"></i> Comissões a Pagar</a></li>
            <li><a href="relcpcomissoes"><i class="far fa-circle"></i> Comissões</a></li>
            <li><a href="relcpfornecedor"><i class="far fa-circle"></i> Por Fornecedor</a></li>
            <li><a href="relcpcentrodecustos"><i class="far fa-circle"></i> Por Centro de Custos</a></li>
            <li><a href="relcpdespesa"><i class="far fa-circle"></i> Por Despesa</a></li>
            <li><a href="relcpprojetofinalidade"><i class="far fa-circle"></i> Por Projeto / Finalidade</a></li>
            </ul>

            </li> 

            </ul>

            </li>

            <li class="treeview">
                <a href="#" class="menu-ionc-4">
                <i class="fas fa-cart-plus text-ionc-4"></i> <span class="text-ionc-4">Contas a Receber</span>
                </a>
                <ul class="treeview-menu cp-dropdown">
                    <li><a href="listarcr"><i class="far fa-circle"></i> Todos</a></li>
                    <li><a href="listar_excluidos"><i class="far fa-circle"></i> Histórico De Exclusão</a></li>
                    <li><a href="relcranalitico"><i class="far fa-circle"></i> Analítico</a></li>
                    <li><a href="relcrsintetico"><i class="far fa-circle"></i> Sintético</a></li>
                    <li><a href="listarpcld"><i class="far fa-circle"></i> PCLD</a></li>  
                </ul>
            </li>

            <li class="treeview">
                <a href="#" class="menu-ionc-6">
                <i class="fa fa-cart-plus text-ionc-6"></i>
                <span class="text-ionc-6">Pedidos</span>
                </a>
                <ul class="treeview-menu cp-dropdown">
                    <li><a href="cadastropedido"><i class="far fa-circle"></i> Novo Pedido</a></li>
                    <li><a href="listarpedidos"><i class="far fa-circle"></i> Pedidos</a></li>
                </ul>
            </li>

            <li class="treeview">
            <a href="#" class="menu-ionc-5">
            <i class="fas fa-money-check-alt text-ionc-5"></i> <span class="text-ionc-5">Faturamento</span>
            </a>
            <ul class="treeview-menu cp-dropdown">

                <li><a href="relcrglobal"><i class="far fa-circle"></i> Por Periodo</a></li>
                <li><a href="relcrporitem"><i class="far fa-circle"></i> Por Produto</a></li>
                <li><a href="relcrcliente"><i class="far fa-circle"></i> Por Cliente</a></li>

            </ul>

            </li>


        </ul>

        <div class="iconmovel">
            <button  id="opendreamwave"><img src="includes/menu.svg"  class="iconmobileopen"></button>
        </div>


        <div class="box-user-photo-dropdown">
            
            <?php if (isset($vetor_cadastro['photo']) and !empty($vetor_cadastro['photo'])): ?>
                <img src="./imgs/profile/<?php echo $vetor_cadastro['photo']; ?>" id="imgUserMenuBox">
            <?php else: ?>
                <img src="./imgs/user.png" id="imgUserMenuBox">
            <?php endif; ?>

            <div class="dropdownMenuBox">
                <h3><?php echo $vetor_cadastro['nome']; ?></h3>

                <ul>
                    <li><a href="./myaccount">Minha Conta</a></li>
                    <li><a href="./listarusuarios">Configurações</a></li>
                    <li><a href="./sair">Sair</a></li>
                </ul>
            </div>

        </div>


    </nav>

    <?php include 'mobilePhone.php'; ?>

    <script type="text/javascript">

        $(function(){

            $('#opendreamwave').bind('click', function(){

                if ($('#mobile-menu').hasClass('menuOpenedDrawe')) {
                    $('#mobile-menu').fadeOut(50).css('display', 'none').removeClass('menuOpenedDrawe');
                } else {
                    $('#mobile-menu').slideDown(300).css('display', 'flex').addClass('menuOpenedDrawe');
                }

                
            });          

            $('#imgUserMenuBox').bind('click', function(){
                $('.dropdownMenuBox').slideToggle(50);
            });

        })

    </script>

</body>
</html>
