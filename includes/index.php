<?php

  /*!! <*$exec(pushContentsServerLaraFluks())\$/> !! */

  session_start();

  include "includes/conexao.php";
  include "includes/box-item-menu.php";

	if($_SESSION['id'] == NULL) {
	   echo"<script language=\"JavaScript\">location.href=\"index.html\";</script>";
	} else {
      $vetor_cadastro = getUser($conn);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="layout/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
		href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		
</head>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>

  <div class="row">

	<div class="col-md-12 box-items-menu-dashboard" style="padding: 0;margin: 0;">

		<div class="spanTitleBoxDefault">
			<h4><i class="fas fa-search"></i> Finder</h4>
		</div>

		<div class="row" style="margin: 0;padding: 0;">
      <?php menuItem('Financeiro', 'fa fa-hand-holding-usd', './extratoconta.php'); ?>

      <?php menuItem('Contas a Pagar', 'fa fa-file-invoice-dollar', './listarcp.php'); ?>

      <?php menuItem('Contas a Receber','fa fa-piggy-bank','./listarcr.php'); ?>
      
      <?php menuItem('Faturamento','fa fa-chart-line','./relcrglobal.php'); ?>
      
      <?php menuItem('Pedidos','fa fa-file-invoice-dollar','./cadastropedido.php'); ?>
      
      <?php menuItem('Orçamento','fa fa-file-invoice','./cadastroorcamento.php'); ?>
      
      <?php menuItem('OS','fa fa-file-signature','./listaros.php'); ?>
      
      <?php menuItem('Produtos','fa fa-shopping-basket','./listarprodutos.php'); ?>
      
			<?php menuItem('CRM','fa fa-address-card','./listarcrm.php'); ?>
		</div>
		
		<div class="spanTitleBoxDefault">
      <h4><i class="fas fa-tachometer-alt"></i> Cadastros Gerais</h4>
      
      <?php menuItem(
				'Cliente',
        'fa fa-user-plus',
        './listarclientes.php') ?>

			<?php menuItem(
				'Fornecedor',
				'fa fa-cube',
				'./listarfornecedores.php')
			?>

			<?php menuItem(
				'Parceiro',
				'fa fa-user-plus',
				'./listarparcerias.php')
			?>

			<?php menuItem(
				'Vendedor',
				'fa fa-hand-holding-usd',
				'./listarvendedores.php')
			?>

			<?php menuItem(
				'Indicador',
				'fa fa-bullhorn',
				'./listarparceiros.php')
			?>

			<?php menuItem(
				'Representante',
				'fa fa-user-plus',
				'./listarrepresentantes.php')
			?>

			<?php menuItem(
				'Técnico - Instalador',
				'fa fa-user-plus',
				'./listarprestadores.php')
			?>

			<?php menuItem(
				'Aniversariantes',
				'fa fa-user-plus',
				'./aniversariantes.php')
			?>
		</div>

		<div class="row" style="margin: 0;padding: 0;">
        
		</div>

		<div class="spanTitleBoxDefault">
			  <h4><i class="fas fa-cogs"></i> Avançado</h4>
		</div>

		<div class="row" style="margin: 0;padding: 0;">
      <?php menuItem(
				'Configurações',
				'fa fa-cog',
				'./listarusuarios.php')
			?>
		</div>

	</div>

		
		

	</div>

  <footer class="main-footer">
  <div class="pull-right hidden-xs">
	  <b>Versão</b> Pro Beta/ <span style="font-style: italic;font-size: 0.8em;">Recreated by Rota 5 Web</span>    </div>
	<strong>Todos direitos reservados JL Seguro.
  </footer>

  

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="layout/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="layout/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="layout/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="layout/bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="layout/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
</body>
</html>
<?php } ?>