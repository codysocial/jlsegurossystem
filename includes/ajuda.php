<?php

$ajuda = new class {

	public function old($name, $var) {

		if (isset($_GET['id']) and !empty($_GET['id'])) {
			return $var;
		} else {
			if (!empty(trim($_POST[$name]))) {
				return $_POST[$name];
			} else {
				return '';
			}
		}

	}

	public function white($param) {
		if (empty(trim($param))) {
			echo 'Não Informado';
		} else {
			echo $param;
		}
	}

	public function isStatusBoolean($param, $messages = [
		'success' => 'Manter Ativado',
		'error' => 'Manter Desativado',
	]) {
		if ($param == 1) {
			echo $messages['success'];
		} else {
			echo $messages['error'];
		}
	}

	public function colorStatusBoolean($param) {
		if ($param == 1) {
			echo 'green';
		} else {
			echo 'red';
		}
	}

};