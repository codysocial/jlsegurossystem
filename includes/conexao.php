<?php

$host = 'localhost';
$user = 'root';
$pass = '8512WEdfr@';
$db = 'jose';

// $host = 'localhost';
// $user = 'jlsegu62_sistema';
// $pass = '8512wedfr';
// $db = 'jlsegu62__sistema';

// conex�o e sele��o do banco de dados
$con = mysqli_connect($host, $user, $pass, $db);
mysqli_set_charset($con, "utf8");

$conn = new PDO("mysql:host=" . $host . ";dbname=" . $db . ";charset=utf8", $user, $pass);

function dd($value)
{
	echo "<pre>";
		print_r($value);
	echo "</pre><br><hr>";
}

// update
function update($tabela, $campos, $where = [], $condicao = " AND ", $conn)
{
		if (!empty($tabela) && (is_array($campos) && count($campos) > 0) && is_array($where)) {
		
		$sql = "UPDATE ".$tabela." SET ";
		
		$dados = array();
		foreach ($campos as $key => $value):
			$dados[] = $key." = '".addslashes($value)."' ";
		endforeach;
		$sql = $sql.implode(',', $dados);

		if (count($where) > 0) {

			$dados = array();

			foreach ($where as $key => $value):
				$dados[] = $key." = '".addslashes($value)."' ";
			endforeach;
			$sql = $sql." WHERE ".implode("".$condicao."", $dados);

		}
	
		$conn->query($sql);

		}
}

// INSERT 

function insert($table, $fields, $conn)
{
	if (!empty($table) && (is_array($fields) && count($fields) > 0)) {

		$sql = "INSERT INTO ".$table." SET ";$dados = array();

		foreach ($fields as $key => $value):
			$dados[] = $key." = '".addslashes($value)."' ";
		endforeach;

		$sql = $sql.implode(',', $dados);
		$conn->query($sql);

	}
}

// MENU DROPDOWN
function delete($id, $conn, $table, $url) {
	try {
		$sql = $conn->prepare("DELETE FROM {$table} WHERE id = ?");
		$sql->execute([$id]);

		header("Location: " . $url);

	} catch (PDOException $e) {
		return false;
	}
}

// MENU DROPDOWN
function updateMenuDropDown($dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $id,  $isdropdown, $conn) {
	try {
		$sql = $conn->prepare("UPDATE menu_dropdrowns SET dropmenu_name = ?, dropdown_icon = ?, id_menu = ?, dropdown_link = ?, drop_is_drop = ? WHERE id_drop = ?");
		$sql->execute([$dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $isdropdown, $id]);
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

function updateMenuDropDropDown($dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $id, $isdropdown,$conn) {
	try {
		$sql = $conn->prepare("UPDATE menu_dropdrowns SET drop_dropmenu_name = ?, drop_dropdown_icon = ?, drop_id_menu = ?, drop_dropdown_link = ?, drop_is_drop = ? WHERE id_drop = ?");
		$sql->execute([$dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $isdropdown, $id]);
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

function saveMenuDropDown($dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $isdropdown,$conn) {
	try {
		$sql = $conn->prepare("INSERT INTO menu_dropdrowns SET dropmenu_name = ?, 
			dropdown_icon = ?, id_menu = ?, dropdown_link = ?, is_drop = ?");
		$sql->execute([$dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $isdropdown]);
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

function saveMenuDropDropDown($dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link,  $isdropdown, $conn) {
	try {
		$sql = $conn->prepare("INSERT INTO menu_drop_dropdrowns SET drop_dropmenu_name = ?, drop_dropdown_icon = ?, drop_id_menu = ?, drop_dropdown_link = ?, drop_is_drop = ?");
		$sql->execute([$dropmenu_name, $dropdown_icon, $id_menu, $dropdown_link, $isdropdown]);
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

function pushMenuDropdownOne($conn, $id) {

	$sql = $conn->prepare("SELECT * FROM menu_dropdrowns WHERE id_drop = ?");
	$sql->execute([$id]);

	if ($sql->rowCount() > 0) {
		return $sql->fetch();
	}

}

function pushMenuDropDropDown($conn, $id) {

	$sql = $conn->prepare("SELECT * FROM menu_drop_dropdrowns WHERE drop_id_menu = ?");
	$sql->execute([$id]);

	if ($sql->rowCount() > 0) {
		return $sql->fetchAll();
	}

}

function pushMenuDropDown($conn, $id) {

	$sql = $conn->prepare("SELECT * FROM menu_dropdrowns WHERE 	id_menu = ?");
	$sql->execute([$id]);

	if ($sql->rowCount() > 0) {
		return $sql->fetchAll();
	}

}

function pushMenuDropDownAll($conn) {

	$sql = $conn->prepare("SELECT * FROM menu_dropdrowns");
	$sql->execute();

	if ($sql->rowCount() > 0) {
		return $sql->fetchAll();
	}

}

function pushMenuDropDropDownAll($conn) {

	$sql = $conn->prepare("SELECT * FROM menu_drop_dropdrowns");
	$sql->execute();

	if ($sql->rowCount() > 0) {
		return $sql->fetchAll();
	}

}

function pushMenuName($conn, $id) {

	$sql = $conn->prepare("SELECT * FROM menu WHERE id = ?");
	$sql->execute([$id]);

	if ($sql->rowCount() > 0) {
		$fetch = $sql->fetch();
	}

	return $fetch['menu_name'];
}

function pushDropMenuName($conn, $id) {

	$sql = $conn->prepare("SELECT * FROM menu_dropdrowns WHERE id_drop = ?");
	$sql->execute([$id]);

	if ($sql->rowCount() > 0) {
		$fetch = $sql->fetch();
	}

	return $fetch['dropmenu_name'];
}

// END MENUDROPDOWN

// MENU
function pushMenuOne($conn, $id) {

	$sql = $conn->prepare("SELECT * FROM menu WHERE id = ?");
	$sql->execute([$id]);

	if ($sql->rowCount() > 0) {
		return $sql->fetch();
	}

}

function pushMenu($conn) {

	$sql = $conn->prepare("SELECT * FROM menu");
	$sql->execute();

	if ($sql->rowCount() > 0) {
		return $sql->fetchAll();
	}

}

function editMenu($menu_name, $menu_link, $menu_icon, $menu_status, $isdropdown, $id, $conn) {

	try {

		$sql = $conn->prepare(
			"UPDATE menu SET menu_name = ?, menu_icon = ?, menu_link = ?, menu_status = ?, is_dropdown = ? WHERE id = ?"
		);

		$sql->execute([
			$menu_name,
			$menu_icon,
			$menu_link,
			$menu_status,
			$isdropdown,
			$id,
		]);

		return true;

	} catch (PDOException $e) {
		return false;
	}

}

function saveMenu($menu_name, $menu_link, $menu_icon, $menu_status, $isdropdown, $conn) {
	try {
		$sql = $conn->prepare("INSERT INTO menu SET menu_name = ?, menu_link = ?, menu_icon = ?, menu_status = ?, is_dropdown = ?");
		$sql->execute([$menu_name, $menu_link, $menu_icon, $menu_status, $isdropdown]);
		return true;
	} catch (PDOException $e) {
		return false;
	}
}
// END MENU

function updateMyAccount($photo, $name, $user, $id, $conn) {
	try {
		$sql = $conn->prepare("UPDATE usuarios SET photo = ?, nome = ?, usuario =  ? WHERE id_user = ?");
		$sql->execute([$photo, $name, $user, $id]);

		header("Location: myaccount.php");
		return true;

	} catch (PDOException $e) {
		$error = $e->getMessage();

		return false;
	}
}

function updateMyAccountData($name, $user, $id, $conn) {
	try {
		$sql = $conn->prepare("UPDATE usuarios SET nome = ?, usuario =  ? WHERE id_user = ?");
		$sql->execute([$name, $user, $id]);

		header("Location: myaccount.php");
		return true;

	} catch (PDOException $e) {
		$error = $e->getMessage();

		return false;
	}
}

function updateRecibo($conn, $array) {
	if (count($array) > 0) {
		$sql = $conn->prepare("UPDATE clientes SET recebide = ?, valorrec =  ?, referenterec = ? WHERE id_cli = ?");
		$sql->execute($array);

		return 1;
	} else {
		return 0;
	}
}

function getDataRecibo($conn, $id) {
	$sql = $conn->prepare("SELECT recebide, valorrec, referenterec FROM clientes WHERE id_cli = ?");
	$sql->execute([$id]);
	$vetor_cadastro = $sql->fetchAll();

	return $vetor_cadastro[0];
}

function getUser($conn) {
	$sql = $conn->prepare("SELECT * FROM usuarios WHERE id_user = ?");
	$sql->execute([$_SESSION['id']]);
	$vetor_cadastro = $sql->fetchAll();

	return $vetor_cadastro[0];
}

function hasContent($value = '') {
	$string = '';

	if (isset($value) and !empty($value)) {
		$string = $value;
	} else {
		$string = 'N�O INFORMADO!';
	}

	return $string;
}

?>
