<ul class="mobile-menu" id="mobile-menu" style="width: 300px;">

    <div id="titleDesktopWhite" style="background-color: none;font-size: 1.4em;margin-bottom: 30px;color: #fff;font-weight: bold;"><span class="hidetitleside">Menu Geral</span> <i title="Exibir/Oculta Menu" style="cursor: pointer;" id="showmob" class="fa fa-eye pull-right"></i></div>

    <?php include 'ajuda.php';?>

    <?php foreach (pushMenu($conn) as $value): ?>

        <li class="navigation-modern-li">

            <a href="<?php $ajuda->white($value['menu_link']);?>">

                <i class="<?php $ajuda->white($value['menu_icon']);?>"></i>

                <div style="width: 60%"><span class="menu_show_on navigation-title-link justify"><?php $ajuda->white($value['menu_name']);?></span></div>

                <span class="menu_show_on pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>

            </a>

            <?php if ($value['is_dropdown'] == 1): ?>

                <ul class="dropdown-navigator-modern  dropdown-navigator-unable">
                    
                    <?php foreach (pushMenuDropDown($conn, $value['id']) as $valueDp): ?>

                        <a class="opened-item" href="<?php $ajuda->white($valueDp['dropdown_link']);?>">
                            <i class="<?php $ajuda->white($valueDp['dropdown_icon']);?>"></i> <?php $ajuda->white($valueDp['dropmenu_name']);?>
                        </a>
                        
                        <?php if ($valueDp['is_drop'] == 1): ?>
                            <ul class="dropdown-navigator-modern-sub  dropdown-navigator-unable-sub">
                                <?php foreach (pushMenuDropDropDown($conn, $valueDp['id_drop']) as $valueDp): ?>
                                        <li>
                                            <a href="<?php $ajuda->white($valueDp['drop_dropdown_link']);?>">
                                                <i class="<?php $ajuda->white($valueDp['drop_dropdown_icon']);?>"></i> 
                                                <?php $ajuda->white($valueDp['drop_dropmenu_name']);?>
                                            </a>
                                        </li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif;?>

                    <?php endforeach;?>



                </ul>

            <?php endif;?>



        </li>

    <?php endforeach;?>

</ul>
<script type="text/javascript">

    $(function(){

        $('#showmob').on('click', () => {

            if ($('#mobile-menu').hasClass('menu-minz')) {

                $('#mobile-menu').css('width', '300px');
                $('#mobile-menu').removeClass('menu-minz');
                
                $('#showmob').css('width', '0')
                .css('height', '0')
                .css('margin-bottom', '0')
                .css('background-color', 'transparent')
                .css({
                    borderRadius: '5px'
                });


                $('.menu_show_on').show();
                $('.container').css('width', '80%');
                $('.dropdown-navigator-modern').css('left', '0');
                $('.hidetitleside').show();

                $('.box-body').css('z-index', '1000');

            }else{
                $('#mobile-menu').css('width', '80px');
                $('#mobile-menu').addClass('menu-minz');
                
                $('#showmob').css('width', '36')
                .css('height', '36')
                .css('margin-bottom', '5px')
                .css('background-color', '#e74c3c')
                .css({
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: '5px'
                });


                $('.menu_show_on').hide();
                $('.container').css('width', '95%');
                $('.dropdown-navigator-modern').css('left', '80px');
                $('.hidetitleside').hide();

                $('.box-body').css('z-index', '-1');
            }
        });

        // dropdownList
        $('.navigation-modern-li').on('click', function() {
            var dropdownList = $(this)[0].lastElementChild;

            if($(dropdownList).hasClass('dropdown-navigator-enable')){
                $(dropdownList).fadeOut().removeClass('dropdown-navigator-enable').addClass('dropdown-navigator-unable');
            }else{
                $(dropdownList).slideDown(300).removeClass('dropdown-navigator-unable').addClass('dropdown-navigator-enable');
            }
        });

    });

    // SUB DROPDOWN
    $('.opened-item').bind('click', function() {
        if($('.dropdown-navigator-modern-sub').hasClass('dropdown-navigator-enable-sub')){
            $('.dropdown-navigator-modern-sub').fadeOut().removeClass('dropdown-navigator-enable-sub').addClass('dropdown-navigator-unable-sub');
        }else{
            $('.dropdown-navigator-modern-sub').slideDown(300).removeClass('dropdown-navigator-unable-sub').addClass('dropdown-navigator-enable-sub');
        }
    });
</script>