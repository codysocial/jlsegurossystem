<?php

	$id = $_GET['id'];

	include"includes/conexao.php";

    /// PRESCISA DA PASTA DO DOMMPDF
	
	$sql_atual = mysqli_query($con, "SELECT * from pcld where id_cliente = '$id' order by datavencimento ASC");

    $sql_sub_total = mysqli_query($con, "SELECT SUM(valor) FROM pcld WHERE id_cliente = '$id' ");
    $vetor_sub_total = mysqli_fetch_array($sql_sub_total);
    $subtotal = $vetor_sub_total['SUM(valor)'];

    $subtotal = format_valor($subtotal);


    function format_valor($num){
        $valor = number_format($num,2,',','.');
        
        return $valor;
    }

    $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$id' ");
    $vetor_cliente = mysqli_fetch_array($sql_cliente);

	$tabela = "<table class='table table-bordered table-striped'><thead>
                        <tr>
                            <th>Contr.</th>
                            <th>Descrição</th>
                            <th>Vencimento</th>
                            <th>Valor Conta</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        $return = "$tabela";

        // Captura os dados da consulta e inseri na tabela HTML
        while ($linha = mysqli_fetch_array($sql_atual)) {

        	$valor = $linha["valor"];

        	$id_cr = $linha["id_cr"];

        	$valor = number_format($valor,2,",",".");

        	$status = $linha["status"];

        	if ($status == 1) {
        		$status = "Pendente";
        	}elseif ($status == 2) {
        		$status = "Pago";
        	}elseif ($status == 3) {
        		$status = "Cancelado";
        	}
            

            $return.= "<tr>";
            $return.= "<td>" . utf8_encode($linha["id_pedido"]) . "</td>";
            $return.= "<td>" . $linha["descricao"] . "</td>";
            $return.= "<td>" . utf8_encode($linha["datavencimento"]) . "</td>";
            $return.= "<td>" . utf8_encode($valor) . "</td>";
            $return.= "<td>" . utf8_encode($status) . "</td>";
            

            $return.= "</tr>";
        }

        $return.= "<tr><td style='background-color: rgb(0, 45, 82); color: #fff; font-weight: bold;' colspan='3'> VALOR TOTAL</td>
                        <td style='background-color: #696969; color: #fff; font-weight: bold;' colspan='2'>
                          R$ " . $subtotal . "   
                        </td></tr>";

       	$return.= "</tbody></table>";

        

        

	
	//referenciar o DomPDF com namespace
	use Dompdf\Dompdf;

	// include autoloader
	require_once("dompdf/autoload.inc.php");

	//Criando a Instancia
	$dompdf = new DOMPDF();
	
	// Carrega seu HTML
	$dompdf->load_html('

        <h2> PCLD - '. $vetor_cliente['nome'] .' </h2>

         <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">

         <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">

         <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

         <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">

         <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        '. $return .'

		');


	//Renderizar o html
	$dompdf->render();

	//Exibibir a página
	$dompdf->stream(
		"PCLD", 
		array(
			"Attachment" => false //Para realizar o download somente alterar para true
		)
	);
?>