<?php

include"includes/conexao.php";

$datainicio = $_GET['datainicio'];
$datafim = $_GET['datafim'];

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <?php
          function data($data){
            return date("d/m/Y", strtotime($data));
          }

          $data1 = data($datainicio);
          $data2 = data($datafim);


        ?>
</head>
<body>
  <div style="width: 100%; display: flex; justify-content: center; margin-bottom: 20px;">
    <h3>Relatório - Renovações Entre a Data <?php echo $data1; ?> a <?php echo $data2;?></h3>
  </div>

  <div style="width: 80%; margin-left: 10%;">

    <table id="example1" class="table table-bordered table-striped">
                
                <thead>
                <tr>
                  <th width="10%">Código</th>
                  <th>Nome do Cliente</th>
                  <th>Contato</th>
                  <th>Data Pedido</th>
                  <th>1° Renovação</th>
                  <th>2° Renovação</th>
                </tr>
                </thead>
                <tbody>
                <?php 

                  $sql_atual = mysqli_query($con, "SELECT * FROM renovacoes WHERE (data1 between '$datainicio' AND '$datafim') OR (data2 between '$datainicio' AND '$datafim')");
				
        				  while ($vetor=mysqli_fetch_array($sql_atual)) {

                  $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
                  $vetor_cliente = mysqli_fetch_array($sql_cliente);

                  $sql_pedido = mysqli_query($con, "select * from pedidos where id_cliente = '$vetor[id_cliente]' order by id_pedido DESC limit 0,1");
                  $vetor_pedido = mysqli_fetch_array($sql_pedido);
        				
        				 ?>
                <tr>
                  <td><?php echo $vetor['id_renovacao']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo $vetor_cliente['telefone']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor_pedido['data'])); ?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['data1'])); ?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['data2'])); ?></td>
                  
                </tr>
                <?php } ?>
                </tbody>
                
              </table>

    </div>
<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
 
    $('#example1').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
    })
  })
</script>
<script type="text/javascript">
  
  setTimeout(function(){ print(); }, 1500);

</script>
</body>