-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 05-Fev-2020 às 21:06
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `jlsegu62_sistema`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `hiscontacorrente`
--

CREATE TABLE `hiscontacorrente` (
  `id` int(11) NOT NULL,
  `nome_banco` varchar(30) NOT NULL,
  `nagencia` varchar(20) NOT NULL,
  `nconta` int(20) NOT NULL,
  `nsaldo` float NOT NULL,
  `data_saldo` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `hiscontacorrente`
--

INSERT INTO `hiscontacorrente` (`id`, `nome_banco`, `nagencia`, `nconta`, `nsaldo`, `data_saldo`) VALUES
(15, 'NET INTERNET E TELEFONIA ', '504845515', 51520515, 500, '2020-02-05'),
(16, 'PLAY ARTES ', '584821-848', 45120890, 0, '2020-02-05');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `hiscontacorrente`
--
ALTER TABLE `hiscontacorrente`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `hiscontacorrente`
--
ALTER TABLE `hiscontacorrente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
