$(function(){

    $('#closeModal').bind('click', function(){
        $('#contentModal').css('display','none').hide(500);
    });     


    $('#regReciboYes').bind('click', function(){
        $('#confirmRecNot').css('display', 'none');
        $('#contentModal').css('display','flex').show(500);
        $('#confirmRecYes').css('display', 'block');
    });  

    $('#regReciboNot').bind('click', function(){
        $('#contentModal').css('display','none');
        $('#confirmRecYes').css('display', 'none');
        $('#confirmRecNot').css('display', 'block');
    });  


    $('#valorRec').mask("#.##0,00", {reverse: true});

    $('#rg').mask("#.##0,00", {reverse: true});
    $('#cpfcnpj').mask("#.##0,00", {reverse: true});

});  