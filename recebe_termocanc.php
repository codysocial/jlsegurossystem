<?php

	function moeda($get_valor) { 
                $source = array('.', ',');  
                $replace = array('', '.'); 
                $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto 
                return $valor; //retorna o valor formatado para gravar no banco 
     }


class Monetary {
    private static $unidades = array("um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze",
                                     "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove");
    private static $dezenas = array("dez", "vinte", "trinta", "quarenta","cinqüenta", "sessenta", "setenta", "oitenta", "noventa");
    private static $centenas = array("cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", 
                                     "seiscentos", "setecentos", "oitocentos", "novecentos");
    private static $milhares = array(
        array("text" => "mil", "start" => 1000, "end" => 999999, "div" => 1000),
        array("text" => "milhão", "start" =>  1000000, "end" => 1999999, "div" => 1000000),
        array("text" => "milhões", "start" => 2000000, "end" => 999999999, "div" => 1000000),
        array("text" => "bilhão", "start" => 1000000000, "end" => 1999999999, "div" => 1000000000),
        array("text" => "bilhões", "start" => 2000000000, "end" => 2147483647, "div" => 1000000000)        
    );
    const MIN = 0.01;
    const MAX = 2147483647.99;
    const MOEDA = " real ";
    const MOEDAS = " reais ";
    const CENTAVO = " centavo ";
    const CENTAVOS = " centavos ";    
     
    static function numberToExt($number, $moeda = true) {
        if ($number >= self::MIN && $number <= self::MAX) {
            $value = self::conversionR((int)$number);       
            if ($moeda) {
                if (floor($number) == 1) {
                    $value .= self::MOEDA;
                }
                else if (floor($number) > 1) $value .= self::MOEDAS;
            }
 
            $decimals = self::extractDecimals($number);            
            if ($decimals > 0.00) {
                $decimals = round($decimals * 100);
                $value .= " e ".self::conversionR($decimals);
                if ($moeda) {
                    if ($decimals == 1) {
                        $value .= self::CENTAVO;
                    }   
                    else if ($decimals > 1) $value .= self::CENTAVOS;
                }
            }
        }
        return trim($value);
    }
     
    private static function extractDecimals($number) {
        return $number - floor($number);
    }
     
    static function conversionR($number) {
        if (in_array($number, range(1, 19))) {
            $value = self::$unidades[$number-1];
        }
        else if (in_array($number, range(20, 90, 10))) {
             $value = self::$dezenas[floor($number / 10)-1]." ";           
        }     
        else if (in_array($number, range(21, 99))) {
             $value = self::$dezenas[floor($number / 10)-1]." e ".self::conversionR($number % 10);           
        }     
        else if (in_array($number, range(100, 900, 100))) {
             $value = self::$centenas[floor($number / 100)-1]." ";           
        }          
        else if (in_array($number, range(101, 199))) {
             $value = ' cento e '.self::conversionR($number % 100);         
        }   
        else if (in_array($number, range(201, 999))) {
             $value = self::$centenas[floor($number / 100)-1]." e ".self::conversionR($number % 100);        
        }  
        else {
            foreach (self::$milhares as $item) {
                if ($number >= $item['start'] && $number <= $item['end']) {
                    $value = self::conversionR(floor($number / $item['div']))." ".$item['text']." ".self::conversionR($number % $item['div']);
                    break;
                }
            }
        }        
        return $value;
    }
}

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

include"includes/conexao.php";

$id = $_GET['id'];
$referencia = $_POST['referencia'];
$debitos = $_POST['debitos'];

$sql_os = mysqli_query($con, "select * from pedidos where id_cliente = '$id'");
$vetor_os = mysqli_fetch_array($sql_os);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$id'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
.style2 {
	font-size: 12
}
.style3 {font-size: 12px}

            .pontilhada {
			border: 2px dotted #999; /*Definindo o estilo da borda*/
			background-color: #FFFFFF; /*Cor de fundo para melhor visualização do exemplo*/
			height: 200px; /*Altura para melhor visualização do exemplo*/
			width: 420px; /*Largura para melhor visualização do exemplo*/
			}

-->
    </style>
</head>
<body>
<div class="style3">
<img src="imgs/logo.png" width="180px">
</br>
</br>
<table width="100%">
	<tr>
		<td>
        </br>
        </br>
        </br>
        </br>
		<div align="justify">
		<p align="center">
		<font size="5"><strong>TERMO DE CANCELAMENTO DE CONTRATO</strong></font>
		</p>
		</br>
        </br>
        </br>
        </br>
        </br>
        </br>
        </br>
        Prezado cliente,
        </br>
		<?php echo $vetor_cliente['nome']; ?>, Portador do CPF: <?php 
                
                $qtddoc = strlen($vetor_cliente['cpfcnpj']);

                if($qtddoc == 14) {
                
                echo Mask("##.###.###/####-##",$vetor_cliente['cpfcnpj']); 
                
                }
                
                if($qtddoc == 11) {
                
                echo Mask("###.###.###-##",$vetor_cliente['cpfcnpj']); 
                
                }
                
                
                ?> Informamos que a partir desta data o contrato de nº <?php echo $vetor_os['id_pedido']; ?> de <?php echo date('d/m/Y', strtotime($vetor_os['data'])); ?> enconta-se devidamente cancelado e todos os serviços e benecios cessados <?php if($debitos == 1) { ?>( X )  Sem débitos <?php } else { ?>( X ) Com débitos<?php } ?> em caso de dúvidas e débitos entre em contatos com o nosso setor financeiro (11) 4171-5781 ou whatsapp (11) 94996-0826. Para a devida regularização. Evite intervenção judicial.

                </br>
                </br>

                Motivo: <?php echo $referencia; ?>
.

		</td>
	</tr>
	
	<tr>

	<td>

	</br>
	</br>
	</br>
    Atenciosamente,
	</br>
	</br>
	</br>

	</td>

	</tr>

	<tr>

	<td>

	<img src="imgs/assinatura.png">
    </br>
    Departamento Financeiro

	</br>
	</br>

	</td>

	</tr>

	<tr>
		<td align="Right">São Paulo, <?php echo date('d/m/Y'); ?></td>
	</tr>

	
</table>
</div>
</body>
</html>

<script type="text/javascript">
<!--
        print();
-->
</script>