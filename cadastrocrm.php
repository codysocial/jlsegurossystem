<?php

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
 
}

	 include "includes/conexao.php";
	 
	 session_start();

   $id_pagina = '17';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$sql_cliente = "select * from clientes where tipocad = '1' order by nome ASC";
  $res1 = mysqli_query($con, $sql_cliente);

  $sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <script type="text/javascript" src="aplicacoes/aplicjava.js"></script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Cadastro de CRM</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form action="recebe_crm.php" method="post" name="cliente" enctype="multipart/form-data" id="formID">

            <table width="100%">
                <tr>
                  <td width="95%"></td>
                  <td><button type="submit" class="btn btn-ionc-10"  style="    float: left;">Cadastrar</button></td>
                </tr>
              </table>
              </br>
              </br>
        
                <div class="row">
          <div class="col-lg-12">
                    <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Cliente</label>
                            <select name="cliente" id="exampleSelect" class="form-control select2" required>
                              <option value="" selected="selected">Selecione um Cliente</option>
                              <?php while ($vetor=mysqli_fetch_array($res1)) { ?>
                              <option value="<?php echo  $vetor['id_cli']; ?>"><?php echo $vetor['nome'] ?></option>
                              <?php } ?>
                            </select>
          </fieldset>
          </div>
        </div>
                
                <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Titulo</label>
              <input type="text" name="titulo" required class="form-control" id="exampleInput" placeholder="Digite o Titulo">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Data de Retorno</label>
              <input type="date" name="dataretorno" maxlength="10" class="form-control" id="exampleInput" placeholder="Digite o Data" required>
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Pessoa de Contato</label>
              <input type="text" name="contato" class="form-control" id="exampleInput" placeholder="Digite a Pessoa de Contato" required>
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">
          <div class="col-lg-12">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Descrição</label>
              <textarea name="descricao" class="form-control" rows="7" required=""></textarea>
            </fieldset>
          </div>
        </div><!--.row-->
                
          <div class="row">
          <div class="col-lg-12">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Status</label>
              <select name="status" class="form-control">
                <option value="1">Em Negociação</option>
                <option value="2">Negociação Concluida</option>
                <option value="3">Negociação Negada</option>
              </select>
            </fieldset>
          </div>
        </div><!--.row-->

        <button type="submit" class="btn btn-ionc-10"  style="    float: right;">Cadastrar</button> </form>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });

  $(function(){
      $('html').css('background-color', '#fbeeff');
      $('body').css('background-color', '#fbeeff');
      $('.container').css('background-color', '#fbeeff');
      $('.content').css('background-color', '#fbeeff');
      $('.box').css('background-color', '#fbeeff');
      $('.tab-content').css('background-color', '#fbeeff');
      $('.box-title').addClass('text-ionc-10');
    });

  $(function(){
      $('html').css('background-color', '#fbeeff');
      $('body').css('background-color', '#fbeeff');
      $('.container').css('background-color', '#fbeeff');
      $('.content').css('background-color', '#fbeeff');
      $('.box').css('background-color', '#fbeeff');
      $('.tab-content').css('background-color', '#fbeeff');
      $('.box-title').addClass('text-ionc-10');
    });
</script>
</body>
</html>
<?php } } ?>