<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

	<?php 
	require 'includes/conexao.php';

	require 'includes/NumeroPorExtenso.php';

	use WGenial\NumeroPorExtenso\NumeroPorExtenso;
	$extenso = new NumeroPorExtenso;

	$sql = $conn->prepare("SELECT * FROM clientes WHERE id_cli = ?");
	$sql->execute([$_GET['idenrec']]);
	$fetch = $sql->fetch();

	$nome = !empty($fetch['recebide']) ? $fetch['recebide'] : 'JL SEGUROS SYSTEM DO BRASIL';
	$valor = empty($fetch['valorrec']) ? 0.00 : $fetch['valorrec'];
	$referente = empty($fetch['referenterec']) ? 'NÃO INFORMADO' : $fetch['referenterec'];

	?>

	<h1 style='text-align: center;font-family: arial;'>RECIBO</h1>
	<div style='font-family: arial;margin-left: 30px;'>
		<h4 style='margin-bottom: -10px;'><strong>RECEBEMOS DE:</strong> <?php echo hasContent($nome); ?>,   O VALOR R$ <?php if (isset($valor) and !empty($valor)) {
			echo number_format(hasContent($valor), 2);
		}else{ echo '0,00'; } ?> (<?php echo $extenso->converter($valor); ?>)</h4>
		<h4><strong>REFERENTE:</strong> <?php echo hasContent($referente); ?></h4>
	</div>

	<br><br><br>

	<div style='display: flex;justify-content: center;'>


	<div>
		<ul style='list-style: none;padding: 0;margin: 0;margin-left: 30px;font-family: arial;'>
			<li>_________________________________________________</li>
			<li style='margin-bottom: 10px;margin-top: 15px;'><strong>NOME:</strong> <?php echo hasContent($fetch['nome']); ?></li>
			<li style='margin-bottom: 10px;'><strong>CPF/CNPJ:</strong> <?php echo hasContent($fetch['cpfcnpj']); ?></li>
			<li><strong>RG:</strong> <?php echo hasContent($fetch['rg']); ?> </li>
			<li>-</li>
			<?php if (!empty($fetch['banco']) and !empty($fetch['agencia']) and !empty($fetch['conta'])): ?>
				<li><strong>BANCO:</strong> <?php echo hasContent($fetch['banco']); ?> </li>
				<li><strong>AGENCIA:</strong> <?php echo hasContent($fetch['agencia']); ?> </li>
				<li><strong>CONTA:</strong> <?php echo hasContent($fetch['conta']); ?> </li>
				<li><strong>TIPO:</strong> <?php echo hasContent($fetch['conta']); ?> </li>
			<?php endif; ?>
			
		</ul>
	</div>

		<br><br><br><br><br><br>

		<br><br><br>

	</div>
	<div style='float: right;font-weight: bold;font-family: arial;'>
		São Paulo, <?php echo hasContent(date('d/m/Y', strtotime($fetch['datacad']))); ?>
	</div></div>
	
</body>
</html>


<script type="text/javascript">
<!--
        print();
-->
</script>