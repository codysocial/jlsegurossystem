<?php

function reverse_date( $date ){
      return ( strstr( $date, '/' ) ) ? implode( '-', array_reverse( explode( '/', $date ) ) ) : implode( '/', array_reverse( explode(                '-', $date ) )      );
}

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
 
}

	 include"includes/conexao.php";
	 
	 session_start();

   $id_pagina = '20';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {

  $datainicio =   $_POST['datainicio'];
  $datafim = $_POST['datafim'];
		
  
  $sql_cr = "select * from cr where datavencimento BETWEEN '$datainicio' AND '$datafim' and status != '3' order by datavencimento ASC";
  $res1 = mysqli_query($con, $sql_cr);

  $sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>
<!DOCTYPE html>
<html>
<?php include 'includes/head.php' ?>
<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include"includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

          <div class="col-md-12">
          
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Faturamento - Relatório Global (Todos)</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

                <div class="box-period-bar" style="float: left;margin-bottom: 20px;">
                    Período de &nbsp; <span style="color: #000;"><?php echo date('d/m/Y', strtotime($datainicio)); ?></span>  &nbsp;a&nbsp;  <span style="color: #000;"><?php echo date('d/m/Y', strtotime($datafim)); ?></span>
                </div>

                <a href="imprimirrelcrglobal.php?datainicio=<?php echo $datainicio; ?>&datafim=<?php echo $datafim; ?>" target="_blank" style="float: left;margin-left: 10px;"><button type="button" class="btn btn-info mesmo-tamanho" title="Imprimir Cadastro"><i class="fa fa-print"></i></button></a>

                </br>
                
                <table width="100%" id="table-sm" class="table table-bordered table-hover table-sm">
                      <tr>
                        <td width="20%" bgcolor="e8e8e8">Descri&ccedil;&atilde;o</td>
                        <td width="20%" bgcolor="e8e8e8">Cliente</td>
                        <td bgcolor="e8e8e8">Data Vencimento</td>
                        <td bgcolor="e8e8e8">Data D+2</td>
                        <?php if($tipo == 2) { ?>
                        <td bgcolor="e8e8e8">Data Pagamento</td>
                        <?php } ?>
                        <td bgcolor="e8e8e8">Valor</td>
                        <?php if($tipo == 2) { ?>
                        <td bgcolor="e8e8e8">Valor Pago</td>
                        <?php } ?>
                      </tr>
                      <?php 

                      while ($vetor1=mysqli_fetch_array($res1)) {


                      $diasemana_numero = date('w', strtotime($vetor1['datavencimento']));

                          if($diasemana_numero == 0) {

                          $datafinal = date('Y-m-d', strtotime('+2 days', strtotime($vetor1['datavencimento'])));

                          } if($diasemana_numero == 1) {

                          $datafinal = date('Y-m-d', strtotime('+2 days', strtotime($vetor1['datavencimento'])));

                          } if($diasemana_numero == 2) {

                          $datafinal = date('Y-m-d', strtotime('+2 days', strtotime($vetor1['datavencimento'])));

                          } if($diasemana_numero == 3) {

                          $datafinal = date('Y-m-d', strtotime('+2 days', strtotime($vetor1['datavencimento'])));

                          } if($diasemana_numero == 4) {

                          $datafinal = date('Y-m-d', strtotime('+4 days', strtotime($vetor1['datavencimento'])));

                          } if($diasemana_numero == 5) {

                          $datafinal = date('Y-m-d', strtotime('+4 days', strtotime($vetor1['datavencimento'])));

                          } if($diasemana_numero == 6) {

                          $datafinal = date('Y-m-d', strtotime('+3 days', strtotime($vetor1['datavencimento'])));

                          }

                      $data = reverse_date($vetor1['datavencimento']);
                      $data1 = reverse_date($vetor1['datapagamento']);

                      $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor1[id_cliente]'");
                      $vetor_cliente = mysqli_fetch_array($sql_cliente);

                      ?>
                      <tr>
                        <td><?php echo $vetor1['descricao']; ?></td>
                        <td><span class="style17"><?php echo $vetor_cliente['nome']; ?></span></td>
                        <td><span class="style17"><?php echo $data; ?></span></td>
                        <td><?php echo date('d/m/Y', strtotime($datafinal)); ?></td>
                        <?php if($tipo == 2) { ?>
                        <td><span class="style17"><?php echo $data; ?></span></td>
                        <?php } ?>
                        <td><span class="style17"><?php $totalizador = $vetor1['valor']; echo $num2 = number_format($vetor1['valor'],2,',','.');  ?></span></td>
                        <?php if($tipo == 2) { ?>
                        <td><span class="style17"><?php $totalizador = $vetor1['valorpago']; echo $num_pago = number_format($vetor1['valorpago'],2,',','.');  ?></span></td>
                        <?php } ?>
                      </tr>
                      <?php 

                      $total1 += $totalizador;

                      } 

                      ?>
                </table>


                <div class="barvaluetot">
                <span class="valortotal"><strong>Total: </strong></span>
                <span class="classtotalcalculo"><?php 
            $calculo = $vetor2['total'];
            $num3 = number_format($total1,2,',','.');
            echo"R$";
            echo $num3;
            ?></span>
                </div>

            </div>
          </div>
          </div>

         
     </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
<?php } } ?>