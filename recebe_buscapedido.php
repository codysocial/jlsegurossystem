<?php

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
 
}

	 include"includes/conexao.php";
	 
	 session_start();

   $id_pagina = '7';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<div class="container">

  <header class="main-header">

    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="imgs/logo.png" width="100px"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="imgs/logo.png" width="100px"></span>    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <?php include "includes/topo.php"; ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Seja bem-vindo,
        <small> <?php echo $vetor_cadastro['nome']; ?></small>      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pedidos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pedidos</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
            <table width="100%">
              <tr>
                <td width="15%"><a href="cadastropedido.php"><button class="btn btn-primary"  style="    float: left;">Cadastrar Novo Pedido</button></a></td>
                <td width="85%">
                  <table width="100%">
                    <form action="recebe_buscapedido.php" method="post" name="busca">
                    <tr>
                      <td><input type="date" name="datainicio" class="form-control"></td>
                      <td width="2%"></td>
                      <td><input type="date" name="datafim" class="form-control"></td>
                      <td width="2%"></td>
                      <td><select name="id_cliente" id="exampleSelect" class="form-control">
                              <option value="" selected="selected">Selecione o Cliente</option>
                              <?php 
                              $sql_clientes = mysqli_query($con, "select * from clientes where tipocad = '1' order by nome ASC");
                              while ($vetor_cliente=mysqli_fetch_array($sql_clientes)) { ?>
                              <option value="<?php echo $vetor_cliente['id_cli']; ?>"><?php echo $vetor_cliente['nome'] ?></option>
                              <?php } ?>
                            </select></td>
                      <td width="2%"></td>
                      <td><select name="id_vendedor" id="exampleSelect" class="form-control">
                              <option value="" selected="selected">Representante/Indicador</option>
                              <?php 
                              $sql_vendedor = mysqli_query($con, "select * from clientes where tipocad IN ('3', '4', '5', '6') order by nome ASC");
                              while ($vetor_vendedor=mysqli_fetch_array($sql_vendedor)) { ?>
                              <option value="<?php echo $vetor_vendedor['id_cli']; ?>"><?php echo $vetor_vendedor['nome'] ?></option>
                              <?php } ?>
                            </select></td>
                      <td width="2%"></td>
                      <td><select name="tipo" id="tipobusca" class="form-control">
                        <option value="" selected="">Selecione...</option>
                        <option value="1">Realizado</option>
                        <option value="2">Cancelados</option>
                      </select></td>
                      <td width="2%"></td>
                      <td><button type="submit" class="btn btn-primary"  style="    float: left;">Buscar</button></td>
                    </tr>
                    </form>
                  </table>
                  </td>
              </tr>
            </table>
            </br>
            </br>
           	</br>

            <?php 

          $datainicio = $_POST['datainicio'];
          $datafim = $_POST['datafim'];
          $tipo = $_POST['tipo'];
          $id_cliente = $_POST['id_cliente'];
          $id_vendedor = $_POST['id_vendedor'];

          if(!empty($datainicio) && !empty($datafim)) { $where .= " AND data BETWEEN '".$datainicio."' AND '".$datafim."'"; }
          if(!empty($tipo)) { $where .= " AND status = '".$tipo."'"; }
          if(!empty($id_cliente)) { $where .= " AND id_cliente = '".$id_cliente."'"; }
          if(!empty($id_vendedor)) { $where .= " AND (id_corretor = '".$id_vendedor."' OR id_indicador = '".$id_vendedor."')"; }
        
          if($vetor_cadastro['tipo'] == 1) { 

          $sql_calculo = mysqli_query($con, "SELECT SUM(mensalidade*12+adesao+vacina+instalacao+assistencia+assistenciaterceiro) as total FROM pedidos where id_corretor = '$vetor_cadastro[id_cadastro]'".$where."");

          } else { 
          
          $sql_calculo = mysqli_query($con, "SELECT SUM(mensalidade*12+adesao+vacina+instalacao+assistencia+assistenciaterceiro) as total FROM pedidos where 1".$where."");

          }
        
          $vetor_calculo=mysqli_fetch_array($sql_calculo);

        
         ?>

            <font color="#FF0000">
              <table width="100%">
                <tr bgcolor="#e8e8e8">
                  <td width="10%"><strong>Total: </strong></td>
                  <td><strong><?php echo number_format($vetor_calculo['total'],2,',','.'); ?></strong></td>
                </tr>
              </table>
              </font>

              </br>
              </br>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="10%">Código</th>
                  <th>Nome do Cliente</th>
                  <th>Data Pedido</th>
                  <th>Valor Total</th>
                  <th>Status</th>
                  <th width="17%">Ação</th>
                </tr>
                </thead>
                <tbody>
                <?php 
				
				  if($vetor_cadastro['tipo'] == 1) { 
            
            $sql_atual = mysqli_query($con, "select * from pedidos where 1".$where." and id_corretor = '$vetor_cadastro[id_cadastro]' order by id_pedido DESC")  or die (mysqli_error($con));

          } else { 
				  
				  $datainicio = $_POST['datainicio'];
          $datafim = $_POST['datafim'];
          $tipo = $_POST['tipo'];
          $id_cliente = $_POST['id_cliente'];
          $id_vendedor = $_POST['id_vendedor'];

          if(!empty($datainicio) && !empty($datafim)) { $where .= " AND data BETWEEN '".$datainicio."' AND '".$datafim."'"; }
          if(!empty($tipo)) { $where .= " AND status = '".$tipo."'"; }
          if(!empty($id_cliente)) { $where .= " AND id_cliente = '".$id_cliente."'"; }
          if(!empty($id_vendedor)) { $where .= " AND (id_corretor = '".$id_vendedor."' OR id_indicador = '".$id_vendedor."')"; }
            
            $sql_atual = mysqli_query($con, "select * from pedidos where 1".$where." order by id_pedido DESC")  or die (mysqli_error($con));

          }
				
				  while ($vetor=mysqli_fetch_array($sql_atual)) {

          $mensalidade = $vetor['mensalidade'] * 12;

          $total = $vetor['adesao'] + $mensalidade + $vetor['vacina'] + $vetor['instalacao'] + $vetor['assistencia'] + $vetor['assistenciaterceiro'];

          $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
          $vetor_cliente = mysqli_fetch_array($sql_cliente);
				
				 ?>
                <tr>
                  <td><?php echo $vetor['id_pedido']; ?></td>
                  <td><?php echo $vetor_cliente['nome']; ?></td> 
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php echo number_format($total,2,',','.'); ?></td>
                  <td><?php if($vetor['status'] == 1) { echo "Realizado"; } if($vetor['status'] == 2) { echo "Cancelado"; } ?></a></td>
                  <td><a href="alterarpedido.php?id=<?php echo $vetor['id_pedido']; ?>"><button type="button" class="btn btn-info mesmo-tamanho" title="Alteração"><i class="fa fa-edit"></i></button></a> <a href="telaimpressoes.php?id=<?php echo $vetor['id_pedido']; ?>"><button type="button" class="btn btn-primary mesmo-tamanho" title="Visualizar impressão"><i class="fa fa-eye"></i></button></a> <?php if($vetor['status'] == 1) { ?> <a href="confexcluirpedido.php?id=<?php echo $vetor['id_pedido']; ?>" ><button type="button" class="btn btn-danger mesmo-tamanho" title="Excluir"><i class="fa fa-close"></i></button></a> <a href="confcancpedido.php?id=<?php echo $vetor['id_pedido']; ?>" ><button type="button" class="btn btn-warning mesmo-tamanho" title="Cancelar"><i class="fa fa-times-circle-o"></i></button></a><?php } ?></td> 
                </tr>
                <?php $totalizador += $total; } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th width="10%">Código</th>
                  <th>Nome do Cliente</th>
                  <th>Data Pedido</th>
                  <th>Valor Total</th>
                  <th>Status</th>
                  <th width="17%">Ação</th>
                </tr>
                </tfoot>
              </table>

              <font color="#FF0000">
              <table width="100%">
                <tr bgcolor="#e8e8e8">
                  <td width="10%"><strong>Total: </strong></td>
                  <td><strong><?php echo number_format($vetor_calculo['total'],2,',','.'); ?></strong></td>
                </tr>
              </table>
              </font>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable({
        "order": [[ 0, "desc" ]]
    })
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
<?php } } ?>