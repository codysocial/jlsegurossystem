<?php

$datacerta = $_GET['data_saldo'];

?>

<?php

function reverse_date( $date )
        {
      return ( strstr( $date, '/' ) ) ? implode( '-', array_reverse( explode( '/', $date ) ) ) : implode( '/', array_reverse( explode(                '-', $date ) )      );
        }

include"includes/conexao.php";

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <title>JL Seguro</title>
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	  <!-- Bootstrap 3.3.7 -->
	  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
	  
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
	  <!-- Ionicons -->
	  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
		  <!-- DataTables -->
	  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
	  <!-- AdminLTE Skins. Choose a skin from the css/skins
	       folder instead of downloading all of them to reduce the load. -->
	  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
	  
	  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

	  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	  <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->

	  <!-- Google Font -->
	  <link rel="stylesheet"
	        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body>
	<section class="content">
	      <div class="row">
	        <div class="col-xs-12">
	          
	            <!-- /.box-header -->
	            <div style="display: flex; flex-direction: row; justify-content: center;">
	            	<h3>Saldo do dia</h3>
	            	<H3 style="margin-left: 5px;"><?php 

	            		function data($data){
							return date("d-m-Y", strtotime($data));
						}

						$data_forn = data($datacerta);

						echo $data_forn;

	            	?>
	            		
	            	</H3>

	            </div>
	        
	            <div class="box-body">

	          <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;">Banco</th>
                        <th style="text-align: right;">Agencia</th>
                        <th style="text-align: right;">Conta</th>
                        <th style="text-align: right;">Saldo</th>
                        <th style="text-align: right;" width="25%">Data</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				  
				$sql_atual = mysqli_query($con, "SELECT * FROM hiscontacorrente WHERE data_saldo = ' $datacerta' ");

				while ($vetor=mysqli_fetch_array($sql_atual)) {
				
				 ?>
                <tr>
                  <td style="text-align: center;"><?php echo $vetor['nome_banco']; ?></td>
                  <td style="text-align: right;"><?php echo $vetor['nagencia']; ?></td>
                  <td style="text-align: right;"><?php echo $vetor['nconta']; ?></td>
                  <td style="text-align: right;">
                  	<?php

                  	if($vetor['tipo'] == 2){
                  		echo "<p style='color: red;'> - " . number_format($vetor['nsaldo'],2,",",".") . "</p>";
                  	}else{
                  		echo number_format($vetor['nsaldo'],2,",",".");
                  	}
                  	
                  	?>
                  </td>
                  <td style="text-align: right;"><?php echo date('d-m-Y', strtotime($vetor['data_saldo'])); ?> / <?php $diaSem = date('D', strtotime($vetor['data_saldo']));
                      if($diaSem == 'Fri')
                      {echo 'Sexta';}
                      elseif($diaSem == 'Mon')
                      {echo 'Segunda';}
                      elseif($diaSem == 'Tue')
                      {echo 'Terça';}
                      elseif($diaSem == 'Wed')
                      {echo 'Quarta';}
                      elseif($diaSem == 'Thu')
                      {echo 'Quinta';}
                      elseif($diaSem == 'Sat')
                      {echo 'Sabado';}
                      elseif($diaSem == 'Sun')
                      {echo 'Domingo';}
                      ?>
                  </td>

                </tr>
                <?php } ?>
                </tbody>
                  <tfoot>
                  	
                  </tfoot>  
              </table>

              		<?php

						$sql_posi = "SELECT data_saldo, SUM(nsaldo) FROM hiscontacorrente WHERE data_saldo = '$datacerta' AND tipo = 1 ";

						$valor_posi = mysqli_query($con, $sql_posi);

						$vetor_posi = mysqli_fetch_array($valor_posi);

						$valorf_posi = $vetor_posi['SUM(nsaldo)'];


						$sql_nega = "SELECT data_saldo, SUM(nsaldo) FROM hiscontacorrente WHERE data_saldo = '$datacerta' AND tipo = 2 ";

						$valor_nega = mysqli_query($con, $sql_nega);

						$vetor_nega = mysqli_fetch_array($valor_nega);

						$valorf_nega = $vetor_nega['SUM(nsaldo)'];


						$valorf = $valorf_posi - $valorf_nega;

			        ?>

			      <div style="float: left; background-color: #2c3e50; border: 1px solid #d2d6de; height: 40px; width: 75%;">
			      	<h4 style="font-weight: bold ;color: white; height: 20px; padding-left: 5px;">Saldo Consolidado: </h4>
			      </div>

   	              <input style="font-weight: bold; width: 25%; height: 40px; float: right; text-align: right;" type="text"  value=" <?php echo number_format($valorf,2,",","."); ?> " Use readonly="true" class="form-control">
   	              	
				  </div>

	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- /.box -->
	        </div>
	        <!-- /.col -->
	      </div>
	      <!-- /.row -->
    </section>


<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->

<script type="text/javascript">
	
$(function () {
    $('#example1').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })

</script>


</body>
</html>

<script type="text/javascript">
	print();
</script>