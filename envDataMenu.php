<?php

require 'includes/conexao.php';

if (isset($_POST['menu_name']) and !empty($_POST['menu_name'])) {

	if (!empty($_POST['menu_name']) and !empty($_POST['menu_status'])) {

		$menu_name = addslashes(htmlspecialchars($_POST['menu_name']));
		$menu_link = addslashes(htmlspecialchars($_POST['menu_link']));
		$menu_icon = addslashes(htmlspecialchars($_POST['menu_icon']));
		$menu_status = addslashes(htmlspecialchars($_POST['menu_status']));
		$isdropdown = addslashes(htmlspecialchars($_POST['isdropdown']));

		if (empty($menu_link)) {
			$menu_link = '#';
		} else {
			$menu_link = $menu_link;
		}

		if (saveMenu($menu_name, $menu_link, $menu_icon, $menu_status, $isdropdown, $conn)) {
			echo json_encode(array('success' => 1));
		} else {
			echo json_encode(array('success' => 0));
		}

	} else {
		echo json_encode(array('success' => 0));
	}

} else {
	echo json_encode(array('success' => 0));
}