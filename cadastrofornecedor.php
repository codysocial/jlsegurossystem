<?php
    
    error_reporting(0);

    function formatarCPF_CNPJ($campo, $formatado = true){
    //retira formato
    $codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
    // pega o tamanho da string menos os digitos verificadores
    $tamanho = (strlen($codigoLimpo) -2);
    //verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
    if ($tamanho != 9 && $tamanho != 12){
    return false; 
    }

    if ($formatado){ 
    // seleciona a mÃ¡scara para cpf ou cnpj
    $mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 

    $indice = -1;
    for ($i=0; $i < strlen($mascara); $i++) {
    if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
    }
    //retorna o campo formatado
    $retorno = $mascara;

    }else{
    //se nÃ£o quer formatado, retorna o campo limpo
    $retorno = $codigoLimpo;
    }

    return $retorno;

    }

    include "includes/conexao.php";

    session_start();

    $id_pagina = '2';

    if($_SESSION['id'] == NULL) {

    echo"<script language=\"JavaScript\">
    location.href=\"index.html\";
    </script>";

    } else {

    $sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
    $res = mysqli_query($con, $sql_permissao);
    $num_busca = mysqli_num_rows($res);

    if ($num_busca == 0) {

    echo"<script language=\"JavaScript\">
    location.href=\"sempermissao.php\";
    </script>";

    } else {

    $sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
    $res_cadastro = mysqli_query($con, $sql_cadastro);
    $vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>

  <!DOCTYPE html>
  <head>
    

    <meta charset=utf-8>
    <meta content="IE=edge" http-equiv=X-UA-Compatible>
    <title>JL Seguro</title>
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name=viewport>
    <link href=layout/bower_components/bootstrap/dist/css/bootstrap.min.css rel=stylesheet>
    <link href=layout/bower_components/font-awesome/css/font-awesome.min.css rel=stylesheet>
    <link href=layout/bower_components/Ionicons/css/ionicons.min.css rel=stylesheet>
    <link href=layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css rel=stylesheet>
    <link href=layout/dist/css/AdminLTE.css rel=stylesheet>
    <link href=layout/dist/css/skins/_all-skins.min.css rel=stylesheet>
    <script src=//code.jquery.com/jquery-2.1.4.min.js></script>
    <link href=favicon-32x32.png rel=icon sizes=32x32 type=image/png>
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

        <!--  PESQUISA COM AJAX -->
<script type="text/javascript">
  var req;
   
  // FUNÇÃO PARA BUSCA NOTICIA
  function buscarNoticias(valor) {
   
  // Verificando Browser
  if(window.XMLHttpRequest) {
     req = new XMLHttpRequest();
  }
  else if(window.ActiveXObject) {
     req = new ActiveXObject("Microsoft.XMLHTTP");
  }
   
  // Arquivo PHP juntamente com o valor digitado no campo (método GET)
  var url = "busca.php?valor="+valor;
   
  // Chamada do método open para processar a requisição
  req.open("Get", url, true); 
   
  // Quando o objeto recebe o retorno, chamamos a seguinte função;
  req.onreadystatechange = function() {
   
    // Exibe a mensagem "Buscando Noticias..." enquanto carrega
    if(req.readyState == 1) {
      document.getElementById('resultado').value = 'Buscando Noticias...';
    }
   
    // Verifica se o Ajax realizou todas as operações corretamente
    if(req.readyState == 4 && req.status == 200) {
   
    // Resposta retornada pelo busca.php
    var resposta = req.responseText;
   
    // Abaixo colocamos a(s) resposta(s) na div resultado
    document.getElementById('resultado').value = resposta;
    }
  }
  req.send(null);
  }

</script>

    <!-- Adicionando Javascript -->
    <script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
                $("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>

    <script type="text/javascript">
/* MÃ¡scaras ER */
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que nÃ£o Ã© dÃ­gito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parÃªnteses em volta dos dois primeiros dÃ­gitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hÃ­fen entre o quarto e o quinto dÃ­gitos
    return v;
}
function id( el ){
  return document.getElementById( el );
}
window.onload = function(){  
    id('telefone').onkeypress = function(){  
        mascara( this, mtel);  
    }
    id('telefone2').onkeypress = function(){  
        mascara( this, mtel);  
    }
}
</script>


<script type="text/javascript">

            $(function(){

            $('#formID').on('submit', function(e) {

            e.preventDefault();

            $.ajax({
                url: url_sys+"recebe_cadastro.php",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                success: function(data){

                    var html = '';


                    if(parseInt(data.success) == 1){
                        html = `<div class="alert alert-success">Cliente cadastrado com Sucesso! <br/>
                                Em 5 segundos você será Redirecionado para a Lista de Clientes</div>`;
                    }

                    if(parseInt(data.error) == 404){
                        html = `<div class="alert alert-danger"><strong>ESTE CPF JÁ ESTA CADASTRADO EM NOSSO SISTEMA!</strong></div>`;
                    }

                    $("#formResult").html(html);

                    $("#formID")[0].reset();


                    if (parseInt(data.tipocad) == 1) {
                        setTimeout(() => {
                            window.location.href = 'listarclientes.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 2) {
                        setTimeout(() => {
                            window.location.href = 'listarfornecedores.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 3) {
                        setTimeout(() => {
                            window.location.href = 'listarvendedores.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 4) {
                        setTimeout(() => {
                            window.location.href = 'listarprestadores.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 5) {
                        setTimeout(() => {
                            window.location.href = 'listarparceiros.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 6) {
                        setTimeout(() => {
                            window.location.href = 'listarrepresentantes.php';
                        }, 5000);
                    }
                    if (parseInt(data.tipocad) == 7) {
                        setTimeout(() => {
                            window.location.href = 'listarparcerias.php';
                        }, 5000);
                    }


                }
            });

            });


            });

            </script>


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="container">
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title text-ionc-9">Cadastro de novo Fornecedor</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div id="formResult"></div>
            <form method="post" name="cliente" enctype="multipart/form-data" id="formID">


            <!-- form recibo -->
            <?php include 'includes/formRecibo.php'; ?>

            <table width="100%">
                <tr>
                  <td width="95%"></td>
                  <td><button type="submit" class="btn btn-ionc-9"  style="    float: left;">Cadastrar</button></td>
                </tr>
              </table>
              </br>
              </br>
              
        <input type="hidden" name="tipocad" value="2">  
        <div class="row">
          
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Nome Fantasia</label>
              <input type="text" name="nomefant" class="form-control" id="exampleInput" placeholder="Digite o nome">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Nome ou Razão Social</label>
              <input type="text" name="nome" class="form-control" id="exampleInput" placeholder="Digite o nome">
            </fieldset>
          </div>

          <div class="col-lg-4">
            <fieldset class=form-group>

                <style>
                    .select_cj{
                        display: flex;
                        flex-direction: row;
                    }

                    .select_cj > label{
                        background-color: #ddd;
                        margin-right: 5px;
                        border-radius: 5px;
                        padding-left: 8px;
                        padding-right: 8px;
                        color: #000;
                        cursor: pointer;
                    }

                    .select_cj > label:hover{
                        background-color: #ccc;
                        transition: 0.5s all;
                        color: #777;
                    }
                </style>
                <div class="select_cj">
                    <label for="cpfselected">CPF:  <input type="radio" name="cpfcnpjselected"  value="cpf" id="cpfselected"></label>
                    <label for="cnpjselected">CNPJ:  <input type="radio" name="cpfcnpjselected" value="cnpj" id="cnpjselected"></label>
                </div>

                <input name="cpfcnpj" value="<?php echo $vetor['cpfcnpj']; ?>" class="form-control" id="cpfcnpj" placeholder="Digite apenas os numeros">
            </fieldset>
            <script>
                $(function(){

                    $('#cpfselected').on('click', function() {
                        $('#cpfcnpj').mask('000.000.000-00', {reverse: true});

                    });

                    $('#cnpjselected').on('click', function() {
                        $('#cpfcnpj').mask('00.000.000/0000-00', {reverse: true});
                    });
                });
            </script>
          </div>
        </div><!--.row-->

        <div class="row">
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">RG</label>
              <input type="text" name="rg" class="form-control" value="<?php echo $vetor['rg']; ?>" id="exampleInput" placeholder="Digite o RG">
            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Data de Nascimento</label>
              <input type="date" id="datanas"  onkeyup="formatar_ano(this.id)" name="datanasc" value="<?php echo $vetor['datanasc']; ?>" class="form-control">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Nome do Responsável</label>
              <input type="text" name="nomeresp" class="form-control" id="exampleInput" placeholder="Digite o nome">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Contribuinte IE?</label>
              <select id="exampleSelect" name="isento" class="form-control">
                <option value="" selected>Selecione</option>
                                <option value="1">Contribuinte</option>
                <option value="2">Isento</option>
              </select>
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Inscrição Estadual (Só numeros)</label>
              <input type="text" name="inscestadual" class="form-control" id="exampleInput" placeholder="Digite apenas os numeros" pattern="[0-9]+$">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Inscrição Municipal</label>
              <input type="text" name="inscmunicipal" class="form-control" id="exampleInput" placeholder="Digite apenas os numeros">
            </fieldset>
          </div>
          <div class=col-lg-4>
              <fieldset class=form-group>
                 <label class=form-label for=exampleInputEmail1>CEP</label>
                  <input name="cep" value="<?php echo $vetor['cep']; ?>" class=form-control id=cep id=exampleInput placeholder=CEP>
              </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Rua</label>
              <input type="text" name="endereco" id="rua" class="form-control" placeholder="Endereço">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Numero</label>
              <input type="text" name="numero" class="form-control" id="exampleInput" placeholder="Numero">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Complemento</label>
              <input type="text" name="complemento" class="form-control" id="exampleInput" placeholder="Complemento">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Bairro</label>
              <input type="text" name="bairro" id="bairro" class="form-control" placeholder="Bairro">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Cidade</label>
              <input type="text" name="cidade" id="cidade" class="form-control" placeholder="Cidade">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Estado</label>
              <input type="text" name="estado" id="uf" class="form-control" placeholder="Estado">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Codigo Municipio</label>
              <input type="text" name="codigoibge" id="ibge" class="form-control" placeholder="Codigo Municipio">
            </fieldset>
          </div>
        </div><!--.row-->
                
                <div class="row">
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Telefone</label>
              <input type="text" name="telefone" id="telefone" class="form-control" placeholder="Telefone">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Celular</label>
              <input type="text" name="celular" id="telefone2" class="form-control" placeholder="Celular">
            </fieldset>
          </div>
          <div class="col-lg-4">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">E-mail</label>
              <input type="text" name="email" class="form-control" id="exampleInput" placeholder="Digite seu E-mail">
            </fieldset>
          </div>
        </div><!--.row-->

        <div class="row">
        <div class="col-lg-1">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Cod. B</label>
              <input type="text" id="busca" onkeyup="buscarNoticias(this.value)" name="banco" value="" class="form-control" placeholder="Cod">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label semibold" for="exampleInput">Banco</label>
              <input type="text" id="resultado" name="banco" value="<?php echo $vetor['banco']; ?>" class="form-control" placeholder="Banco">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputEmail1">Agencia</label>
              <input type="text" name="agencia" value="<?php echo $vetor['agencia']; ?>" class="form-control" placeholder="Agencia">
            </fieldset>
          </div>
          <div class="col-lg-3">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Conta</label>
              <input type="text" name="conta" value="<?php echo $vetor['conta']; ?>" class="form-control" id="exampleInput" placeholder="Conta">
            </fieldset>
          </div>
           <div class="col-lg-2">
            <fieldset class="form-group">
              <label class="form-label" for="exampleInputPassword1">Tipo da Conta</label>
              <select name="tipoconta" class="form-control">
                <option value=" <?php echo $vetor['tipoconta']; ?> " selected="">
                  <?php if ($vetor['tipoconta'] == 1) {
                    echo "Corrente";
                  }elseif ($vetor['tipoconta'] == 2) {
                    echo "Poupança";
                  }
                ?>
                </option>
                <option value="1" <?php if (strcasecmp($vetor['tipoconta'], '1') == 0) : ?>selected="selected"<?php endif; ?>>Corrente</option>
                <option value="2" <?php if (strcasecmp($vetor['tipoconta'], '2') == 0) : ?>selected="selected"<?php endif; ?>>Poupança</option>
              </select>
            </fieldset>
          </div>
        </div><!--.row-->
        
                
                <div class="form-group row">
                  <div class="col-sm-12">
            <label for="exampleSelect" class="col-sm-1 form-control-label">Observações</label>
            
              <textarea rows="10" id="exampleSelect" name="anotacoes" class="form-control" placeholder="Digite suas Observações"></textarea>
            </div>
        </div>

        

        <h3>CADASTRAR RECIBO</h3>
         <div class="row">
          <div class="form-control" style="padding-right: 40px;margin-bottom: 20px;border:none;">
              

              <label for="confirmRecYes" id="regReciboYes" class="btn btn-success"style="float: left;margin-right: 10px;" >SIM</label>

              <label for="confirmRecNot" id="regReciboNot" class="btn btn-danger"style="float: left;margin-right: 10px;" >NÃO</label>

              <button type="submit" class="btn btn-ionc-9">Cadastrar</button>

          </div>
         </div>
                
      </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>

</div>

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<script src="jquery.mask.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  $(function(){
      $('html').css('background-color', '#fbeeff');
      $('body').css('background-color', '#fbeeff');
      $('.container').css('background-color', '#fbeeff');
      $('.content').css('background-color', '#fbeeff');
      $('.box').css('background-color', '#fbeeff');
      $('.tab-content').css('background-color', '#fbeeff');
    });

</script>

<!-- Script que não deixa o usuario digitar mais do que 4 digitos no campo de ano no input type date,
requer somente um id e um onkeyup passando o this.id lembre-se de verificar se não outro id no mesmo input-->
<script type="text/javascript">

function formatar_ano(id){
    var array_data = [];

      var data = $("#"+id).val();

      if (data.length > 10) {

        array_data = data.split("-");

        var ano = array_data[0];

        ano = ano.substring(0,(ano.length - 1));

        var dia = array_data[2];
        var mes = array_data[1];

        var data_final = ano + "-" + mes + "-" + dia;

        $("#"+id).val(data_final);

      }
}

</script>

</body>
</html>
<?php } } ?>