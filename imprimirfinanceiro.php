<?php

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

include"includes/conexao.php";

$id_pedido = $_GET['id_pedido'];

$ultimas12linhas = mysqli_query($con, "SELECT * from cr WHERE id_pedido = '$id_pedido' ORDER BY id_cr DESC LIMIT 12");

$sqlnumrenova = mysqli_query($con, "SELECT ordem FROM cr WHERE id_pedido = '$id_pedido' ORDER BY id_cr DESC LIMIT 1");
$numdeordem_featch = mysqli_fetch_array($sqlnumrenova);

$numdeordem = $numdeordem_featch['ordem'];


$increment = 0; 

foreach ($ultimas12linhas as $linhatual) {

	if ($linhatual['tipo'] <> 1) {
		$increment += 1;
	}
}

$sql_consultando = mysqli_query($con, "SELECT id_cr FROM cr WHERE id_pedido = '$id_pedido' && tipo = 1");
$sql_consultando_9 = mysqli_query($con, "SELECT id_cr FROM cr WHERE id_pedido = '$id_pedido' AND tipo = 9 OR tipo = 1");

$sql_pedido = mysqli_query($con, "select * from pedidos where id_pedido = '$id_pedido'");
$vetor_pedido = mysqli_fetch_array($sql_pedido);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$sql_os = mysqli_query($con, "select * from os where id_pedido = '$id_pedido'");
$vetor_os = mysqli_fetch_array($sql_os);

//$sql_n_men = mysqli_query($con, "SELECT * from cr WHERE id_pedido = 741 AND tipo != '1' ORDER BY tipo DESC");

//$total_n_mensa = mysqli_num_rows($sql_n_men);

$total_num = $increment + 12;




	if($numdeordem != null){
		
		$sql_cr = mysqli_query($con, "SELECT SUM(valor) AS total, datavencimento, formapag, tipo, COUNT(*) AS quant FROM cr WHERE id_pedido = '$id_pedido' AND ordem = '$numdeordem' GROUP BY id_cr ASC ORDER BY datavencimento ASC");
	}elseif($numdeordem == null){
		
		$sql_cr = mysqli_query($con, "SELECT SUM(valor) AS total, datavencimento, formapag, tipo, COUNT(*) AS quant FROM cr WHERE id_pedido = '$id_pedido' GROUP BY id_cr ASC ORDER BY id_cr DESC LIMIT $total_num ");
	}else{
		$sql_cr = mysqli_query($con, "SELECT SUM(valor) AS total, datavencimento, formapag, tipo, COUNT(*) AS quant FROM cr WHERE id_pedido = '$id_pedido' GROUP BY datavencimento, formapag ORDER BY datavencimento ASC");

	}
	

?>
<!DOCTYPE html>
<html>
<meta charset="utf-8">
<head>
	<title></title>
	<style>
		td{
			font-size: 15px !important;
		}
	</style>
</head>
<body>

<img src="imgs/logoimpressao.png" width="300" height="auto" style="display: block;margin-top: 20px;margin-bottom: 40px;">

<strong>
<p align="center"> PLANILHA CONTROLE DE PAGAMENTO</p>
<p align="center"> E RESPONSABILIDADE</p>
</strong>
</br>
<table width="100%">
	<tr>
		<td width="15%"><strong>Contrato:</strong></td>
		<td><?php echo $id_pedido;

			if(mysqli_num_rows($sql_consultando) > 20){echo "   - RENOVAÇÃO";} ?>  </td>
	
	</tr>
	<tr>
		<td width="15%"><strong>Cliente:</strong></td>
		<td><?php echo $vetor_cliente['nome']; ?></td>
	</tr>
</table>
</br>
<table width="100%" BGCOLOR='#e8e8e8'>
	<tr>
		<td width="25%" align="center"><strong><p style="font-size: 12px;">Referente a Utilização Periodo</p></strong></td>
		<td width="25%" align="center"><strong>Vencimento</strong></td>
		<td width="25%" align="center"><strong>Valor</strong></td>
		<td width="25%" align="center"><strong>Forma de Pagamento</strong></td>
	</tr>
	<?php while($vetor_cr = mysqli_fetch_array($sql_cr)) { ?>
	<tr>
		<td width="25%" align="left">
			<?php 

			if ($vetor_cr['tipo'] == 1) {

				$data1 = date('d/m/Y', strtotime("-1 months",strtotime($vetor_cr['datavencimento'])));
				$data2 = date('d/m/Y', strtotime($vetor_cr['datavencimento']));


				echo "de ". $data1 . " a ". $data2;
				
			}elseif($vetor_cr['tipo'] == 2){
				//adesão

				echo "Adesão";

			}elseif($vetor_cr['tipo'] == 3){
				
				//assistencia 24H

				echo "Assistencia 24H";

			}elseif($vetor_cr['tipo'] == 4){
				
				//assistencia a Terceiros

				echo "Assistencia a Terceiros";

			}elseif($vetor_cr['tipo'] == 5){

				//Parcela CARRO RESERVA

				echo "Carro Reserva";
				
			}elseif($vetor_cr['tipo'] == 6){

				//Vacina

				echo "Vacina";
				
			}elseif($vetor_cr['tipo'] == 7){

				//Instalação

				echo "Instalação";
				
			}elseif($vetor_cr['tipo'] == 8){

				//Parcela CARRO APP

				echo "Carro APP";
				
			}elseif($vetor_cr['tipo'] == 9){
				
				$data1 = date('d/m/Y', strtotime("-30 days",strtotime($vetor_cr['datavencimento'])));
				$data2 = date('d/m/Y', strtotime($vetor_cr['datavencimento']));


				echo "de ". $data1 ." a ". $data2;
			}


			?>
		</td>
		<td width="25%" align="center"><?php echo date('d/m/Y', strtotime($vetor_cr['datavencimento'])); ?></td>
		<td width="25%" align="center">R$ <?php echo $num = number_format($vetor_cr['total'],2,',','.'); ?></td>
		<td width="25%" align="center"><?php echo $vetor_cr['formapag']; ?></td>
	</tr>
	<?php } ?>
</table>
<table width="100%">
	<tr>
		<td>Eu, <?php echo $vetor_cliente['nome']; ?> declaro  que estou ciente que o  não  pagamento  de  uma  ou  mais  parcela a Empresa <strong>JL SEGURO SYSTEM</strong> poderá interromper o serviço de monitoramento entre outros serviços contratados e ainda me protestar  após  7 (sete)  dias  vencidos   junto  aos  orgãos  SERASA,  SCPC e demais orgãos.</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td align="center">
			________________________________
			</br><?php echo $vetor_cliente['nome']; ?>
			</br>CPF: <?php echo $cpf = Mask("###.###.###-##",$vetor_cliente['cpfcnpj']); ?>
		</td>
		<td align="center">Data: ________/________/________.</td>
	</tr>
</table>
</br>
<table width="100%">
	<tr>
		<td align="center">
		PAGUE SEMPRE NO VENCIMENTO TENHA SEU DIREITO GARANTIDO.
		<p>DEPARTAMENTO DE COBRANÇA</p>
		<p>(11) 4171-5781</p>
		</td>
	</tr>
</table>
</body>
</html>

<script type="text/javascript">
	print();
</script>