<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8"/>
    <title>Untitled Document</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>


    <nav id="sidebar">

        <a href="index.php"><img src="http://v2.systemjl.com.br/components/assets/images/empresa/logo.png" id="logomarca" alt="JL SEGUROS SYSTEM" /></a>

        <ul class="sidebar-menu" id="sidebar-menu" data-widget="tree">

            <h4 id="noneTitleDesktop">Menu Geral</h4>

            <li class="treeview">

                <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Cadastros Gerais</span>
                </a>

                <ul class="treeview-menu cadastros-gerais-pop">
                    <li><a href="listarclientes.php"><i class="far fa-circle"></i> Cliente</a></li>
                    <li><a href="listarfornecedores.php"><i class="far fa-circle"></i> Fornecedor</a></li>
                    <li><a href="listarparcerias.php"><i class="far fa-circle"></i> Parceiro</a></li>
                    <li><a href="listarvendedores.php"><i class="far fa-circle"></i> Vendedor</a></li>
                    <li><a href="listarparceiros.php"><i class="far fa-circle"></i> Indicador</a></li>
                    <li><a href="listarrepresentantes.php"><i class="far fa-circle"></i> Representante</a></li>
                    <li><a href="listarprestadores.php"><i class="far fa-circle"></i> Técnico - Instalador</a></li>
                    <li><a href="aniversariantes.php"><i class="far fa-circle"></i> Aniversariantes</a></li>
                </ul>

            </li>

            <li class="treeview">
                <a href="#">
                <i class="fa fa-money"></i> <span>Financeiro</span>
                </a>
                <ul class="treeview-menu financeiro-dropdown">

                    <li><a href="extratoconta.php"><i class="far fa-circle"></i> Fluxo de Caixa</a></li>

                </ul>
            </li>

            <li class="treeview hideToFromMovel">
            <a href="#">
            <i class="fas fa-cart-arrow-down"></i> <span>Contas a Pagar</span>
            </a>
            <ul class="treeview-menu cp-dropdown">
            <li><a href="listarcp.php"><i class="far fa-circle"></i> Todos</a></li>
            <li><a href="gerarcomissoes.php"><i class="far fa-circle"></i> Gerar Comissões</a></li>
            
            <li class="treeview">
            <a href="#"><i class="far fa-circle"></i> Relatórios C. a Pagar
            </a>

            <ul class="treeview-menu">
            <li><a href="relcpanalitico.php"><i class="far fa-circle"></i> Analítico</a></li>
            <li><a href="relcpsintetico.php"><i class="far fa-circle"></i> Sintético</a></li>
            <li><a href="relcomissoesapagar.php"><i class="far fa-circle"></i> Comissões a Pagar</a></li>
            <li><a href="relcpcomissoes.php"><i class="far fa-circle"></i> Comissões</a></li>
            <li><a href="relcpfornecedor.php"><i class="far fa-circle"></i> Por Fornecedor</a></li>
            <li><a href="relcpcentrodecustos.php"><i class="far fa-circle"></i> Por Centro de Custos</a></li>
            <li><a href="relcpdespesa.php"><i class="far fa-circle"></i> Por Despesa</a></li>
            <li><a href="relcpprojetofinalidade.php"><i class="far fa-circle"></i> Por Projeto / Finalidade</a></li>
            </ul>

            </li> 

            </ul>

            </li>


            <li class="treeview">
                <a href="#">
                <i class="fas fa-cart-plus"></i> <span>Contas a Receber</span>
                </a>
                <ul class="treeview-menu cr-dropdown">
                    <li><a href="listarcr.php"><i class="far fa-circle"></i> Todos</a></li>
                    <li><a href="relcranalitico.php"><i class="far fa-circle"></i> Analítico</a></li>
                    <li><a href="relcrsintetico.php"><i class="far fa-circle"></i> Sintético</a></li>            
                </ul>
            </li>


            <li class="treeview">
            <a href="#">
            <i class="fas fa-money-check-alt"></i> <span>Faturamento</span>
            </a>
            <ul class="treeview-menu fatu-dropdown">

            <li><a href="relcrglobal.php"><i class="far fa-circle"></i> Por Periodo</a></li>
            <li><a href="relcrporitem.php"><i class="far fa-circle"></i> Por Produto</a></li>
            <li><a href="relcrcliente.php"><i class="far fa-circle"></i> Por Cliente</a></li>

            </ul>

            </li>

            <li class="treeview">
                <a href="#">
                <i class="fa fa-cart-plus"></i>
                <span>Pedidos</span>
                </a>
                <ul class="treeview-menu pedido-dropdown">
                <li><a href="cadastropedido.php"><i class="far fa-circle"></i> Novo Pedido</a></li>
                <li><a href="listarpedidos.php"><i class="far fa-circle"></i> Pedidos</a></li>
                </ul>
            </li>


            <li class="treeview hideToFromMovel">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>OS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu pedido-dropdown">
            <li><a href="listaros.php"><i class="far fa-circle"></i> OS</a></li>
          </ul>
        </li>

        <li class="treeview hideToFromMovel">
          <a href="#">
            <i class="fa fa-check-circle"></i>
            <span>Orçamentos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu pedido-dropdown">
            <li><a href="cadastroorcamento.php"><i class="far fa-circle"></i> Novo Orçamento</a></li>
            <li><a href="listarorcamentos.php"><i class="far fa-circle"></i> Orçamentos</a></li>
          </ul>
        </li>

        <li class="treeview hideToFromMovel">
          <a href="#">
            <i class="fa fa-archive"></i>
            <span>Produtos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu pedido-dropdown">
            <li><a href="listarprodutos.php"><i class="far fa-circle"></i> Produtos</a></li>
            <li><a href="entradaestoque.php"><i class="far fa-circle"></i> Entrada de Estoque</a></li>
          </ul>
        </li>
            
            <li class="treeview hideToFromMovel">

                <a href="#">
                    <i class="fa fa-address-card"></i>
                    <span>CRM</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu pedido-dropdown">
                    <li><a href="listarcrm.php"><i class="far fa-circle"></i> CRM</a></li>
                    <li><a href="listarrenovacoes.php"><i class="far fa-circle"></i> Renovações</a></li>
                    <li><a href="listarcontratos.php"><i class="far fa-circle"></i> Contratos</a></li>
                </ul>

            </li> 

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Configurações</span>
                </a>
                <ul class="treeview-menu pedido-dropdown">
                    <li><a href="listarusuarios.php"><i class="far fa-circle"></i> Usuários</a></li>
                    <li><a href="listarchecklist.php"><i class="far fa-circle"></i> CheckList</a></li>
                    <li><a href="listarservicos.php"><i class="far fa-circle"></i> Serviços</a></li>
                    <li><a href="listarcentrodecustos.php"><i class="far fa-circle"></i> Centro de Custo</a></li>
                    <li><a href="listardespesas.php"><i class="far fa-circle"></i> Despesas</a></li>
                    <li><a href="listareventos.php"><i class="far fa-circle"></i> Projetos / Finalidades</a></li>
                    <li><a href="fipe/fipe_form.php"><i class="far fa-circle"></i> Atualizar Fipe</a></li>
                </ul>
            </li>

            <div class="boxThemeSystem hideToFromMovel">
            <h4>Tema do Sistema</h4>
            <form method="POST" id="altertheme">
                <ul>
                    <li><input type="radio" onchange="alert(this)" name="theme" value="claro" id="claro"><label for="claro">Claro</label> </li>
                    <li><input type="radio" onchange="alert(this)" name="theme" value="dark" id="dark"><label for="dark">Escuro</label></li>
                </ul>
            </form>
            </div>

        </ul>

        <div class="iconmovel">
            <button  id="opendreamwave"><img src="includes/menu.svg"></button>
        </div>

        <div class="box-user-photo-dropdown">
            
            <img src="./imgs/user.png" id="imgUserMenuBox">

            <div class="dropdownMenuBox">
                <h3><?php echo $vetor_cadastro['nome']; ?></h3>

                <ul>
                    <li><a href="./listarusuarios.php">Configurações</a></li>
                    <li><a href="./sair.php">Sair</a></li>
                </ul>
            </div>

        </div>


    </nav>

    <script src="../jquery-1.3.2.min.js"></script>
    <script type="text/javascript">

        $(function(){

            $('#opendreamwave').bind('click', function(){
                $('.sidebar-menu').fadeIn(400).css('display', 'flex').addClass('menuOpenedDrawe');
            });          

            $('#imgUserMenuBox').bind('click', function(){
                $('.dropdownMenuBox').slideToggle(50);
            });
        })

    </script>

</body>
</html>
