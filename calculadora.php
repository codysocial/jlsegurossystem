<script>
        function insert(num) {
            document.form.textView.value = document.form.textView.value + num
        }

        function equal() {
            var exp = document.form.textView.value;

            if(exp.indexOf("%") !== -1){
                
                var vetor = exp.split("%");

                var num1 = vetor[0];
                var num2 = vetor[1];

                var preco = num1;
                var porcentagem = num2 ;
                var total = preco * (porcentagem/100);

                document.form.textView.value = total;

            }else if(exp) {
                document.form.textView.value = eval(exp);
            }
        }

        function clean() {
            document.form.textView.value = '';
        }

        function backspace() {
            var exp = document.form.textView.value;

            document.form.textView.value = exp.substring(0, exp.length - 1);
        }
</script>

<script type="text/javascript">
  var i = 2;

  function controller_calculadora(){
    if (i == 1) {

      $("#calculadora").animate({ 
        // distancia da margem a esquerda
        height: "0px"
        // tempo de execucao - milissegundos
        }, 500, function(){
          var muda_tamanho = $("#calculadora").width("0px");
        });



      $("#button_on").animate({ 
        // distancia da margem a esquerda
        bottom: "15px"
        // tempo de execucao - milissegundos
        }, 500, function(){

        });

      var muda_html = $("#button_onon").html("+");

      var muda_class = $("#button_onon").removeClass("btn_vermelho").addClass("btn_verde");

      i = 2;
    }else{

      var muda_tamanho = $("#calculadora").width("240px");

      $("#calculadora").animate({ 
        // distancia da margem a esquerda
        height: "330px"
        // tempo de execucao - milissegundos
        }, 500, function(){

        });

      $("#button_on").animate({ 
        // distancia da margem a esquerda
        bottom: "340px"
        // tempo de execucao - milissegundos
        }, 500, function(){

        });

      var muda_html = $("#button_onon").html("-");

      var muda_class = $("#button_onon").removeClass("btn_verde").addClass("btn_vermelho");

      i = 1;
    }
  }
</script>

<div id="button_on">
  <button class="btn btn_verde" onclick="controller_calculadora()" id="button_onon">+</button>
</div>

<div id="calculadora">
  <div class="main">
        <form name="form">
            <input class="textView" id="visor" name="textView">
        </form>
        <table>
            <tr>
                <td><input class="button backspace btn" type="button" value="C" onclick="clean()"></td>
                <td><input class="button yl btn" type="button" value="%" onclick="insert('%')"></td>
                <td><input class="button yl btn" type="button" value="/" onclick="insert('/')"></td>
                <td><input class="button yl btn es" type="button" value="*" onclick="insert('*')"></td>
            </tr>
            <tr>
                <td><input class="button btn" type="button" value="7" onclick="insert(7)"></td>
                <td><input class="button btn" type="button" value="8" onclick="insert(8)"></td>
                <td><input class="button btn" type="button" value="9" onclick="insert(9)"></td>
                <td><input class="button yl btn es" type="button" value="-" onclick="insert('-')"></td>
            </tr>
            <tr>
                <td><input class="button btn" type="button" value="4" onclick="insert(4)"></td>
                <td><input class="button btn" type="button" value="5" onclick="insert(5)"></td>
                <td><input class="button btn" type="button" value="6" onclick="insert(6)"></td>
                <td><input class="button yl btn es" type="button" value="+" onclick="insert('+')"></td>
            </tr>
            <tr>
                <td><input class="button btn" type="button" value="1" onclick="insert(1)"></td>
                <td><input class="button btn" type="button" value="2" onclick="insert(2)"></td>
                <td><input class="button btn" type="button" value="3" onclick="insert(3)"></td>
                <td rowspan="5"><input style="height: 106px;" class="button yl btn es" type="button" value="=" onclick="equal()">
                </td>
            </tr>
            <tr>
                <td colspan="2"><input style="width: 106px;" class="button btn" type="button" value="0" onclick="insert(0)">
                </td>
                <td><input class="button btn" type="button" value="." onclick="insert('.')"></td>
            </tr>
        </table>
    </div>
</div>