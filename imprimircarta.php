<?php

require 'vendor/autoload.php';

use Dompdf\Dompdf;

$dompdf = new Dompdf();

function Mask($mask,$str){

    $str = str_replace(" ","",$str);

    for($i=0;$i<strlen($str);$i++){
        $mask[strpos($mask,"#")] = $str[$i];
    }

    return $mask;

}

include"includes/conexao.php";

$id = $_GET['id_pedido'];

$sql_pedidos = mysqli_query($con, "select * from pedidos where id_pedido = '$id'");
$vetor_pedido = mysqli_fetch_array($sql_pedidos);

$sql_os = mysqli_query($con, "select * from os where id_pedido = '$id'");
$vetor_os = mysqli_fetch_array($sql_os);

$sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor_pedido[id_cliente]'");
$vetor_cliente = mysqli_fetch_array($sql_cliente);

$sql_veiculo = mysqli_query($con, "select * from cliente_veiculo where id_cliente = '$vetor_pedido[id_cliente]' order by id_veiculo DESC limit 0,1");
$vetor_veiculo = mysqli_fetch_array($sql_veiculo);

$sql_marca = mysqli_query($con, "select * from fp_marca where codigo_marca = '$vetor_veiculo[marca]'") or die (mysqli_error($con));
$vetor_marca = mysqli_fetch_array($sql_marca);

$sql_anomod = mysqli_query($con, "SELECT * FROM fp_ano WHERE id_ano = '$vetor_veiculo[anomod]'") or die (mysqli_error($con));
$vetor_anomod = mysqli_fetch_array($sql_anomod);

if($vetor_veiculo['valor'] != NULL) { $valorveiculo = number_format($vetor_veiculo['valor'],2,',','.'); } else { $valorveiculo = number_format($vetor_anomod['valor'],2,',','.'); }

$qtddoc = strlen($vetor_cliente['cpfcnpj']);

if($qtddoc == 14) {
                
$cpfcnpj = Mask("##.###.###/####-##",$vetor_cliente['cpfcnpj']); 
                
}
                
if($qtddoc == 11) {
                
$cpfcnpj = Mask("###.###.###-##",$vetor_cliente['cpfcnpj']); 
                
}

$vigenciainicio = date('d/m/Y', strtotime($vetor_pedido['vigenciainicio']));
$vigenciafim = date('d/m/Y', strtotime($vetor_pedido['vigenciafim']));
$datainstalacao = date('d/m/Y', strtotime($vetor_os['datainstalacao']));

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
.style2 {
	font-size: 12
}
.style3 {font-size: 12px}

            .pontilhada {
			border: 2px dotted #999; /*Definindo o estilo da borda*/
			background-color: #FFFFFF; /*Cor de fundo para melhor visualização do exemplo*/
			height: 200px; /*Altura para melhor visualização do exemplo*/
			width: 420px; /*Largura para melhor visualização do exemplo*/
			}

-->
    </style>
</head>
<body>
<div class="style3">
<img src="imgs/logo.png" width="180px">
</br>
</br>
<table width="100%">
	<tr>
		<td>
		<strong>Prezado (a) Cliente,</strong>
		</td>
	</tr>
	<tr>
		<td>
		<?php echo $vetor_cliente['nome']; ?>
		</td>
	</tr>
	<tr>
		<td>
		<div align="justify">
		<p>
		Você acaba de adquirir o mais completo e avançado sistema de rastreamento e localização automática de veículos por satélite-GPSPRS para o seu veiculo, aproveitamos a oportunidade para lhe agradecer por ter escolhido a JL SEGURO SYSTEM DO BRASIL como sua prestadora de serviço de rastreamento e localização. Esperamos plenamente a sua satisfação com a NOSSA EMPRESA, pois, a partir de agora, você tem a tranquilidade e segurança para o seu veículo.
		</p>
		O sistema JL SEGURO SYSTEM DO BRASIL – GPSPRS fabricado no Brasil (Tecnologia importada) com um dos mais sofisticados softwares de monitoramento e rastreamento, que permite localizar e bloquear o seu veículo em caso de furto ou roubo com precisão pelo sistema GPS (Global Positioning System) através de nossa Central de Monitoramento, que funciona 24 horas/365 dias por ano. 

		</br>
		</br>

		</td>
	</tr>
	
	<tr>

	<td align="center">

	<strong><span class="style3"><font color="#FF0000">PREZADO (A) CLIENTE PRESTE ATENÇÃO PARA CADA NECESSIDADE UM 0800 DIFERENTE PARA MELHOR LHE ATENDER</font></span></strong>

	</br>
	</br>
	</br>

	</td>

	</tr>

	<tr>

	<td>

	<strong>PROCEDIMENTO IMPORTANTE APÓS A INSTALAÇÃO:</strong>

	</br>
	</br>

	</td>

	</tr>

	<tr>

	<td>

	Após a instalação do equipamento em seu veículo o (a) Sr.(a) precisa baixar o APP: Central 24 horas no Google Play se seu celular for Android e  App Store se seu celular Apple (ISO)  Após este procedimento entrar em contato com a nossa central monitoramento pelo número <font color="#FF0000">0800 030 6672 ou  0800 777 9740</font>  Opção  2 informe que acabou de ser instalado o rastreador no seu veículo que o Sr. (a)  precisa ser informado(a)  do seu loguin e senha.

	</td>

	</tr>

	<tr>

	<td>

	<strong>COMO AGIR EM CASO DE ROUBO E FURTO:</strong>

	</td>

	</tr>

	<tr>

	<td>

	Basta ligar para a Central de Monitoramento JL SEGURO SYSTEM O MAIS RAPIDO POSSÍVEL E NOS PRIMEIROS MINUTOS  DO ACONTECIMENTO, informar a ocorrência do roubo/furto, informando a placa do seu veículo e o nome do titular do contrato, que será prontamente iniciado a recuperação do veículo. Seu veículo sendo recuperado, será solicitada a presença do proprietário do veículo para a devida entrega mediante a presença das autoridades policiais). TELEFONE DA CENTRAL 24hrs JL SEGURO SYSTEM: <font color="#FF0000">0800 030 6672 ou  0800 777 9740</font>.

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	

	</td>

	</tr>

	<tr>

	<td>

	NUNCA DEIXE SEU VEICULO EM LOCAIS SEM SEGURANÇA. 

	</td>

	</tr>

	<tr>

	<td>


	</td>

	</tr>

	<tr>

	<td align="center"> 
	<font color="#FF0000">
	</br>
	</br>
	CENTRAL DE VENDAS: JL SEGURO SYSTEM DO BRASIL (11) 4171-5781<p>
	Participe do programa indique e ganhe, indique um amigo ou mais e ganhe $$$, quanto mais indica mais ganha $$$.
	</font>


	</td>

	</tr>

	<tr>

	<td>

	<table width="100%">
		<tr>
			<td width="100%" align="center">
				<div style="background-color: #FFFFFF; border: 2px dotted #999; width: 330px; height: 200px; padding:2px;">
				
				<table width="100%">

					<tr>

						<td align="center">

							<table>

								<tr>
									<td width="33%"></td>
									<td width="33%" align="center"><img src="imgs/logo.png" width="150px"></td>
									<td width="33%"></td>
								</tr>

								<tr>
									<td width="33%"></td>
									<td width="33%" align="center"><strong>CENTRAL DE AJUDA</strong></td>
									<td width="33%"></td>
								</tr>

							</table>

						</td>

					</tr>

					<tr>
						<td align="center">Emergência e soluções de problemas com o rastreador ligue:</td>
					</tr>

					<tr>
						<td align="center"><font size="4px" color="#003333"> 0800 030 6672</font> Opção 02</td>
					</tr>

					<tr>
						<td></td>
					</tr>

					<tr>
						<td></br><strong>Cliente:</strong> <?php echo $vetor_cliente['nome']; ?></td>
					</tr>

					<tr>
						<td><strong>Veículo:</strong> <?php echo $vetor_marca['marca'].' - '.$vetor_veiculo['modelo']; ?></td>
					</tr>

					<tr>
						<td><strong>Placa:</strong> <?php echo $vetor_veiculo['placa']; ?></td>
					</tr>

				</table>

				</div>
			</td>
		</tr>
		<tr>
			<td align="center">
			</td>
		</tr>
		<tr>
			<td align="center">
			<font color="#FF0000">corte e guarde</font>
			</td>
		</tr>
	</table>

	</td>

	</tr>



	<tr>
		<td style="font-size: 20px;">
			</br>
			</br>
			</br>
			<strong>Prezado(a) Cliente,</strong><br>adicione este número abaixo na sua lista de contatos e ao final da instalação nos envie uma mensagem para que
			possamos fazer a ativação do seu plano, aguardamos.
		</td>
	</tr>
	<br>

	<tr>
		<td align="center">
			</br>
			</br>
			<font style="font-size: 32px; color: red">(11) 94996-0826</font>
		</td>
	</tr>

	
</table>
</div>
</body>
</html>

<script type="text/javascript">
<!--
        print();
-->
</script>