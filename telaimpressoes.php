<?php

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do cÃ³digo informado Ã© vÃ¡lido
	if ($tamanho != 9 && $tamanho != 12){
		return false; 
	}
 
	if ($formatado){ 
		// seleciona a mÃ¡scara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;
 
	}else{
		//se nÃ£o quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}
 
	return $retorno;
 
}

	 include"includes/conexao.php";
	 
	 session_start();

   $id_pagina = '7';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$id = $_GET['id'];

  $sql = mysqli_query($con, "select * from pedidos where id_pedido = '$id'");
  $vetor = mysqli_fetch_array($sql);

  $sql_cliente = mysqli_query($con, "select * from clientes where id_cli = '$vetor[id_cliente]'");
  $vetor_cliente = mysqli_fetch_array($sql_cliente);

  $sql_cadastro = "select * from usuarios where id_user = '$_SESSION[id]'";
	$res_cadastro = mysqli_query($con, $sql_cadastro);
	$vetor_cadastro = mysqli_fetch_array($res_cadastro);
	
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <!-- <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css"> -->
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include"includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Impressão do Pedido do Cliente: <?php echo $vetor_cliente['nome']; ?></h3>
              </br>
              <h3 class="box-title"><font color="#FF0000">Pedido N°: <?php echo $id; ?></font></h3>
            </div>
            <!-- /.box-header -->
 
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="20%">Carta Boas Vindas  (1 via)</th>
                  <th width="20%">Pedido de Venda  (1 via)</th>
                  <th width="20%">Os intalação  (1 via)</th>
                  <th width="20%">CAPA (2 vias)</th>
                  <th width="20%">Financeiro Cliente (2 vias)</th>
                  <th width="20%">Contrato 2 VIAS</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>
                    <a href="imprimircarta.php?id_pedido=<?php echo $id; ?>" class="btn btn-app btn-color-logo-blue" target="_blank">
                    <i class="fa fa-file"></i> Carta Boas Vindas
                    </a>
                  </td>
                  <td>
                    <a href="imprimirpedido.php?id_pedido=<?php echo $id; ?>" class="btn btn-app btn-color-logo-blue" target="_blank">
                    <i class="fa fa-file"></i> Pedido de Venda
                    </a>
                  </td>
                  <td>
                    <a href="imprimiros.php?id_pedido=<?php echo $id; ?>&tipo=1" class="btn btn-app btn-color-logo-blue" target="_blank">
                    <i class="fa fa-copy"></i> OS
                    </a>
                  </td>
                  <td>
                    <a href="imprimircapa.php?id_pedido=<?php echo $id; ?>" class="btn btn-app btn-color-logo-blue" target="_blank">
                    <i class="fa fa-file"></i> Capa
                    </a>
                  </td> 
                  <td>

                    <a href="imprimirfinanceiro.php?id_pedido=<?php echo $id; ?>" class="btn btn-app btn-color-logo-blue" target="_blank">
                    <i class="fa fa-money"></i> Financeiro
                    </a>

                  </td>
                  <td>

                    <a href="<?php if($vetor['tipo'] == 1) { ?>imprimircontrato<?php } if($vetor['tipo'] == 2) { ?>imprimircontrato_parcial<?php } if($vetor['tipo'] == 3) { ?>imprimircontrato_sem<?php } ?>.php?id_pedido=<?php echo $id; ?>" class="btn btn-app btn-color-logo-blue" target="_blank">
                    <i class="fa fa-file-pdf-o"></i> Contrato
                    </a>

                  </td>
                </tr>
                </tbody>
              </table>

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
<?php } } ?>