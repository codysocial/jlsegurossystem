<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

    <style type="text/css">
    .mesmo-tamanho{
      padding-right: 10px;
      padding-left: 1%;
      margin-left: 5px;
      width: 100px;
    }
    .btn_restaurar{
      border: 0px;
      border-radius: 3px;
      width: 100px;
      height: 33px;
      margin-left: 2px;
      background-color: green;
    }
    .min{
      width: 5%;
    }
    .alinhar{
      float: left; 
      margin-top: 3px; 
      margin-right: auto;
    }
    .alinhar2{
      float: left; 
      margin-top: 3px; 
      margin-right: auto;
      margin-left: 3px;
    }
    .alinhar3{
      float: right; 
      margin-top: 3px; 
      margin-right: auto;
      margin-right: 3px;
    }
    #ball{
      width: 15px;
      height: 15px;
      border-radius: 50%;
      margin-left: 50%;
    }
    #legenda{
      width: 210px;
      height: 100px;
      float: right;
      margin-right: 5%;
      display: flex;
      flex-direction: column;
      background-color: #eee;
      border-radius: 8px;
    }
    .linha_legen{
      width: 100%;
      height: 50%;
      display: flex;
      flex-direction: row;
    }
    .ball_lege{
      position: relative;
      width: 15px;
      height: 15px;
      border-radius: 50%;
      margin-top: 10px;
      margin-left: 5px;
    }
    .legend{

      margin-top: 8px;
      margin-left: 4px;

    }
  </style>

</head>

  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">

  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>

<?php include"includes/conexao.php"; ?>

 <?php
  $escolha = $_GET['escolha'];
 ?>

  <div style="width: 80%; margin-left: 10%;">

  <?php if($escolha == 1){ ?>

  <h3 style="text-align: center; margin-bottom: 20px">Relatório - Todos os Rastreadores</h3>

<table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>IMEI</th>
                  <th>Fabricante</th>
                  <th>Modelo</th>
                  <th>DataCad</th>
                  <th>Linha</th>
                  <th>Operadora</th>
                  <th>Data Vin.</th>
                  <th>Data</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  
                  $sql_rastreadores = mysqli_query($con, "SELECT * FROM rastreador");

                  while ($vetor = mysqli_fetch_array($sql_rastreadores)) {

                    $id_rastreador = $vetor['id_rastreador'];

                    $query = mysqli_query($con, "SELECT * from rastreador_chip WhERE id_rastreador = '$id_rastreador'");

                    if (mysqli_num_rows($query) > 0) {

                      $vetor_vinculado = mysqli_fetch_array($query);

                      $data_vinculo = $vetor_vinculado['data_vinculo'];

                      $data_vinculo_format = new DateTime($data_vinculo);

                      $data_pedido = $vetor_vinculado['data_pedido'];

                      if($data_pedido == "0000-00-00 00:00:00"){
                        $data_pedido_f = '';
                      }else{
                        $data_pedido_format = new DateTime($data_pedido);

                        $data_pedido_f = $data_pedido_format->format('d-m-Y H:i:s'); 
                      }


                      $id_chip = $vetor_vinculado['id_chip'];

                      $sql_chip = mysqli_query($con, "SELECT * FROM chips where id_chip = '$id_chip' ");
                      $vetor_chip = mysqli_fetch_array($sql_chip);

                      $linha = $vetor_chip['linha'];
                      $operadora = $vetor_chip['operadora'];
                    }
                    




                    if ($vetor['situacao'] == 1) {
                      $color = "blue";
                    }
                    elseif ($vetor['situacao'] == 2) {
                      $color = "yellow";
                    }
                    elseif ($vetor['situacao'] == 3) {
                      $color = "red";
                    }

                ?>

                <tr>
                  
                  <input type="hidden" name="id" value="<?php echo $vetor[id_rastreador]; ?>">
                  <td><?php echo $vetor['imei'];?></td>
                  <td><?php echo $vetor['fabricante'];?></td>
                  <td><?php echo $vetor['modelo'];?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $linha; } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $operadora; } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $data_vinculo_format->format('d-m-Y H:i:s'); } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $data_pedido_f; } ?></td>
              
                </tr>

                <?php  } ?>
                </tbody>
                
              </table>
            <?php } ?>



            <?php if($escolha == 2){ ?>

  <h3 style="text-align: center; margin-bottom: 20px">Relatório - Vinculados aos Chips</h3>

<table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>IMEI</th>
                  <th>Fabricante</th>
                  <th>Modelo</th>
                  <th>DataCad</th>
                  <th>Linha</th>
                  <th>Operadora</th>
                  <th>Data Vin.</th>
                  <th>Data</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  
                  $sql_rastreadores = mysqli_query($con, "SELECT * FROM rastreador where situacao = '2' ");

                  while ($vetor = mysqli_fetch_array($sql_rastreadores)) {

                    $id_rastreador = $vetor['id_rastreador'];

                    $query = mysqli_query($con, "SELECT * from rastreador_chip WhERE id_rastreador = '$id_rastreador'");

                    if (mysqli_num_rows($query) > 0) {

                      $vetor_vinculado = mysqli_fetch_array($query);

                      $data_vinculo = $vetor_vinculado['data_vinculo'];

                      $data_vinculo_format = new DateTime($data_vinculo);

                      $data_pedido = $vetor_vinculado['data_pedido'];

                      if($data_pedido == "0000-00-00 00:00:00"){
                        $data_pedido_f = '';
                      }else{
                        $data_pedido_format = new DateTime($data_pedido);

                        $data_pedido_f = $data_pedido_format->format('d-m-Y H:i:s'); 
                      }


                      $id_chip = $vetor_vinculado['id_chip'];

                      $sql_chip = mysqli_query($con, "SELECT * FROM chips where id_chip = '$id_chip' ");
                      $vetor_chip = mysqli_fetch_array($sql_chip);

                      $linha = $vetor_chip['linha'];
                      $operadora = $vetor_chip['operadora'];
                    }
                    




                    if ($vetor['situacao'] == 1) {
                      $color = "blue";
                    }
                    elseif ($vetor['situacao'] == 2) {
                      $color = "yellow";
                    }
                    elseif ($vetor['situacao'] == 3) {
                      $color = "red";
                    }

                ?>

                <tr>
                  
                  <input type="hidden" name="id" value="<?php echo $vetor[id_rastreador]; ?>">
                  <td><?php echo $vetor['imei'];?></td>
                  <td><?php echo $vetor['fabricante'];?></td>
                  <td><?php echo $vetor['modelo'];?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $linha; } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $operadora; } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $data_vinculo_format->format('d-m-Y H:i:s'); } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $data_pedido_f; } ?></td>
              
                </tr>

                <?php  } ?>
                </tbody>
                
              </table>
            <?php } ?>


            <?php if($escolha == 3){ ?>

  <h3 style="text-align: center; margin-bottom: 20px">Relatório - Vinculados aos Pedidos</h3>

<table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>IMEI</th>
                  <th>Fabricante</th>
                  <th>Modelo</th>
                  <th>DataCad</th>
                  <th>Linha</th>
                  <th>Operadora</th>
                  <th>Data Vin.</th>
                  <th>Data</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  
                  $sql_rastreadores = mysqli_query($con, "SELECT * FROM rastreador where situacao = '3' ");

                  while ($vetor = mysqli_fetch_array($sql_rastreadores)) {

                    $id_rastreador = $vetor['id_rastreador'];

                    $query = mysqli_query($con, "SELECT * from rastreador_chip WhERE id_rastreador = '$id_rastreador'");

                    if (mysqli_num_rows($query) > 0) {

                      $vetor_vinculado = mysqli_fetch_array($query);

                      $data_vinculo = $vetor_vinculado['data_vinculo'];

                      $data_vinculo_format = new DateTime($data_vinculo);

                      $data_pedido = $vetor_vinculado['data_pedido'];

                      if($data_pedido == "0000-00-00 00:00:00"){
                        $data_pedido_f = '';
                      }else{
                        $data_pedido_format = new DateTime($data_pedido);

                        $data_pedido_f = $data_pedido_format->format('d-m-Y H:i:s'); 
                      }


                      $id_chip = $vetor_vinculado['id_chip'];

                      $sql_chip = mysqli_query($con, "SELECT * FROM chips where id_chip = '$id_chip' ");
                      $vetor_chip = mysqli_fetch_array($sql_chip);

                      $linha = $vetor_chip['linha'];
                      $operadora = $vetor_chip['operadora'];
                    }
                    




                    if ($vetor['situacao'] == 1) {
                      $color = "blue";
                    }
                    elseif ($vetor['situacao'] == 2) {
                      $color = "yellow";
                    }
                    elseif ($vetor['situacao'] == 3) {
                      $color = "red";
                    }

                ?>

                <tr>
                  
                  <input type="hidden" name="id" value="<?php echo $vetor[id_rastreador]; ?>">
                  <td><?php echo $vetor['imei'];?></td>
                  <td><?php echo $vetor['fabricante'];?></td>
                  <td><?php echo $vetor['modelo'];?></td>
                  <td><?php echo date('d/m/Y', strtotime($vetor['data'])); ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $linha; } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $operadora; } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $data_vinculo_format->format('d-m-Y H:i:s'); } ?></td>
                  <td><?php if (mysqli_num_rows($query) > 0) { echo $data_pedido_f; } ?></td>
              
                </tr>

                <?php  } ?>
                </tbody>
                
              </table>
            <?php } ?>
      </div>
      
     <script type="text/javascript">
        print();
    </script>

</body>
</html>