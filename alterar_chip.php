<?php

	 include"includes/conexao.php";
	 
	 session_start();

   $id_pagina = '9';
   $id_pagina1 = '10';

	if($_SESSION['id'] == NULL) {
	
	echo"<script language=\"JavaScript\">
	location.href=\"index.html\";
	</script>";
	
	} else {

$sql_permissao = "select * from permissao_paginas where id_user = '$_SESSION[id]' and id_pagina = '$id_pagina' or id_pagina = '$id_pagina1'";
$res = mysqli_query($con, $sql_permissao);
$num_busca = mysqli_num_rows($res);

if ($num_busca == 0) {
                
echo"<script language=\"JavaScript\">
location.href=\"sempermissao.php\";
</script>";
        
} else {
		
	$id_chip = $_GET['id_chip'];

  $sql_infos = mysqli_query($con, "SELECT * FROM chips WHERE id_chip = '$id_chip' ");
  $vetor_info = mysqli_fetch_array($sql_infos);

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JL Seguro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="layout/bower_components/bootstrap/dist/css/bootstrap.min.css">

  <link rel="stylesheet" href="layout/bower_components/select2/dist/css/select2.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="layout/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="layout/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="layout/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="layout/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="layout/dist/css/skins/_all-skins.min.css">
  
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="aplicacoes/aplicjava.js"></script>
  
</head>
<script src="jquery-1.3.2.min.js" type="text/javascript"></script>
<script>
        function mascara(e, n) {
            v_obj = e, v_fun = n, setTimeout("execmascara()", 1)
        }

        function execmascara() {
            v_obj.value = v_fun(v_obj.value)
        }

        function mtel(e) {
            return e = (e = (e = e.replace(/\D/g, "")).replace(/^(\d{2})(\d)/g, "($1) $2")).replace(/(\d)(\d{4})$/, "$1-$2")
        }

        function id(e) {
            return document.getElementById(e)
        }
        window.onload = function() {
            id("linha").onkeypress = function() {
                mascara(this, mtel)
            }
        }
    </script>

<body>
<div class="container">

  <!-- Left side column. contains the logo and sidebar -->
  <?php include"includes/menu_sistema.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->

        <form action="recebe_alt_chip.php?id_chip=<?php echo $id_chip?>" method="post">   
      
        <div style="padding: 15px; background-color: rgb(241, 222, 278); border-radius: 10px;">
            <div class="box-header">
              <h1 class="box-title">Alteração de Chip</h1>
            </div>

            <div class="row">
            <div class="col-lg-4">
              <fieldset class="form-group">
                <label class="form-label" for="linha">LINHA.:</label>
                <input class="form-control" value="<?php echo $vetor_info['linha'] ?>" type="text" name="linha" id="linha" placeholder="Digite a Linha" required>
              </fieldset>
            </div>
            
            <div class="col-lg-4">
              <fieldset class="form-group">
                <label class="form-label semibold" for="operadora">OPERADORA.:</label>
                <select name="operadora" id="operadora" class="form-control" required>
                  <option value="" selected="selected">Selecione a Operadora</option>
                  <?php 
                  $sql_produtos2 = mysqli_query($con, "select * from produtos where estoque > '0' order by modelo ASC");
                  while ($vetor_produto2=mysqli_fetch_array($sql_produtos2)) { ?>
                  <option value="<?php echo $vetor_produto2['chipprimario']; ?>" <?php if (strcasecmp($vetor_info['operadora'], $vetor_produto2['chipprimario']) == 0) : ?>selected="selected"<?php endif; ?>><?php echo $vetor_produto2['chipprimario'] ?></option>
                  <?php } ?>
                </select>
              </fieldset>
            </div>

            <div class="col-lg-4">
              <fieldset class="form-group">
                <label class="form-label" for="ismc">ISMC.:</label>
                <input class="form-control" value="<?php echo $vetor_info['ismc'] ?>" type="text" name="ismc" id="ismc" placeholder="Digite o ISMC" required>
              </fieldset>
            </div>
          </div><!--.row-->
          </div>                
      
              <br>
              <table width="100%">
                <tr>
                  <td width="95%"></td>
                  <td><button type="submit" class="btn btn-primary" style="float: left;">Alterar</button></td>
                </tr>
              </table>
            </form>
              

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include 'includes/footer.php'; ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="layout/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="layout/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="layout/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="layout/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="layout/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="layout/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="layout/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="layout/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });

    $(function(){
        $('html').css('background-color', '#F1FCFF');
        $('body').css('background-color', '#F1FCFF');
        $('.container').css('background-color', '#F1FCFF');
        $('.content').css('background-color', '#F1FCFF');
        $('.box').css('background-color', '#F1FCFF');
        $('.tab-content').css('background-color', '#F1FCFF');
        $('.box-title').addClass('text-ionc-4');
    });
    
  });
</script>
</body>
</html>
<?php } } ?>